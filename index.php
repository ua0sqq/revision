<?php

if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once  H . 'sys/inc/start.php';
include_once  H . 'sys/inc/sess.php';
include_once  H . 'sys/inc/settings.php';
include_once  H . 'sys/inc/db_connect.php';
include_once  H . 'sys/inc/ipua.php';
include_once  H . 'sys/inc/fnc.php';
include_once  H . 'sys/inc/icons.php';
include_once  H . 'sys/inc/user.php';

include_once  H . 'sys/inc/thead.php';

require_once(H . 'pages/plugins/controller_m_menu.php');

require_once(H . 'sys/inc/tfoot.php');
