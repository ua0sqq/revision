<?php
//загружаем класс для работы с изображениями
require_once  H . 'sys/inc/ImgType.class.php';

if ((user_access('foto_alb_del') || isset($user) && $user['id'] == $ank['id']) && isset($_GET['act']) && $_GET['act'] =='delete' && isset($_GET['ok'])) {
    // TODO: remake deletted
    if ($db->query('SELECT count(*) FROM `gallery_foto` WHERE `id_gallery`=?i', [$gallery['id']])->el()) {
        $col = $db->query('SELECT id FROM `gallery_foto` WHERE `id_gallery`=?i', [$gallery['id']])->col();
        foreach ($col as $id_file) {
            $id_col[] = $id_file;
            if (is_file(H . 'files/foto/' . $id_file . '.png')) {
                unlink(H . 'files/foto/' . $id_file . '.png');
            }
        }
        $cnt = count($id_col);
        $db->query('DELETE FROM `gallery_foto` WHERE `id` IN (' . join(',', $id_col) . ')');
    }
    if ($user['id'] != $ank['id']) {
        admin_log('Фотогалерея', 'Фотоальбомы', '"Удаление альбома ' . $gallery['name'] . ' (фотографий: ' . $cnt . ')"');
    }
    $db->query('DELETE FROM `gallery` WHERE `id`=?i LIMIT ?i', [$gallery['id'], 1]);
    $db->query('OPTIMIZE TABLE `gallery`, `gallery_foto`;');
    msg(lang('Фотоальбом успешно удален'));
    aut();

    echo "<div class=\"foot\">\n";
    echo "&laquo;<a href='/foto/$ank[id]/'>".lang('К фотоальбомам')."</a><br />\n";
    echo "</div>\n";
    include_once '../sys/inc/tfoot.php';
    exit;
}

if (isset($user) && $user['id']==$ank['id'] && isset($_FILES['file'])) {
    // TODO: it is too remake
    if ($imgc=@imagecreatefromstring(file_get_contents($_FILES['file']['tmp_name']))) {
        $name = esc(stripcslashes(htmlspecialchars($_POST['name'])), 1);
        if (isset($_POST['translit1']) && $_POST['translit1'] == 1) {
            $name = translit($name);
        }
        if ($name == null) {
            $name = esc(stripcslashes(htmlspecialchars(preg_replace('#\.[^\.]*$#i', null, $_FILES['file']['name'])))); // имя файла без расширения)),1);
        }
        if (!preg_match("#^([A-zА-я0-9\-\_\ ])+$#ui", $name)) {
            $err='В названии фото присутствуют запрещенные символы';
        }
        if (mb_strlen($name) < 3) {
            $name = $name . '_' . mt_rand(1, 99);
        }

        if (mb_strlen($name) > 32) {
            $err='Название не должно быть длиннее 32-х символов';
        }

        $msg = $_POST['opis'];

        if (isset($_POST['translit2']) && $_POST['translit2']==1) {
            $msg = translit($msg);
        }
        if (mb_strlen($msg) > 1024) {
            $err='Длина описания превышает предел в 1024 символов';
        }

        $img_x = imagesx($imgc);
        $img_y = imagesy($imgc);

        if ($img_x > $set['max_upload_foto_x'] || $img_y > $set['max_upload_foto_y']) {
            $err = 'Размер изображения превышает ограничения в ' . $set['max_upload_foto_x'] . 'x' . $set['max_upload_foto_y'];
        }

        if (!isset($err)) {
            $id_foto = $db->query('INSERT INTO `gallery_foto` (`id_gallery`, `name`, `ras`, `type`, `opis` ,`id_user`,`time`,`act`)
					   VALUES ( ?i, ?, ?, ?, ?, ?i, ?i, ?i)', [$gallery['id'], $name, 'png', 'image/png', $msg, $user['id'], time(), 1])->id();
            $db->query('UPDATE `gallery` SET `time`=?i WHERE `id`=?i LIMIT ?i', [$time, $gallery['id'], 1]);


   //класс для работы с файлом изображения
   $image = new ImgType();

   //сам файл
   $image->load($_FILES['file']['tmp_name']);
   //размеры
   $image->resizeToWidth($set['max_upload_foto_x']);
   //сохраняем под ...
   $image->save(H.'files/foto/'. $id_foto .'.png');


            if ($set['img_copyright'] == 1) {
                if ($img_x == $img_y) {
                    $dstW = $set['max_upload_foto_x']; // ширина
    $dstH = $set['max_upload_foto_y']; // высота
                } elseif ($img_x > $img_y) {
                    $prop = $img_x/$img_y;
                    $dstW = $set['max_upload_foto_x'];
                    $dstH = ceil($dstW/$prop);
                } else {
                    $prop = $img_y/$img_x;
                    $dstH = $set['max_upload_foto_y'];
                    $dstW = ceil($dstH/$prop);
                }

                $screen = imagecreatetruecolor($dstW, $dstH);
                $y=imagesy($screen);
                imagecopyresampled($screen, $imgc, 0, 0, 0, 0, $dstW, $dstH, $img_x, $img_y);
// наложение копирайта
$screen = img_copyright($screen);
//Куда накладываем
imagejpeg($screen, H.'files/foto/'. $id_foto .'.png', 90);
// Освобождаем память
imagedestroy($screen);
            }

            $_SESSION['message'] = 'Фотография успешно добавлена';

            exit(header("Location: ?"));
        }
    } else {
        $err='Выбранный Вами формат изображения не поддерживается';
    }
}
