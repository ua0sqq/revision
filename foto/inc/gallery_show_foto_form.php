<?php

if (isset($_GET['act']) && $_GET['act']=='rename') {
    echo "<form class='foot' action='?act=rename&amp;ok' method=\"post\">";
    echo lang('Название').":<br />\n";
    echo "<input name='name' type='text' value='$foto[name]' /><br />\n";
    echo lang('Описание').":<br />\n";
    echo "<textarea name='opis'>".esc(stripcslashes(htmlspecialchars($foto['opis'])))."</textarea><br />\n";
    echo "<input class='submit' type='submit' value='".lang('Применить')."' /><br />\n";
    echo "&laquo;<a href='?'>".lang('Отмена')."</a><br />\n";
    echo "</form>";
}


if (isset($_GET['act']) && $_GET['act']=='delete') {
    echo "<form class='foot' action='?act=delete&amp;ok' method=\"post\">";
    echo "<div class='err'>".lang('Подтвердите удаление фотографии')."</div>\n";
    echo "<input class='submit' type='submit' value='".lang('Удалить')."' /><br />\n";
    echo "&laquo;<a href='?'>".lang('Отмена')."</a><br />\n";
    echo "</form>";
}


echo "<div class=\"foot\">\n";
echo "&raquo;<a href='?act=delete'>".lang('Удалить')."</a><br />\n";
echo "&raquo;<a href='?act=rename'>".lang('Переименовать')."</a><br />\n";
if ($ank['id'] == $user['id']) {
    echo "&raquo;<a href='?avatars'>".lang('Установить как аватар')."</a><br />";
}
echo "</div>\n";
