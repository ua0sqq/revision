<?php

if ((user_access('foto_alb_del') || isset($user) && $user['id']==$ank['id']) && isset($_GET['act']) && $_GET['act']=='delete') {
    echo "<form class='foot' action='?act=delete&amp;ok&amp;page=$page' method=\"post\">";
    echo "<div class='err'>".lang('Подтвердите удаление фотоальбома')."</div>\n";
    echo "<input class=\"submit\" type=\"submit\" value=\"".lang('Удалить')."\" /><br />\n";
    echo "&laquo;<a href='?'>".lang('Отмена')."</a><br />\n";
    echo "</form>";
}

if (isset($user) && $user['id']==$ank['id'] && isset($_GET['act']) && $_GET['act']=='upload') {
    echo "<form class='foot' enctype=\"multipart/form-data\" action='?act=upload&amp;ok&amp;page=$page' method=\"post\">";
    echo lang('Название').":<br />\n";
    echo "<input name='name' type='text' /><br />\n";
    echo lang('Файл').":<br />\n";
    echo "<input name='file' type='file' accept='image/*,image/jpeg' /><br />\n";
    echo lang('Описание').":<br />\n";
    echo "<textarea name='opis'></textarea><br />\n";

    #-------правила-----------------#
    if (!is_file(H.'sys/lang/'.$user['lang'].'/rules_foto.txt')) {
        msg(lang('Не найден файл правил'));
    } else {
        $rules = file_get_contents(H.'sys/lang/'.$user['lang'].'/rules_foto.txt');
    }
    echo output_text($rules);
    #-------правила-----------------#

    echo "<br /><input class=\"submit\" type=\"submit\" value=\"".lang('Выгрузить')."\" /><br />\n";
    echo "&laquo;<a href='?'>".lang('Отмена')."</a><br />\n";
    echo "</form>";
}

if (isset($user) && $user['id']==$ank['id'] || user_access('foto_alb_del')) {
    echo "<div class=\"foot\">\n";
    if (isset($user) && $user['id']==$ank['id']) {
        echo "&raquo;<a href='?act=upload'>".lang('Загрузить фото')."</a><br />\n";
    }
    if (user_access('foto_alb_del') || isset($user) && $user['id']==$ank['id']) {
        echo "&raquo;<a href='?act=delete'>".lang('Удалить фотоальбом')."</a><br />\n";
    }
    echo "</div>\n";
}
