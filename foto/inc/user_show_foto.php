<?php

if (!isset($user) && !isset($_GET['id_user'])) {
    exit(header("Location: /foto/"));
}
if (isset($user)) {
    $ank['id'] = $user['id'];
}
if (isset($_GET['id_user'])) {
    $ank['id'] = intval($_GET['id_user']);
}
$ank = get_user($ank['id']);
if (!$ank) {
    header("Location: /foto/?" . SID);
    exit;
}
$gallery['id'] = intval($_GET['id_gallery']);
if (!$db->query('select count(*) from `gallery` where `id`=?i AND `id_user`=?i', [$gallery['id'], $ank['id']])->el()) {
    header("Location: /foto/$ank[id]/?" . SID);
    exit;
}
$gallery = $db->query('SELECT `id`, `name` FROM `gallery` WHERE `id`=?i AND `id_user`=?i', [$gallery['id'], $ank['id']])->row();
$foto['id'] = intval($_GET['id_foto']);
if (!$db->query('select count(*) from `gallery_foto` where `id`=?i AND `id_gallery`=?i AND `id_user`=?i', [$foto['id'], $gallery['id'], $ank['id']])->el()) {
    header('Location: /foto/' . $ank['id'] . '/' . $gallery['id'] . '/?' . SID);
    exit;
}

$foto = $db->query('SELECT * FROM `gallery_foto` WHERE `id`=?i', [$foto['id']])->row();
$set['title'] = $ank['nick'] . ' - ' . $gallery['name'] . ' - ' . $foto['name']; // заголовок страницы
include_once '../sys/inc/thead.php';
title();

if (user_access('foto_foto_edit') && $ank['level'] > $user['level'] || isset($user) && $foto['id_user'] == $user['id']) {
    include 'inc/gallery_show_foto_act.php';
}
if (isset($user) && $user['id'] != $ank['id'] && $user['balls'] >= 50 && $user['rating'] >= 0 &&
    $db->query('select count(*) from `gallery_rating` where `id_user`=?i AND `id_foto`=?i', [$user['id'], $foto['id']])->el() == 0) {
    if (isset($_GET['rating']) && $_GET['rating'] == 'down') {
        #-------------------------------------#
        #Себе
        rating::minus($user['id'], (0.05), 1, null, 'foto_minus');
        //обнуляем что б и автору добавить рейтинга
        $_SESSION['rating'] = null;
        #Автору
        rating::minus($ank['id'], (0.05), 1, null, 'foto_minus');
        //Ставим анти флуд
        $_SESSION['rating'] = time() + 60;
        #-------------------------------------#
        $db->query('UPDATE `gallery_foto` SET `rating`=`rating`-?i WHERE `id`=?i LIMIT ?i', [1, $foto['id'], 1]);
        $db->query('INSERT INTO `gallery_rating` (`id_user`, `id_foto`) VALUES( ?i, ?i)', [$user['id'], $foto['id']]);
        msg(lang('Ваш отрицательный отзыв принят'));
        $foto = $db->query('SELECT * FROM `gallery_foto` WHERE `id`=?i', [$foto['id']])->row();
    } elseif (isset($_GET['rating']) && $_GET['rating'] == 'up') {
        #-------------------------------------#
        #Себе
        rating::add($user['id'], (0.03), 1, null, 'foto');
        //обнуляем что б и автору добавить рейтинга
        $_SESSION['rating'] = null;
        #Автору
        rating::add($ank['id'], (0.03), 1, null, 'foto');
        //Ставим анти флуд
        $_SESSION['rating'] = time() + 60;
        #-------------------------------------#
        $db->query('UPDATE `gallery_foto` SET `rating`=`rating`+?i WHERE `id`=?i LIMIT ?i', [1, $foto['id'], 1]);
        $db->query('INSERT INTO `gallery_rating` (`id_user`, `id_foto`) VALUES( ?i, ?i)', [$user['id'], $foto['id']]);
        msg(lang('Ваш положительный отзыв принят'));
        $foto = $db->query('SELECT * FROM `gallery_foto` WHERE `id`=?i', [$foto['id']])->row();
    }
}
if (isset($_POST['msg']) && isset($user)) {
    $msg = $_POST['msg'];
    if (isset($_POST['translit']) && $_POST['translit'] == 1) {
        $msg = translit($msg);
    }
    $mat = antimat($msg);
    if ($mat) {
        $err[] = lang('В тексте сообщения обнаружен мат') . $mat;
    }
    if (mb_strlen($msg) > 1024) {
        $err = lang('Сообщение слишком длинное');
    } elseif (mb_strlen($msg) < 2) {
        $err = lang('Короткое сообщение');
    } elseif ($db->query('select count(*) from `gallery_komm` where `id_foto`=?i AND `id_user`=?i AND `msg`=?', [$foto['id'], $user['id'], $msg])->el() != 0) {
        $err = 'Ваше сообщение повторяет предыдущее';
    } elseif (!isset($err)) {
        if ($ank['id'] != $user['id']) {
            $jurnal = $user['nick'] . ' оставил [url=/foto/' . $ank['id'] . '/' . $gallery['id'] . '/' . $foto['id'] .
            '/?page=end]комментарий к вашему фото[/url]';
            $db->query('INSERT INTO `jurnal` (`id_user`, `id_kont`, `msg`, `time`, `type`) VALUES( ?i, ?i, ?, ?i, ?string)',
                       [0, $ank['id'], $jurnal, $time, 'my_foto']);
                       //values('0', '$ank[id]', '$user[nick] оставил [url=/foto/$ank[id]/$gallery[id]/$foto[id]/?page=end]комментарий к вашему фото[/url]', '$time', 'my_foto')");
        }
        $db->query('INSERT INTO `gallery_komm` (`id_foto`, `id_user`, `time`, `msg`) VALUES( ?i, ?i, ?i, ?)',
                   [$foto['id'], $user['id'], $time, $msg]);
        $db->query('UPDATE `user` SET `balls`=`balls`+?i WHERE `id`=?i LIMIT ?i', [1, $user['id'], 1]);
        msg(lang('Сообщение успешно добавлено'));
    }
}
if (user_access('foto_komm_del') && isset($_GET['delete']) &&
    $db->query('select count(*) from `gallery_komm` where `id`=?i AND `id_foto`=?i', [$_GET['delete'], $foto['id']])->el() != 0) {
    $db->query('DELETE FROM `gallery_komm` WHERE `id`=?i LIMIT ?i', [$_GET['delete'], 1]);
    admin_log('Фотогалерея', 'Фотографии', "Удаление комментария к фото [url=/id$ank[id]]$ank[nick][/url]");
    msg(lang('Комментарий успешно удален'));
}
err() . aut();

?>
<script type="text/javascript" src="/sys/js/lightBox/refresh_st.js"></script>
<script type="text/javascript" src="/sys/js/lightBox/refresh_st_0.js"></script>
<link rel="stylesheet" href="/sys/js/lightBox/lightBox.css" type="text/css" media="screen" />
<script src="/sys/js/lightBox/lightBox.js" type="text/javascript"></script>
<script type="text/javascript">
	window.addEvent('domready',function(){
		$$('.popup_text').each(function(item){
			item.getParent('li').getElement('a').store('title',item.get('html'));
		});
	});
</script>
<?php

echo "  <div class='p_m'>\n";
echo '<a js="fiera_lightBox[1]" class="image" href="/ImgType?imgLink=' .
    base64_encode('files/foto/' . $foto['id'] . '.png') .
    '&amp;width=500&amp;height=500&amp;ImgType=0">
	<img src="/ImgType?imgLink=' . base64_encode('files/foto/' . $foto['id'] .
    '.png') . '&amp;width=150&amp;height=150&amp;ImgType=0" alt="*"></a>
	<div class="popup_text"> ' . lang('Описание') . ': 	' . ($foto['opis'] != null ?
    output_text($foto['opis']) : false) . '</div>';
include_once H . 'sys/inc/downloadfile.php';
if (isset($_GET['DownloadFile'])) {
    exit(DownloadFile(H . $_GET['DownloadFile'], $_SERVER['SERVER_NAME'] . '_Фото_' .
        $foto['id'] . '_' . $ank['nick'] . '.png', ras_to_mime('png')));
}
if ($foto['opis'] != null) {
    echo output_text($foto['opis']) . "<br />\n";
}
if (is_file(H . 'files/foto/' . $foto['id'] . '.png')) {
    echo "<br /><a href='?DownloadFile=/files/foto/$foto[id].png' title='" . lang('Скачать оригинал') . "'>";
    echo lang('Скачать оригинал');
    echo " (" . size_file(filesize(H . 'files/foto/' . $foto['id'] . '.png')) . ")";
    echo "</a><br />";
}
echo "<span class=\"ank_n\">" . lang('Рейтинг') . ":</span> ";
if (isset($user) && $user['id'] != $ank['id'] && $user['balls'] >= 50 && $user['rating'] >= 0 &&
    $db->query('select count(*) from `gallery_rating` where `id_user`=?i AND `id_foto`=?i', [$user['id'], $foto['id']])->el() == 0) { // ???
        echo "[<a href=\"?id=$foto[id]&amp;rating=down\" title=\"Отдать отрицательный голос\">-</a>] ";
}
echo "<span class=\"ank_d\">$foto[rating]</span>";
if (isset($user) && $user['id'] != $ank['id'] && $user['balls'] >= 50 && $user['rating'] >= 0 &&
    $db->query('select count(*) from `gallery_rating` where `id_user`=?i AND `id_foto`=?i', [$user['id'], $foto['id']])->el() == 0) { // ???
    echo " [<a href=\"?id=$foto[id]&amp;rating=up\" title=\"Отдать положительный голос\">+</a>]";
}
echo "</div>\n";
if ($user['id'] != $ank['id'] and $ank['group_access'] == 0 and $user['group_access'] == 0) {
    if (!isset($_GET['Block'])) {
        echo "<span class='ank_span' style='float:right'>";
        echo "<a href='?Block'> " . lang('Жалоба на фотографию') . "</a><br />";
        echo "</span>";
    }
    //Жалоба
    if (isset($_POST['Block'])) {
        $msg = lang('Новая жалоба фото от пользователя') . ' [url=/' . $ank['mylink'] .
            ']' . nick($ank['id'], null, 0) . '[/url]
';
        $msg .= trim($_POST['prich']) . '
[url=/foto/' . $ank['id'] . '/' . $gallery['id'] . '/' . $foto['id'] .
            '/][color=red]LINK[/color][/url]
';
        $msg .= lang('Жалоба подана от') . ' : [url=/' . $user['mylink'] . ']' . nick($user['id'], null,
            0) . '[/url] ';
        $c_v = mb_strlen($msg);
        if ($c_v < 100) {
            $_SESSION['message'] = lang('Нужно указать причину подробнее');
            exit(header("Location: /" . $ank['mylink'] . '?Block'));
        }
        if ($c_v > 5000) {
            $_SESSION['message'] = lang('Причина слишком большая');
            exit(header("Location: ?Block"));
        }
        $db->query('INSERT INTO `jurnal_system` (`time`, `type`, `read`, `id_user`, `msg`, `id_kont`) VALUES ( ?i,  ?,  ?string, ?i, ?, ?i);',
                  [time(), 'spam_foto', 0, $ank['id'], $msg, 0]);
        $_SESSION['message'] = lang('Успешно');
        //Удаляем кэш
        cache_delete::user($ank['id']);
        exit(header('Location: ?'));
    }
    //Форма блокировки
    if (isset($_GET['Block'])) {
        echo "<div class='p_m'><form action='' method='post'>";
        echo lang('Причина') . " <textarea name='prich' class='form_a'></textarea>";
        echo "<input class='form_a_bottom'  name='Block' type='submit' value='" . lang('Отправить') .
            "' /> <a href='/" . $ank['mylink'] . "'>" . lang('Отмена') . "</a>";
        echo "</form></div>";
    }
}
if (user_access('foto_foto_edit') && $ank['level'] < $user['level'] || isset($user) &&
    $foto['id_user'] == $user['id']) {
    if (isset($_GET['avatars']) and $ank['id'] == $user['id']) {
        //загружаем класс для работы с изображениями
        require_once H . "sys/inc/ImgType.class.php";
        if (is_file(H . 'files/avatars/' . $user['id'] . '.png')) { // ???
            unlink(H . 'files/avatars/' . $user['id'] . '.png');
        }
        //класс для работы с файлом изображения
        $image = new ImgType();
        //сам файл
        $image->load(H . "files/foto/{$foto['id']}.png");
        //размеры
        $image->resizeToWidth(1000);
        //сохраняем под ...
        $image->save(H . 'files/avatars/' . $user['id'] . '.png');
        $_SESSION['message'] = lang('Аватар установлен');
        exit(header('Location: ?'));
    }
    include 'inc/gallery_show_foto_form.php';
}
$k_post  = $db->query('select count(*) from `gallery_komm` where `id_foto`=?i', [$foto['id']])->el();
$k_page = k_page($k_post, $set['p_str']);
$page    = page($k_page);
$start    = $set['p_str'] * $page - $set['p_str'];
if ($k_post == 0) {
    msg(lang('Нет комментариев'));
}
echo "<table class='post'>\n";
$data = [$foto['id'], $start, $set['p_str']];
$q = $db->query('SELECT g . * , u.id AS idUser FROM `gallery_komm` g
LEFT JOIN `user` u ON g.id_user = u.id
WHERE `id_foto`=?i
ORDER BY g.`id` ASC  LIMIT ?i, ?i', $data
);
while ($post = $q->row()) {
    echo "   <tr>\n";
    if ($set['avatar_show'] == 1) {
        echo "  <td class='avar' rowspan='2'>";
        avatar($post['idUser'], 85, 85);
        echo "  </td>";
    }
    echo "<td class='p_t'>";
    echo nick($post['idUser']);
    echo " (" . vremja($post['time']) . ")";
    echo "  </td>\n";
    echo "   </tr>\n";
    echo "   <tr>\n";
    echo "  <td class='p_m'>\n";
    echo output_text($post['msg']) . "<br />\n";
    if (user_access('foto_komm_del')) {
        echo " <a href='?delete=$post[id]' title='Удалить комментарий'>Удалить</a>";
    }
    echo "  </td>\n";
    echo "   </tr>\n";
}
echo "</table>\n";
if ($k_page > 1) {
    str('?', $k_page, $page); // Вывод страниц
}
if (isset($user)) {
    panel_form::head();
    echo "<div class=\"foot\">\n";
    echo "<form method='post' name='message' action='?'>\n";
    echo lang('Сообщение') . ":<br /><textarea name=\"msg\"></textarea><br />\n";
    if ($user['set_translit'] == 1) {
        echo "<label><input type=\"checkbox\" name=\"translit\" value=\"1\" /> " . lang('Транслит') .
            "</label><br />\n";
    }
    echo "<input value=\"" . lang('Отправить') . "\" type=\"submit\" />\n";
    echo "</form></div>";
    panel_form::foot();
}
echo "<div class=\"foot\">\n";
echo "&laquo;<a href='/foto/$ank[id]/$gallery[id]/'>" . lang('К фотографиям') .
    "</a><br />\n";
echo "&laquo;<a href='/foto/$ank[id]/'>" . lang('К фотоальбомам') . "</a></div>\n";
include_once '../sys/inc/tfoot.php';
exit;

?>