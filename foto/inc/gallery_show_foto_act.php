<?php

if (isset($_GET['act']) && $_GET['act'] == 'delete' && isset($_GET['ok'])) {
    if ($user['id'] != $ank['id']) {
        admin_log('Фотогалерея', 'Фотографии', 'Удаление фото пользователя [url=/id' . $ank['id'] .
            ']' . $ank['nick'] . '[/url]');
    }
    if (is_file(H . 'files/foto/' . $foto['id'] . '.png')) { // ???
        unlink(H . 'files/foto/' . $foto['id'] . '.png');
    }
    $db->query('DELETE FROM gallery_foto WHERE id=?i ', [$foto['id']]);
    $db->query('DELETE FROM gallery_komm WHERE id_foto=?i ', [$foto['id']]);
    $db->query('DELETE FROM gallery_rating WHERE id_foto=?i ', [$foto['id']]);
    $db->query('OPTIMIZE TABLE `gallery_foto`, `gallery_komm`, `gallery_rating`');

    msg(lang('Фотография успешно удалена'));
    aut(); ?>
<div class="foot">
    &laquo;<a href="/foto/<?php echo $ank['id'] . '/' . $gallery['id']; ?>/"><?php echo lang('К фотографиям'); ?></a>
</div>
<?php

    include_once H . 'sys/inc/tfoot.php';
    exit;
}

if (isset($_GET['act']) && $_GET['act'] == 'rename' && isset($_GET['ok']) &&
    isset($_POST['name']) && isset($_POST['opis'])) {
    $name = trim($_POST['name']);
    if (!preg_match("#^([A-zА-яёЁ0-9\-\_\(\)\,\.\ :])+$#ui", $name)) {
        $err[] = lang('В названии фото присутствуют запрещенные символы');
    }
    if (isset($_POST['translit1']) && $_POST['translit1'] == 1) {
        $name = translit($name);
    }
    if (mb_strlen($name) < 3) {
        $err[] = lang('Короткое название');
    }
    if (mb_strlen($name) > 32) {
        $err[] = lang('Название не должно быть длиннее 32-х символов');
    }

    $msg = $_POST['opis'];
    if (isset($_POST['translit2']) && $_POST['translit2'] == 1) {
        $msg = translit($msg);
    }
    if (mb_strlen($msg) > 1024) {
        $err[] = lang('Длина описания превышает предел в 1024 символа');
    }

    if (!isset($err)) {
        if ($user['id'] != $ank['id']) {
            admin_log('Фотогалерея', 'Фотографии',
                'Переименование фото пользователя [url=/id' . $ank['id'] . ']' . $ank['nick'] . '[/url]');
        }
        $db->query('UPDATE `gallery_foto` SET `name`=?, `opis`=? WHERE `id`=?i LIMIT ?i',
        [$name, $msg, $foto['id'], 1]);
        $foto = $db->query('SELECT * FROM `gallery_foto` WHERE `id`=?i LIMIT ?i', [$foto['id'], 1])->row();
        msg(lang('Фотография успешно переименована'));
    }
}
