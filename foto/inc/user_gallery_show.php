<?php

if (!isset($user) && !isset($_GET['id_user'])) {
    header('Location: /foto/?' . SID);
    exit;
}
if (isset($user)) {
    $ank['id'] = $user['id'];
}
if (isset($_GET['id_user'])) {
    $ank['id'] = intval($_GET['id_user']);
}
$ank = get_user($ank['id']);
if (!$ank) {
    header("Location: /foto/?".SID);
    exit;
}
$gallery['id'] = intval($_GET['id_gallery']);

if (!$db->query('select count(*) from `gallery` where `id`=?i AND `id_user`=?i', [$gallery['id'], $ank['id']])->el()) {
    header('Location: /foto/' . $ank['id'] . '/?'.SID);
    exit;
}

$gallery = $db->query('SELECT * FROM `gallery` WHERE `id`=?i AND `id_user`=?i LIMIT ?i', [$gallery['id'], $ank['id'], 1])->row();



$set['title'] = $ank['nick'] . ' - ' . output_text($gallery['name']) . ' - ' . lang('Фотоальбом'); // заголовок страницы
include_once '../sys/inc/thead.php';
title();

include 'inc/gallery_show_act.php';
err() . aut();


$k_post  = $db->query('select count(*) from `gallery_foto` where `id_gallery`=?i', [$gallery['id']])->el();
$k_page = k_page($k_post, $set['p_str']);
$page    = page($k_page);
$start     = $set['p_str']*$page-$set['p_str'];

if ($k_post == 0) {
    msg(lang("Нет фотографий"));
}

echo "<table class='post'>\n";

$q = $db->query('SELECT foto. * , (
SELECT count( * ) FROM `gallery_komm` WHERE `id_foto`=`foto`.`id` GROUP BY `id_foto`) AS cnt
FROM `gallery_foto` foto
WHERE foto.`id_gallery` =?i
ORDER BY foto.`id` DESC LIMIT ?i, ?i', [$gallery['id'], $start, $set['p_str']]);
while ($post = $q->row()) {
    echo "   <tr>\n";

    echo "  <td class='avar' rowspan='2'>\n";
    echo '<img src="/ImgType?imgLink='.base64_encode('files/foto/'.$post['id'].'.png').'&amp;width=110&amp;height=110&amp;ImgType=0" alt="">';
    echo "  </td>\n";


    echo "<td class='p_t'><a href='/foto/$ank[id]/$gallery[id]/$post[id]/'>";
    echo output_text($post['name']) . '  (' . (int)$post['cnt'] . ')
<span class="status" style="float:right;">[' . vremja($post['time']) . '] </span>';
    echo "  </a></td>";
    echo "   </tr>";
    echo "   <tr>";
    echo "  <td class='p_m'>";
    if ($post['opis'] == null) {
        echo lang('Без описания').'<br />';
    } else {
        echo output_text($post['opis'])."<br />";
    }
    echo "<span class=\"ank_n\">" . lang('Рейтинг') . ":</span> [<span class='ank_d'>$post[rating]</span>]<br />";
    echo "  </td>";
    echo "   </tr>";
}
echo "</table>";

if ($k_page>1) {
    str('?', $k_page, $page); // Вывод страниц
}

include 'inc/gallery_show_form.php';

echo "<div class=\"foot\">";
echo "&laquo;<a href='/foto/$ank[id]/'> ".lang('К фотоальбомам')."</a><br />";
echo "</div>";
include_once H.'sys/inc/tfoot.php';
exit;
