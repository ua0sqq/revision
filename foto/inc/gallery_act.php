<?php

if (isset($user) && $user['id']==$ank['id']) {
    if (isset($_GET['act']) && $_GET['act']=='create' && isset($_GET['ok']) && isset($_POST['name']) && isset($_POST['opis'])) {
        $name = $_POST['name'];
        if (isset($_POST['translit1']) && $_POST['translit1'] == 1) {
            $name = translit($name);
        }
        if (mb_strlen($name) < 3) {
            $err = lang('Короткое название');
        }
        if (mb_strlen($name) > 32) {
            $err = lang('Название не должно быть длиннее 32-х символов');
        }

        $msg = isset($_POST['opis']) ? $_POST['opis'] : false;

        if (isset($_POST['translit2']) && $_POST['translit2']==1) {
            $msg = translit($msg);
        }
        if (mb_strlen($msg) > 256) {
            $err = lang('Длина описания превышает предел в 256 символов');
        }

        if ($db->query('select count(*) from `gallery` where `id_user`=?i AND `name`=?', [$ank['id'], $name])->el()) {
            $err = lang('Альбом с таким названием уже существует');
        }

        if (!isset($err)) {
            $db->query('INSERT INTO `gallery` (`opis`, `time_create`, `id_user`, `name`, `time`) VALUES( ?, ?i, ?i, ?, ?i)',
                       [$msg, $time, $ank['id'], $name, $time]);
            msg(lang('Фотоальбом успешно создан'));
        }
    }
}
