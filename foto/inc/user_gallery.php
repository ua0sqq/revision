<?php

if (!isset($user) && !isset($_GET['id_user'])) {
    header("Location: /foto/?".SID);
    exit;
}
if (isset($user)) {
    $ank['id']=$user['id'];
}
if (isset($_GET['id_user'])) {
    $ank['id']=intval($_GET['id_user']);
}
$ank=get_user($ank['id']);
if (!$ank) {
    header("Location: /foto/?".SID);
    exit;
}

$set['title'] = $ank['nick'] .' - '.lang('Фотоальбомы'); // заголовок страницы

include_once '../sys/inc/thead.php';
title();

include 'inc/gallery_act.php';
err() . aut();

$k_post  = $db->query('SELECT COUNT(*) FROM `gallery` WHERE `id_user`=?i', [$ank['id']])->el();
$k_page = k_page($k_post, $set['p_str']);
$page    = page($k_page);
$start    = $set['p_str']*$page-$set['p_str'];

if ($k_post == 0) {
    msg(lang("Нет фотоальбомов"));
} else {
    echo "<table class='post'>\n";

    $q = $db->query('SELECT glr.*, (
    SELECT MAX(`id`) FROM `gallery_foto` WHERE glr.id=id_gallery GROUP BY id_gallery) AS id_foto, (
    SELECT COUNT( * ) FROM `gallery_foto` WHERE glr.id=id_gallery GROUP BY id_gallery) AS cnt
    FROM `gallery` glr
WHERE glr.`id_user`=?i ORDER BY glr.`time` DESC LIMIT ?i, ?i', [$ank['id'], $start, $set['p_str']]);
    while ($post = $q->row()) {
        echo "   <tr>\n";
        echo "  <td class='avar' rowspan='2'>\n";
        $foto['id'] = $post['id_foto'];

        if ($post['id_foto'] == null) {
            echo '<img src="/ImgType?imgLink=' . base64_encode('files/foto/0.png') . '&amp;width=110&amp;height=110&amp;ImgType=0" alt="Альбом пуст">';
        } else {
            echo '<img src="/ImgType?imgLink=' . base64_encode('files/foto/' . $post['id_foto'] . '.png') . '&amp;width=110&amp;height=110&amp;ImgType=0" alt="Фото_' . $foto['id'] . '">';
        }

        echo "  </td>\n";
        echo "  <td class='p_t'>\n";
        echo '<a href="/foto/' . $ank['id'] . '/' . $post['id'] . '/">' . output_text($post['name']) . '</a> (' . (int)$post['cnt'] . ' фото)';
        echo "  </td>\n";
        echo "   </tr>\n";
        echo "   <tr>\n";
        echo "  <td class='p_m'>\n";

        if ($post['opis'] == null) {
            echo lang('Без описания') . '<br />';
        } else {
            echo output_text($post['opis']) . '<br />';
        }

        echo lang('Создан') . ': ' . vremja($post['time_create']) . '<br />';
        echo "  </td>\n";
        echo "   </tr>\n";
    }
    echo "</table>\n";

    if ($k_page > 1) {
        str('?', $k_page, $page); // Вывод страниц
    }
}

include 'inc/gallery_form.php';

echo "<div class=\"foot\">\n";
echo "&laquo;<a href='/foto/'>" . lang('Все альбомы') . "</a><br />";
echo "</div>\n";

include_once '../sys/inc/tfoot.php';
exit;
