<?php

$set['title'] = lang('Фотоальбомы'); // заголовок страницы

include_once H . 'sys/inc/thead.php';
title() . err() . aut();

$k_post  = $db->query('select count(*) from `gallery`')->el();
$k_page = k_page($k_post, $set['p_str']);
$page    = page($k_page);
$start    = $set['p_str']*$page-$set['p_str'];

if ($k_post == 0) {
    msg(lang("Нет фотоальбомов"));
} else {
    echo "<table class='post'>\n";

    $q = $db->query('SELECT glr . * , ank.id AS id_author, (
    SELECT max(`id`) FROM `gallery_foto` WHERE glr.id = id_gallery GROUP BY `id_gallery`) AS id_foto, (
    SELECT COUNT( * ) FROM `gallery_foto` where id_gallery=glr.id GROUP BY id_gallery) AS cnt
FROM `gallery` glr
LEFT JOIN `user` ank ON glr.id_user = ank.id
ORDER BY glr.`time` DESC  LIMIT ?i, ?i', array($start, $set['p_str']));
    while ($post = $q->row()) {
        echo "   <tr>\n";
        echo "  <td class='avar' rowspan='2'>\n";
        if ($post['id_foto'] == null) {
            echo '<img src="/ImgType?imgLink=' . base64_encode('files/foto/0.png') .
            '&amp;width=110&amp;height=110&amp;ImgType=0" alt="' . lang('Альбом пуст').'">';
        } else {
            echo '<img src="/ImgType?imgLink=' . base64_encode('files/foto/' . $post['id_foto'] . '.png') .
            '&amp;width=110&amp;height=110&amp;ImgType=0" alt="' . lang('фото') . '_' . $post['id'] . '">';
        }
        echo "  </td>\n";
        echo "  <td class='p_t'>\n";
        echo '       <a href="/foto/' . $post['id_author'] . '/' . $post['id'] . '/">' . $post['name'] . '</a> (' .
        ($post['cnt']?:0) . ' ' . lang('фото') . ')';
        echo "  </td>\n";
        echo "   </tr>\n";
        echo "   <tr>\n";
        echo "  <td class='p_m'>\n";

        if ($post['opis']==null) {
            echo lang('Без описания').'<br />';
        } else {
            echo output_text($post['opis'])."<br />\n";
        }
        echo lang('Создан').': '.vremja($post['time_create'])."<br />\n";
        echo lang('Автор').': '.nick($post['id_author']);
        echo "  </td>\n";
        echo "   </tr>\n";
    }
    echo "</table>\n";

    if ($k_page>1) {
        str('?', $k_page, $page); // Вывод страниц
    }
}

if (isset($user)) {
    echo "<div class=\"foot\">\n";
    echo "&raquo;<a href='/foto/$user[id]/'>".lang('Мои альбомы')."</a><br />\n";
    echo "</div>\n";
}

include_once H . 'sys/inc/tfoot.php';
exit;
