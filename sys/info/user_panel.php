<?php

echo "<div class='user_panel'>";
echo nick($ank['id'], 'text');
if ($ank['group_access'] > 1) {
    echo " <span class='status' style='float:right;'>".lang($ank['group_name'])." </span>  ";
}
echo "</div>";

if ($user['level'] > $ank['level'] and user_access('user_prof_edit')) {
    echo " <a href='".APANEL."/user.php?id=$ank[id]'><span style='float:right' class='ank_span_m'>".lang('Отредактировать анкету')." [".nick($ank['id'], null, 0, 1) ."]</span></a>";
}

if (user_access('user_ban_set') || user_access('user_ban_set_h') || user_access('user_ban_unset') and $user['level'] > $ank['level']) {
    echo "<a href='".APANEL."/ban.php?id=$ank[id]'><span style='float:right' class='ank_span_m'>".lang('Забанить')."</span></a>";
}
  

if ($set['banbase'] == 1) {
    include_once H.'sys/inc/Banbase.php';

    //Данные для проверки
    $info['email'] = $ank['ank_mail'];
    $info['skype'] = $ank['ank_skype'];
    $info['icq'] = $ank['ank_icq'];
    $info['name'] = $ank['nick'];
    $info['name'] = $ank['ank_name'];
    $info['name'] = $ank['ank_family'];
    $info['name'] = $ank['mylink'];

    //Поиск записей в базе
    $result = Banbase::search_arr($info);
    if (isset($result['answer']) and $result['answer'] != null) {
        $ans = $result['answer'];
        foreach ($ans as $var):
echo "<div class='err'><br/><br/>"; ?>Пользователь обнаружен в banbase
Добавил <?php echo output_text($var['admin']); ?> на сайт <?php echo output_text($var['url']); ?>
<?php if (!empty($var['wmid'])):?><p><b>Wmid:</b> <?php echo implode(', ', $var['wmid']); ?></p><?php endif; ?>
<?php if (!empty($var['email'])):?><p><b>Email:</b> <?php echo implode(', ', $var['email']); ?></p><?php endif; ?>
<?php if (!empty($var['icq'])):?><p><b>ICQ:</b> <?php echo implode(', ', $var['icq']); ?></p><?php endif; ?>
<?php if (!empty($var['skype'])):?><p><b>Skype:</b> <?php echo implode(', ', $var['skype']); ?></p><?php endif; ?>
<?php if (!empty($var['sites'])):?><p><b>Site:</b> <?php echo implode(', ', $var['sites']); ?></p><?php endif; ?>
<?php if (!empty($var['name'])):?><p><b>Name:</b> <?php echo implode(', ', $var['name']); ?></p><?php endif;
        echo output_text($var['descr']);
        echo "</div>";
        endforeach;
    }
}
