<?php

if (user_access('user_collisions') and $user['level'] > $ank['level']) {
    $mass[0] = $ank['id'];
    $collisions = user_collision($mass);


    if (count($collisions) > 1) {
        echo "<div class='user_collisions'>";

        echo "<span class=\"ank_n\"><img src='/style/icons/def.png' alt='' /> ".lang('Возможные ники').":</span><br />";
        echo "<img src='/style/icons/def2.png' alt='' />";

        for ($i=1;$i<count($collisions);$i++) {
            $ank_coll = $db->query('SELECT * FROM `user` WHERE `id` = ?i LIMIT ?i', [$collisions[$i], 1])->el();
            echo " <a href='/$ank_coll[mylink]'>".nick($ank_coll['id'], null, 0, 1)."</a>".($i > 1 ? ',': false);
        }

        echo "</div>";
    }
}
