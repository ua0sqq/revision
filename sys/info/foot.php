<?php

if ($user['level']>0) {
    if ($user['level']>$ank['level']) {
        echo "<div class='foot'>";

        if ($ank['ip']!=null) {
            if (user_access('user_show_ip') && $ank['ip']!=0) {
                echo "<span class=\"ank_n\"><img src='/style/icons/def.png' alt='' /> IP:</span> <span class=\"ank_d\">".long2ip($ank['ip'])."</span>";
                if (user_access('adm_ban_ip')) {
                    echo " [<a href='".APANEL."/ban_ip.php?min=$ank[ip]'>".lang('Забанить')."</a>]";
                }
                echo "<br />\n";
            }
        }

        if ($ank['ip_cl']!=null) {
            if (user_access('user_show_ip') && $ank['ip_cl']!=0) {
                echo "<span class=\"ank_n\"><img src='/style/icons/def.png' alt='' /> IP (CLIENT):</span> <span class=\"ank_d\">".long2ip($ank['ip_cl'])."</span>";
                if (user_access('adm_ban_ip')) {
                    echo " [<a href='".APANEL."/ban_ip.php?min=$ank[ip_cl]'>".lang('Забанить')."</a>]";
                }
                echo "<br />\n";
            }
        }

        if ($ank['ip_xff']!=null) {
            if (user_access('user_show_ip') && $ank['ip_xff']!=0) {
                echo "<span class=\"ank_n\"><img src='/style/icons/def.png' alt='' /> IP (XFF):</span> <span class=\"ank_d\">".long2ip($ank['ip_xff'])."</span>";
                if (user_access('adm_ban_ip')) {
                    echo " [<a href='".APANEL."/ban_ip.php?min=$ank[ip_xff]'>".lang('Забанить')."</a>]";
                }
                echo "<br />\n";
            }
        }

        if (user_access('user_show_ua') && $ank['ua']!=null) {
            echo "<span class=\"ank_n\"><img src='/style/icons/def.png' alt='' /> UA:</span> <span class=\"ank_d\">$ank[ua]</span><br />\n";
        }
        if (user_access('user_show_ip') && opsos($ank['ip'])) {
            echo "<span class=\"ank_n\"><img src='/style/icons/def.png' alt='' /> Пров:</span> <span class=\"ank_d\">".opsos($ank['ip'])."</span><br />\n";
        }
        if (user_access('user_show_ip') && opsos($ank['ip_cl'])) {
            echo "<span class=\"ank_n\"><img src='/style/icons/def.png' alt='' /> Пров (CL):</span> <span class=\"ank_d\">".opsos($ank['ip_cl'])."</span><br />\n";
        }
        if (user_access('user_show_ip') && opsos($ank['ip_xff'])) {
            echo "<span class=\"ank_n\"><img src='/style/icons/def.png' alt='' /> Пров (XFF):</span> <span class=\"ank_d\">".opsos($ank['ip_xff'])."</span><br />\n";
        }

        echo "</div>";
    }

    if (user_access('adm_ref') && ($ank['level'] < $user['level']) && count::query('user_ref', '`id_user`="'.$ank['id'].'"')) {
        echo "<div class='foot'>";
        $q = $db->query('SELECT * FROM `user_ref` WHERE `id_user`=?i ORDER BY `time` DESC LIMIT ?i',
                [$ank['id'], $set['p_str']]);
        echo "<img src='/style/icons/def.png' alt='' /> ".lang('Посещаемые сайты').":<br />";
        while ($url = $q->row()) {
            $site = htmlentities($url['url'], ENT_QUOTES, 'UTF-8');
            echo "<img src='/style/icons/def2.png' alt='' /> <a ".($set['web'] ? " target='_blank'":null)." href='/linkGo?go=".base64_encode("http://$site")."'>$site</a> (".vremja($url['time']).")<br />\n";
        }
        echo "</div>\n";
    }
}

if ($user['level'] > $ank['level']) {
    echo "<div class='foot'>";
    if ($user['id']!=$ank['id']) {
        if (user_access('user_delete')) {
            echo '<a style="display:block;" href="' . APANEL . '/delete_user.php?id=' . $ank['id'] . '"><img src="/style/icons/def.png" alt="*" /> ' . lang('Удалить пользователя') . '</a>' . PHP_EOL;
            echo "<br />\n";
        }
    }
    if (user_access('adm_log_read') && $ank['level']!=0 && ($ank['id']==$user['id'] || $ank['level']<$user['level'])) {
        echo "<a href='".APANEL."/adm_log.php?id=$ank[id]'><img src='/style/icons/def.png' alt='' /> ".lang('Отчет по администрированию')."</a>";
    }
    echo "</div>";
}
