<?php

  function user_icon($id = 0)
  {
      global $set;
      $sql = 'SELECT `nick`, `pol`, `date_last`, `group_access`, (
SELECT count( * ) FROM `ban` WHERE `id_user`=`user`.`id` AND `time` >?i GROUP BY `id_user`) AS ban
FROM `user` WHERE `id` =?i';
      $ank = go\DB\query($sql, [time(), $id])->row();
       
        #	если забанен
        $ban_user = $ank['ban'];

        #	Онлайн
        $uslast = $ank['date_last'] > time() - $set['user_online'];
        
      $style = 'margin-bottom: -1px;';
        
      # Какашка
        if ($ban_user) {
            $result = ' <img src="/style/user/ban.png" alt="" style="'. $style .'"/> ';
        } else {
            # Администраторы
            if ($ank['group_access'] > 7 and !$uslast) {
                $result = ' <img src="/style/user/'.($ank['pol'] == 1 ? '1_1':'2_2').'.png" alt="Офлайн Иконка администратора '.$ank['nick'].'" style="'. $style .'" /> ';
            } elseif ($ank['group_access'] > 7) {
                $result = ' <img src="/style/user/'.($ank['pol'] == 1 ? 1: 2).'.png" alt="Иконка администратора '.$ank['nick'].' "  style="'. $style .'"/> ';
            }
        # Модераторы
            elseif ($ank['group_access'] > 1 and $ank['group_access'] < 7 and !$uslast) {
                $result = ' <img src="/style/user/'.($ank['pol'] == 1 ?'3_3':'4_4').'.png" alt="офлайн Иконка модератора '.$ank['nick'].'"  style="'. $style .'"/> ';
            } elseif ($ank['group_access'] > 1 and $ank['group_access'] < 7) {
                $result = ' <img src="/style/user/'.($ank['pol'] == 1 ? 3 : 4).'.png" alt="офлайн Иконка модератора '.$ank['nick'].'""  style="'. $style .'"/> ';
            }
        # Пользователи
            else {
                $result = !$uslast?' <img src="/style/user/'.($ank['pol'] == 1 ?'5_5':'6_6').'.png" alt="Иконка '.$ank['nick'].'"  style="'. $style .'"/> ':' <img src="/style/user/'.($ank['pol']==1?5:6).'.png" alt="Иконка '.$ank['nick'].'" style="'. $style .'" /> ';
            }
        }

      return  $result;
  }
