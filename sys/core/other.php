<?php

    
    
    
    
    
    #	Тестовый класс чтения ini файлов
    #	by Saint
    class ini
    {
        public static function parse_file($file, $text_eror = null)
        {
            if (file_exists($file)) {
                $File_PARSER = @file_get_contents($file);
                $result = @parse_ini_string($File_PARSER);
                return $result;
            } else {
                msg($text_eror != null ? $text_eror : lang('Файл') .' '. $file .' '.lang('не найден'));
            }
        }



        public static function string_arr($iniText)
        {
            if (!empty($iniText)) {
                return @parse_ini_string($iniText);
            } else {
                msg(lang('Возникли ошибки при разборе массива данных или он пуст'));
            }
        }
    }
    
    
    
    
    class functions
    {
        public function page_parser($cache_file, $url, $teme = 3600, $header = '?')
        {
            /*
    Данная функция спарсит страницу и сохранит её на сайт + кэш
    */
        if (file_exists($cache_file) and time() - $teme < filemtime($cache_file)) {
            return $cache_file;
        } else {
            ob_start();
            echo file_get_contents($url);
            $cached = fopen($cache_file, 'w');
            fwrite($cached, ob_get_contents());
            fclose($cached);
            ob_end_flush();
            exit(header('Location: '.$header));
        }
        }
    }
