<?php

function filter_arr(&$text)
{
    //байт разворота
    $text = str_replace(chr(226) . chr(128) . chr(174), '.', $text);
    //одна из не очень популярных xss но считается давольно вредной, так же добавил обработку от бага Google Chrome и других хромоподобных браузеров на движке Blink
    $text = str_replace(array('̏','͓','http://a/%%30%30','file:///%%300'), null, $text);
    if (is_array($text)) {
        array_walk($text, 'filter_arr');
    }
    return $text;
}
 
foreach (array('_SERVER', '_GET', '_POST', '_COOKIE', '_REQUEST') as $v) {
    if (!empty(${$v})) {
        array_walk(${$v}, 'filter_arr');
    }
}
