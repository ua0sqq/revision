<?php

function get_user($user_id = 0)
{
    $user_id = (int) $user_id;

    global $users, $set;

    if ($set['id_system'] != null) {
        $set['id_system'] = 0;
    }

    if (!empty($users[$user_id])) {
        return $users[$user_id];
    } elseif ($user_id == 0) {
        return array(
     'id' => (int) $set['id_system'],
     'nick' => '  '.$set['nick_system'].'  ',
     'level' => 999,
     'pol' => 1,
     'mylink' =>  null,
     'group_name' => lang('Системный робот'),
     'group_access' => 2,
     'date_last' => time(),
     'date_reg' => (int) $set['data_install'],
     'browser' => 'web',
     'ank_o_sebe' => lang('Для уведомлений'),
     'status_ank' => $_SERVER['SERVER_SIGNATURE'],
    'ank_city' => $_SERVER['HTTP_HOST'],
    'ank_family' => NULL,
    'ank_name' => NULL,
    'ank_countr' => $_SERVER['HTTP_HOST'],
    'ank_icq' => NULL,
    'ank_mail' => $_SERVER['SERVER_ADMIN'],
    'ank_skype' => NULL,
    'set_show_mail' => 1);
    } else {
        $file = H.'sys/cache/users/'.$user_id.'.json';

        if ($set['cache_get_user'] != null) {
            $set['cache_get_user'] = 5;
        }

        if (file_exists($file) and (time() - filemtime($file) < $set['cache_get_user'])) {
            $cache = false;
        } else {
            $cache = true;
        }

        if ($cache) {
            $u = go\DB\query('SELECT u.*, gr.level AS gr_level, gr.name AS group_name
FROM `user` u
LEFT JOIN `user_group` gr ON u.group_access=gr.id
WHERE u.`id`=?i LIMIT ?i', [$user_id, 1])->row();
            $tmp_us = [
                       'level' => $u['gr_level'],
                       'group_name' => $u['group_name']
                    ];
            if (!$tmp_us['group_name']) {
                $u['level'] = (string) 0;
                $u['group_name'] = lang('Пользователь');
            } else {
                $u['level'] = (string) $tmp_us['level'];
                $u['group_name'] = $tmp_us['group_name'];
            }
            file_put_contents($file, json_encode($u));
        }
        $users[$user_id] = empty($temp_us) ? json_decode(file_get_contents($file), true): $temp_us;
        return $users[$user_id];
    }
}
