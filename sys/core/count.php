<?php

class count
{
    public static function query($table = null, $where = null, $color = null)
    {
        if ($table != null) {
            if ($where != null) {
                $where = ' where '. $where;
            } else {
                $where = null;
            }
        
            if ($color != null) {
                $color = '<span style="color:#'. $color .';">';
                $color_end = '</span>';
            } else {
                $color = null;
                $color_end = null;
            }
            $cnt = go\DB\query('SELECT COUNT(*) AS cnt FROM ?t ?q', [$table, $where])->row();

            return $cnt['cnt'] ? $cnt['cnt'] : 0;
        }
    }
}
