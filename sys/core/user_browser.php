<?php

function user_browser($id = 0)
{
    global $set;
    $sql = 'SELECT `date_last`, `browser` FROM `user` WHERE `id` =?i';
    $ank = go\DB\query($sql, [$id])->row();
    $uslast = $ank['date_last'] > time() - $set['user_online'];
    $browser = " <img style='margin-bottom: 1px;' src='/style/icons/".($ank['browser'] == 'web' ? 'web':'wap').".png' alt='browser'/> ";
    if (!$uslast) {
        $browser = null;
    }
    return  $browser;
}
