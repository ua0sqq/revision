<?php



// Местонахождение пользователя \\
    
    # Отмечам где не показываем местонахождение
    if (isset($_SESSION['refer']) && $_SESSION['refer']!=null && !preg_match('#(rules)|(smiles)|(secure)|(aut)|(reg)|(umenu)|(zakl)|(mail)|(anketa)|(settings)|(avatar)|(info)|(my_aut)\.php#', $_SERVER['SCRIPT_NAME'])) {
        $_SESSION['refer']=null;
    }
    #	немного обновил
    function otkuda($ref)
    {
        if (preg_match('#^/forum/#', $ref)) {
            $mesto='Форум';
        } elseif (preg_match('#^/chat/#', $ref)) {
            $mesto='Чат';
        } elseif (preg_match('#^/lib/#', $ref)) {
            $mesto='Библиотека';
        } elseif (preg_match('#^/news/#', $ref)) {
            $mesto='Новости';
        } elseif (preg_match('#^/adm_panel/#', $ref)) {
            $mesto='Админка';
        } elseif (preg_match('#^/pages/guestbook/#', $ref)) {
            $mesto='Гостевая';
        } elseif (preg_match('#^/loads/#', $ref)) {
            $mesto='Загрузки';
        } elseif (preg_match('#^/pages/smiles/#', $ref)) {
            $mesto='Смотрит смайлы';
        } elseif (preg_match('#^/pages/bbcode/#', $ref)) {
            $mesto='В бб кодах';
        } elseif (preg_match('#^/exit\.php#', $ref)) {
            $mesto='Выходит';
        } elseif (preg_match('#^/pages/jurnal/#', $ref)) {
            $mesto='В журнале';
        } elseif (preg_match('#^/rules\.php#', $ref)) {
            $mesto='Учит правила';
        } elseif (preg_match('#^/settings\.php#', $ref)) {
            $mesto='В настройках';
        } elseif (preg_match('#^/users\.php#', $ref)) {
            $mesto='Пользователи';
        } elseif (preg_match('#^/online\.php#', $ref)) {
            $mesto='Сейчас на сайте';
        } elseif (preg_match('#^/online_g\.php#', $ref)) {
            $mesto='Гости';
        } elseif (preg_match('#^/reg\.php#', $ref)) {
            $mesto='Регистрация';
        } elseif (preg_match('#^/obmen/#', $ref)) {
            $mesto='Обменник';
        } elseif (preg_match('#^/aut\.php#', $ref)) {
            $mesto='Авторизация';
        } elseif (preg_match('#^/index\.php#', $ref)) {
            $mesto='Главная';
        } elseif (preg_match('#^/\??$#', $ref)) {
            $mesto='Главная';
        } else {
            $mesto=false;
        }
        return $mesto;
    }
    
    function url($url)
    {
        $url2=preg_split('#&(amp;)?#', $url);
        $url3=null;
        for ($i=0;$i<count($url2);$i++) {
            $url4=explode('=', $url2[$i]);
            if (isset($url4[1])) {
                $url3.=$url4[0].'='.urlencode($url4[1]);
            } else {
                $url3.=$url4[0];
            }
            if ($i<count($url2)-1) {
                $url3.='&amp;';
            }
        }
        return $url3;
    }


    function url2($url)
    {
        $url2=explode('/', $url);
        for ($i=0;$i<sizeof($url2);$i++) {
            $url2[$i]=urlencode($url2[$i]);
        }
        return implode('/', $url2);
    }
    
    
    
    // для форм
    function input_value_text($str)
    {
        return output_text($str, 0, 1, 0, 0, 0);
    }
    
// оптимизация всех таблиц
function db_optimize()
{
    set_time_limit(20);// Ставим ограничение на 20 секунд
    $tab = $db->query('SHOW TABLES') ;
    while ($tables = $tab->el()) {
        $db->query('OPTIMIZE TABLE `' . $tables . '`');
    }
}

// рекурсивное удаление папки
    function delete_dir($dir)
    {
        if (is_dir($dir)) {
            $od=opendir($dir);
            while ($rd=readdir($od)) {
                //пропускаем
            if ($rd == '.' || $rd == '..') {
                continue;
            }
            if (is_dir("$dir/$rd")) {
                @chmod("$dir/$rd", 0777);
                delete_dir("$dir/$rd");
            } else {
                @chmod("$dir/$rd", 0777);
                @unlink("$dir/$rd");
            }
            }
            closedir($od);
            @chmod("$dir", 0777);
            return @rmdir("$dir");
        } else {
            @chmod("$dir", 0777);
            @unlink("$dir");
        }
    }





    # Защита от частых запросов с одного IP
    if ($set['antidos']) {
        $antidos[] = array('time'=> $time);
        $k_loads = 0;
        if (is_file(H.'sys/tmp/antidos_'.$iplong.'.dat')) {
            $antidos_dat = unserialize(file_get_contents(H.'sys/tmp/antidos_'.$iplong.'.dat'));
            for ($i = 0; $i < 150 and $i < sizeof($antidos_dat); ++$i) {
                if ($antidos_dat[$i]['time'] > $time-5) {
                    $k_loads++;
                    $antidos[] = $antidos_dat[$i];
                }
            }
        }
    }
