<?php

    # opsos() :: определение оператора
function opsos($ips = null)
{
    global $ip, $db;
    if ($ips == null) {
        $ips = $ip;
    }
    $ipl = ip2long($ips);
    $cntip = $db->query('SELECT COUNT(*) cnt FROM `opsos` WHERE `min`<=?i AND `max`>=?i', [$ipl, $ipl])->row();
    if ((int)$cntip['cnt'] != 0) {
        $opsos = $db->query('SELECT opsos FROM `opsos` WHERE `min`<=?i AND `max`>=?i LIMIT ?i', [$ipl, $ipl, 1])->row();
        return stripcslashes(htmlspecialchars($opsos['opsos']));
    }
    return false;
}
