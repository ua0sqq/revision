<?php

//потом допишу
function vremja_last($time, $full = 2, $textis = 'только что')
{
    if ($time < time()) {
        return ('null');
    }
    
    $time = abs(time() - $time);

    if ($time < 60) {
        $ltime='<span style="color:#FF8C00"> '.$time.' '. lang('с').'. </span>';
    } elseif ($time>=60 && $time < 3600) {
        $ltime=round($time/60).' '. lang('мин').'. ';
    } elseif ($time>=3600 && $time < 86400) {
        $ltime=round($time/3600).' '. lang('ч').'. ';
    } elseif ($time>=86400 && $time<2592000) {
        $ltime=round($time/86400).' '. lang('дн').'. ';
    } else {
        $ltime=round($time/(3600*24*30)).' '. lang('мес').'.';
    }


    if ($full==1) {
        $ltime.= ' '. lang('назад');
    } elseif ($full == 2) {
        $ltime.=null;
    } else {
        $ltime.=' &laquo;';
    }

    if ($time >= 0 and $time <= 15) {
        $ltime ='<span style="color:#F8A059"> '. lang($textis) .'  </span>';
    }
    return $ltime;
}
