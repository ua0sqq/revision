<?php
/*
    Для отладки
 $set['antihah_files_format'] = 0;
 $set['antihack'] = 1;
 $set['antihah_upload_intansiv'] = 1;
 $set['antihah_upload_msg'] = 1;
 $set['antihah_upload_ban'] = 0;
 $set['antihah_upload_ban_ip'] = 0;
 $set['antihah_upload_ban_time'] = 86400;
 */
 if ($set['antihack'] == 1) {
     //опасные значения (угрозы)
 $shell_arr = array(
    "ini_get('safe_mode')",
    "eval",
    "gzinflate",
    "base64_decode",
    "rootshell",
    'phpinfo',
    'rmdir',
    'opendir',
    'exec',
    'system',
    'shell',
    '\$_FILES',
    'chmod',
    'file_get_contents',
    'copy',
    'mail',
    'mkdir',
    'unlink',
    'fopen',
    'fclose',
    'fputs',
    'escapeshellarg',
    'escapeshellcmd',
    'passthru',
    'proc_open',
    'popen',
    'shell_exec',
    'include',
    'include_once',
    'require',
    'require_once'
    );
    
     if (isset($_FILES) and $_FILES != null) {
         $Tmpfile = @$_FILES['file']['tmp_name'];
     } else {
         $Tmpfile = null;
     }
    
        //тек. страница
        $location = $_SERVER['REQUEST_URI'];
        
        //файл где ищем исключения
        $shell_dir = file_get_contents(H.'sys/dat/if_shell_dir.txt');
     if (preg_match("#". $shell_dir."#i", $location)) {
         $continue = true;
     } else {
         $continue = false;
     }

   
     if (file_exists($Tmpfile) and $Tmpfile != null and $continue == null) {
         //проверяемый файл
            $malicious_file = file_get_contents($Tmpfile);
            
            //обявляем переменную
            $attempts = 0;
            
            //перебираем данные
            foreach ($shell_arr as $key => $val) {
                if (preg_match("#". $val ."#i", $malicious_file)) {
                    //записываем угрозы
                ++$attempts;
                }
            }
             //если угроза обнаружена
            if ($attempts > $set['antihah_upload_intansiv']) {
                if (isset($_SESSION['id_user'])) {
                    $user_ses = $db->query('SELECT `id`, `nick` FROM `user` WHERE `id`=?i LIMIT ?i', [$_SESSION['id_user'], 1])->row();
                } else {
                    $user_ses = null;
                }
                    //сообщаем
                    if ($set['antihah_upload_msg'] == 1) {
                        $msg =
                        "[b]Анти-шелл[/b], возможно это попытка залить вредоносный шелл!
						Атакуемая директория  : [url={$location}]{$location}[/url]
						Нарушитель : ".(@$user_ses['nick'] != null ? "[url=/id{$user_ses['id']}][color=red][b]{$user_ses['nick']}[/b][/color][/url]":'Гость')."
						ip : ".($set['antihah_upload_ban_ip'] == 0 ? "[Забанить [url=".APANEL."/ban_ip.php?min={$ip}]{$ip}[/url]]":false);
                        $db->query('INSERT INTO `jurnal_system` (`time`, `type`, `id_user`, `msg`, `id_kont`)
						VALUES ( ?i,  ?,  ?i, ?, ?i);', [time(), 'system', $user_ses['id'], $msg, 0]);
                    }
                    
                    //баним
                    if ($set['antihah_upload_ban'] == 1 and isset($user_ses['nick'])) {
                        $data = [
                                      (time() + $set['antihah_upload_ban_time']),
                                      'За попытку загрузки Вредоносного файла !',
                                      $user['id'],
                                      0
                                ];
                        $db->query('INSERT INTO `ban` SET `time`=?i, `prich`=?, `id_user`=?i, `id_ban`=?i', $data);
                    }
                    
                    //баним по ip
                    if ($set['antihah_upload_ban_ip'] == 1) {
                        //$ip = my_esc($ip);
                        $db->query('INSERT INTO `ban_ip` (`min`, `max`,`id_user`,`time`) VALUES( ?i, ?i, ?i, ?i)', [$ip, $ip, 0, time()]);
                        //values('{$ip}', '{$ip}', '0', '".time()."')");
                    }
                    
                    //удаляем опасный файл
                    unlink($Tmpfile);
                unset($_FILES);
                    //$_SESSION['message'] = 'Формат файла запрещен';
            }
     }
 }
    
    //банальная проверка
    if (isset($_FILES) && !isset($continue) && $set['antihah_files_format'] == 1) {
        foreach ($_FILES as $fileArr => $Filter) {
            if (
            strpos($_FILES[$fileArr]['name'], "\0") !== false or
            strpos($_FILES[$fileArr]['name'], 0) !== false or
            strpos($_FILES[$fileArr]['name'], '/') !== false or
            stripos($_FILES[$fileArr]['name'], '.php') !== false or
            stripos($_FILES[$fileArr]['name'], '.phtml') !== false or
            stripos($_FILES[$fileArr]['name'], '.js') !== false or
            stripos($_FILES[$fileArr]['name'], '.htaccess') !== false
            ) {
                unlink($Tmpfile);
                unset($_FILES);
            //$_SESSION['message'] = 'Формат файла запрещен';
            }
        }
    }
