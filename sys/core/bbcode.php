<?php

    # ВВ коды
    function bbcode($msg)
    {
        //глобальные переменные
        global $set,$user,$_browser;
        
        //Преобразуем переменную в массив
        $bbcode = array();
        
        //Видео с ютуба (если бб код включен)
    if ($set['bb_youtube'] == 1) {
        //Для планщетов и мобильных устройств
        if ($_browser->isMobile()) {
            $w_you = $set['w_you'];
            $h_you = $set['h_you'];
            $f_you = $set['f_you'];
        } else {
            $w_you = $set['w_you_pc'];
            $h_you = $set['h_you_pc'];
            $f_you = $set['f_you_pc'];
        }
        
        // немного скрипта для плагина youtube
        // echo '<script type="text/javascript" src="/sys/js/youtube/plugins.js"></script>';
                
        //Видео с ютуба
        $bbcode['/\[youtube\](.+)\[\/youtube\]/isU']='<br /><div class="youtube_fon" style="text-align:center;"> <div class="youtube" id="$1" data-params="modestbranding=1&showinfo=1&controls=1&vq='.$f_you.'" style=" height: '.$h_you.'px;width: '.$w_you.'px;"></div></div>';
    }
        //Куксив
        if ($set['bb_i'] == 1) {
            $bbcode['/\[i\](.+)\[\/i\]/isU']='<em>$1</em>';
        }
        
        //Жирный
        if ($set['bb_b'] == 1) {
            $bbcode['/\[b\](.+)\[\/b\]/isU']='<strong>$1</strong>';
        }
        
        //Подчеркнутый
        if ($set['bb_u'] == 1) {
            $bbcode['/\[u\](.+)\[\/u\]/isU']='<span style="text-decoration:underline;">$1</span>';
        }
        
        //Большой
        if ($set['bb_big'] == 1) {
            $bbcode['/\[big\](.+)\[\/big\]/isU']='<span style="font-size:large;">$1</span>';
        }
        
        //Малый
        if ($set['bb_small'] == 1) {
            $bbcode['/\[small\](.+)\[\/small\]/isU']='<span style="font-size:small;">$1</span>';
        }
        
        //Цветной текст
        if ($set['bb_color'] == 1) {
            $bbcode['/\[color=([a-z0-9-#]+)\](.+)\[\/color\]/isU']='<span style="color:$1;">$2</span>';
        }

        //Размеры текста
        if ($set['bb_size'] == 1) {
            $bbcode['/\[size=([0-9]+)\](.+)\[\/size\]/isU']='<span style="font-size:$1px;">$2</span>';
        }

        if ($set['bb_video'] == 1) {
            //Видео
        $bbcode['/\[video\](.+)\[\/video\]/isU']="<center><object type=application/x-shockwave-flash data=/sys/js/video/v.swf width=550 height=400><param name=bgcolor value=#ffffff /><param name=allowFullScreen value=true /><param name=allowScriptAccess value=always /><param name=wmode value=transparent /><param name=movie value=/sys/js/video/v.swf/><param name=flashvars value=file=$1/></object></center>";
        }
        
        if ($set['bb_mp3'] == 1) {
            //Mp3
        $bbcode['/\[mp3\](.+)\[\/mp3\]/isU']='<center><object type="application/x-shockwave-flash" data="/sys/js/mp3/i.swf" width="200" height="20" id="dewplayer" name="dewplayer"><param name="movie" value="/sys/js/mp3/i.swf" /><param name="flashvars" value="mp3=$1" /><param name="wmode" value="transparent" /></object></center>';
        }
                
        if ($set['bb_admin'] == 1) {
            //Aдмин текст
        $bbcode['/\[admin\](.+)\[\/admin\]/isU'] =    $user['group_access'] >= 1 ? 'Админ текст :<br/>  <span style="color:red">$1 </span> ':' <span style="color:red">Текст доступный только Администрации </span>';
        }
        
        //Спойлер
        if ($set['bb_spoiler'] == 1) {
            $bbcode['/\[spoiler=([a-zа-я0-9]+)\](.+)\[\/spoiler\]/isU']='<div><div class="spoilerhead" style="cursor:pointer;" onclick="var _n=this.parentNode.getElementsByTagName(\'div\')[1];if(_n.style.display==\'none\'){_n.style.display=\'\';}else{_n.style.display=\'none\';}">$1 (+/-)</div><div class="spoilerbody" style="display:none">$2</div></div>';
        }

        //Загрузка доп. плагинов
        $msg = bbcode_add($msg);
        
        //Если найдены
        if (count($bbcode)) {
            $msg = preg_replace(array_keys($bbcode), array_values($bbcode), $msg);
        }
        
        //Подсветка php кода
        if ($set['bb_code'] == 1) {
            $msg = preg_replace_callback('#&lt;\?(.*?)\?&gt;#sui', 'bbcodehightlight', $msg);
        }

        return $msg;
    }
