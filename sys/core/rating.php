<?php

class rating
{

    //	Добавление и обработка
    public static function add($id = 0, $rating = null, $flud = 5, $pr = null)
    {
        //если пусто задаем время
        if (!isset($_SESSION['rating'])) {
            $_SESSION['rating'] = time();
        }

        if ($id > 0 && $_SESSION['rating'] <= time()) {
            // округляем
            $rating = number_format($rating, 2);
            // делим на процент
            if ($pr != null) {
                $rating = $rating - $rating * '0.' . $pr;
            }
            // запрос в базу
            $data = [$rating, $id];
            if (go\DB\query('update `user` set `rating`=`rating`+? where `id`=?i', $data)) {
                // Удаляем кэш
                cache_delete::user($id);
                $_SESSION['rating'] = time() + $flud;
            }
        }
    }
    public static function minus($id = 0, $rating = null, $flud = 5, $pr = null)
    {
        if (!isset($_SESSION['rating_minus'])) {
            $_SESSION['rating_minus'] = time();
        }

        if ($id > 0 && $_SESSION['rating_minus'] <= time()) {
            $rating = number_format($rating, 2);
            if ($pr != null) {
                $rating = $rating - $rating * "0.$pr";
            }
            $data = [$rating, $id];
            if (go\DB\query('update `user` set `rating`=`rating`-? where `id`=?i', $data)) {
                cache_delete::user($id);
                $_SESSION['rating_minus'] = time() + $flud;
            }
        }
    }

    public static function output($id = 0)
    {
        if ($id > 0) {
            if ($rating = go\DB\query('select `rating` from `user` where `id`=?i', [$id])->el()) {
                return number_format($rating, 2);
            }
        }
    }

    public static function msgnum($nums = 0.00)
    {
        $nums = mb_strlen($nums, 'UTF-8');
        $nums = ($nums / 100000);
        return number_format($nums, 2);
    }

    public static function ban($id = 0)
    {
        if (!isset($_SESSION['rating_ban'])) {
            $_SESSION['rating_ban'] = time();
        }

        if ($id > 0 && $_SESSION['rating_ban'] <= time()) {
            $oldrating = go\DB\query('select `rating` from `user` where `id`=?i', [$id])->el();
            if ($oldrating > 0.00) {
                $rating = number_format(($oldrating - $oldrating * 0.50), 2);
                $data = [$rating, $id];
                if (go\DB\query('update `user` set `rating`=? where `id`=?i', $data)) {
                    cache_delete::user($id);
                }
            }
            // от флуда
            $_SESSION['rating_ban'] = time() + 60;
        }
    }
}
