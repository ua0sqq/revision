<?php

class image
{
    public static function is($img = null, $height = false, $width = false, $null =
        '/style/icons/default.png')
    {
        $is = false;
        if (is_file('/' . $img)) {
            $is = getimagesize('http://' . $_SERVER['HTTP_HOST'] . $img);
        }
        return '<img src="' . ($is ? $img : $null) . '" style="height:' . ($height ?
            $height . 'px' : 'auto') . ';width:' . ($width ? $width . 'px' : 'auto') .
            ';" alt="*"/>';
    }
}
