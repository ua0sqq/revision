<?php

function dir_clear($path, $conf = 1)
{
    if ($handle = opendir($path)) {
        while (false !== ($file = readdir($handle))) {
            if ($file != '.' &&
            $file != '..' &&
            $file != '.htaccess' &&
            $file != 'clear_conf.txt'  &&
            $file != '0.png' &&
            $file != 'index.php' &&
            $file != 'guest.png') {
                @unlink($path.'/'.$file);
            }
        }
    
        if ($conf == 1) {
            $text = 'Последняя зачистка папки была ['.date("F j, Y, H:i:s") .']';
            $files=fopen($path.'clear_conf.txt', 'w');
            fwrite($files, $text);
            fclose($files) ;
            closedir($handle);
        }
    }
}
