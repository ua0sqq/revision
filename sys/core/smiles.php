<?php

    # Смайлы
    function smiles($msg)
    {
        if (preg_match_all('/(\:\-D)|(\:\-\/)|(\=\])|(\;\-\))|(8\-\))|(\=\)\))|(\:\-\))|(\:\-\()|(\.\w*?\.)|(\:\w*?\:)|(\*\w*?\*)/ui', $msg, $matches)) {
            $val = array_unique($matches[0], SORT_STRING);
        } else {
            return $msg;
        }
        $smiles = go\DB\query('SELECT `zamena`, `name` FROM `smiles` WHERE `zamena` IN(?l)', [$val])->assoc();
        foreach ($smiles as $row) {
            $explode = explode('|', $row['zamena']);
            $msg = str_replace($explode, ' <img src="/style/smiles/' . htmlentities($row['name'], ENT_QUOTES, 'UTF-8') . '"/> ', $msg);
        }

        return $msg;
    }

