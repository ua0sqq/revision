<?php

//	запись действий администрации
function admin_log($mod, $act, $opis)
{
    global $user;
    $sql = go\DB\query('SELECT * FROM `admin_log_mod` WHERE `name`=? LIMIT ?i', [$mod, 1])->row();
    if (!$sql['id']) {
        $id_mod = go\DB\query('INSERT INTO `admin_log_mod` (`name`) VALUES (?)', [$mod])->id();
    } else {
        $id_mod = $sql['id'];
    }

    $sqls = go\DB\query('SELECT * FROM `admin_log_act` WHERE `name`=? AND `id_mod`=?i LIMIT ?i', [$act, $id_mod, 1])->row();
    if (!$sqls['id']) {
        $id_act = go\DB\query('INSERT INTO `admin_log_act` (`name`, `id_mod`) VALUES (?, ?i)', [$act, $id_mod])->id();
    } else {
        $id_act =  $sqls['id'];
    }
    go\DB\query('INSERT INTO `admin_log` (`time`, `id_user`, `mod`, `act`, `opis`) VALUES (?i, ?i, ?i, ?i, ?)',
                    [time(), $user['id'], $id_mod, $id_act, $opis]);
}
