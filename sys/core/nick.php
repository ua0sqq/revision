<?php
// ???
function nick($us = null, $type = 'link', $img = 1, $ban_text = null)
{
    global $set;
    $sql = 'SELECT `id`, `nick`, `pol`, `date_reg`, `mylink`, (
SELECT count( * ) FROM `ban` WHERE `id_user`=`user`.`id` AND `time` >?i GROUP BY `id_user`) AS cnt
FROM `user` WHERE `id` =?i';
    $ank = go\DB\query($sql, [time(), $us])->row();
    if (!$ank) {
        $ank = ['id' => $set['id_system'],
              'mylink' => '',
              'nick' => $set['nick_system'],
              'date_reg' => $set['install_system'],
              'pol' => 0,
              'cnt' => 0];
    }
    if ($ank['mylink'] == null) {
        $ank['mylink'] = 'id' . $ank['id'] ? $ank['id'] : 0;
    }
    if ($type == 'link') {
        $link = '<a href="/' . $ank['mylink'].'">';
        $link_end = '</a>';
    } else {
        $link = null;
        $link_end = null;
    }

    if ($us && $img == 1) {
        $browser = user_browser($us);
        $icons = user_icon($us);
        $medal = user_medal($us);
    } else {
        $browser = null;
        $icons = null;
        $medal = null;
    }
    #	если забанен
    $ban_user = $ank['cnt'] ?: false;
    #	Рега от
    $usreg = $ank['date_reg'] > time() - $set['user_reg_time'];

    $nicks = $ank['nick'];

    if ($ban_text == null) {
        $ban_text = '<span style="text-decoration:line-through;">';
        $ban_text_end = '</span>';
    } else {
        $ban_text = null;
        $ban_text_end = null;
    }

    if (!$ban_user) {
        $nick_r = (isset($ank['nick']) ? $nicks : $set['nick_system']);
    } else {
        $nick_r = $ban_text.' '.(isset($ank['nick']) ? $nicks : $set['nick_system']).' '.$ban_text_end;
    }

    if ($type != null) {
        $new = ($usreg ? $ank['pol'] == 0 ? ' <span style="color:#DE2DE9"> NEW </span> ' : ' <span style="color:#0080ff"> NEW </span> ' : false);
    } else {
        $new = null;
    }
    $nick =    $nick_r . $new;

    return ($link . $medal . $icons . $nick . $browser . $link_end);
}
