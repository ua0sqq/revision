<?php

    function links_preg1($arr)
    {
        global $set;
        
        #-----------------------------------------------------#
        $arr[1] = str_replace(PHP_EOL, '', $arr[1]);
        if (preg_match('~^ *(javascript|data)~i', $arr[1])) {
            return '#error JavaScript';
        }
        #-----------------------------------------------------#
            
        $LinkRe = str_replace(array('http://','www.','https://'), null, $arr[2]);

        if (preg_match('#^http://'.preg_quote($_SERVER['HTTP_HOST']).'#', $arr[1]) || !preg_match('#://#', $arr[1])) {
            return '<a class="link_bb_2" href="'.$arr[1].'">'.$LinkRe.'</a>';
        } else {
            return '<a'. ($set['web'] ? ' target="_blank"':null) .' class="link_bb" href="http://'.$_SERVER['HTTP_HOST'].'/linkGo?go='.base64_encode(html_entity_decode($arr[1])).'">'.$LinkRe.'</a>';
        }
    }
    
    function links_preg2($arr)
    {
        global $set;
        //не увеерн что это тут нужно
        #-----------------------------------------------------#
        $arr[1] = str_replace(PHP_EOL, '', $arr[1]);
        if (preg_match('~^ *(javascript|data)~i', $arr[1])) {
            return '#error JavaScript';
        }
        #-----------------------------------------------------#
            
        $LinkRe = str_replace(array('http://','www.','https://'), null, $arr[2]);
        
        if (preg_match('#^http://'.preg_quote($_SERVER['HTTP_HOST']).'#', $arr[2])) {
            return $arr[1].'<a class="link_bb_2" href="'.$arr[2].'">'.$LinkRe.'</a>'.$arr[4];
        } else {
            return $arr[1].'<a'.($set['web'] ? ' target="_blank"':null).' class="link_bb" href="http://'.$_SERVER['HTTP_HOST'].'/linkGo?go='.base64_encode(html_entity_decode($arr[2])).'">'.$LinkRe.'</a>'.$arr[4];
        }
    }
    
    function links($msg)
    {
        global $set;
        if ($set['bb_img']) {
            $msg=preg_replace_callback('/\[img\](.+)\[\/img\]/isU', 'img_preg', $msg);
        }
        if ($set['bb_url']) {
            $msg=preg_replace_callback('/\[url=(.+)\](.+)\[\/url\]/isU', 'links_preg1', $msg);
        }
        if ($set['bb_http']) {
            $msg=preg_replace_callback('~(^|\s)([a-z]+://([^ \r\n\t`\'"]+))(\s|$)~iu', 'links_preg2', $msg);
        }
        return $msg;
    }
