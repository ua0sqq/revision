<?php
    # Постраничная навигация
    function page($k_page=1)
    { # Выдает текущую страницу
    $page=1;
        if (isset($_GET['page'])) {
            if ($_GET['page']=='end') {
                $page=intval($k_page);
            } elseif (is_numeric($_GET['page'])) {
                $page=intval($_GET['page']);
            }
        }
        if ($page<1) {
            $page=1;
        }
        if ($page>$k_page) {
            $page=$k_page;
        }
        return $page;
    }

    function k_page($k_post=0, $k_p_str=10)
    { # Высчитывает количество страниц
        if ($k_post!=0) {
            $v_pages=ceil($k_post/$k_p_str);
            return $v_pages;
        } else {
            return 1;
        }
    }

    function str($link='?', $k_page=1, $page=1)
    { # Вывод номеров страниц (только на первый взгляд кажется сложно ;))
        if ($page<1) {
            $page=1;
        }
        echo '<div class="str">';

        $str_div = 'btn btn-info';

        if ($page!=1) {
            echo '<a href=\''.$link.'page=1\' title="Страница №1"><span class="'.$str_div.'">1</span></a>';
        } else {
            echo '<span class="'.$str_div.'">[<b>1</b>]</span>';
        }
          
        for ($ot=-3; $ot<=3; $ot++) {
            if ($page+$ot>1 && $page+$ot<$k_page) {
                if ($ot==-3 && $page+$ot>2) {
                    echo ' ..';
                }
                if ($ot!=0) {
                    echo ' <a href="'.$link.'page='.($page+$ot).'" title="Страница №'.($page+$ot).'"><span class="'.$str_div.'">'.($page+$ot).'</span></a> ';
                } else {
                    echo ' <span class="'.$str_div.'" style="color:#fff"> [<b>'.($page+$ot).'</b>] </span>';
                }
                if ($ot==3 && $page+$ot<$k_page-1) {
                    echo '<span class="'.$str_div.'"> ...</span>';
                }
            }
        }
        if ($page!=$k_page) {
            echo ' <a href="'.$link.'page=end" title="Страница №'.$k_page.'"><span class="'.$str_div.'">'.$k_page.'</span></a>';
        } elseif ($k_page>1) {
            echo '<span class="'.$str_div.'"> [<b>'.$k_page.'</b>]</span>';
        }
        echo '</div>';
    }
