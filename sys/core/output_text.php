<?php

    // функция обрабатывает текстовые строки перед выводом в браузер
    // настоятельно не рекомундуется тут что-либо менять
    function output_text($str, $br=1, $html=1, $smiles=1, $links=1, $bbcode=1)
    {
        global $theme_ini;
        // преобразуем все к нормальному перевариванию браузером
        if ($html) {
            $str=htmlentities($str, ENT_QUOTES, 'UTF-8');
        }
        
        // вставка смайлов
        if ($smiles) {
            $str = smiles($str);
        }
    
        // обработка ссылок
        if ($links) {
            $str = links($str);
        }

        // обработка bbcode
        if ($bbcode) {
            $tmp_str = $str;
            $str = bbcode($str);
        }
        // переносы строк
        if ($br) {
            $str=br($str);
        }
        
        $str = antispam($str);
    
        //Дополнительная обработка текста
        $str = output_add($str);
    
    // возвращаем обработанную строку
    return stripslashes($str);
    }
