<?php
    # Подсветка кода
    function bbcodehightlight($arr)
    {
        $arr[0] = html_entity_decode($arr[0], ENT_QUOTES, 'UTF-8');
        return '<div class="cit" style="overflow:scroll;clip:auto;max-width:480px;">'.
        preg_replace('#<code>(.*?)</code>#si', '\\1', highlight_string($arr[0], 1)).'</div>';
    }
