<?php

        
    # только для тех, у кого уровень доступа больше или равен $level
    function only_level($level=0, $link = null)
    {
        global $user;
        if (!isset($user) or $user['level'] < $level) {
            if ($link == null) {
                $link='/';
            }
            exit(header('Location: '.$link));
        }
    }
    
    
    
    # только для незарегистрированых
    function only_unreg($link = null)
    {
        global $user;
        if (isset($user)) {
            if ($link == null) {
                $link='/';
            }
            exit(header('Location: '.$link));
        }
    }
    
    
    # только для зарегистрированых
    function only_reg($link = null)
    {
        global $user;
        if (!isset($user)) {
            if ($link == null) {
                $link = '/';
            }
            $_SESSION['message'] = lang('Только для зарегистрированых');
            exit(header('Location: '.$link));
        }
    }
