<?php
    //  Определение доступа группам \\
    function user_access($access, $u_id = null, $exit = false)
    {
        if ($u_id == null) {
            global $user;
        } else {
            $user = go\DB\query('SELECT `group_access` FROM `user` WHERE id=?i', [$u_id]);
        }
        if (!isset($user['group_access']) or $user['group_access'] == null) {
            if ($exit !== false) {
                exit(header('Location: ' . $exit));
            } else {
                return false;
            }
        }

        $cnta = go\DB\query('SELECT COUNT(*) cnt FROM `user_group_access` WHERE `id_group`=?i AND `id_access`=?string',
                               [$user['group_access'], $access])->row();
        if ($exit !== false) {
            if ((int)$cnta['cnt'] == 0) {
                exit(header('Location: ' . $exit));
            }
        }
        return (int)$cnta['cnt'] ? true : false;
    }
