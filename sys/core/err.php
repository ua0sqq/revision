<?php

# вывод ошибок
function err()
{
    global $err;
    if (isset($err)) {
        if (is_array($err)) {
            foreach ($err as $key=> $value) {
                echo "<div class='err'>$value</div>";
            }
        } else {
            echo "<div class='err'>$err</div>";
        }
    }
}
