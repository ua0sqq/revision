<?php

//-- 1. Верхнее wap меню + уведомление --\\
function aut()
{
    global $set;
    if ($set['non_aut'] == false) {
        global $user;

        if (isset($user)) {
            lang::start('default');
                //global $db;

                // start Почта
                if (isset($_COOKIE['mail_count']) && $_COOKIE['mail_count'] == null) {
                    $mail = go\DB\query('SELECT COUNT(*) FROM `mail` WHERE `id_kont`=?i AND `read`=?string',
                                   [$user['id'], 0])->el();
                    setcookie('mail_count', $mail, time() + 15);
                }

            if (isset($_COOKIE['mail_count']) && $_COOKIE['mail_count'] != null) {
                $mail = (int)($_COOKIE['mail_count']);
            } else {
                $mail = go\DB\query('SELECT COUNT(*) FROM `mail` WHERE `id_kont`=?i AND `read`=?string',
                                   [$user['id'], 0])->el();
            }
                // end Почта
                // start Спам
                if (user_access('system_jurnal')) {
                    if (isset($_COOKIE['spam_count']) && $_SESSION['spam_count'] <= time()) {
                        $spam = $db->query('SELECT COUNT(*) FROM `jurnal_system` WHERE `read`=?string', [0])->el();
                        $_SESSION['spam_count'] = $spam;
                        $_SESSION['spam_count_time'] = time() + 60;
                    }

                    if (isset($_COOKIE['spam_count'])  and $_SESSION['spam_count'] != null) {
                        $spam = intval(@$_SESSION['spam_count']);
                    } else {
                        $spam = go\DB\query('SELECT COUNT(*) FROM `jurnal_system` WHERE `read`=?string', [0])->el();
                    }
                }
                // end Спам

                // start Журнал
                if (isset($_COOKIE['jurnal_count'])  and $_COOKIE['jurnal_count'] == null) {
                    $jurnal = go\DB\query('SELECT COUNT(*) FROM `jurnal` WHERE `id_kont`=?i AND `read`=?string',
                                     [$user['id'], 0])->el();
                    setcookie('jurnal_count', $jurnal, time() + 60);
                }

            if (isset($_COOKIE['jurnal_count'])  and $_COOKIE['jurnal_count'] != null) {
                $jurnal = intval(@$_COOKIE['jurnal_count']);
            } else {
                $jurnal = go\DB\query('SELECT COUNT(*) FROM `jurnal` WHERE `id_kont`=?i AND `read`=?string',
                                     [$user['id'], 0])->el();
            }
                // end Журнал

                // start форум Журнал
                if (isset($_COOKIE['f_journal'])  and $_COOKIE['f_journal'] == null) {
                    $journal_f = go\DB\query('SELECT COUNT(*) FROM `jurnal` WHERE `id_user`=?i AND `read`=?string',
                                     [$user['id'], 0])->el();
                    setcookie('f_journal', $journal_f, time() + 60);
                }

            if (isset($_COOKIE['f_journal'])  and $_COOKIE['f_journal'] != null) {
                $journal_f = intval(@$_COOKIE['f_journal']);
            } else {
                $journal_f = go\DB\query('SELECT COUNT(*) FROM `jurnal` WHERE `id_user`=?i AND `read`=?string',
                                     [$user['id'], 0])->el();
            }
                // end форум Журнал

                // Профиль-кабинет
                //если тек. ссылка не страница владельца
                $REQUEST_URI = $_SERVER['REQUEST_URI']; ?>
<table class="table-aut">
    <tr>
<?php

            if ($REQUEST_URI == '/' || $REQUEST_URI == '/index.php') {
                ?>
        <td class="au">
            <a style="display: block;" href="/user"><?php echo lang('Кабинет')?></a>
        </td>
<?php

            }

            if ($REQUEST_URI != '/'.$user['mylink']) {
                //выводим ссылку на неё
?>
        <td class="au">
            <a style="display: block;" href="/<?php echo $user['mylink']?>"><span style="color:#EE7023;"><?php echo nick($user['id'], null, 0)?></span></a>
        </td>
<?php

            } else {
                //если страница владельца $user['mylink'] то выводим ссылку на кабинет
?>
        <td class="au">
            <a style="display: block;" href="/user">  <span style="color:#EE7023;"><?php echo lang('Кабинет')?> </span></a>
        </td>
<?php

            }
            #---------------------------------------Профиль-кабинет--------------------------------#

            if ($journal_f != 0) {
                ?>
        <td class="au">
            <a style="display: block;" href="/forum/journal.php?new"><span style="color:#EE7023;"><?php echo lang('Форум')?>[+<?php echo $journal_f ?>] </span></a>
        </td>
<?php

            }
            if ($user['level'] > 0) {
                ?>
        <td class="au">
            <a style="display: block;" href="<?php echo APANEL?>"><span style="color:#E57216;"> <?php echo lang('Админка')?>  </span></a>
        </td>
<?php

            }
            if ($mail > 0) {
                ?>
        <td class="au">
            <a style="display: block;" href="/pages/mail/new.php"><img src="/style/icons/mess0.png" alt="new"/> +<?php echo $mail?> </a>
        </td>
<?php

            }
            if ($jurnal > 0) {
                ?>
        <td class="au">
            <a style="display: block;" href="/user/jurnal"><img src="/style/icons/jurnal.png" alt="jurnal"/> +<?php echo $jurnal?> </a>
        </td>
<?php

            }
            if (user_access('system_jurnal') && $spam > 0) {
                ?>
        <td class="au">
            <a style="display: block;" href="<?php echo APANEL?>/jurnal_system.php"><img src="/style/icons/spam.png" alt="spam"/> +<?php echo $spam?> </a>
        </td>
<?php

            } ?>
    </tr>
</table>
<?php
                // Загрузка дополнительных плагинов
                $Search = glob(H.'sys/panel_aut/*.php');
            if (count($Search)) {
                foreach ($Search as $load_plugins) {
                    include_once $load_plugins;
                }
            }
        } else {
            ?>
				<table style="width:100%;border:1px;border-spacing:1px 1px;padding:0px;table-layout:fixed;">
				<tr>
					<td class='au'>
						<a style='display: block;' href='/aut.php'>  <?php echo lang('Авторизация')?></a></td>
					<td class='au'><a  style='display: block;' href='/reg.php'><?php echo lang('Регистрация')?></a></td>
				</tr>
				</table>
				<?php

        }
    }
}
