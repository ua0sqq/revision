<?php
    #	Функция для работы внешних изображений
    #	Возможна будет заменена или доработана
    
    function img_preg($arr)
    {
        if (function_exists('set_time_limit')) {
            @set_time_limit(60);
        }
        $ext = substr($arr[1], -3);
        $myhost = md5($arr[1]) . '.' . $ext;
        $nonpic = "/style/icons/img_null.png";
        $ext_valid = array('jpg', 'jpeg', 'png', 'gif');
        if (is_file(H . 'sys/cache/images/' . $myhost)) {
            return '<a href="http://' . $_SERVER['HTTP_HOST'] . '/linkGo?go=' . base64_encode(html_entity_decode($arr[1])) . '">   
		<img src="/sys/cache/images/' . $myhost .'" alt="'.$_SERVER['HTTP_HOST'].'-image" />
		</a>';
        } elseif (in_array($ext, $ext_valid) && getDomain('http://' . $_SERVER['HTTP_HOST']) !== getDomain($arr[1])) {
            $ext = substr($arr[1], -3);
            $myhost = md5($arr[1]) . '.' . $ext;
            $locale = H . 'sys/tmp/' . $myhost;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $arr[1]);
            $fp = fopen($locale, "w+");
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_REFERER, $arr[1]);
            curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
            curl_exec($ch);
            if (curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200) {
                unlink($locale);// удаляем ненужный файл
        // делаем что-то ещё (пишем адрес в лог, выводим ошибку, etc...)
            }
            curl_close($ch);
            fclose($fp);
    
            require_once  H."sys/inc/ImgType.class.php";
            if (is_file(H . 'sys/tmp/' . $myhost) && filesize(H . 'sys/tmp/' . $myhost) !== 0) {
                $image = new ImgType();
                $image->load(H.'sys/tmp/' . $myhost);
                $image->resizeToWidth(128);
                $image->save(H.'sys/cache/images/'.$myhost);
            }
            return '
        <a href="http://' . $_SERVER['HTTP_HOST'] . '/linkGo?go=' . base64_encode(html_entity_decode($arr[1])) . '">
          <img src="/sys/cache/images/' . $myhost .'" alt="'.$_SERVER['HTTP_HOST'].'-image" />
        </a>';
        } else {
            return '<img src="'.$nonpic.'" width="128" height="128" />';
        }
    }

function getDomain($domain)
{
    preg_match('~^(?:http://)?([^/]+)~i', $domain, $matches);
    $host = isset($matches[1]) ? $matches[1] : false;
    preg_match('/[^.]+.[^.]+$/', $host, $matches);
    return isset($matches[0]) ? $matches[0] : false;
}
