<?php

# антимат сделает автоматическое предупреждение, а затем бан
function antimat($str)
{
    global $user, $time, $set, $db;
    if ($set['antimat']) {
        $antimat = empty($_SESSION['antimat']) ? null : $_SESSION['antimat'];
        require_once H . 'sys/inc/censure.php';
        $censure = censure($str);
        if ($censure) {
            $antimat[$censure] = $time;
            if (count($antimat) > 3 and isset($user) and $user['level'] == 0) { // если сделано больше 3-х предупреждений
                $prich = lang('Обнаружен мат') . ' ' . $censure;
                $timeban = $time + 60*60; // бан на час
                $db->query('INSERT INTO `ban` (`id_user`, `id_ban`, `prich`, `time`)
						   VALUES (?i, ?i, ?, ?i)', [$user['id'], 0, $prich, $timeban]);
                $tt = lang('Бан пользователя');
                admin_log(lang('Пользователи'), lang('Бан'),
                          ' "' . $tt . ' [url=' . APANEL . '/ban.php?id=' . $user['id'] . ']' . $user['nick'] . '[/url]"(id#$user[id]) ' . lang('До') . ' ' . vremja($timeban) . ' ' . lang('по причине') . '  "' . $prich .'"');
                exit(header('Location: /ban.php?'));
            }
            return $censure;
        }
        return false;
    }
    return false;
}
