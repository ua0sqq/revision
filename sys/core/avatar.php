<?php

#  Аватары пользователей
function avatar($id, $width = 50, $height = 50, $ImgType = 0, $link = 'link')
{
    global $set;
    if ($id) {
        $ank = go\DB\query('SELECT `id`, `pol`, `mylink` FROM `user` WHERE `id`=?i', [$id])->row();
        if ($ank['id'] == 0) {
            $link = null;
        }
        if ($ank['mylink'] == null) {
            $ank['mylink'] = 'id' . $ank['id'];
        }
        //если вкл. ссылка на авку
        if ($link == 'link') {
            $link = '<a href="/' . $ank['mylink'] . '">';
            $link_end = '</a>';
        } else {
            $link = null;
            $link_end = null;
        }
        //если картинка есть в папке
        if (is_file(H . 'files/avatars/' . $id . '.png')) {
            echo $link . '<img src="/ImgType?imgLink=' . base64_encode('files/avatars/' . $id . '.png') .
        '&amp;width=' . $width . '&amp;height=' . $height . '&amp;ImgType=' . $ImgType . '" alt="*"/>' . $link_end;
        } else {
            //если нет аватара
            echo $link . '<img src="/ImgType?imgLink=' . base64_encode('style/themes/' . $set['set_them'] .
            '/pol_' . $ank['pol'] . '.png') . '&amp;width=' . $width . '&amp;height=' . $height .
            '&amp;ImgType=' . $ImgType . '" alt="*"/>' . $link_end;
        }
    } else {
        echo '<img src="/ImgType?imgLink=' . base64_encode('files/avatars/' . $id . '.png') .
        '&amp;width=' . $width . '&amp;height=' . $height . '&amp;ImgType=' . $ImgType . '" alt="*"/>';
    }
}
