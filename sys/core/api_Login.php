<?php
#	Класс для работы с ulogin и не только с ним ,при желание можно переделать
#   by Saint and ulogin

  class api_Login
  {
      public static function out($soc_redirect_uri)
      {
          global $set;

        //если пусто
        if ($set['socList'] == null) {
            //задаем по умолчанию
        $socList = 'vkontakte,odnoklassniki,mailru,twitter,facebook,youtube,steam';
        } else {
            //если нет то берем инфу из переменной системных настроек
        $socList = $set['socList'];
        }

          $output = '
		<div class="">
		<script src="//ulogin.ru/js/ulogin.js"></script>
		<div id="uLogin" data-ulogin="display=panel;fields=first_name,last_name,city,sex,photo,photo_big,country,email,nickname,bdate;providers='. $socList .';hidden=;redirect_uri=http%3A%2F%2F'. $soc_redirect_uri .'">
		</div>
		</div>
		';

          return $output;
      }

      public static function arr($token)
      {
          if (ini_get('allow_url_fopen') == 0) {
              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, 'http://ulogin.ru/token.php?token=' . $token . '&host=' . $_SERVER['HTTP_HOST']);
              curl_setopt($ch, CURLOPT_HEADER, 0);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
              curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

              $file = curl_exec($ch);
              curl_close($ch);
          } else {
              $file = file_get_contents('http://ulogin.ru/token.php?token=' . $token . '&host=' . $_SERVER['HTTP_HOST']);
          }
          return json_decode($file, true);
      }
  }
