<?php
/**
  * update cache
  **/
if (isset($_COOKIE['times_update_cache_delete']) && $_COOKIE['times_update_cache_delete'] <= time() && isset($user['id'])) {
    if ($user['flood_count'] < 0) {
        $user['flood_count'] = 0;
    }

    cache_delete::user($user['id']);
    //определяем с чего сидит юзер
    $db->query('UPDATE `user` SET `browser`=?, `flood_count`=`flood_count`-1
	WHERE `id` =?i', [($webbrowser == 'web' ? 'web':'wap'), $user['id']]);
    setcookie('times_update_cache_delete', time() + $set['user_online'], time() + 60*60*24*365);
} elseif (isset($user)) {
    setcookie('times_update_cache_delete', time() + $set['user_online'], time() + 60*60*24*365);
}
