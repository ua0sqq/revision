<?php

if (isset($user) && time() > (2592000 + $user['date_reg']) && $user['id'] == 1 && $user['license_time'] == 0) {
    include_once H . 'sys/inc/api_config.php';

    $text = 'Привет прошел месяц как ты пользуешься движком DCMS-FIERA
Думаю самое время пробрести лицензию  '.FIERA_URL.'/license/ которая отроет тебе дополнительный функционал твоего сайта
Если у тебя есть вопросы ,пожелания или предложения по работе движка обращайся на форум  '.FIERA_URL.'/forum/ и мы тебе поможем
Так же на сайте бывают акции где лицензию можно будет выиграть совершено бесплатно
Спасибо за внимания =)
';

    mail_send(0, 1, $text);

    $db->query('UPDATE `user` SET `license_time`=?i WHERE `id`=?i', [1, 1]);
    cache_delete::user($user['id']);
    $user['license_time'] = 1;
}
