<?php

    if (function_exists('set_time_limit')) {
        set_time_limit(60);
    }
    $db->query('DELETE FROM `guests` WHERE `date_last`<?i',
               [(time() - $set['user_online'])]);
    
    # Удаление профилей из базы данных ,которые небыли активированы ,обычно это в случаи активации по email
    $db->query('DELETE FROM `user` WHERE `activation` IS NOT NULL AND `date_reg`<?i',
               [(time() - 60*60*24)]);

    if (function_exists('curl_version')) {
        if ($curl = curl_init()) {
            include_once H . 'sys/inc/api_config.php';
            curl_setopt($curl, CURLOPT_URL, TODAY_STAT_FIERA .
                        '?vers=' . $set['fiera_vers'] . '&stat=' . $set['fiera_status'] . '&url=' . $_SERVER['HTTP_HOST']);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_close($curl);
        }
    }

    $od = opendir(H.'sys/cache/images/');
    while ($rd=readdir($od)) {
        if (!preg_match('#^\.#', $rd) && filectime(H . 'sys/cache/images/' . $rd) < time() - 60*60*24) {
            @delete_dir(H.'sys/cache/images/' . $rd);
        }
    }
    closedir($od);
