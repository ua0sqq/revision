<?php

    # удаляем все контакты, помеченные на удаление более месяца назад
    $qd = $db->query('SELECT * FROM `users_konts` WHERE `type`=?string AND `time`<?i',
        ['deleted', (time()-60*60*24*30)]);

    while ($deleted = $qd->row()) {
        $db->query('DELETE FROM `users_konts` WHERE `id_user`=?i AND `id_kont`=?i', [$deleted['id_user'], $deleted['id_kont']]);
        # если юзер не находится в контакте у другого, то удаляем и все сообщения
        if (count::query('users_konts', '`id_kont`="'.$deleted[id_user].'" AND `id_user`="'.$deleted[id_kont].'"') == 0) {
            $db->query('DELETE FROM `mail` WHERE `id_user`=?i AND `id_kont`=?i OR `id_kont`=?i AND `id_user`=?i',
            [$deleted['id_user'], $deleted['id_kont'], $deleted['id_user'], $deleted['id_kont']]);
        }
    }
