<?php

#	сверяем левые ники
$pattern = 'SELECT `id` FROM `user` WHERE `ip`=?i AND `ua`=? AND `date_last`>?i AND `id`<>?i';
$data = [$iplong, $ua, (time() - $set['collision_time']), $user['id']];
$res = $db->query($pattern, $data)->assoc();
if (isset($res)) {
    foreach ($res as $collision) {
        if (!$db->query('SELECT COUNT(*) FROM user_collision WHERE `id_user`=?i AND `id_user2`=?i OR `id_user2`=?i AND `id_user`=?i',
                        [$user['id'], $collision['id'], $user['id'], $collision['id']])->el()) {
            $db->query('INSERT INTO `user_collision` (`id_user`, `id_user2`, `type`)
VALUES(?i, ?i, ?)', [$user['id'], $collision['id'], 'ip_ua_time']);
        }
    }
}
