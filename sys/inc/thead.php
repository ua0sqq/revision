<?php

ob_start();

$set['meta_keywords'] = $set['meta_keywords'] != null  ? $set['meta_keywords']:null;
$set['meta_description'] = $set['meta_description'] != null ? $set['meta_description']:null;
/*
if (isset($user) && $set['title'] != null) {
    $db->query("UPDATE `user` SET `url_title` =? WHERE `id` =?i LIMIT 1", [$set['title'], $user['id']]);
}
*/

if ($set['meta_keywords'] != null or $set['meta_description'] != null) {
    function meta_function($out)
    {
        global $set;
        return str_replace('</head>', '
		<meta name="keywords" content="'.$set['meta_keywords'].'" />
		<meta name="description" content="'.$set['meta_description'].'" />'."\n</head>", $out);
    }
    ob_start('meta_function');
}

include_once H . 'sys/inc/style_add.php';

if (is_file(H . 'style/themes/' . $set['set_them'] . '/head.php')) {
    include_once H . 'style/themes/' . $set['set_them'] . '/head.php';
} else {
    $set['web'] = false;
    header('Content-type: text/html'); ?><!DOCTYPE html>
<html lang="ru">
<head>
<title><?php echo $set['title']; ?></title>
<link rel="shortcut icon" href="/favicon.ico" />
<link rel="stylesheet" href="/style/themes/<?echo $set['set_them'];?>/style.css" type="text/css" />
<link rel="alternate" title="Новости RSS" href="/news/rss.php" type="application/rss+xml" />
</head>
<body>
<div class="body">
<?php

}
