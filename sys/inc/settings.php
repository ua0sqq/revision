<?php

$set = array(); // массив с настройками
$set_default = array();
$set_dinamic = array();
$set_replace = array();
// загрузка настроек по умолчанию. Позволяет исключить отсутствие неопределенных переменных
$default = parse_ini_file(H . 'sys/dat/default.ini', true);
$set_default = $default['DEFAULT'];
$set_replace = $default['REPLACE'];
if (is_file(H . 'sys/dat/settings.conf') && $fset = file_get_contents(H . 'sys/dat/settings.conf')) {
    $set_dinamic = unserialize($fset);
} elseif (file_exists(H . 'install/index.php')) {
    exit(header("Location: /install/"));
}
$set = array_merge($set_default, $set_dinamic, $set_replace);

$fiera_vers = parse_ini_file(H . 'sys/ini/vers.ini');
$set['fiera_vers'] = $fiera_vers['vers'];
$set['fiera_status'] = $fiera_vers['status'];
define('FIERA_VERS', $set['fiera_vers']);
if ($set['show_err_php'] == 2) {
    error_reporting(E_ALL);
    ini_set('display_errors', true);
} else {
    ini_set('display_errors', false);
    error_reporting(0);
}
if ($set['frames_stop'] == 1) {
    header("X-Frame-Options: SAMEORIGIN");
}
# 	ShaMan (определение версии сайта)
//if (isset($_SESSION['browser_use']))
//{
//$webbrowser = true;
//}
//else
//{
if (isset($_SERVER["HTTP_USER_AGENT"]) and preg_match('#Opera Mini|android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i#i',
    $_SERVER["HTTP_USER_AGENT"])) {
    $_SESSION['browser_use'] = false;
    $webbrowser = false;
} elseif (isset($_SERVER["HTTP_USER_AGENT"]) and preg_match('#linux|bsd|x11|unix|macos|macintosh|#i',
        $_SERVER["HTTP_USER_AGENT"])) {
    $_SESSION['browser_use'] = true;
}
//}
# Переключатель wap
if (isset($_GET['wap'])) {
    $_SESSION['browser_use'] = false;
    header('Location: ?' . SID);
}
# Переключатель web
elseif (isset($_GET['web'])) {
    $_SESSION['browser_use'] = true;
    header('Location: ?' . SID);
}
$webbrowser = $_SESSION['browser_use'];
$set['web'] = false;
