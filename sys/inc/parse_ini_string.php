<?php

/**
 * @author http://php.net/manual/en/function.parse-ini-string.php
 * parse_ini_string_m аналог функции parse_ini_string.
 * написана из-за отсутствия php 5.3 на одном хостинге.
 * parse_ini_string_m:
 * - Игнорирует строки комментариев, которые начинаются с ";" или "#"
 * - игнорирует пробелы, которые не имеют "="
 * - поддерживает массив значений и ключи к значениям массива
 */
if (!function_exists('parse_ini_string')) {
    function parse_ini_string($str)
    {
        if (empty($str)) {
            return false;
        }

        $lines = explode("\n", $str);
        $ret = array();
        $inside_section = false;

        foreach ($lines as $line) {
            $line = trim($line);

            if (!$line || $line[0] == "#" || $line[0] == ";") {
                continue;
            }
       
        
            if ($line[0] == "[" && $endIdx = strpos($line, "]")) {
                $inside_section = substr($line, 1, $endIdx-1);
                continue;
            }

            if (!strpos($line, '=')) {
                continue;
            }

            $tmp = explode("=", $line, 2);

            if ($inside_section) {
                $key = rtrim($tmp[0]);
                $value = ltrim($tmp[1]);

                if (preg_match("/^\".*\"$/", $value) || preg_match("/^'.*'$/", $value)) {
                    $value = mb_substr($value, 1, mb_strlen($value) - 2);
                }

                $t = preg_match("^\[(.*?)\]^", $key, $matches);
                if (!empty($matches) && isset($matches[0])) {
                    $arr_name = preg_replace('#\[(.*?)\]#is', '', $key);

                    if (!isset($ret[$inside_section][$arr_name]) || !is_array($ret[$inside_section][$arr_name])) {
                        $ret[$inside_section][$arr_name] = array();
                    }

                    if (isset($matches[1]) && !empty($matches[1])) {
                        $ret[$inside_section][$arr_name][$matches[1]] = $value;
                    } else {
                        $ret[$inside_section][$arr_name][] = $value;
                    }
                } else {
                    $ret[$inside_section][trim($tmp[0])] = $value;
                }
            } else {
                $ret[trim($tmp[0])] = ltrim($tmp[1]);
            }
        }
        return $ret;
    }
}
