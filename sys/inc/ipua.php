<?php

$ip = false;
if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR']!='127.0.0.1' && preg_match("#^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$#", $_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip2['xff']=$_SERVER['HTTP_X_FORWARDED_FOR'];
    $ipa[] = $_SERVER['HTTP_X_FORWARDED_FOR'];
}
if (isset($_SERVER['HTTP_CLIENT_IP']) && $_SERVER['HTTP_CLIENT_IP']!='127.0.0.1' && preg_match("#^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$#", $_SERVER['HTTP_CLIENT_IP'])) {
    $ip2['cl']=$_SERVER['HTTP_CLIENT_IP'];
    $ipa[] = $_SERVER['HTTP_CLIENT_IP'];
}
if (isset($_SERVER['REMOTE_ADDR']) && preg_match("#^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$#", $_SERVER['REMOTE_ADDR'])) {
    $ip2['add']=$_SERVER['REMOTE_ADDR'];
    $ipa[] = $_SERVER['REMOTE_ADDR'];
}

$ip = $ipa[0];
$iplong = ip2long($ip);

//новый вариант проверки ua
function browser($agent)
{
    // регулярное выражение, которое позволяет отпределить 90% браузеров
    preg_match("/(MSIE|Opera|Firefox|Chrome|Version|Opera Mini|Netscape|Konqueror|SeaMonkey|Camino|Minefield|Iceweasel|K-Meleon|Maxthon)(?:\/| )([0-9.]+)/", $agent, $browser_info);
        
        // получаем данные из массива в переменную
        list(, $browser, $version) = $browser_info;
        
        // определение _очень_старых_ версий Оперы (до 8.50), при желании можно убрать
        if (preg_match("/Opera ([0-9.]+)/i", $agent, $opera)) {
            return 'Opera '.$opera[1];
        }

        // если браузер определён как IE
       if ($browser == 'MSIE') {
           // проверяем, не разработка ли это на основе IE
                preg_match("/(Maxthon|Avant Browser|MyIE2)/i", $agent, $ie);
                
                // если да, то возвращаем сообщение об этом
                if ($ie) {
                    return $ie[1].' based on IE '.$version;
                }
                
                // иначе просто возвращаем IE и номер версии
                return 'IE '.$version;
       }
        
        // если браузер определён как Firefox
        if ($browser == 'Firefox') {        // проверяем, не разработка ли это на основе Firefox
                preg_match("/(Flock|Navigator|Epiphany)\/([0-9.]+)/", $agent, $ff);
                
                // если да, то выводим номер и версию
                if ($ff) {
                    return $ff[1].' '.$ff[2];
                }
        }
        
    if (isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA']) && preg_match('#Opera#i', $browser)) {
        return 'Opera mini'.substr($agent, -5);
    }
        // если браузер определён как Opera 9.80, берём версию Оперы из конца строки
        if ($browser == 'Opera' && $version == '9.80') {
            return 'Opera '.substr($agent, -5);
        }
        // Опера мини тоже посылает данные о телефоне :)
        if ($browser == 'Version') {
            return 'Safari '.$version; // определяем Сафари
        }
        
        // для неопознанных браузеров проверяем, если они на движке Gecko, и возращаем сообщение об этом
        if (!$browser && strpos($agent, 'Gecko')) {
            return 'Browser based on Gecko';
        }
    return $browser.' '.$version; // для всех остальных возвращаем браузер и версию
}

include H . 'sys/inc/Browser.php';
if (isset($_SERVER['HTTP_USER_AGENT'])) {
    $browser = new Browser();
    $ua = $browser->getBrowser() . ' ' . $browser->getVersion();
    // На всякий случай
    if (isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA']) && preg_match('#Opera#i', $ua)) {
        $ua_om = $browser->getUserAgent();
        $version_om = explode(' ', $ua_om);
        $ua = 'Opera Mini (' . $version_om[1] . ')';
    }
} else {
    $ua = 'Unknown';
}
