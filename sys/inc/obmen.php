<?php

function obmen_path($path)
{
    $path = preg_replace("#(/){1,}#", "/", $path);
    $path = preg_replace("#(^(/){1,})|((/){1,}$)#", "", $path);
    $path_arr = explode('/', $path);
    $rdir = null;
    $rudir = null;
    for ($i = 0; $i < count($path_arr); $i++) {
        $of = '/';
        for ($z = 0; $z <= $i; $z++) {
            $of .= $path_arr[$z] . '/';
        }
        $rdir .= $path_arr[$i] . '/';
        $data = ['/' . $rdir, $rdir . '/', $rdir, 1];
        $dir_id = go\DB\query('SELECT * FROM `obmennik_dir` WHERE `dir`=? OR `dir`=? OR `dir`=? LIMIT ?i', $data)->row();
        $dirname = $dir_id['name'];
        $rudir .= "<a href=\"/obmen/" . url(preg_replace("#(^(/){1,})|((/){1,}$)#", "", $rdir))."/?page=$_SESSION[page]\">" . $dirname . '</a> &gt; ';
    }

    return preg_replace("# &gt; $#", "", $rudir);
}
