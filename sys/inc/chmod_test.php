<?php

/*
Получаем список папок и файлов
*/
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
    $chmod = file_get_contents(H . 'sys/dat/chmod_win.txt');
} else {
    $chmod = file_get_contents(H . 'sys/dat/chmod.txt');
}
echo '<div class="p_m">' . PHP_EOL;
$file = explode("\n", $chmod);
foreach ($file as $v) {
    $k = explode(':', $v);
    if ($install_mod != false and $k[0] == '/sys/dat/shif.conf' or $k[0] ==
        '/sys/dat/settings.conf') {
        continue;
    }
    //Пропускаем ошибочные пути или без указания прав
    if ($k[0] == null or $k[1] == null) {
        continue;
    }
    #------------------------#
    $is_cmod = (is_writable(H . $k[0]) ? 1 : 0);
    #------------------------#
    echo '<div class="adm_panel"> ' . $k[0] . PHP_EOL;
    $fc = substr(sprintf('%o', fileperms(H . $k[0])), -3);
    if ($fc == $k[1] && $is_cmod) {
        echo '   <span style="color:green;float:right;" class="adm_panel_span">OK</span>' . PHP_EOL;
    } else {
        echo '  <span style="color:red;float:right;" class="adm_panel_span"> ' . ($fc ==
            0 ? lang('Не существует') : $fc . '&gt;' . $k[1]) . '</span>' . PHP_EOL;
        $err = lang('Установите cmod');
    }
    echo '</div>' . PHP_EOL;
    $nochmod[] = $k[0];
    clearstatcache();
}
if (isset($err)) {
    echo '<div class="msg">' .
PHP_EOL . ' <a href="?type=cmod&amp;step=2&amp;yes_chmod">Получить права!</a><br />' .
PHP_EOL . ' Поддерживается не на всех хостингах' .
PHP_EOL . '</div>' . PHP_EOL;
}
if (isset($_GET['yes_chmod'])) {
    foreach ($nochmod as $val) {
        if (is_dir($_SERVER['DOCUMENT_ROOT'] . $val)) {
            chmod($_SERVER['DOCUMENT_ROOT'] . $val, 0755);
        }
        if (is_file($_SERVER['DOCUMENT_ROOT'] . $val)) {
            chmod($_SERVER['DOCUMENT_ROOT'] . $val, 0644);
        }
    }
    header('Location: ?type=cmod&step=2');
}
echo '</div>' . PHP_EOL;
