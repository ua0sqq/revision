<?php

define('APANEL', '/adm_panel');

$Search = glob(H . 'sys/core/*.php');
foreach ($Search as $function) {
    include_once $function;
}

 //подключаем языковой пакет
lang::start('default');

//	Крон :: Планировщик задач
$q_cron = $db->query('SELECT * FROM `cron` WHERE `time`<?i', [time()])->assoc();
if (count($q_cron) > 0) {
    foreach ($q_cron as $cron) {
        if (!isset($process)) {
            //если ноль
            if ($cron['time_update'] == 0) {
                $cron['time_update'] = 86400;
            }
            $cron['time_update'] += time();
            //Что б не повторило
            $process = true;
            //Для счетчика
            $countUpdate = true;
            //если есть файл
            if ($cron['file'] != null and is_file(H . 'sys/cron/' . $cron['file'])) {
                //загружаем его
                include_once(H . 'sys/cron/' . $cron['file']);
            }

            if ($countUpdate) {
                $count_update_c = ', `count`=`count`+1';
            } else {
                $count_update_c = '';
            }
            //обновляем данные
            $pattern = 'UPDATE `cron` SET  `time`=?i ?q WHERE `id`=?i';
            $data = [$cron['time_update'], $count_update_c, $cron['id']];
            $db->query($pattern, $data);
        }
    }
    unset($cron);
}

// запись о посещении
$agent= isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'Unknown';
$db->query('INSERT INTO `visit_today` (`ip`, `ua`, `time`)
VALUES (?i, ? ,?i)', [$iplong, $agent, $time]);

// запись о переходах на сайт
if (isset($_SERVER['HTTP_REFERER']) and !preg_match('#'.preg_quote($_SERVER['HTTP_HOST']).'#', $_SERVER['HTTP_REFERER']) and $ref = @parse_url($_SERVER['HTTP_REFERER'])) {
    if (isset($ref['host'])) {
        $_SESSION['http_referer'] = $ref['host'];
    }
}
