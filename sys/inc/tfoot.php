<?php
/*
echo $set['web']==false?"<div class ='foot'><a href='/?web'>Полная версия</a></div>
":"<div class ='foot'><a href='/?wap'>Мобильная версия</a></div>";
*/
if (is_file(H . 'style/themes/' . $set['set_them'] . '/foot.php')) {
    include_once H . 'style/themes/' . $set['set_them'] . '/foot.php';
} else {
    list($msec, $sec) = explode(chr(32), microtime());
    echo "<div class='foot'>";
    echo "<a href='/'>На главную</a><br />\n";

    $back = (time() - 600);
    $cnt = $db->query('SELECT * FROM (
                  SELECT COUNT( * ) alluser FROM `user`) q1, (
                  SELECT COUNT( * ) lastuser FROM `user` WHERE `date_last`>?i) q2, (
                  SELECT COUNT( * ) guest FROM `guests` WHERE `date_last`>?i AND `pereh`>?i) q3', [$back, $back, 0]
)->row();
    echo '<a href="/users.php">Регистраций: ' . $cnt['alluser'] . '</a><br />';
    echo '<a href="/online.php">Сейчас на сайте: ' . $cnt['lastuser'] . '</a><br />';
    echo '<a href="/online_g.php">Гостей на сайте: ' . $cnt['guest'] . '</a><br />';

    $page_size = ob_get_length();
    ob_end_flush();
    if (!isset($_SESSION['traf'])) {
        $_SESSION['traf'] = 0;
    }
    $_SESSION['traf'] += $page_size;

    echo '
	Вес страницы: '.round($page_size / 1024, 2).' Кб<br />
	Ваш трафик: '.round($_SESSION['traf'] / 1024, 2).' Кб <br />
	Генерация страницы: '.round(($sec + $msec) - $conf['headtime'], 3).'сек' ;
    echo '</div>';
    echo '<div class="rekl">';
    rekl(3);
    echo '</div>';
    echo '</div>
</body>
</html>';
}

exit;
