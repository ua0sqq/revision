<?php
/**
 *  Новый вариант v2
 *  Более интегрированное соединение с базой
 */

$file_db = $_SERVER['DOCUMENT_ROOT'] . '/sys/ini/db.ini';
if (is_file($file_db)) {
    $db_conf = parse_ini_file($file_db);

    require_once $_SERVER['DOCUMENT_ROOT'] . '/sys/library/goDB/autoload.php';
    $params = array(
                '_adapter' => 'mysql',
                'host'     => $db_conf['mysql_host'],
                'username' => $db_conf['mysql_user'],
                'password' => $db_conf['mysql_pass'],
                'dbname'   => $db_conf['mysql_db_name'],
                'charset'  => $db_conf['charset_names'],
                '_debug'   => false,
                '_prefix'  => '',
            );
    // Mysql connecting, for depricate php modules
	// Настоятельно рекомендуется переделать свои модули,
	// и не пользоваться этим соединением!
/*
	mysql_connect($db_conf['mysql_host'], $db_conf['mysql_user'], $db_conf['mysql_pass'])
	or die ('Невозможно подключиться к базе данных');
	mysql_select_db($db_conf['mysql_db_name'])
	or die ('Не найдена база : ' . $db_conf['mysql_db_name']);
	if ($db_conf['charset'] != null) {
        mysql_set_charset($db_conf['charset_names']);
    }
	if ($db_conf['charset_names'] != null) {
        mysql_query('SET NAMES ' . $db_conf['charset_names']);
    }
	if ($db_conf['charset_client'] != null) {
        mysql_query('SET character_set_client=' . $db_conf['charset_client']);
    }
	if ($db_conf['charset_connection'] != null) {
        mysql_query('SET character_set_connection=' . $db_conf['charset_connection']);
    }
	if ($db_conf['charset_result'] != null) {
        mysql_query('SET character_set_result=' . $db_conf['charset_result']);
    }
*/
    // e. g. for sqlite data base connect
/*$params = [
           '_adapter' => 'sqlite',
           'filename' => $_SERVER['DOCUMENT_ROOT'] . '/sys/dat/orm.db',
           'mysql_quot' => false,
           '_debug' => false,
           ];*/
    \go\DB\autoloadRegister();

    $db = go\DB\DB::create($params);

    go\DB\Storage::getInstance()->create($params);
} else {
    die('<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name=viewport content="width=device-width, initial-scale=1">
    <style type="text/css">
/*<![CDATA[*/body,.body{margin:auto;width:100%;font-family:tahoma,areal;font-size:16px;margin-top:15px}.menu_s{background:url(/style/themes/default/element_style/title.png) repeat scroll 0 0 #BDCCD5;padding:6px;border:1px solid #D5D6D6;color:#366EA1;line-height:1.8em;border:20px solid #B6C7DE}/*]]>*/
    </style>
    <title>Ошибка соединения!</title>
</head>
  <body>
    <div class="menu_s"><img src="/install/img/960.png" alt="" style="float: left;width: auto;opacity:0.2;"/>
        Отсутствует файл конфигурации с базой данных /sys/ini/db.ini<br />
        Загрузите файл и пропишите верные данные и обновите страницу еще раз<br />
        Если не помогло обратитесь на форум поддержки движка <br />
        <a href="//dcms-fiera.ru/forum/">http://dcms-fiera.ru/forum</a>
    </div>
  </body>
</html>');
}

// e. g. run debug $db->setDebug('mydebug');
function mydebug($query, $duration, $info)
{
    static $i;
    if ($duration > 0.000001) {
        $i++;
    }
    echo $i . '-'.$query.' (' . $duration . ')<br />';
}
