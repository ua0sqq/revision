<?php

rekl(2);
//подключаем языковой пакет
lang::start('main_menu');
$db = $Storage->fiera;
$q_menu = $db->query("SELECT * FROM `menu` ORDER BY `pos` ASC", 'assoc');
foreach ($q_menu as $post_menu) {
    if ($post_menu['type'] != 'inc') {
        if ($post_menu['type']=='link') {
            echo '<a class="menu_s" style="display: block" href="' . $post_menu['url'] . '">';
        } else {
            echo '<div class="menu_razd">' . PHP_EOL;
        }
        if ($post_menu['type']=='link') {
            echo icons($post_menu['icon'], 'code');
        }

        echo lang($post_menu['name']);

        if ($post_menu['counter'] != null and is_file(H.$post_menu['counter'])) {
            echo ' (';
            include_once H . $post_menu['counter'];
            echo ')';
        }

        if ($post_menu['type'] == 'link') {
            echo '</a>' . PHP_EOL;
        } else {
            echo '</div>' . PHP_EOL;
        }
    } else {
        if ($post_menu['type'] =='inc' and is_file(H.$post_menu['url'])) {
            include_once H . $post_menu['url'];
        }
    }
}
