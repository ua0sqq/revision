<?php

$copyright = parse_ini_file(H . 'sys/ini/vers.ini');
echo '<div class="p_m">';
echo 'Fiera: ' . $copyright['vers'] . '&nbsp;' . $copyright['status'];
echo '</div>';
echo '<div class="p_m">';
list($php_ver1, $php_ver2, $php_ver3) = explode('.', strtok(strtok(phpversion(), '-'), ' '), 3);
if ($php_ver1 . '.' . $php_ver2 >= 5.6) {
    echo '<span class="on">Версия PHP: ' . $php_ver1 . '.' . $php_ver2 . '.' .$php_ver3 . '(OK)</span><br />';
} else {
    echo '<span class="off">Версия PHP: ' . $php_ver1 . '.' .$php_ver2 . '.' .$php_ver3 . '</span><br />';
    $err[] = 'Тестирование на версии php ' . $php_ver1 . '.' .$php_ver2 . '.' .$php_ver3 . ' не осуществялось';
}
if (function_exists('set_time_limit')) {
    echo '<span class="on">set_time_limit: OK</span><br />';
} else {
    echo '<span class="on">set_time_limit: Запрещено</span><br />';
}
if (ini_get('session.use_trans_sid') == true) {
    echo '<span class="on">session.use_trans_sid: OK</span><br />';
} else {
    echo '<span class="off">session.use_trans_sid: Нет</span><br />';
    $err[] = 'Будет теряться сессия на браузерах без поддержки COOKIE';
    $err[] = 'Добавьте в корневой .htaccess строку <strong>php_value session.use_trans_sid 1</strong>';
}
if (ini_get('magic_quotes_gpc') == 0) {
    echo '<span class="on">magic_quotes_gpc: 0 (OK)</span><br />';
} else {
    echo '<span class="off">magic_quotes_gpc: Включено</span><br />';
    $err[] = 'Включено экранирование кавычек';
    $err[] = 'Добавьте в корневой .htaccess строку <b>php_value magic_quotes_gpc 0</b>';
}
if (ini_get('arg_separator.output') == '&amp;') {
    echo '<span class="on">arg_separator.output: &amp;amp; (OK)</span><br />';
} else {
    echo '<span class="off">arg_separator.output: ' . output_text(ini_get('arg_separator.output')) .
        '</span><br />';
    $err[] = 'Возможно появление ошибки xml';
    $err[] = 'Добавьте в корневой .htaccess строку <b>php_value arg_separator.output &amp;amp;</b>';
}
if (function_exists('apache_get_modules')) {
    $apache_mod = apache_get_modules();
    if (array_search('mod_rewrite', $apache_mod)) {
        echo '<span class="on">mod_rewrite: OK</span><br />';
    } else {
        echo '<span class="off">mod_rewrite: Нет</span><br />';
        $err[] = 'Необходима поддержка mod_rewrite';
    }
} else {
    echo '<span class="off">mod_rewrite: Нет данных</span><br />';
}
if (function_exists('imagecreatefromstring') && function_exists('gd_info')) {
    $gdinfo = gd_info();
    echo '<span class="on">GD: ' . $gdinfo['GD Version'] . ' OK</span><br />';
} else {
    echo '<span class="off">GD: Нет</span><br />';
    $err[] = 'GD необходима для корректной работы движка';
}
if (function_exists('mysqli_info')) {
    echo '<span class="on">MySQL: OK</span><br />';
} else {
    echo '<span class="off">MySQL: Нет</span><br />';
    $err[] = 'Без MySQL работа не возможна';
}
if (function_exists('iconv')) {
    echo '<span class="on">Iconv: OK</span><br />';
} else {
    echo '<span class="off">Iconv: Нет</span><br />';
    $err[] = 'Без Iconv работа не возможна';
}
if (ini_get('register_globals') == false) {
    echo '<span class="on">register_globals off: OK</span><br />';
} else {
    echo '<span class="off">register_globals on: !!!</span><br />';
    $err[] = 'register_globals включен. Грубое нарушение безопасности';
}
if (extension_loaded('openssl')) {
    echo '<span class="on">Шифрование COOKIE: OK(OpenSSL)</span><br />';
} else {
    echo '<span class="on">Шифрование COOKIE: нет</span><br />';
    echo '* mcrypt не доступен<br />';
}
echo '</div>';
if (!file_exists(H . 'install/index.php')) {
    if (user_access('adm_mysql')) {
        function formatfilesize($data)
        {
            if ($data < 1024) {
                return $data . ' bytes';
            } elseif ($data < 1024000) {
                return round(($data / 1024), 1) . ' KB';
            } else {
                return round(($data / 1024000), 1) . ' MB';
            }
        }
        $result = $db->query("SHOW TABLE STATUS");
        $dbsize = 0;
        while ($row = $result->row()) {
            $dbsize += $row["Data_length"] + $row["Index_length"];
        }
        echo '<div class="p_m">';
        echo 'Размер базы данных составляет <span class="on">' . formatfilesize($dbsize);
        echo '</div>';
    }
    if (function_exists('disk_free_space') && function_exists('disk_total_space')) {
        echo '<div class="p_m">';
        $free_space = disk_free_space(H);
        $total_space = disk_total_space(H);
        if ($free_space > 1024 * 1024 * 5) {
            echo '<span class="on">Место на сервере:</span> ' . size_file($free_space) .
                ' / ' . size_file($total_space) . '<br />';
        } else {
            echo '<span class="on">Место на сервере:</span> ' . size_file($free_space) .
                ' / ' . size_file($total_space) . '<br />';
            $err[] = 'Мало свободного места на диске';
        }
    }
    echo '</div>';
}
