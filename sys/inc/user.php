<?php
    /* *
     * Dcms-Fiera 3x
     */
$sessIdUser = isset($_SESSION['id_user']) ? $_SESSION['id_user'] : 0;

if ($sessIdUser) {
    if ($db->query('SELECT COUNT(*) FROM `user` WHERE `id`=?i', [$sessIdUser])->el() == 1) {
        $user = get_user($sessIdUser);
        include_once H . 'sys/inc/shif.php';


        if (empty($_COOKIE['pass']) or empty($_COOKIE['id_user'])) {
            setcookie('id_user', $user['id'], time()+60*60*24*365);
            setcookie('pass', cookie_encrypt($user['pass'], $user['id']), time()+60*60*24*365);
        }

        $timeactiv = time() - $user['date_last'];
        if ($timeactiv < 120) {
            $sqlup['timeactiv'] = ', `time`=`time`+' . $timeactiv;
            unset($nevtimeactiv, $timeactiv);
        } else {
            $sqlup['timeactiv'] = '';
        }

        if ($webbrowser) { # для web темы
          if (is_dir(H . 'style/themes/' . $user['set_them2'])) {
              $set['set_them'] = $user['set_them2'];
              $sqlup['setthem'] = '';
          } else {
              $sqlup['setthem'] = ', `set_them2`="' . $set['set_them'] . '"';
          }
        } else {
            if (is_dir(H . 'style/themes/' . $user['set_them'])) {
                $set['set_them'] = $user['set_them'];
                $sqlup['setthem'] = '';
            } else {
                $sqlup['setthem'] = ', `set_them`="' . $set['set_them'] . '"';
            }
        }
     // перенесено из thead.php
    if ($set['title'] != null) {
        $url_title = ', `url_title`="' . $set['title'] . '"';
    } else {
        $url_title = '';
    }
    //
     $where = $sqlup['timeactiv'] . $sqlup['setthem'] . $url_title;

        if (isset($ip2['add'])) {
            $sqlup['ip'] = ip2long($ip2['add']);
        } else {
            $sqlup['ip'] = 0;
        }
        if (isset($ip2['cl'])) {
            $sqlup['ip_cl'] = ip2long($ip2['cl']);
        } else {
            $sqlup['ip_cl'] = 0;
        }
        if (isset($ip2['xff'])) {
            $sqlup['ip_xff'] = ip2long($ip2['xff']);
        } else {
            $sqlup['ip_xff'] = 0;
        }
        if ($ua) {
            $sqlup['ua'] = $ua;
        } else {
            $sqlup['ua'] = 'unknown';
        }

        $sqlup['url'] = $_SERVER['REQUEST_URI'];
        $hash = md5(md5($ip.md5($ua).$user['id']));
        $pattern = 'UPDATE `user` SET `hash`=?, `date_last`=?i, `level`=?, `ip`=?i, `ip_cl`=?i, `ip_xff`=?i, `ua`=?, `url`=?, `sess`=? ?q  WHERE `id`=?i LIMIT ?i';
        $data = [$hash, $time, $user['level'], $sqlup['ip'], $sqlup['ip_cl'], $sqlup['ip_xff'], $sqlup['ua'], $sqlup['url'], $sess, $where, $user['id'], 1];
        $db->query($pattern, $data);
        $user['type_input'] = 'session';

        unset($sqlup);
    }
} elseif (!isset($input_page) && isset($_COOKIE['id_user'], $_COOKIE['pass']) && $_COOKIE['id_user'] && $_COOKIE['pass']) {
    exit(header('Location: /login.php?return=' . urlencode($_SERVER['REQUEST_URI']) . '&'));
}

// если аккаунт не активирован
if (isset($user['activation']) && $user['activation'] != null) {
    $err[] = 'Вам необходимо активировать Ваш аккаунт по ссылке, высланной на Email, указанный при регистрации';
    unset($user);
}

if (isset($user)) {
    //записываем посещание реферов
    if (isset($user['type_input'], $ref['host'], $_SERVER['HTTP_REFERER'])  &&
        !preg_match('#' . preg_quote($_SERVER['HTTP_HOST']) . '#', $_SERVER['HTTP_REFERER']) &&
        preg_match('#^https?://#i', $_SERVER['HTTP_REFERER']) && $ref = @parse_url($_SERVER['HTTP_REFERER'])) {
        if (!$db->query('SELECT COUNT(*) FROM `user_ref` WHERE `id_user`=?i AND `url`=?',
                        [$user['id'], $ref['host']])->el()) {
            $db->query('INSERT INTO `user_ref` (`time`, `id_user`, `type_input`, `url`)
VALUES (?i, ?i, ?, ?)', [time(), $user['id'], $user['type_input'], $ref['host']]);
        } else {
            $db->query('UPDATE `user_ref` SET `time`=?i WHERE `id_user`=?i AND `url`=?',
                     [time(), $user['id'], $ref['host']]);
        }
    }
    // указываем число пунктов на страницу
    if ($user['set_p_str'] != null) {
        $set['p_str'] = $user['set_p_str'];
    }
    // большие или маленткие иконки (вероятно будет удалено)
    $set['set_show_icon'] = $user['set_show_icon'];

    // бан пользователя
    if (!isset($banpage)) {
        if ($db->query('SELECT COUNT(*) FROM `ban` WHERE `id_user`=?i AND (`time`>?i OR `view`=?)',
                 [$user['id'], time(), '0'])->el()) {
            exit(header('Location: /ban.php?'));
        }
    }
} else {
    // если веб тема то включаем ту что в админки указана
    if ($webbrowser) {
        $set['set_them']=$set['set_them2'];
    }
    // записываем гостей
    $sc = isset($_SERVER['SCRIPT_NAME']) ? $_SERVER['SCRIPT_NAME'] : '/';
    if ($ip && $ua) {
        if ($db->query('SELECT COUNT(*) FROM `guests` WHERE `ip`=?i AND `ua`=? LIMIT ?i',
                        [$iplong, $ua, 1])->el() == 1) {
            $pattern = 'UPDATE `guests` SET `date_last`=?i, `url`=?, `pereh`=`pereh`+1 WHERE `ip`=?i AND `ua`=? LIMIT ?i';
            $data = [time(), $sc, $iplong, $ua, 1];
            $db->query($pattern, $data);
        } else {
            $db->query("INSERT INTO `guests` (`ip`, `ua`, `date_aut`, `date_last`, `url`)
VALUES (?i, ?, ?i, ?i, ?)", [$iplong, $ua, time(), time(), $sc]);
        }
    }

    unset($access); // че за херня :-)?
}

//	Показ ошибок
if (isset($user) &&  $user['group_access'] > 1 && $set['show_err_php'] == 1) {
    error_reporting(E_ALL);
    ini_set('display_errors', true);
}

//	Включаем режим если гость кидаем на авторизацию
if (!isset($user) && $set['guest_select'] == 1 && !isset($show_all)) {
    exit(header('Location: /aut.php'));
}

// Загрузка дополнительных плагинов
$Search = glob(H . 'sys/user_inc/*.php');
foreach ($Search as $load_plugins) {
    sort($Search);
    include_once $load_plugins;
}
