<?php

function css_add_ob($buffer)// функцию можно убрать.
{
    /* Удаляем комментарии */
    $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
    /* Удаляем табуляции, пробелы, переводы строки и так далее */
    $buffer = str_replace(array(
        "\r\n",
        "\r",
        "\n",
        "\t",
        '  ',
        '    ',
        '    '), '', $buffer);
    return $buffer;
}
ob_start();
/* Список CSS файлов */
foreach (glob(H . 'sys/css_add/*.css') as $load_plugins) {
    if ($load_plugins != null) {
        ?><link rel="stylesheet" href="<?php
        $load_plugins = str_replace(H, '', $load_plugins);
        echo '/' . $load_plugins; ?>" type="text/css" /><?php echo PHP_EOL;
    }
}
$css = ob_get_contents();
ob_end_clean();

?>