<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/user.php';

only_reg();

$_GET['type'] = isset($_GET['type']) ? $_GET['type'] : false;
switch ($_GET['type']) {
    case 'favorite':
        $type = 'favorite';
        $type_name = 'Избранные';
        break;
    case 'ignor':
        $type = 'ignor';
        $type_name = 'Игнорируемые';
        break;
    case 'deleted':
        $type = 'deleted';
        $type_name = 'Удаленные';
        break;
    default:
        $type = 'common';
        $type_name = 'Общие';
        break;
}
$set['title'] = $type_name . ' контакты';
include_once 'sys/inc/thead.php';
title();
if (isset($_GET['id'])) {
    $ank = $db->query('SELECT id FROM `user` WHERE `id`=?i LIMIT ?i', [$_GET['id'], 1])->row();
    if ($ank) {
        if (isset($_GET['act'])) {
            switch ($_GET['act']) {
                case 'add':
                    if ($db->query('SELECT COUNT(*) FROM `users_konts` WHERE `id_user`=?i AND `id_kont` =?i', [$user['id'], $ank['id']])->el() == 1) {
                        $err[] = 'Этот пользователь уже есть в вашем списке контактов';
                    } else {
                        $db->query('INSERT INTO `users_konts` (`id_user`, `id_kont`, `time`) VALUES ( ?i, ?i, ?i)', [$user['id'], $ank['id'], $time]);
                        msg('Контакт успешно добавлен');
                    }
                    break;
                case 'del':
                    if ($db->query('SELECT COUNT(*) FROM `users_konts` WHERE `id_user` =?i AND `id_kont` =?i', [$user['id'], $ank['id']])->el() == 0) {
                        $warn[] = 'Этого пользователя нет в вашем списке контактов';
                    } else {
                        $db->query('UPDATE `users_konts` SET `type`="deleted", `time`=?i WHERE `id_user`=?i AND `id_kont`=?i LIMIT ?i',
                                   [$time, $user['id'], $ank['id'], 1]);
                        msg('Контакт перенесен в группу удаленные');
                        $type = 'deleted';
                    }
                    break;
            }
        }
    } else {
        $err[] = 'Пользователь не найден';
    }
}
if (isset($_GET['act']) && $_GET['act'] === 'edit_ok' && isset($_GET['id']) &&
    $db->query('SELECT COUNT(*) FROM `user` WHERE `id` =?i LIMIT ?i', [$_GET['id'], 1])->el() == 1) {
    $ank = $db->query('SELECT id, nick FROM `user` WHERE `id`=?i LIMIT ?i', [$_GET['id'], 1])->row();
    if ($db->query('SELECT COUNT(*) FROM `users_konts` WHERE `id_user`= ?i AND `id_kont`= ?i', [$user['id'], $ank['id']])->el() == 1) {
        $kont = $db->query('SELECT * FROM `users_konts` WHERE `id_user`=?i AND `id_kont`=?i', [$user['id'], $ank['id']])->row();
        if (isset($_POST['name']) && $_POST['name'] != ($kont['name'] != null ? $kont['name'] :
            $ank['nick'])) {
            if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)|(\-\:\,\.)]/", $_POST['name'])) { // здесь не все в порядке было
                $err[] = 'В названии контакта присутствуют запрещенные символы';
            }
            if (mb_strlen($_POST['name']) > 64) {
                $err[] = 'Название контакта длиннее 64-х символов';
            }
            if (!isset($err)) {
                $db->query('UPDATE `users_konts` SET `name`=? WHERE `id_user`=?i AND `id_kont`=?i LIMIT ?i',
                           [$_POST['name'], $user['id'], $ank['id'], 1]);
                msg('Контакт успешно переименован');
            }
        }
        if (isset($_POST['type']) && preg_match('#^(common|ignor|favorite|deleted)$#', $_POST['type']) &&
            $_POST['type'] != $type) {
            $db->query('UPDATE `users_konts` SET `type`=?string, `time`=?i WHERE `id_user`=?i AND `id_kont`=?i LIMIT ?i',
                       [$_POST['type'], $time, $user['id'], $ank['id'], 1]);
            msg('Контакт успешно перенесен');
        }
    } else {
        $err[] = 'Контакт не найден';
    }
}
aut();
err(); // ??? !!!!!!!!!!!!!!!!!!!!!!!
$cnt = $db->query("SELECT * FROM (
SELECT COUNT( * ) AS allKont
FROM `users_konts`
WHERE `id_user` = '" . $user['id'] . "'
AND `id_kont` = '0') AS q1, (
SELECT COUNT( * ) AS allPost
FROM `users_konts`
WHERE `id_user` = '" . $user['id'] . "'
AND `type` = 'common') AS q2, (
SELECT COUNT( * ) AS common
FROM `users_konts`
WHERE `id_user` = '" . $user['id'] . "'
AND `type` = 'common') AS q3, (
SELECT COUNT( * ) AS favorite
FROM `users_konts`
WHERE `id_user` = '" . $user['id'] . "'
AND `type` = 'favorite') AS q4, (
SELECT COUNT( * ) AS ignor
FROM `users_konts`
WHERE `id_user` = '" . $user['id'] . "'
AND `type` = 'ignor') AS q5, (
SELECT COUNT( * ) AS deleted
FROM `users_konts`
WHERE `id_user` = '" . $user['id'] . "'
AND `type` = 'deleted') AS q6")->row();


if ($cnt['allKont'] == 0) {
    $db->query("INSERT INTO `users_konts` (`id_user` ,`id_kont` ,`time` ,`new_msg` ,`type` ,`name`)
	VALUES ('" . $user['id'] . "',  '0',  '" . time() . "',  '0',  'common',  '')");
}
if ($type == 'deleted') {
    msg('Контакты из этой группы удаляются через 30 дней');
}
if ($type == 'ignor') {
    msg('Уведомления о сообщениях от этих контактов не появляются');
}
if ($type == 'favorite') {
    msg('Уведомления о сообщениях от этих контактов выделяются');
}

$k_post = $cnt['allPost'];
echo '
<!-- pins -->
<div class="p_m">Диалогов с контактами: (' . $k_post . ')</div>' . PHP_EOL;
if ($k_post) {
    $k_page = k_page($k_post, $set['p_str']);
    $page = page($k_page);
    $start = $set['p_str'] * $page - $set['p_str'];

    $data = [$user['id'], $type, $start, $set['p_str']];
    $q = $db->query('SELECT t1 . * , t2.id AS id_u, t2.nick AS nick_u
FROM `users_konts` t1
LEFT JOIN `user` t2 ON t1.id_kont = t2.id
WHERE t1.`id_user`=?i
AND t1.`type`=?string
ORDER BY t1.`time` DESC , t1.`new_msg` DESC LIMIT ?i, ?i', $data);

    while ($post = $q->row()) {
        if ($post['id_kont'] == 0 && $post['id_u'] == null) {
            $ank_kont = array(
     'id_u' => 0,
     'nick_u' => ' ' . $set['nick_system'].' ',
     'pol'  => 1,
     );
        } else {
            $ank_kont = $post;
        }
        $data = [$ank_kont['id_u'], $user['id'], $ank_kont['id_u'], $user['id'], 0];
        $count = $db->query('SELECT * FROM (
SELECT COUNT( * ) mess
FROM `mail` WHERE `id_user`=?i
AND `id_kont`=?i) AS q1, (
SELECT COUNT( * ) new_mess
FROM `mail` WHERE `id_user`=?i
AND `id_kont`=?i AND `read`=?string) AS q2', $data)->row();
        $k_mess = $count['mess'];
        $k_new_mess = $count['new_mess'];
        echo '<table class="p_m"  style="/*width:avto;*/"><tr><td class="avar" style="position:relative;"><a href="/mail.php?id=' . $ank_kont['id_u'] . '">';

        echo '<span class="ank_span_m" style="float:left;margin-top: -14px;position:absolute;top:8px;left:-4px;">
        <strong>' . ($k_new_mess != 0 ? '<span class="on">+' . $k_new_mess . '</span>' : $k_mess) . '</strong></span>';
        avatar($ank_kont['id_u'], 80, 80, 0, null);

        echo '</a></td>';
        echo '<td style="" class="">';
        echo($k_new_mess != 0 ? '<span class="on">[new]</span><br /> ' : null) . ($post['name'] != null ? $post['name'] : $ank_kont['nick_u']/*nick($ank_kont['id_u'], null, 0)*/) . '<br/>';
        if ($type != 'deleted' and $ank_kont['id_u'] != 0) {
            echo '<a href="/konts.php?type='.$type.'&amp;act=del&amp;id=' . $ank_kont['id_u'] . '">Удалить контакт </a><br />';
        }
        if (isset($_GET['act']) && $_GET['act'] === 'edit' && isset($_GET['id']) && $_GET['id'] ==
            $ank_kont['id_u'] and $ank_kont['id_u'] != 0) {
            echo '<form method="post" action="?type=' . $type . '&amp;act=edit_ok&amp;id=' . $ank_kont['id_u'] . '&amp;page=' . $page . '">' . PHP_EOL;
            echo 'Отображать как:<br />' . PHP_EOL;
            echo '<input type="text" maxlenght="64" name="name" value="' . ($post['name'] != null ? $post['name'] : $ank_kont['nick_u']) . '" /><br />' . PHP_EOL;
            echo 'Группа:<br />' . PHP_EOL;
            echo '<select name="type">' . PHP_EOL;
            echo '<option value="common"' . ($type == 'common' ? ' selected="selected"' : null) . '>Общая</option>' . PHP_EOL;
            echo '<option value="favorite"' . ($type == 'favorite' ? ' selected="selected"' : null) . '>Избранные</option>' . PHP_EOL;
            echo '<option value="ignor"' . ($type == 'ignor' ? ' selected="selected"' : null) . '>Игнор</option>' . PHP_EOL;
            echo '<option value="deleted"' . ($type == 'deleted' ? ' selected="selected"' : null) . '>Удаленные</option>' . PHP_EOL;
            echo '</select><br />' . PHP_EOL;
            echo '<input type="submit" name="apply" value="Применить" /><br />' . PHP_EOL;
            echo '</form>' . PHP_EOL;
            echo '<a href="?type=' . $type . '&amp;id=' . $ank_kont['id_u'] . '&amp;page=' . $page . '">Отмена</a><br />';
        } else {
            if ($ank_kont['id_u'] != 0) {
                echo '<a href="?type=' . $type . '&amp;act=edit&amp;id=' . $ank_kont['id_u'] . '&amp;page=' . $page . '">Редактировать</a><br />';
            } else {
                echo 'Для оповещений и рассылок от системы сайта';
            }
        }
        echo '</td>';
        echo '</tr></table>';
    }
    if ($k_page > 1) {
        str('?type=' . $type . '&amp;', $k_page, $page); // Вывод страниц
    }
}
echo '<div class="p_m">' . PHP_EOL;
echo($type == 'common' ? '<strong>' : null) . '<img src="/style/icons_admpanel/1.png" alt="*"/>
    <a href="?type=common">Общие</a>' . ($type == 'common' ? '</strong>' : null) . ' [' . $cnt['common'] . ']<br />' . PHP_EOL;
echo($type == 'favorite' ? '<strong>' : null) . '<img src="/style/icons_admpanel/1.png" alt="*"/>
    <a href="?type=favorite">Избранные</a>' . ($type == 'favorite' ? '</strong>' : null) . ' [' . $cnt['favorite'] . ']<br />' . PHP_EOL;
echo($type == 'ignor' ? '<strong>' : null) . '<img src="/style/icons_admpanel/1.png" alt="*"/>
    <a href="?type=ignor">Игнорируемые</a>' . ($type == 'ignor' ? '</strong>' : null) . ' [' . $cnt['ignor'] . ']<br />' . PHP_EOL;
echo($type == 'deleted' ? '<strong>' : null) . '<img src="/style/icons_admpanel/1.png" alt="*"/>
<a href="?type=deleted">Удаленные</a>' . ($type == 'deleted' ? '</strong>' : null) . ' [' . $cnt['deleted'] . ']<br />' . PHP_EOL;
echo '</div>' . PHP_EOL;
echo '<div class="foot">' . PHP_EOL;
echo '&laquo;<a href="/user">В кабинет</a><br />' . PHP_EOL;
echo '</div>' . PHP_EOL;
include_once 'sys/inc/tfoot.php';
