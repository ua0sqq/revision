<?php

include_once 'sys/inc/start.php';
include_once 'sys/inc/sess.php';
include_once 'sys/inc/home.php';
include_once 'sys/inc/settings.php';
include_once 'sys/inc/db_connect.php';
include_once 'sys/inc/ipua.php';
include_once 'sys/inc/fnc.php';
$show_all = true; // показ для всех
include_once 'sys/inc/user.php';
include_once 'sys/inc/captcha.php';
//подключаем языковой пакет
lang::start('default');
only_unreg();
$set['title'] = lang('Авторизация');
include_once 'sys/inc/thead.php';
title();

$_SESSION['aut_captcha'] = 4;
if (!isset($user) && $_SERVER['REMOTE_ADDR'] !== '127.0.0.1') {
    echo '<div class="api_Login" style="text-align:center;">' . lang('Вы можете авторизоваться через') .
        '<br />' . api_Login::out($_SERVER['HTTP_HOST'] . '/pages/plugins/ulogin.php') .
        '</div>';
}
//Сессия для гостей колл. авторизаций если их нету создаем 0
if (!isset($_SESSION['aut_captcha'])) {
    $_SESSION['aut_captcha'] = 0;
}
if (isset($_GET['pass']) and $_GET['pass'] = 'ok') {
    msg(lang('Пароль отправлен вам на E-mail'));
}
if ($set['guest_select'] == 1) {
    msg(lang('Доступ к сайту разрешен только авторизованым пользователям'));
}
// если блее 6 раз скрываем авторизацию
if ($_SESSION['aut_captcha'] > 6) {
    aut();
    // заносим время в сессию
    $_SESSION['reg_captcha'] = $time + $set['aut_captcha'];
    msg(lang('Доступ к авторизации для вас временно закрыт, cлишком много авторизаций  , подождите пол часа'));
} else { // если менее 6
    // если 3 раза выводим сообщение
    if ($_SESSION['aut_captcha'] == 3) {
        msg(lang('Сейчас появится код с картинки , у вас еще 3 попытки до получасовой блокировки'));
    }
    echo '<div class="p_m" style="text-align:center;">
<form method="post" action="/login.php">' . lang('Логин') .
        '<br /><input type="text" name="nick" maxlength="32" class="inp"/><br />' . lang('Пароль') .
        '<br /><input type="password" name="pass" maxlength="32" class="inp"/><br />
<br />
';
    // если более 3 раз запускаем капчу
    if ($_SESSION['aut_captcha'] > 3) {
        echo '<input type="submit" value="' . lang('Авторизация') . '" />
<label><input type="checkbox" name="aut_save" value="1" checked="checked" /> ' .
            lang('Запомнить меня') . '</label>
</form></div>
<div class="foot" style="text-align:center;">
<a href="/reg.php" style="display:block;">' . lang('Регистрация') . '</a>
</div>
<div class="foot" style="text-align:center;">
<a href="/pass.php" style="display: block;">' . lang('Забыли пароль?') . '
</a>
</div>';
    }
}
include_once 'sys/inc/tfoot.php';
