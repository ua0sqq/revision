<?php
include_once 'sys/inc/start.php';
include_once 'sys/inc/sess.php';
include_once 'sys/inc/settings.php';
include_once 'sys/inc/db_connect.php';
include_once 'sys/inc/ipua.php';
include_once 'sys/inc/fnc.php';
include_once 'sys/inc/user.php';
$set['title'] = 'Выходит';

include_once 'sys/inc/thead.php';
title();
aut();
only_reg();
 
msg(lang("Вы уверены что хотите выйти?") ." 
<form method='post' action=''>
	<input type='submit' name='exit' value='".lang('Да')."' />
<b>	:: <a href='".getenv('HTTP_REFERER')."'>".lang('Нет')."</a></b>
</form>
");
#	getenv() если нет возврощаем обратно на страницу
  
#	если выбрано чистим куки и выкидываем на главную
if (isset($_POST['exit'])) {
    cache_delete::user($user['id']);
    setcookie('id_user', '', time() - 3600);
    setcookie('pass', '', time() - 3600);
    setcookie('SESS', '', time() - 3600);
    session_destroy();
    exit(header('Location: /'));
}
include_once 'sys/inc/tfoot.php';
