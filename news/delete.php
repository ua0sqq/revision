<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/user.php';

if (isset($_GET['id']) && count::query('news_komm', ' `id`="' . intval($_GET['id']) . '"') == 1) {
    $post = $db->query('SELECT komm.*, u.group_access
FROM `news_komm` komm
JOIN `user` u ON komm.id_user=u.id
WHERE komm.`id`=?i LIMIT ?i', [$_GET['id'], 1])->row();

    if (isset($user) && ($user['group_access'] > $post['group_access'] ||
                         $user['group_access'] == $post['group_access'] && $user['id'] == $post['id_user'])) {
        $db->query('DELETE FROM `news_komm` WHERE `id`=?i LIMIT ?i', [$post['id'], 1]);
        $_SESSION['message'] = 'Post deletted!';
    }
}

if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != null) {
    header("Location: ".$_SERVER['HTTP_REFERER']);
} else {
    header("Location: komm.php?".SID);
}
