<?php

$count = $db->query('SELECT * FROM (
SELECT COUNT( * ) AS news_posts FROM `news`) AS q1, (
SELECT COUNT( * ) AS news_posts_new FROM `news` WHERE `time` >?i) AS q2
', [(time()-60*60*24)])->row();

$c_g = $count['news_posts_new'];
if ($c_g==0) {
    $c_g=null;
} else {
    $c_g='/+'.$c_g;
}

echo $count['news_posts'] . $c_g;
