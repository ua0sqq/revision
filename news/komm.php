<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/user.php';

//подключаем языковой пакет
lang::start('news');


$set['title'] = lang('Новости - комментарии');
include_once H . 'sys/inc/thead.php';
title();


if (!isset($_GET['id']) && !is_numeric($_GET['id'])) {
    header('Location: index.php?' . SID);
    exit;
}
if (!$db->query('select count(*) from `news` where `id`=?i', [$_GET['id']])->el()) {
    header('Location: index.php?' . SID);
    exit;
}


if (isset($_POST['msg']) && isset($user)) {
    $msg = $_POST['msg'];
    if (isset($_POST['translit']) && $_POST['translit'] == 1) {
        $msg = translit($msg);
    }

    $mat = antimat($msg);
    if ($mat) {
        $err[]= lang('В тексте сообщения обнаружен мат:') . $mat;
    }

    if (strlen2($msg)>1024) {
        $err = lang('Сообщение слишком длинное');
    } elseif (strlen2($msg)<2) {
        $err = lang('Короткое сообщение');
    } elseif ($db->query('select count(*) from news_komm where `id_news`=?i AND `id_user`=?i  AND `msg`=?',
                         [$_GET['id'], $user['id'], $msg])->el()) {
        $err = lang('Ваше сообщение повторяет предыдущее');
    } elseif (!isset($err)) {
        $db->query('INSERT INTO `news_komm` (`id_user`, `time`, `msg`, `id_news`) VALUES( ?i, ?i, ?, ?i)',
                   [$user['id'], $time, $msg, $_GET['id']]);
        $db->query('UPDATE `user` SET `balls`=`balls`+?i WHERE `id`=?i LIMIT ?i', [1, $user['id'], 1]);
        msg(lang('Ваш комментарий успешно оставлен'));
        if (is_file(H . 'sys/cache/other/news_index.html')) {
            unlink(H . 'sys/cache/other/news_index.html');
        }
    }
}

err() . aut(); // форма авторизации

if (isset($user)) {
    panel_form :: head();
    echo "  <div class='p_m'>";
    echo "<form method=\"post\" name='message' action=''>\n";
    echo lang('Сообщение').":<br />\n<textarea name='msg'></textarea><br />\n";
    if ($user['set_translit']==1) {
        echo "<label><input type=\"checkbox\" name=\"translit\" value=\"1\" />".lang('Транслит')."</label><br />\n";
    }
    echo "<input value='".lang('Отправить')."' type=\"submit\" />\n";
    echo "</form></div>";
    panel_form :: foot();
}


$k_post  = $db->query('select count(*) from `news_komm` where `id_news`=?i', [$_GET['id']])->el();
$k_page = k_page($k_post, $set['p_str']);
$page    = page($k_page);
$start    = $set['p_str']*$page-$set['p_str'];

echo "<table class='post'>\n";
if ($k_post==0) {
    echo "   <tr>\n";
    echo "  <td class='p_t'>\n";
    echo lang('Нет комментариев');
    echo "  </td>\n";
    echo "   </tr>\n";
} else {
    $q = $db->query('SELECT n.*, u.nick, u.id AS u_id, u.group_access
FROM `news_komm` n
JOIN `user` u ON n.id_user=u.id
WHERE n.`id_news`=?i ORDER BY n.`id` DESC LIMIT ?i, ?i',
                    [$_GET['id'], $start, $set['p_str']]);
    while ($post = $q->row()) {
        echo "   <tr>\n";
        echo "  <td class='p_t'>\n";
        echo nick($post['u_id'])." (".vremja($post['time']).")";
        echo "  </td>\n";
        echo "   </tr>\n";
        echo "   <tr>\n";

        if ($set['set_show_icon']==1) {
            echo "  <td class='p_m' colspan='2'>\n";
        } else {
            echo "  <td class='p_m'>\n";
        }

        echo output_text($post['msg'])."<br />\n";
// TODO: ???
        if (isset($user) && ($user['group_access'] > $post['group_access'] ||
                            $user['group_access'] == $post['group_access'] && $user['id'] == $post['id_user'])) {
            echo "<a href='delete.php?id=$post[id]' class='trash' ></a>";
        }

        echo "  </td>\n";
        echo "   </tr>\n";
    }

}
    echo "</table>\n";

    if ($k_page>1) {
        str("?id=".intval($_GET['id']).'&amp;', $k_page, $page); // Вывод страниц
    }


echo "  <div class='p_m'><a href='/news'>".lang('К новостям')."</a></div>";

include_once H . 'sys/inc/tfoot.php';
