<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/user.php';

//подключаем языковой пакет
lang::start('news');
  
$set['title'] = lang('Новости');
include_once H . 'sys/inc/thead.php';
title() . aut();

if (user_access('adm_news')) {
    echo "<div class='p_m'><a href='".APANEL."/news.php'><img src='/style/icons/3.png' alt='' />".lang('Создать новость')."</a></div>";
}

$k_post  = count::query('news');
$k_page = k_page($k_post, $set['p_str']);
$page    = page($k_page);
$start    = $set['p_str']*$page-$set['p_str'];
$q=$db->query('SELECT * FROM `news` ORDER BY `id` DESC LIMIT ?i, ?i', [$start, $set['p_str']]);
echo "<table class='post'>\n";
if ($k_post==0) {
    echo "   <tr>";
    echo "  <td class='p_t'>";
    echo lang('Нет новостей');
    echo "  </td>";
    echo "   </tr>";
}
while ($post = $q->row()) {
    if ($post['avtor'] > 0) {
        $avtor = "<span style='float : right;' >".nick($post['avtor'], null, 0)."</span>";
    } else {
        $avtor = null;
    }

    echo "   <tr>\n";
    echo "  <td class='p_t'>\n";
    echo "$post[title]\n";
    echo "(".vremja($post['time']).")  $avtor ";
    echo "  </td>\n";
    echo "   </tr>\n";
    echo "   <tr>\n";
    echo "  <td class='p_m'>";
    echo output_text($post['msg'])."<br />";
    echo "<a href='komm.php?id=$post[id]'>".lang('Комментарии')."</a> (" .
count::query('news_komm', ' `id_news`="' . $post['id'] . '"') . ")<br />\n";
    echo "  </td>\n";
    echo "   </tr>\n";
}
echo "</table>\n";

if ($k_page>1) {
    str('?', $k_page, $page); // Вывод страниц
}
include_once H . 'sys/inc/tfoot.php';
