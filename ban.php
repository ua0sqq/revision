<?php

include_once 'sys/inc/start.php';
include_once 'sys/inc/sess.php';
include_once 'sys/inc/home.php';
include_once 'sys/inc/settings.php';
include_once 'sys/inc/db_connect.php';
include_once 'sys/inc/ipua.php';
include_once 'sys/inc/fnc.php';

$banpage = true;

include_once 'sys/inc/user.php';

only_reg();

$set['title'] = lang('Бан');
include_once 'sys/inc/thead.php';
title() . err() . aut();


if (!isset($user)) {
    header("Location: /index.php?".SID);
    exit;
}
if ($db->query('SELECT COUNT(*) FROM `ban` WHERE `id_user`=?i AND (`time`>?i OR `view`=?string)',
               [$user['id'], $time, 0])->el() == 0) {
    exit(header('Location: /'));
}

$db->query('UPDATE `ban` SET `view`=?string WHERE `id_user`=?i', [1, $user['id']]); // увидел причину бана

$k_post  = $db->query('SELECT COUNT(*) FROM `ban` WHERE `id_user`=?i', [$user['id']])->el();
$k_page = k_page($k_post, $set['p_str']);
$page    = page($k_page);
$start    = $set['p_str']*$page-$set['p_str'];

echo "<table class='post'>\n";
$q= $db->query('SELECT * FROM `ban` WHERE `id_user`=?i ORDER BY `time` DESC LIMIT ?i, ?i', [$user['id'], $start, $set['p_str']]);
while ($post = $q->row()) {
    //$ank=get_user($post['id_ban']);
echo "   <tr>";
    echo "  <td class='avar' rowspan='2'>";
    avatar($post['id_ban'], 100, 100, 0, null);
    echo "  </td>\n";
    echo "  <td class='p_t'>\n";
    echo nick($post['id_ban']).": ".lang('до')." ".vremja($post['time']);
    echo "  </td>\n";
    echo "   </tr>\n";
    echo "   <tr>\n";
    echo "  <td class='p_m'>";
    echo output_text($post['prich'])."<br />";
    echo "  </td>";
    echo "   </tr>";
}

echo "</table>";
if ($k_page>1) {
    str('?', $k_page, $page); // Вывод страниц
}

echo "<div class='p_m'>Чтобы больше не возникало подобных ситуаций, рекомендуем Вам изучить <a href='/user/rules'>правила</a> нашего сайта</div>";

include_once 'sys/inc/tfoot.php';
