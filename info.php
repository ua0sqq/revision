<?php

include_once 'sys/inc/start.php';
include_once 'sys/inc/sess.php';
include_once 'sys/inc/home.php';
include_once 'sys/inc/settings.php';
include_once 'sys/inc/db_connect.php';
include_once 'sys/inc/ipua.php';
include_once 'sys/inc/fnc.php';
include_once 'sys/inc/user.php';

if (isset($user)) {
    $ank['id'] = $user['id'];
}

if (isset($_GET['user'])) {
    if (isset($user) and $_GET['user'] == null) {
        $mylink = $user['mylink'];
    } else {
        $mylink = $_GET['user'];
    }

    if ($db->query('SELECT COUNT(*) FROM `user` WHERE `mylink`=?', [$mylink])) {
        $link = $db->query('SELECT `id`,`mylink` FROM `user` WHERE `mylink`=? LIMIT ?i', [$mylink, 1])->row();
        $ank['id'] = $link['id'];
    } else {
        //$ank['id'] = $user['id'];
        exit(header('Location: /err.php?err=404'));
    }
}


if (isset($_GET['id'])) {
    $ank['id'] = intval($_GET['id']);
}

$ank = get_user($ank['id']);

//для сео META данные
#-----------------------------------------------------------------------------#
$meta_keywords = mb_substr($ank['status_ank'], 0, 160);
#--->
$set['meta_keywords'] = meta_out_keywords($meta_keywords);
#---------------#
$ank['_ank_city']   = $ank['ank_city'] != null ? ','.$ank['ank_city'] : null;
$ank['_mylink']     = $ank['mylink'] != null ? ','.$ank['mylink'] : null;
$ank['_ank_family'] = $ank['ank_family'] != null ? ','.$ank['ank_family'] : null;
$ank['_ank_name']   = $ank['ank_name'] != null ? ','.$ank['ank_name'] : null;
$ank['_ank_countr'] = $ank['ank_countr'] != null ? ','.$ank['ank_countr'] : null;
$ank['_ank_icq']    = $ank['ank_icq'] != null ? ','.$ank['ank_icq'] : null;
$ank['_ank_mail']   = $ank['ank_mail'] != null  && $ank['set_show_mail'] != 0 ? ','.$ank['ank_mail'] : null;
$ank['_ank_skype']  = $ank['ank_skype'] != null ? ','.$ank['ank_skype'] : null;
#--->
$set['meta_description'] = meta_out_description($ank['nick'].''.$ank['_mylink'].''.$ank['_ank_city'].''.$ank['_ank_family'].''.$ank['_ank_name'].''.$ank['_ank_countr'].''.$ank['_ank_icq'].''.$ank['_ank_mail'].''.$ank['_ank_skype']);
#-----------------------------------------------------------------------------#

//подключаем языковой пакет
lang::start('profile');

#--------------system----------------------------#
if ($ank['id'] == 0 or $ank['id'] < 0) {
    if ($set['system_ank'] == 1) {
        exit(header('Location: /'));
    }
    $set['title'] = nick($ank['id'], null, 0, 1) .' - '. lang('Профиль');
    include_once 'sys/inc/thead.php';
    title();
    aut();
    echo "<span class='status'> ".lang($ank['group_name'])."</span><br />";
    if ($ank['ank_o_sebe'] != null) {
        echo "<span class='ank_n'> ".lang('О себе').":</span> <span class='ank_d'>$ank[ank_o_sebe]</span>";
    }
    include_once 'sys/inc/tfoot.php';
    exit;
}
#--------------system----------------------------#

if (!$ank) {
    exit(header('Location: /'));
}
$set['title'] = $ank['nick'] . ' - ' . lang('Профиль');
include_once 'sys/inc/thead.php';
title();
aut();

$menu = $db->query('SELECT * FROM `info_menu` ORDER BY `pos` ASC')->assoc();
if (!count($menu)) {
    msg(lang('Разделы еще не созданы'));
} else {
    foreach ($menu as $post) {
        //если раздел
if ($post['type'] == 'razd') {
    echo "<div class='info_razd'> <img src='/style/icons/info_razd.png'> ".output_text($post['name']);
}

//файл
if ($post['file'] != null and is_file(H . 'sys/info/'. $post['file']) and $post['type'] == 'inc') {
    include_once(H . 'sys/info/'. $post['file']);
}
//закрывающие дивы
if ($post['type'] == 'razd') {
    echo "</div>";
}
    }
}
include_once H . 'sys/inc/tfoot.php';
