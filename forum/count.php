<?php

$count = go\DB\query("SELECT * FROM (
SELECT COUNT( * ) AS forum_posts FROM `forum_posts`) AS q1, (
SELECT COUNT( * ) AS forum_posts_new FROM `forum_posts` WHERE `time`>?i) AS q2, (
SELECT COUNT( * ) AS forum_themes FROM `forum_themes`) AS q3, (
SELECT COUNT( * ) AS forum_themes_new FROM `forum_themes` WHERE `time`>?i) AS q4
", [(time()-60*60*24), (time()-60*60*24)])->row();

echo $count['forum_posts'].'/+'.$count['forum_posts_new'].' | '.$count['forum_themes'].'/+'.$count['forum_themes_new'];
