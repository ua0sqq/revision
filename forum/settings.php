<?php

/**
 * @const FORUM Название папки модуля.
 */
define('FORUM', '/forum');

/**
 * @const ICONS Ширина и высота иконок.
 */
define('ICONS', 'style = "width: 22px; height: 22px"');

/**
 * @const RATING Рейтинг поста в теме, при котором появится над постом "Лучший ответ".
 */
define('RATING', $set['forum_r_post']);

/**
 * @const RATING2 Отрицательный рейтинг поста в теме, при котором будет отправляться жалоба админам.
 */
define('RATING2', '-'.$set['forum_r_post_j']);

/**
 * @const COLOR Цвет надписи "Лучший ответ". По умолчанию: жёлтый
 */
define('COLOR', $set['forum_color_best']);

// Установка кодировки для mb_strlen() и mb_substr().
mb_internal_encoding('UTF-8');
