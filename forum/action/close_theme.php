<?php

if (isset($_POST['close'])) {
    $reason = trim($_POST['reason']);
    $cause = ($reason == null) ? ' неизвестна.' : ': '.output_text($reason);
    $a = ($user['pol'] == 0) ? 'a' : null;
    $reason = 'Тему закрыл'.$a.' '.$user['nick'].'<br />Причина'.$cause;
    if (mb_strlen($reason) > 100) {
        ?>
        <div class = 'err'>Слишком длинная причина. Максимум 100 символов.</div>
        <?php

    } else {
        $msg_sys = '[url=/info.php?id='.$user['id'].']'.$user['nick'].'[/url] закрыл тему. Причина указана выше.';
        $db->query('INSERT INTO `forum_posts` (`id_theme`, `id_user`, `id_admin`, `text`, `time`, `cit`, `rating`) VALUES( ?i, ?i, ?i, ?, ?i, ?i, ?i)',
                   [$theme->id, 0, 0, $msg_sys, $time, 0, 0]);
        $db->query('UPDATE `forum_themes` SET `reason_close` = ? WHERE `id` = ?i', [$reason, $theme->id]);
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
        exit;
    }
} else {
    ?>
    <div class = 'menu_razd'>Закрытие темы "<?php echo  output_text($theme->name, 1, 1, 0, 0, 0) ?>"</div>
    <form action = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/close_theme.html' method = 'post' class="p_m">
        <b>Укажите причину:</b><br />
        <input type = 'text' name = 'reason' value = '' style = 'width: 92%' /><br />
        <input type = 'submit' name = 'close' value = 'Закрыть тему' />
    </form>
    <div class = 'p_m' style = 'text-align: right'><a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'>Отменить закрытие</a></div>
    <?php
    include_once '../sys/inc/tfoot.php';
    exit;
}

?>