<?php

if (!user_access('forum_post_ed')) {
    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
    exit;
} else {
    if (isset($_POST['clear'])) {
        $files = $db->query('SELECT `name` FROM `forum_post_files` WHERE `id_theme` = ?i', [$theme->id]);
        while ($file = $files->object()) {
            unlink(H.FORUM.'/files/'.$file->name);
        }
        $db->query('DELETE FROM `forum_post_rating` WHERE `id_theme` = '.$theme->id);
        $db->query('DELETE FROM `forum_posts` WHERE `id_theme` = '.$theme->id);
        $msg_sys = '[url=/info.php?id='.$user['id'].']'.$user['nick'].'[/url] почистил всю тему.';
        $db->query('INSERT INTO `forum_posts` SET `id_theme` = ?i, `id_user` = ?i, `id_admin` = ?i, `text` = ?, `time` = ?i', [$theme->id, 0, 0, $msg_sys, $time]);
        $_SESSION['success'] = '<div class = "msg">Тема успешно очищена.</div>';
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
        exit;
    } elseif (isset($_POST['cancel'])) {
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
        exit;
    } else {
        ?>
        <form action = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/cleare_theme.html' method = 'post' class="p_m" style = 'text-align: center'>
            <b>Вы уверены, что хотите очистить эту тему от сообщений?</b><br />
            <input type = 'submit' name = 'clear' value = 'Очистить' /> <input type = 'submit' name = 'cancel' value = 'Отменить' />
        </form>
        <?php

    }
}

?>