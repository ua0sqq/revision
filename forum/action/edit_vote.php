<?php

if (isset($_POST['save'])) {
    $name = trim($_POST['name']);
    if ($_POST['time_end'] == 1 || ($_POST['time_end'] != 1 && $_POST['time_end'] != 2 && $_POST['time_end'] != 3 && $_POST['time_end'] != 4 && $_POST['time_end'] != 5 && $_POST['time_end'] != 6)) {
        $time_end = 0;
    } elseif ($_POST['time_end'] == 1) {
        $time_end = 0;
    } elseif ($_POST['time_end'] == 2) {
        $time_end = ($vote->time_end < time()) ? $vote->time_end+time()+60*60*24 : time()+60*60*24;
    } elseif ($_POST['time_end'] == 3) {
        $time_end = ($vote->time_end < time()) ? $vote->time_end+time()+60*60*72 : time()+60*60*72;
    } elseif ($_POST['time_end'] == 4) {
        $time_end = ($vote->time_end < time()) ? $vote->time_end+time()+60*60*24*7 : time()+60*60*24*7;
    } elseif ($_POST['time_end'] == 5) {
        $time_end = ($vote->time_end < time()) ? $vote->time_end+time()+60*60*24*30 : time()+60*60*24*30;
    } elseif ($_POST['time_end'] == 6) {
        $time_end = ($vote->time_end < time()) ? $vote->time_end+time()+60*60*24*90 : time()+60*60*24*90;
    }
    if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)|(\:\,\.\-\?)]/", $_POST['name'], $m)) {
        ?>
<div class = 'err'>
    В поле &laquo;Название опроса&raquo; присутствуют запрещенные <span style="font-weight: bold; color: red;"><?php echo  $m[0]?></span> символы!.
</div>
            <?php

    } elseif (mb_strlen($name) < 5) {
        ?>
        <div class = 'err'>Слишком короткое содержание опроса.</div>
        <?php

    } else {
        unset($_SESSION['name']);
        unset($_SESSION['time_end']);
        $var = $_POST['var'];
        $count_var = count($var);
        $check = 0;
        for ($i = 0; $i < $count_var; $i++) {
            unset($_SESSION['var'][$i]);
            if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)|(\:\,\.\-\?)]/", $var[$i])) {
                echo '<div class = "err">Ошибка в описании вариантов!</div>';
                break;
            }
            if (!$id = $db->query('SELECT `id` FROM `forum_votes_var` WHERE `id_vote` = ?i AND `variant` = ?', [$vote->id, $var[$i]])->el()) {
                $db->query('INSERT INTO `forum_votes_var` (`variant`, `id_vote`, `id_theme`) VALUES( ?, ?i, ?i)', [$var[$i], $vote->id, $theme->id]);
            } else {
                $db->query('UPDATE `forum_votes_var` SET `variant`=? WHERE `id`=?i', [$var[$i], $id]);
            }
        }
        $db->query('DELETE FROM `forum_votes_var` WHERE `id_theme`=?i AND `variant`=?', [$theme->id, ""]);
        $_SESSION['success'] = '<div class = "msg">Опрос успешно изменён.</div>';
        $db->query('UPDATE `forum_votes` SET  `name` = ?, `time_end` = ?i WHERE `id_theme` = ?i', [$name, $time_end, $theme->id]);
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
        exit;
    }
} elseif (isset($_POST['add_var']) && $vars < 9) {
    $_SESSION['name'] = $_POST['name'];
    $_SESSION['time_end'] = $_POST['time_end'];
    $var = $_POST['var'];
    $count_var = count($var);
    for ($i = 0; $i < $count_var; $i++) {
        $_SESSION['var'][$i] = trim($var[$i]);
    }// `id_theme` = '.$theme->id.', `id_vote` = '.$vote->id.', `variant` = ""
    $db->query('INSERT INTO `forum_votes_var` (`id_theme`, `id_vote`, `variant`) VALUES( ?i, ?i, ?)', [$theme->id, $vote->id, ""]);
    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/edit_vote.html');
    exit;
} elseif (isset($_POST['delete_var']) && $vars > 2) {
    $_SESSION['name'] = $_POST['name'];
    $_SESSION['time_end'] = $_POST['time_end'];
    $var = $_POST['var'];
    $count_var = count($var)-1;
    unset($_SESSION['var'][$count_var]);
    $variant = $db->query('SELECT MAX(`id`) FROM `forum_votes_var` WHERE `id_theme` = '.$theme->id)->el();
    $db->query('DELETE FROM `forum_votes_var` WHERE `id_theme` = ?i AND `id` = ?i', [$theme->id, $variant]);
    $db->query('DELETE FROM `forum_vote_voices` WHERE `id_variant` = ?i', [$variant]);
    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/edit_vote.html');
    exit;
} elseif (isset($_POST['cancel']) && ($theme->type == 0 || ($theme->type == 1 && user_access('forum_post_close')))) {
    $_SESSION['success'] = '<div class = "msg">Редактирование успешно отменено.</div>';
    $var = $_POST['var'];
    $count_var = count($var);
    for ($i = 0; $i < $count_var; $i++) {
        unset($_SESSION['var'][$i]);
    }
    unset($_SESSION['name']);
    unset($_SESSION['time_end']);
    $db->query('DELETE FROM `forum_votes_var` WHERE `id_theme` = '.$theme->id.' AND `variant` = ""');
    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
    exit;
} elseif (isset($_POST['delete'])) {
    $_SESSION['success'] = '<div class = "msg">Голосование успешно удалено.</div>';
    $var = $_POST['var'];
    $count_var = count($var);
    for ($i = 0; $i < $count_var; $i++) {
        unset($_SESSION['var'][$i]);
    }
    unset($_SESSION['name']);
    unset($_SESSION['time_end']);
    $db->query('DELETE FROM `forum_vote_voices` WHERE `id_vote` = '.$vote->id);
    $db->query('DELETE FROM `forum_votes_var` WHERE `id_theme` = '.$theme->id);
    $db->query('DELETE FROM `forum_votes` WHERE `id_theme` = '.$theme->id);
    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
    exit;
}
?>
<div class = 'menu_razd'>Редактирование опроса в теме</div>
<form action = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/edit_vote.html' method = 'post' class="p_m">
    <b>Содержание опроса:</b><br />
    <textarea name = 'name' style = 'width: 96%'><?php echo  (isset($_SESSION['name'])) ? htmlspecialchars($_SESSION['name']) : $vote->name; ?></textarea><br /><br />
    <b>Варианты ответов:</b><br />
    <?php
    $i = 0;
    $all_vars = $db->query('SELECT `variant` FROM `forum_votes_var` WHERE `id_vote` = '.$vote->id.' ORDER BY `id` ASC');
    while ($var = $all_vars->object()) {
        ?>
        <input type = 'text' name = 'var[]' value = '<?php echo  (isset($_SESSION['var'][$i])) ? htmlspecialchars($_SESSION['var'][$i]) : $var->variant; ?>' style = 'width: 96%;'><br />
        <?php
        $i++;
    }
    ?>
    <input type = 'submit' name = 'add_var' value = 'Ещё вариант' <?php echo  ($vars > 7) ? 'disabled = "disabled"' : null ?> /> <input type = 'submit' name = 'delete_var' value = 'Убрать вариант' <?php echo  ($vars < 3) ? 'disabled = "disabled"' : null ?> /><br />
    <br />
    <b><?php echo  ($vote->time_end < time()) ? 'Продлить на:' : 'Дата окончания через:' ?></b><br />
    <select name = 'time_end'>
        <?php
        if ($vote->time_end < time()) {
            ?>
            <option value = "0">Без изменений</option>
            <?php

        }
        ?>
        <option value = "1" <?php echo  (isset($_SESSION['time_end']) && $_SESSION['time_end'] == 1) ? 'selected = "selected"' : null ?>>Бессрочно</option>
        <option value = "2" <?php echo  (isset($_SESSION['time_end']) && $_SESSION['time_end'] == 2) ? 'selected = "selected"' : null ?>>1 День</option>
        <option value = "3" <?php echo  (isset($_SESSION['time_end']) && $_SESSION['time_end'] == 3) ? 'selected = "selected"' : null ?>>3 Дня</option>
        <option value = "4" <?php echo  (isset($_SESSION['time_end']) && $_SESSION['time_end'] == 4) ? 'selected = "selected"' : null ?>>1 Неделю</option>
        <option value = "5" <?php echo  (isset($_SESSION['time_end']) && $_SESSION['time_end'] == 5) ? 'selected = "selected"' : null ?>>1 месяц</option>
        <option value = "6" <?php echo  (isset($_SESSION['time_end']) && $_SESSION['time_end'] == 6) ? 'selected = "selected"' : null ?>>3 месяца</option>
    </select><br />
    <input type = 'submit' name = 'save' value = 'Сохранить' /> <input type = 'submit' name = 'delete' value = 'Удалить' /> <input type = 'submit' name = 'cancel' value = 'Отменить' /><br />
</form>
<?php

include_once '../sys/inc/tfoot.php';
exit;

?>