<?php

if ($razdel->type == 0 || ($razdel->type == 1) && $user['group_access'] > 2) {
    $my_last_theme = $db->query('SELECT `time` FROM `forum_themes` WHERE `id_user` = '.$user['id'].' ORDER BY `id` DESC')->object();
    if ($my_last_theme && (time() - $my_last_theme->time < $set['new_t_time'] && $set['new_t_time'] != 0) && $user['group_access'] < 3) {
        if ($set['new_t_time'] == 60) {
            $theme_time = 'минуту';
        } elseif ($set['new_t_time'] == 120) {
            $theme_time = '2 минуты';
        } elseif ($set['new_t_time'] == 180) {
            $theme_time = '3 минуты';
        } elseif ($set['new_t_time'] == 300) {
            $theme_time = '5 минут';
        } elseif ($set['new_t_time'] == 600) {
            $theme_time = '10 минут';
        } elseif ($set['new_t_time'] == 1200) {
            $theme_time = '20 минут';
        }

        $part = ($set['new_t_time']-(time()-$my_last_theme->time));
        $min = ($part > 60) ? floor(($part / 60) % 60).' минут(ы) '.($part % 60).' секунд(у/ы)' : ' секунд(у/ы)'; ?>
        <div class="err">
            Тему можно создавать 1 раз в <?php echo  $theme_time ?>.<br />
            Вы сможете создать тему через <?php echo  $min ?>.
        </div>

        <?php
        include_once '../sys/inc/tfoot.php';
        exit;
    }

    if (isset($_POST['create'])) {
        $name = trim($_POST['name']);
        $description = trim($_POST['description']);
        $type = ($user['group_access'] > 7 && isset($_POST['type'])) ? 1 : 0;
        if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)|(\,\.\-)]/", $_POST['name'], $m)) {
            ?>
            <div class = 'err'>
                В поле &laquo;Название темы&raquo; присутствуют запрещенные <span style="font-weight: bold; color: red;"><?php echo $m[0]?></span> символы!.
            </div>
            <?php

        } elseif (mb_strlen($name) < $set['forum_new_them_name_min_pod'] || mb_strlen($name) > $set['forum_new_them_name_max_pod']) {
            ?>
            <div class = 'err'>В поле &laquo;Название темы&raquo; можно использовать от <?php echo  $set['forum_new_them_name_min_pod'] ?> до <?php echo  $set['forum_new_them_name_max_pod'] ?> символов.</div>
            <?php

        } elseif (mb_strlen($description) < 3) {
            ?>
            <div class = 'err'>Слишком короткое содержание темы.</div>
            <?php

        } else {
            $_SESSION['create_theme'] = '<div class = "msg">Тема успешно создана.</div>';
            $data = [$forum->id, $razdel->id, $name, $description, $type, $time, 0, $time, "", $user['id']];
            $insert = $db->query('INSERT INTO `forum_themes` (`id_forum`, `id_razdel`, `name`, `description`, `type`, `time`, `time_edit`, `time_post`, `reason_close`, `id_user`)
 VALUES( ?i, ?i, ?, ?, ?i, ?i, ?i, ?i, ?string, ?i)', $data)->id();
            if ($set['new_them_Location'] == 1) {
                header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$insert.'.html');
            } else {
                header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/');
            }
            exit;
        }
    } elseif (isset($_POST['cancel'])) {
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/');
        exit;
    } ?>
    <form action = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id ?>/create_theme.html' method = 'post' class="p_m">
        <b>Название темы (<?php echo  $set['forum_new_them_max_i'] ?> символ(а/ов)):</b><br />
        <input type = 'text' name = 'name' value = '' style = 'width: 96%' /><br /><br />
        <b>Содержание темы:</b><br />
        <textarea name = 'description' style = 'width: 96%'></textarea><br /><br />
        <?php
        if ($user['group_access'] > 7) {
            ?>
            <b>Вывод темы в разделе:</b><br />
            <label><input type = 'checkbox' name = 'type' value = '1' /> Тема всегда вверху</label><br /><br />
            <?php

        } ?>
        <input type = 'submit' name = 'create' value = 'Создать' /> <input type = 'submit' name = 'cancel' value = 'Отмена' />
    </form>
    <?php
    include_once '../sys/inc/tfoot.php';
} else {
    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/');
}
exit;

?>