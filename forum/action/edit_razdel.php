<?php

$razdel = $db->query('SELECT * FROM `forum_razdels` WHERE `id` =?i', [$_GET['razdel']])->object();

if (user_access('forum_razd_edit') && $razdel) {
    if (isset($_POST['edit'])) {
        $name = trim($_POST['name']);
        $description = trim($_POST['description']);
        $number = abs(intval($_POST['number']));
        $forum_id = abs(intval($_POST['id_forum']));
        $type = (isset($_POST['type'])) ? 1 : 0;
        $output = (isset($_POST['output'])) ? 1 : 0;
        $isset_name = $db->query('SELECT COUNT(*) FROM `forum_razdels` WHERE `name` = ? AND `id_forum` = ?i AND `id` != ?i', [$name, $forum->id, $razdel->id])->el();
        $razdel_n = ($razdel->number != $number) ? $db->query('SELECT `id`, `number` FROM `forum_razdels` WHERE `id_forum` = ?i AND `number` = ?i AND `id` <> ?i', [$forum->id, $number, $razdel->id])->object() : false;
        if ($razdel_n) {
            $db->query('UPDATE `forum_razdels` SET `number` = ?i WHERE `id` = ?i', [$number, $razdel->id]);
            $db->query('UPDATE `forum_razdels` SET `number` = ?i WHERE `id` = ?i', [$razdel->number, $razdel_n->id]);
        }
        if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)|(\.\-)]/", $_POST['name'], $m)) {
            ?>
            <div class = 'err'>
                В поле &laquo;Название раздела&raquo; присутствуют запрещенные <span style="font-weight: bold; color: red;"><?php echo  $m[0]?></span> символы!.
            </div>
            <?php

        } elseif (mb_strlen($name) < $set['forum_new_them_name_min_pod'] || mb_strlen($name) > $set['forum_new_them_name_max_pod']) {
            ?>
            <div class = 'err'>В поле &laquo;Название раздела&raquo; можно использовать от <?php echo  $set['forum_new_them_name_min_pod'] ?> до <?php echo  $set['forum_new_them_name_max_pod'] ?> символов.</div>
            <?php

        } elseif ($number < 0 || $number == null || $number == 0) {
            ?>
            <div class = 'err'>Введите уровень.</div>
            <?php

        } elseif (mb_strlen($description) > 100) {
            ?>
            <div class = 'err'>Слишком длинное описание раздела.</div>
            <?php

        } else {
            if ($razdel->id_forum != $forum_id) {
                admin_log('Форум', 'Разделы', 'Раздел "'.$razdel->name.'" перенесён из подфорума "'.$forum->name.'" в "'.$new_forum->name.'".');
            }
            if ($razdel->name != $name) {
                admin_log('Форум', 'Разделы', 'Раздел "'.$razdel->name.'" переименован в "'.$name.'" в подфоруме "'.$forum->name.'".');
            } else {
                admin_log('Форум', 'Разделы', 'Раздел "'.$razdel->name.'" был изменён.');
            }
            $db->query('UPDATE `forum_razdels` SET `id_forum` = ?i, `name` = ?, `description` = ?, `number` = ?i, `type` = ?i, `output` = ?i WHERE `id` = ?i',
                       [$forum_id, $name, $description, $number, $type, $output, $razdel->id]);
            $_SESSION['msg'] = '<div class = "msg">Раздел успешно изменён.</div>';
            header('Location: '.FORUM.'/'.$forum_id.'/');
            exit;
        }
    } elseif (isset($_POST['delete'])) {
        ?>
        <div class = 'menu_razd' style = 'text-align: left'>
            <a href = '<?php echo  FORUM ?>'>Форум</a> / <a href = '<?php echo  FORUM.'/'.$forum->id ?>/'><?php echo  output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?php echo  output_text($razdel->name) ?></a> / Удаление
        </div>
        <form action = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id ?>/edit_razdel.html' method = 'post' class="p_m" style = 'text-align: center'>
            Вы уверены, что хотите удалить этот раздел со всем его содержимым?<br />
            <input type = 'submit' name = 'del' value = 'Удалить' /> <input type = 'submit' name = 'cancel' value = 'Отменить' />
        </form>
        <?php
        include_once '../sys/inc/tfoot.php';
    } elseif (isset($_POST['del'])) {
        admin_log('Форум', 'Разделы', 'Раздел "'.$razdel->name.'" был удалён из подфорума "'.$forum->name.'".');
        $themes = $db->query('SELECT `id` FROM `forum_themes` WHERE `id_razdel` = '.$razdel->id);
        while ($theme = $themes->object()) {
            $files = $db->query('SELECT `name` FROM `forum_post_files` WHERE `id_theme` = '.$theme->id);
            while ($file = $files->object()) {
                unlink(H.FORUM.'/files/'.$file->name);
            }
            $db->query('DELETE FROM `forum_post_rating` WHERE `id_theme` = '.$theme->id);
            $db->query('DELETE FROM `forum_posts` WHERE `id_theme` = '.$theme->id);
            $db->query('DELETE FROM `forum_post_files` WHERE `id_theme` = '.$theme->id);
            $db->query('DELETE FROM `forum_votes_var` WHERE `id_theme` = '.$theme->id);
            $votes = $db->query('SELECT `id` FROM `forum_votes` WHERE `id_theme` = '.$theme->id);
            while ($vote = $votes->object()) {
                $db->query('DELETE FROM `forum_vote_voices` WHERE `id_vote` = '.$vote->id);
            }
            $db->query('DELETE FROM `forum_votes` WHERE `id_theme` = '.$theme->id);
        }
        $db->query('DELETE FROM `forum_themes` WHERE `id_razdel` = '.$razdel->id);
        $n_razdels = $db->query('SELECT `id`, `number` FROM `forum_razdels` WHERE `number` > '.$razdel->number);
        while ($n_razdel = $n_razdels->object()) {
            $db->query('UPDATE `forum_razdels` SET `number` = '.($n_razdel->number-1).' WHERE `id` = '.$n_razdel->id);
        }
        $db->query('DELETE FROM `forum_razdels` WHERE `id` = '.$razdel->id);
        $_SESSION['msg'] = '<div class = "msg">Раздел со всем его содержимым успешно удалён.</div>';
        header('Location: '.FORUM.'/'.$forum->id.'/');
        exit;
    } elseif (isset($_POST['cancel'])) {
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/edit_razdel.html');
        exit;
    } ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?php echo  FORUM ?>'>Форум</a> / <a href = '<?php echo  FORUM.'/'.$forum->id ?>/'><?php echo  output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?php echo  output_text($razdel->name) ?></a> / Редактирование
    </div>
    <form action = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id ?>/edit_razdel.html' method = 'post' class="p_m">
        <b>Название раздела (<?php echo  $set['forum_new_them_name_max_pod'] ?> символ(а/ов)):</b><br />
        <input type = 'text' name = 'name' value = '<?php echo  $razdel->name ?>' /><br /><br />
        <b>Описание раздела (<?php echo  $set['forum_new_them_name_max_pod_opis'] ?> символ(а/ов)):</b><br />
        <textarea name = 'description'><?php echo  $razdel->description ?></textarea><br /><br />
        <b>Подфорум:</b><br />
        <select name = 'id_forum'>
            <?php
            $forums_s = $db->query('SELECT `id`, `name`, `access` FROM `forum` ORDER BY `number` ASC');
    while ($forum_s = $forums_s->object()) {
        if ($forum_s->access == 0 || ($forum_s->access == 1 && $user['group_access'] > 7) || ($forum_s->access == 2 && $user['group_access'] > 2)) {
            ?>
                    <option value = '<?php echo  $forum_s->id ?>' <?php echo  ($forum_s->id == $razdel->id_forum) ? 'selected = "selected"' : null ?>><?php echo  $forum_s->name ?></option>
                    <?php

        } else {
            continue;
        }
    } ?>
        </select><br /><br />
        <b>Позиция:</b> <input type = 'text' name = 'number' value = '<?php echo  $razdel->number ?>' size = '3' /><br />
        <br /><b>Могут создавать темы только:</b><br />
        <label><input type = 'checkbox' name = 'type' value = '1' <?php echo  ($razdel->type == 1) ? 'checked = "checked"' : null ?> /> Администраторы + модераторы</label><br />
        <br /><b>Вывод раздела:</b><br />
        <label><input type = 'checkbox' name = 'output' value = '1' <?php echo  ($razdel->output == 1) ? 'checked = "checked"' : null ?> /> Отображать посл. 3 темы вместо описания</label><br /><br />
        <input type = 'submit' name = 'edit' value = 'Сохранить' />
        <?php echo  user_access('forum_for_delete') ? "<input type = 'submit' name = 'delete' value = 'Удалить' />" : null ?>
    </form>
    <div class = 'p_m' style = 'text-align: right'><a href = '<?php echo  FORUM.'/'.$forum->id ?>/'>Отменить редактирование</a></div>
    <?php
    include_once '../sys/inc/tfoot.php';
} else {
    header('Location: '.FORUM.'/'.$forum->id.'/');
}
exit;

?>