<?php

if ($vars == 0) {
    $insert = $db->query('INSERT INTO `forum_votes` (`id_theme`, `id_user`, `name`, `time`, `time_end`) VALUES( ?i, ?i, ?, ?i, ?i)',
                         [$theme->id, $user['id'], ' ', 0, 0])->id();
    $db->query('INSERT INTO `forum_votes_var` (`id_theme`, `id_vote`, `variant`) VALUES( ?i, ?i, ?)',
               [$theme->id, $insert, '']);
    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/create_vote.html');
    exit;
}
if (isset($_POST['create'])) {
    $name = trim($_POST['name']);
    if ($_POST['time_end'] == 1 || ($_POST['time_end'] != 1 && $_POST['time_end'] != 2 && $_POST['time_end'] != 3 && $_POST['time_end'] != 4 && $_POST['time_end'] != 5 && $_POST['time_end'] != 6)) {
        $time_end = 0;
    } elseif ($_POST['time_end'] == 2) {
        $time_end = time()+60*60*24;
    } elseif ($_POST['time_end'] == 3) {
        $time_end = time()+60*60*72;
    } elseif ($_POST['time_end'] == 4) {
        $time_end = time()+60*60*24*7;
    } elseif ($_POST['time_end'] == 5) {
        $time_end = time()+60*60*24*30;
    } elseif ($_POST['time_end'] == 6) {
        $time_end = time()+60*60*24*90;
    }
    if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)|(\:\,\.\-\?)]/", $_POST['name'], $m)) {
        ?>
<div class = 'err'>
    В поле &laquo;Название опроса&raquo; присутствуют запрещенные <span style="font-weight: bold; color: red;"><?php echo  $m[0]?></span> символы!.
</div>
            <?php

    } elseif (mb_strlen($name) < 5) {
        ?>
        <div class = 'err'>Слишком короткое содержание опроса.</div>
        <?php

    } else {
        $var = $_POST['var'];
        $count_var = count($var);
        $check = 0;
        for ($i = 0; $i < $count_var; $i++) {
            unset($_SESSION['var'][$i]);
            $check += preg_match("/[^(\w)|(\x7F-\xFF)|(\s)|(\:\,\.\-\?)]/", $var[$i]);
            $val[] = "('".$theme->id."', '".$var[$i]."', '".$vote->id . "')";
        }
        $sql = 'INSERT INTO `forum_votes_var` (`id_theme`, `variant`, `id_vote`)
        VALUES ' . join(',', $val) . '';
        if ($check == false) {
            $db->query($sql);
            unset($_SESSION['name']);
            unset($_SESSION['time_end']);
            $_SESSION['success'] = '<div class = "msg">Опрос успешно прикреплён к теме.</div>';
            $db->query('UPDATE `forum_votes` SET  `name` = ?, `time` = ?i, `time_end` = ?i WHERE `id_theme` = ?i', [$name, $time, $time_end, $theme->id]);
            $db->query('DELETE FROM `forum_votes_var` WHERE `id_theme` = '.$theme->id.' AND `variant` = ""');
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
            exit;
        } else {
            echo '<div class = "err">Ошибка в описании вариантов!</div>';
        }
    }
} elseif (isset($_POST['add_var']) && $vars < 8) {
    $_SESSION['name'] = $_POST['name'];
    $_SESSION['time_end'] = $_POST['time_end'];
    $var = $_POST['var'];
    $count_var = count($var);
    for ($i = 0; $i < $count_var; $i++) {
        $_SESSION['var'][$i] = addslashes(trim($var[$i])); // TODO: только я так и не понял... а это надо?
    }// `id_theme` = '.$theme->id.', `id_vote` = '.$vote->id.', `variant` = ""
    $db->query('INSERT INTO `forum_votes_var` (`id_theme`, `id_vote`, `variant`) VALUES( ?i, ?i, ?)', [$theme->id, $vote->id, '']);
    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/create_vote.html');
    exit;
} elseif (isset($_POST['delete_var']) && $vars > 2) {
    $_SESSION['name'] = $_POST['name'];
    $_SESSION['time_end'] = $_POST['time_end'];
    $var = $_POST['var'];
    $count_var = count($var)-1;
    unset($_SESSION['var'][$count_var]);
    $db->query('DELETE FROM `forum_votes_var` WHERE `id_theme` ORDER by `id` DESC LIMIT 1');
    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/create_vote.html');
    exit;
} elseif (isset($_POST['cancel'])) {
    $_SESSION['success'] = '<div class = "msg">Опрос успешно отменён.</div>';
    $var = $_POST['var'];
    $count_var = count($var);
    for ($i = 0; $i < $count_var; $i++) {
        unset($_SESSION['var'][$i]);
    }
    unset($_SESSION['name']);
    unset($_SESSION['time_end']);
    $db->query('DELETE FROM `forum_votes` WHERE `id_theme` = '.$theme->id);
    $db->query('DELETE FROM `forum_votes_var` WHERE `id_theme` = '.$theme->id);
    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
    exit;
}
?>
<div class = 'menu_razd'>Создание опроса в теме</div>
<form action = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/create_vote.html' method = 'post' class="p_m">
    <b>Содержание опроса:</b><br />
    <textarea name = 'name' style = 'width: 96%;'><?php echo  (isset($_SESSION['name'])) ? output_text($_SESSION['name']) : null ?></textarea><br /><br />
    <b>Варианты ответов:</b><br />
    <?php
    for ($i = 0; $i < $vars; $i++) {
        ?>
        <input type = 'text' name = 'var[]' value = '<?php echo  (isset($_SESSION['var'][$i])) ? output_text($_SESSION['var'][$i]) : null ?>' style = 'width: 96%'><br />
        <?php

    }
    ?>
    <input type = 'submit' name = 'add_var' value = 'Ещё вариант' <?php echo  ($vars > 7) ? 'disabled = "disabled"' : null ?> /> <input type = 'submit' name = 'delete_var' value = 'Убрать вариант' <?php echo  ($vars < 3) ? 'disabled = "disabled"' : null ?> /><br />
    <br />
    <b>Дата окончания через:</b><br />
    <select name = 'time_end'>
        <option value = "1" <?php echo  (isset($_SESSION['time_end']) && $_SESSION['time_end'] == 1) ? 'selected = "selected"' : null ?>>Бессрочно</option>
        <option value = "2" <?php echo  (isset($_SESSION['time_end']) && $_SESSION['time_end'] == 2) ? 'selected = "selected"' : null ?>>1 День</option>
        <option value = "3" <?php echo  (isset($_SESSION['time_end']) && $_SESSION['time_end'] == 3) ? 'selected = "selected"' : null ?>>3 Дня</option>
        <option value = "4" <?php echo  (isset($_SESSION['time_end']) && $_SESSION['time_end'] == 4) ? 'selected = "selected"' : null ?>>1 Неделю</option>
        <option value = "5" <?php echo  (isset($_SESSION['time_end']) && $_SESSION['time_end'] == 5) ? 'selected = "selected"' : null ?>>1 месяц</option>
        <option value = "6" <?php echo  (isset($_SESSION['time_end']) && $_SESSION['time_end'] == 6) ? 'selected = "selected"' : null ?>>3 месяца</option>
    </select><br />
    <input type = 'submit' name = 'create' value = 'Создать опрос' /> <input type = 'submit' name = 'cancel' value = 'Отменить' /><br />
</form>
<?php

include_once H . 'sys/inc/tfoot.php';
exit;

?>