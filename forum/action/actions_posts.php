<?php

if (user_access('forum_post_ed')) {
    $d_posts = array_map('intval', $_POST['act']);
    $count_p = count($d_posts);

    if ($count_p != 0) {
        for ($i = 0; $i < $count_p; $i++) {
            if (isset($_POST['delete_posts'])) {
                $db->query('DELETE FROM `forum_posts` WHERE `id` = '.$d_posts[$i]);
            } elseif (isset($_POST['hide_posts'])) {
                $db->query('UPDATE `forum_posts` SET `hide` = "1", `id_admin` = '.$user['id'].' WHERE `id` = '.$d_posts[$i]);
            } elseif (isset($_POST['display_posts'])) {
                $db->query('UPDATE `forum_posts` SET `hide` = "0", `id_admin` = "0" WHERE `id` = '.$d_posts[$i]);
            }
        }
        if (isset($_POST['delete_posts'])) {
            $_SESSION['success'] = '<div class = "msg">Успешно удалено '.$count_p.' пост(а/ов).</div>';
            $msg_sys = '[url=/info.php?id='.$user['id'].']'.$user['nick'].'[/url] почистил тему. Было удалено '.$count_p.' пост(а/ов)';
            $db->query('INSERT INTO `forum_posts` SET `id_theme` = ?i, `id_user` = ?i, `id_admin` = ?i, `text` = ?, `time` = ?i', [$theme->id, 0, 0, $msg_sys, $time]);
        } elseif (isset($_POST['hide_posts'])) {
            $_SESSION['success'] = '<div class = "msg">Успешно скрыто '.$count_p.' пост(а/ов).</div>';
        } elseif (isset($_POST['display_posts'])) {
            $_SESSION['success'] = '<div class = "msg">Успешно показано вновь '.$count_p.' пост(а/ов).</div>';
        }
    } else {
        ?>
        <div class = 'err'>Выбрано 0 элементов. Удаление недоступно.</div>
        <?php

    }
}

header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
exit;

?>