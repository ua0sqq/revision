<?php

if (user_access('forum_for_create')) {
    if (isset($_POST['create'])) {
        $name = trim($_POST['name']);
        $description = trim($_POST['description']);
        $number = abs(intval($_POST['number']));
        $access = abs(intval($_POST['access']));
        $output = (isset($_POST['output'])) ? 1 : 0;
        $isset_name = $db->query('SELECT COUNT(*) FROM `forum` WHERE `name` = ?', [$name])->el();
        $isset_number = $db->query('SELECT COUNT(*) FROM `forum` WHERE `number` = ?', [$number])->el();
        if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)|(\.\-)]/", $_POST['name'], $m)) {
            ?>
            <div class = 'err'>
                В поле &laquo;название подфорума&raquo; присутствуют запрещенные <span style="font-weight: bold; color: red;"><?php echo  $m[0]?></span> символы!.
            </div>
            <?php

        } elseif (mb_strlen($name) < $set['forum_new_them_name_min_pod'] || mb_strlen($name) > $set['forum_new_them_name_max_pod']) {
            ?>
            <div class = 'err'>
                В поле &laquo;Название подфорума&raquo; можно использовать от <?php echo  $set['forum_new_them_name_min_pod'] ?> до <?php echo  $set['forum_new_them_name_max_pod'] ?> символов.
            </div>
            <?php

        } elseif ($number < 0 || $number == null || $number == 0) {
            ?>
            <div class = 'err'>Введите уровень.</div>
            <?php

        } elseif (mb_strlen($description) > $set['forum_new_them_name_max_pod_opis']) {
            ?>
            <div class = 'err'>Слишком длинное описание подфорума.</div>
            <?php

        } elseif ($isset_number > 0) {
            ?>
            <div class = 'err'>Данный уровень уже использует другой подфорум.</div>
            <?php

        } elseif ($isset_name > 0) {
            ?>
            <div class = 'err'>Подфорум с таким названием уже существует.</div>
            <?php

        } else {
            admin_log('Форум', 'Подфорумы', 'Создание подфорума "'.$name.'".');
            $data = [$name, $description, $number, $access, $output];
            //$db->query('INSERT INTO `forum` SET `name` = ?, `description` = ?, `number` = ?i, `access` = ?i, `output` = ?i', $data);
            $db->query('INSERT INTO `forum` (`name`, `description`, `number`, `access`, `output`) VALUES( ?, ?, ?i, ?i, ?i)', $data);
            $_SESSION['msg'] = '<div class = "msg">Подфорум успешно создан.</div>';
            header('Location: '.FORUM);
            exit;
        }
    } elseif (isset($_POST['cancel'])) {
        header('Location: '.FORUM);
        exit;
    }
    $next_number = $db->query('SELECT MAX(`number`) FROM `forum`')->el() + 1; ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?php echo  FORUM ?>'>Форум</a> / Создание подфорума
    </div>
    <form action = '<?php echo  FORUM ?>/?create_forum' method = 'post' class="p_m">
        <strong>Название подфорума (<?php echo  $set['forum_new_them_name_max_pod'] ?> символ(а/ов)):</strong><br />
        <input type = 'text' name = 'name' value = '' /><br /><br />
        <strong>Описание подфорума (<?php echo  $set['forum_new_them_name_max_pod_opis'] ?> символ(а/ов)):</strong><br />
        <textarea name = 'description'></textarea><br /><br />
        <strong>Позиция:</strong> <input type = 'text' name = 'number' value = '<?php echo  $next_number ?>' size = '3' /><br /><br />
        <strong>Доступ:</strong><br />
        <label><input type = 'radio' name = 'access' value = '0' checked = 'checked' /> Все</label><br />
        <label><input type = 'radio' name = 'access' value = '1' /> Только администраторы</label><br />
        <label><input type = 'radio' name = 'access' value = '2' /> Администраторы + модераторы</label><br />
        <br /><strong>Вывод подфорума:</strong><br />
        <label><input type = 'checkbox' name = 'output' value = '1' /> Отображать список разделов вместо описания</label><br /><br />
        <input type = 'submit' name = 'create' value = 'Создать' /> <input type = 'submit' name = 'cancel' value = 'Отменить' />
    </form>
    <?php
    include_once '../sys/inc/tfoot.php';
} else {
    header('Location: '.FORUM);
}
exit;

?>