<?php

if (!user_access('forum_them_edit') && $user['id'] != $theme->id_user) {
    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
    exit;
} else {
    if (isset($_POST['save'])) {
        $name = trim($_POST['name']);
        $description = trim($_POST['description']);
        $type = ($user['group_access'] > 7 && isset($_POST['type'])) ? 1 : 0;
        $razdel_id = abs(intval($_POST['razdel']));
        $forum_id = $db->query('SELECT `id_forum` FROM `forum_razdels` WHERE `id` = ?i', [$razdel_id])->el();
        if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)|(\,\.\-)]/", $_POST['name'], $m)) {
            ?>
            <div class = 'err'>
                В поле &laquo;Название темы&raquo; присутствуют запрещенные <span style="font-weight: bold; color: red;"><?php echo  $m[0]?></span> символы!.
            </div>
            <?php

        } elseif (mb_strlen($name) < $set['forum_new_them_min_i'] || mb_strlen($name) > $set['forum_new_them_max_i']) {
            ?>
            <div class = 'err'>В поле "Название темы" можно использовать от <?php echo  $set['forum_new_them_min_i'] ?> до <?php echo  $set['forum_new_them_max_i'] ?> символов.</div>
            <?php

        } elseif (mb_strlen($description) < 3) {
            ?>
            <div class = 'err'>Слишком короткое содержание темы.</div>
            <?php

        } else {
            $_SESSION['success'] = '<div class = "msg">Тема успешно изменена.</div>';
            $db->query('UPDATE `forum_themes` SET `id_forum` = ?i, `id_razdel` = ?i, `id_admin` = ?i, `name` = ?, `description` = ?, `type` = ?i, `time_edit` = ?i WHERE `id` = ?i',
                       [$forum_id, $razdel_id, $user['id'], $name, $description, $type, $time, $theme->id]);
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
            exit;
        }
    } elseif (isset($_POST['delete']) && (user_access('forum_them_del'))) {
        admin_log('Форум', 'Темы', 'Тема "'.$theme->name.'" была удалена из раздела "'.$razdel->name.'".');
        $files = $db->query('SELECT `name` FROM `forum_post_files` WHERE `id_theme` = '.$theme->id);
        while ($file = $files->object()) {
            unlink(H.FORUM.'/files/'.$file->name);
        }
        $votes = $db->query('SELECT `id` FROM `forum_votes` WHERE `id_theme` = '.$theme->id);
        while ($vote = $votes->object()) {
            $db->query('DELETE FROM `forum_vote_voices` WHERE `id_vote` = '.$vote->id);
        }
        $db->query('DELETE FROM `forum_votes` WHERE `id_theme` = '.$theme->id);
        $db->query('DELETE FROM `forum_post_rating` WHERE `id_theme` = '.$theme->id);
        $db->query('DELETE FROM `forum_posts` WHERE `id_theme` = '.$theme->id);
        $db->query('DELETE FROM `forum_post_files` WHERE `id_theme` = '.$theme->id);
        $db->query('DELETE FROM `forum_votes_var` WHERE `id_theme` = '.$theme->id);
        $db->query('DELETE FROM `forum_themes` WHERE `id` = '.$theme->id);
        $_SESSION['msg'] = '<div class = "msg">Тема успешно удалена.</div>';
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/');
        exit;
    } ?>
    <form action = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/edit_theme.html' method = 'post' class="p_m">
        <b>Название темы (<?php echo  $set['forum_new_them_max_i'] ?> символ(а/ов)):</b><br />
        <input type = 'text' name = 'name' value = '<?php echo  $theme->name ?>' style = 'width: 96%' /><br /><br />
        <b>Содержание темы:</b><br />
        <textarea name = 'description' style = 'width: 96%'><?php echo  $theme->description ?></textarea><br /><br />
        <b>Категория:</b><br />
        <select name = 'razdel'>
            <?php
            $forums = $db->query('SELECT `id`, `name`, `access` FROM `forum`');
    while ($forum_s = $forums->object()) {
        if ($forum_s->access == 0 || ($forum_s->access == 1 && $user['group_access'] > 7) || ($forum_s->access == 2 && $user['group_access'] > 2)) {
            ?>
                    <option disabled = 'disabled'><b><?php echo  $forum_s->name ?></b></option>
                    <?php
                    $razdels = $db->query('SELECT `id`, `name` FROM `forum_razdels` WHERE `id_forum` = '.$forum_s->id);
            while ($cat = $razdels->object()) {
                ?>
                        <option value = '<?php echo  $cat->id ?>' <?php echo  ($cat->id == $theme->id_razdel) ? 'selected = "selected"' : null ?>>>> <?php echo  $cat->name ?></option>
                        <?php

            }
        } else {
            continue;
        }
    } ?>
        </select><br /><br />
        <?php
        if ($user['group_access'] > 7) {
            ?>
            <b>Вывод темы в разделе:</b><br />
            <label><input type = 'checkbox' name = 'type' value = '1' <?php echo  ($theme->type == 1) ? 'checked = "checked"' : null ?> /> Тема всегда вверху</label><br /><br />
            <?php

        } ?>
        <input type = 'submit' name = 'save' value = 'Сохранить' />
        <?php
        if (user_access('forum_them_del')) {
            ?>
            <input type = 'submit' name = 'delete' value = 'Удалить тему' />
            <?php

        } ?>
    </form>
    <div class = 'p_m' style = 'text-align: right'><a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'>Отменить редактирование</a></div>
    <?php
    include_once '../sys/inc/tfoot.php';
    exit;
}

?>