<?php

$forum = $db->query('SELECT * FROM `forum` WHERE `id` = ?i', [$_GET['edit_forum']])->object();

if (user_access('forum_for_edit') && $forum) {
    if (isset($_POST['edit'])) {
        $name = trim($_POST['name']);
        $description = trim($_POST['description']);
        $number = abs(intval($_POST['number']));
        $access = abs(intval($_POST['access']));
        $output = (isset($_POST['output'])) ? 1 : 0;
        $isset_name = $db->query('SELECT COUNT(*) FROM `forum` WHERE `name` = ? AND `id` != ?i', [$name, $forum->id])->el();
        $forum_n = ($forum->number != $number) ? $db->query('SELECT `id`, `number` FROM `forum` WHERE `number` = ?i AND `id` != ?i', [$number, $forum->id])->object() : false;
        if ($forum_n) {
            $db->query('UPDATE `forum` SET `number` = ?i WHERE `id` = ?i', [$number, $forum->id]);
            $db->query('UPDATE `forum` SET `number` = ?i WHERE `id` = ?i', [$forum->number, $forum_n->id]);
        }
        if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)|(\.\-)]/", $_POST['name'], $m)) {
            ?>
            <div class = 'err'>
                В поле &laquo;название подфорума&raquo; присутствуют запрещенные <span style="font-weight: bold; color: red;"><?php echo $m[0]?></span> символы!.
            </div>
            <?php

        } elseif (mb_strlen($name) < $set['forum_new_them_name_min_pod'] || mb_strlen($name) > $set['forum_new_them_name_max_pod']) {
            ?>
            <div class = 'err'>
                В поле &laquo;название подфорума&raquo; можно использовать от <?php echo $set['forum_new_them_name_min_pod'] ?> до <?php echo $set['forum_new_them_name_max_pod'] ?> символов.
            </div>
            <?php

        } elseif ($isset_name != 0) {
            ?>
            <div class = 'err'>Подфорум с таким названием уже существует.</div>
            <?php

        } elseif ($number < 0 || $number == null || $number == 0) {
            ?>
            <div class = 'err'>Введите уровень.</div>
            <?php

        } elseif (mb_strlen($description) > 100) {
            ?>
            <div class = 'err'>Слишком длинное описание подфорума.</div>
            <?php

        } else {
            if ($forum->name != $name) {
                admin_log('Форум', 'Подфорумы', 'Подфорум "'.$forum->name.'" переименован в "'.$name.'".');
            } else {
                admin_log('Форум', 'Подфорумы', 'Подфорум "'.$forum->name.'" был изменён.');
            }
            $db->query('UPDATE `forum` SET `name` = ?, `description` = ?, `number` = ?i, `access` = ?i, `output` = ?i WHERE `id` = ?i',
                       [$name, $description, $number, $access, $output, $forum->id]);
            $_SESSION['msg'] = '<div class = "msg">Подфорум успешно изменён.</div>';
            header('Location: '.FORUM);
            exit;
        }
    } elseif (isset($_POST['delete']) && user_access('forum_for_delete')) {
        ?>
        <div class = 'menu_razd' style = 'text-align: left'>
            <a href = '<?php echo FORUM ?>'>Форум</a> / <a href = '<?php echo FORUM.'/'.$forum->id ?>/'><?php echo output_text($forum->name) ?></a> / Удаление
        </div>
        <form action = '<?php echo FORUM ?>/?edit_forum=<?php echo $forum->id ?>' method = 'post' style = 'text-align: center'>
            Вы уверены, что хотите удалить этот подфорум со всем его содержимым?<br />
            <input type = 'submit' name = 'del' value = 'Удалить' /> <input type = 'submit' name = 'cancel' value = 'Отменить' />
        </form>
        <?php
        include_once '../sys/inc/tfoot.php';
    } elseif (isset($_POST['del']) && user_access('forum_for_delete')) {
        admin_log('Форум', 'Подфорумы', 'Подфорум "'.$forum->name.'" был удалён.');
        $themes = $db->query('SELECT `id` FROM `forum_themes` WHERE `id_forum` = '.$forum->id);
        while ($theme = $themes->object()) {
            $files = $db->query('SELECT `name` FROM `forum_post_files` WHERE `id_theme` = '.$theme->id);
            while ($file = $files->object()) {
                unlink(H.FORUM.'/files/'.$file->name);
            }
            $db->query('DELETE FROM `forum_post_rating` WHERE `id_theme` = '.$theme->id);
            $db->query('DELETE FROM `forum_posts` WHERE `id_theme` = '.$theme->id);
            $db->query('DELETE FROM `forum_post_files` WHERE `id_theme` = '.$theme->id);
            $db->query('DELETE FROM `forum_votes_var` WHERE `id_theme` = '.$theme->id);
            $votes = $db->query('SELECT `id` FROM `forum_votes` WHERE `id_theme` = '.$theme->id);
            while ($vote = $votes->object()) {
                $db->query('DELETE FROM `forum_vote_voices` WHERE `id_vote` = '.$vote->id);
            }
            $db->query('DELETE FROM `forum_votes` WHERE `id_theme` = '.$theme->id);
        }
        $db->query('DELETE FROM `forum_themes` WHERE `id_forum` = '.$forum->id);
        $db->query('DELETE FROM `forum_razdels` WHERE `id_forum` = '.$forum->id);
        $n_forums = $db->query('SELECT `id`, `number` FROM `forum` WHERE `number` > '.$forum->number);
        while ($n_forum = $n_forums->object()) {
            $db->query('UPDATE `forum` SET `number` = '.($n_forum->number-1).' WHERE `id` = '.$n_forum->id);
        }
        $db->query('DELETE FROM `forum` WHERE `id` = '.$forum->id);
        $_SESSION['msg'] = '<div class = "msg">Подфорум со всем его содержимым успешно удалён.</div>';
        header('Location: '.FORUM);
        exit;
    } elseif (isset($_GET['cancel'])) {
        header('Location: '.FORUM);
        exit;
    } ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?php echo FORUM ?>'>Форум</a> / <a href = '<?php echo FORUM.'/'.$forum->id ?>/'><?php echo output_text($forum->name) ?></a> / Редактирование
    </div>
    <form action = '<?php echo FORUM ?>/?edit_forum=<?php echo $forum->id ?>' method = 'post' class="p_m">
        <b>Название подфорума (<?php echo $set['forum_new_them_name_max_pod'] ?> символ(а/ов)):</b><br />
        <input type = 'text' name = 'name' value = '<?php echo $forum->name ?>' /><br /><br />
        <b>Описание подфорума (<?php echo $set['forum_new_them_name_max_pod_opis'] ?> символ(а/ов)):</b><br />
        <textarea name = 'description'><?php echo $forum->description ?></textarea><br /><br />
        <b>Позиция:</b> <input type = 'text' name = 'number' value = '<?php echo $forum->number ?>' size = '3' /><br /><br />
        <b>Доступ:</b><br />
        <label><input type = 'radio' name = 'access' value = '0' <?php echo ($forum->access == 0) ? 'checked = "checked"' : null ?> /> Все</label><br />
        <label><input type = 'radio' name = 'access' value = '1' <?php echo ($forum->access == 1) ? 'checked = "checked"' : null ?> /> Только администраторы</label><br />
        <label><input type = 'radio' name = 'access' value = '2' <?php echo ($forum->access == 2) ? 'checked = "checked"' : null ?> /> Администраторы + модераторы</label><br />
        <br /><b>Вывод подфорума:</b><br />
        <label><input type = 'checkbox' name = 'output' value = '1' <?php echo ($forum->output == 1) ? 'checked = "checked"' : null ?> /> Отображать список разделов вместо описания</label><br /><br />
        <input type = 'submit' name = 'edit' value = 'Сохранить' />
        <?php echo user_access('forum_for_delete') ? "<input type = 'submit' name = 'delete' value = 'Удалить' />" : "<input type = 'submit' name = 'cancel' value = 'Отменить' />" ?>
    </form>
    <div class = 'p_m' style = 'text-align: right'><a href = '<?php echo FORUM ?>'>Отменить редактирование</a></div>
    <?php
    include_once '../sys/inc/tfoot.php';
} else {
    header('Location: '.FORUM);
}
exit;

?>