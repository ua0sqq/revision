<?php

$post = $db->query('SELECT `id`, `id_theme` FROM `forum_posts` WHERE `id` = ?i AND `id_user` = ?i', [$_GET['post'], $user['id']])->object();
$forum = $db->query('SELECT `id`, `access`, `name` FROM `forum` WHERE `id` = ?i', [$_GET['forum']])->object();
$razdel = $db->query('SELECT `id`, `id_forum`, `name` FROM `forum_razdels` WHERE `id_forum` = ?i AND `id` = ?i', [$forum->id, $_GET['razdel']])->object();
$theme = $db->query('SELECT `id`, `id_razdel`, `name` FROM `forum_themes` WHERE `id_razdel` = ?i AND `id` = ?i', [$razdel->id, $_GET['theme']])->object();

if (!$theme || !$razdel || !$forum || !$post || ($forum->access == 1 && $user['group_access'] < 8) || ($forum->access == 2 && $user['group_access'] < 3)) {
    header('Location: '.FORUM);
    exit;
} else {
    $set['title'] = 'Добавление файлов';
    include_once '../sys/inc/thead.php';
    title().aut();

    if (isset($_POST['download']) && isset($_FILES)) {
        $file = stripcslashes($_FILES['file']['name']);
        $file = preg_replace('(\#|\?)', NULL, $file);
        $name = preg_replace('#\.[^\.]*$#', NULL, $file);
        $size = filesize($_FILES['file']['tmp_name']);
        $ras = strtolower(preg_replace('#^.*\.#', NULL, $file));
        if ($ras == 'php' || $ras == 'exe' || $ras == 'js' || $ras == 'html' || $ras == 'htaccess' || $ras == NULL) {
            ?>
            <div class = 'err'>Ошибка при выгрузке файла.</div>
            <?php
        } else {
            $count_files = $db->query('SELECT COUNT(*) FROM `forum_post_files` WHERE `id_post` = '.$post->id)->el();
            $data = [$theme->id, $post->id, $user['id'], $post->id.'_'.($count_files+1).'.'.$ras, $file, $size];
            $db->query('INSERT INTO `forum_post_files` (`id_theme`, `id_post`, `id_user`, `name`, `real_name`, `size`) VALUES( ?i, ?i, ?i, ?, ?, ?i)', $data);
            move_uploaded_file($_FILES["file"]["tmp_name"], 'files/'.$post->id.'_'.($count_files+1).'.'.$ras);
            $_SESSION['download'] = '<div class = "msg">Файл успешно прикреплён.</div>';
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/'.$post->id.'/add_file');
            exit;
        }
    } elseif (isset($_GET['del_file']) && ($db->query('SELECT `id_user` FROM `forum_post_files` WHERE `id` = ?i', [$_GET['del_file']])->el() == $user['id'] || user_access('forum_post_ed'))) {
        $_SESSION['download'] = '<div class = "msg">Файл успешно удалён.</div>';
        $file = $db->query('SELECT `id`, `id_post`,  `name` FROM `forum_post_files` WHERE `id` = ?i', [$_GET['del_file']])->object();
        unlink(H.FORUM.'/files/'.$file->name);
        $db->query('DELETE FROM `forum_post_files` WHERE `id` = '.$file->id);
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/'.$post->id.'/add_file');
        exit;
    }
    if (isset($_SESSION['download'])) {
        echo $_SESSION['download'];
        unset($_SESSION['download']);
    }
    ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?php echo  FORUM ?>'>Форум</a> / <a href = '<?php echo  FORUM.'/'.$forum->id ?>/'><?php echo  output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?php echo  output_text($razdel->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?php echo  output_text($theme->name, 1, 1, 0, 0, 0) ?></a>
    </div>
    <table class = 'post' cellspacing = '0' cellpadding = '0'>
        <?php
        $files = $db->query('SELECT * FROM `forum_post_files` WHERE `id_post` = '.$post->id);
        while ($p_file = $files->object()) {
            $ras = strtolower(preg_replace('#^.*\.#', NULL, $p_file->name));
            if ($ras == 'jpg' || $ras == 'jpeg' || $ras == 'gif' || $ras == 'png' || $ras == 'bmp' || $ras == 'ico') {
                $icon = FORUM.'/files/'.$p_file->name;
            } elseif ($ras == '3gp' || $ras == 'mp4' || $ras == 'avi' || $ras == 'mpeg' || $ras == 'flv' || $ras == 'wmv' || $ras == 'mkv') {
                $icon = FORUM.'/icons/files/video.png';
            } elseif ($ras == 'docx' || $ras == 'doc' || $ras == 'docm' || $ras == 'dotx' || $ras == 'dot' || $ras == 'dotm') {
                $icon = FORUM.'/icons/files/word.png';
            } elseif ($ras == 'mp1' || $ras == 'mp2' || $ras == 'mp3' || $ras == 'wav' || $ras == 'aif' || $ras == 'ape' || $ras == 'flac' || $ras == 'ogg' || $ras == 'asf' || $ras == 'wma') {
                $icon = FORUM.'/icons/files/music.png';
            } elseif ($ras == 'zip' || $ras == 'rar' || $ras == 'tar' || $ras == '7-zip' || $ras == 'gzip' || $ras == 'jar' || $ras == 'jad' || $ras == 'war' || $ras == 'xar') {
                $icon = FORUM.'/icons/files/archive.png';
            } elseif ($ras == 'txt' || $ras == 'xml') {
                $icon = FORUM.'/icons/files/txt.png';
            } elseif ($ras == 'pdf') {
                $icon = FORUM.'/icons/files/pdf.png';
            } elseif ($ras == 'psd') {
                 $icon = FORUM.'/icons/files/psd.png';
            } else {
                $icon = FORUM.'/icons/files/file.png';
            }
            ?>
            <tr>
                <td class = 'p_t' style = 'width: 50px; border-right: none'>
                    <a href = '<?php echo  $icon ?>'><img src = '<?php echo  $icon ?>' alt = '' style = 'width: 50px; height: 50px' /></a>
                </td>
                <td class = 'p_t' style = 'border-left: none'>
                    <?php echo  output_text($p_file->real_name) ?><br />
                    Размер файла: <?php echo  size_file($p_file->size) ?><br />
                    [ <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/'.$post->id.'/del_file='.$p_file->id ?>'>Удалить файл</a> ]
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
    <form action = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/'.$post->id ?>/add_file' method = 'post' enctype = 'multipart/form-data' class="p_m">
        <input type = 'file' name = 'file'><br />
        <input type = 'submit' name = 'download' value = 'Добавить файл' />
    </form>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?php echo  FORUM ?>'>Форум</a> / <a href = '<?php echo  FORUM.'/'.$forum->id ?>/'><?php echo  output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?php echo  output_text($razdel->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?php echo  output_text($theme->name, 1, 1, 0, 0, 0) ?></a>
    </div>
    <?php
    include_once '../sys/inc/tfoot.php';
    exit;
}

?>