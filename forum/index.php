<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once  H . 'sys/inc/start.php';
include_once  H . 'sys/inc/sess.php';
include_once  H . 'sys/inc/settings.php';
include_once  H . 'sys/inc/db_connect.php';
include_once  H . 'sys/inc/ipua.php';
include_once  H . 'sys/inc/fnc.php';
include_once  H . 'sys/inc/user.php';

if (!preg_match("/\bforum\b/i", $_SERVER['SCRIPT_NAME'])) {
    unset($_SESSION['sort_'.$user['id']]);
}
if (isset($user)) {
	$db->query("UPDATE `user` SET `url` =? WHERE `id` =?i LIMIT ?i",
	           [$_SERVER['REQUEST_URI'], $user['id'], 1]);
}

include_once 'settings.php'; // Настройки.

$menu = (isset($_GET['menu'])) ? htmlspecialchars($_GET['menu']) : false;
switch ($menu) {
    default:
    $set['title'] = (isset($_GET['create_forum'])) ? 'Форум - создание подфорума' : 'Форум';
    include_once H . 'sys/inc/thead.php';
    title().aut();

    if (isset($_GET['create_forum'])) {
        include_once 'action/create_forum.php'; // Создание подфорума.
    } elseif (isset($_GET['edit_forum'])) {
        include_once 'action/edit_forum.php'; // Редактирование подфорума.
    } else {
        ?>
        <div class = 'menu_razd' style = 'text-align: left'>
            <a href = '<?php echo  FORUM ?>'>Форум</a>
		   : <a href = '<?php echo  FORUM ?>/search.html'>Поиск по форуму</a>


        </div>
        <?php

    }
    if (isset($_SESSION['msg'])) {
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
    }
    if (user_access('forum_for_create')) {
        ?>
        <div class = 'p_m' style = 'text-align: left'><a href = '<?php echo  FORUM ?>?create_forum'>Создать подфорум</a></div>
        <?php

    }

    $k_post = $db->query('SELECT COUNT(*) FROM `forum`')->el();
    $k_page = k_page($k_post, $set['p_str']);
    $page = page($k_page);
    $start = $set['p_str']*$page-$set['p_str'];
    if ($k_post == 0) {
        ?>
        <div class = 'err'>Подфорумы ещё не созданы.</div>
        <?php

    } else {
        ?>
        <table class="post">
            <?php
            $forums = $db->query('SELECT frm.*, (
SELECT COUNT(*) FROM `forum_razdels` WHERE `id_forum`=frm.id) cntRazd, (
SELECT COUNT(*) FROM `forum_themes` WHERE `id_forum`=frm.id) cntThem
FROM `forum` frm ORDER BY frm.`number` ASC LIMIT ?i, ?i', [$start, $set['p_str']]);
        while ($forum = $forums->object()) {
            if ($forum->access == 0 || ($forum->access == 1 && $user['group_access'] > 7) || ($forum->access == 2 && $user['group_access'] > 2)) {
                ?>
                    <tr>
                        <td class = 'icon14'>
                            <img src = '<?php echo  FORUM ?>/icons/forum.png' alt = '' <?php echo  ICONS ?> />
                        </td>
                        <td class = 'p_t'>
                            <a href = '<?php echo  FORUM.'/'.$forum->id ?>/'><?php echo  output_text($forum->name, 1, 1, 0, 0, 0) ?></a> (<?php echo  $forum->cntRazd.'/'.$forum->cntThem ?>)
                        </td>
                        <?php
                        if (user_access('forum_for_edit')) {
                            ?>
                            <td class = 'icon14'>
                                <a href = '<?php echo  FORUM.'/?edit_forum='.$forum->id ?>'><img src = '<?php echo  FORUM ?>/icons/edit.png' alt = '' <?php echo  ICONS ?> /></a>
                            </td>
                            <?php

                        } ?>
                    </tr>
                    <?php
                    if ($forum->description != null && $forum->output == 0) {
                        ?>
                        <tr>
                            <td class = 'p_m' colspan = '3'>
                                <?php echo  output_text($forum->description, 1, 1, 0, 1, 1) ?>
                            </td>
                        </tr>
                        <?php

                    } elseif ($forum->output == 1 && $forum->cntRazd> 0) {
                        ?>
                        <tr>
                            <td class = 'p_m' colspan = '3'>
                                <?php
                                $razdels = $db->query('SELECT `id`, `name` FROM `forum_razdels` WHERE `id_forum` = ?i', [$forum->id]);
                        while ($razdel = $razdels->object()) {
                            ?>
                                    <img src = '<?php echo  FORUM ?>/icons/razdel.png' alt = '' style = 'width: 16px; height: 16px;' /> <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?php echo  $razdel->name; ?></a><br />
                                    <?php

                        } ?>
                            </td>
                        </tr>
                        <?php

                    }
            }
        } ?>
        </table>
        <?php
        if ($k_page > 1) {
            str('?', $k_page, $page);
        }
    }
    $new_themes = $db->query('SELECT COUNT(*) FROM `forum_themes` WHERE `time` > '.(time()-60*60*24))->el();
    $my_themes = (isset($user)) ? $db->query('SELECT COUNT(*) FROM `forum_themes` WHERE `id_user` = '.$user['id'])->el() : null;
    ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?php echo  FORUM ?>'>Форум</a>
    </div>
    <div class = 'p_t' style = 'text-align: left'>
        <a href = '<?php echo  FORUM ?>/new_themes.html'>Новые темы</a> (<?php echo  $new_themes ?>)
        <?php
        if (isset($user)) {
            ?>
             | <a href = '<?php echo  FORUM ?>/my_themes.html'>Мои темы</a> (<?php echo  $my_themes ?>)
            <?php

        }
        ?>
    </div>

    <?php
    break;
    case 'forum':
        include_once 'view/forum.php'; // Подфорум.
    break;
    case 'razdel':
        include_once 'view/razdel.php'; // Раздел.
    break;
    case 'theme':
        include_once 'view/theme.php'; // Тема.
    break;
    case 'who':
        include_once 'view/who.php'; // Кто в теме.
    break;
    case 'files':
        include_once 'view/files_theme.php'; // Файлы темы.
    break;
    case 'add_file':
        include_once 'action/add_file_post.php'; // Добавление файла.
    break;
    case 'reports':
        include_once 'view/reports.php'; // Жалобы на темы.
    break;
    case 'new_themes':
        include_once 'view/new_themes.php'; // Новые темы.
    break;
    case 'my_themes':
        include_once 'view/my_themes.php'; // Новые темы.
    break;
    case 'search':
        include_once 'view/search.php'; // Поиск по форуму.
    break;
}
include_once H . 'sys/inc/tfoot.php';

?>