<?php

$forum = $db->query('SELECT * FROM `forum` WHERE `id` = ?i', [$_GET['forum']])->object();
$razdel = $db->query('SELECT * FROM `forum_razdels` WHERE `id_forum` = ?i AND `id` = ?i', [$forum->id, $_GET['razdel']])->object();
$theme = $db->query('SELECT * FROM `forum_themes` WHERE `id_razdel` = ?i AND `id` = ?i', [$razdel->id, $_GET['theme']])->object();

//для сео META данные
#-----------------------------------------------------------------------------#
$set['meta_keywords'] = meta_out_keywords(mb_substr($theme->description, 0, 160));
$_forum = $forum->name != null ? ','.$forum->name : null;
$_razdel = $razdel->name != null ? ','.$razdel->name : null;
$set['meta_description'] = meta_out_description($theme->name.''.$_forum.''.$_razdel);
#-----------------------------------------------------------------------------#

if (!$theme || !$razdel || !$forum || ($forum->access == 1 && $user['group_access'] < 8) || ($forum->access == 2 && $user['group_access'] < 3)) {
    header('Location: '.FORUM);
    exit;
} else {


    $set['title'] = 'Тема - '.output_text($theme->name, 1, 1, 0, 0, 0);
    include_once '../sys/inc/thead.php';
    title().aut();

    $my_report = $db->query('SELECT COUNT(*) FROM `forum_reports` WHERE `id_theme` = ?i AND `id_user` = ?i', [$theme->id, $user['id']])->el();
    $k_post = $db->query('SELECT COUNT(*) FROM `forum_posts` WHERE `id_theme` = '.$theme->id)->el();

    if ($db->query('SELECT COUNT(*) FROM `forum_votes` WHERE `id_theme` = '.$theme->id)->el() != 0) {
        $vote = $db->query('SELECT * FROM `forum_votes` WHERE `id_theme` = '.$theme->id)->object();
        $vars = $db->query('SELECT COUNT(*) FROM `forum_votes_var` WHERE `id_theme` = '.$theme->id)->el();
    }
    if (isset($_GET['sort_0'])) {
        unset($_SESSION['sort_'.$user['id']]);
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
        exit;
    } elseif (isset($_GET['sort_1'])) {
        $_SESSION['sort_'.$user['id']] = 1;
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
        exit;
    } elseif (isset($_GET['download'])) {
        $file = $db->query('SELECT `id`, `id_post`, `name`, `count_downloads` FROM `forum_post_files` WHERE `id` = ?i', [$_GET['download']])->object();
        $this_post = $db->query('SELECT `hide`, `privat` FROM `forum_posts` WHERE `id` = '.$file->id_post)->object();
        if ($this_post->privat == 0 || ($this_post->privat == $user['id'] || $this_post->id_user == $user['id']) || ($this_post->hide != 0 && user_access('forum_post_ed'))) {
            $db->query('UPDATE `forum_post_files` SET `count_downloads`=`count_downloads`+?i WHERE `id` = ?i', [1, $file->id]);
            header('Location: '.FORUM.'/files/'.$file->name);
        } else {
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
        }
        exit;
    } elseif (isset($user) && isset($_GET['cleare_theme']) && user_access('forum_them_edit')) {
        include_once 'action/clear_theme.php'; // Очистка темы.
    } elseif (isset($user) && isset($_GET['edit_theme'])) {
        include_once 'action/edit_theme.php'; // Редактирование темы.
    } elseif (isset($user) && isset($_GET['close_theme']) && $theme->reason_close == NULL && user_access('forum_them_edit')) {
        include_once 'action/close_theme.php'; // Закрытие темы.
    } elseif (isset($user) && isset($_GET['open_theme']) && $theme->reason_close != NULL && user_access('forum_them_edit')) {
        $_SESSION['success'] = '<div class = "msg">Тема успешно открыта.</div>';
        $msg_sys = 'Проблема решена. [url=/info.php?id='.$user['id'].']'.$user['nick'].'[/url] открыл тему.';
        $db->query('INSERT INTO `forum_posts` (`id_theme`, `id_user`, `id_admin`, `text`, `time`, `cit`, `rating`) VALUES( ?i, ?i, ?i, ?, ?i, ?i, ?i)',
                   [$theme->id, 0, 0, $msg_sys, $time, 0, 0]);
        $db->query('UPDATE `forum_themes` SET `reason_close` = "" WHERE `id` = '.$theme->id);
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
        exit;
    } elseif (isset($user) && isset($_GET['create_vote']) && ($theme->reason_close == NULL || ($theme->reason_close != NULL && user_access('forum_post_close'))) && $user['id'] == $theme->id_user) {
        include_once 'action/create_vote.php'; // Создание голосования.
    } elseif (isset($user) && isset($_GET['edit_vote']) && $theme->reason_close == NULL && $user['id'] == $theme->id_user) {
        include_once 'action/edit_vote.php'; // Редактирование голосования.
    } elseif (isset($user) && isset($_GET['report'])) {
        if ($my_report != 0 || user_access('forum_them_edit') || $user['id'] == $theme->id_user || $theme->reason_close != NULL) {
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
            exit;
        } else {
            if (isset($_POST['send'])) {
                $text = trim($_POST['text']);
                if (mb_strlen($text) < 5) {
                    ?>
                    <div class = 'err'>Слишком короткая причина. Указывайте нормальную причину, если не хотите быть забаненым администрацией.</div>
                    <?php
                } else {
                    $persons = $db->query('SELECT `id`, `group_access` FROM `user` WHERE `group_access` > "2"');
                    while ($person = $persons->object()) {
                        $access = $db->query('SELECT COUNT(*) FROM `user_group_access` WHERE `id_group` = '.$person->group_access.' AND `id_access` = "forum_them_edit"')->el();
                        if ($access != 0) {
                            $msg = 'Пользователь [url=/info.php?id='.$user['id'].']'.$user['nick'].'[/url] подал жалобу на тему [url='.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html]'.$theme->name.'[/url].
                            Причина указана тут - [url='.FORUM.'/reports.html]ссылка[/url]';
                            $db->query('INSERT INTO `mail` (`id_user`, `id_kont`, `msg`, `time`) VALUES( ?,  ?i,  ?, ?i)',
                                       ['0', $person->id, $msg, $time]);
                        }
                    }
                    $db->query('INSERT INTO `forum_reports` (`id_theme`, `id_user`, `text`) VALUES( ?i,  ?i, ?)', [$theme->id, $user['id'], $text]);
                    $_SESSION['success'] = '<div class = "msg">Жалоба успешно отправлена на рассмотрение администрацией.</div>';
                    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
                    exit;
                }
            }
            ?>
            <form method = 'post' action = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/report.html' class="p_m">
                <b>Укажите причину:</b><br />
                <textarea name = 'text' style = 'width: 92%; height: 50px'></textarea><br />
                <input type = 'submit' name = 'send' value = 'Отправить жалобу' />
            </form>
            <?php
            include_once '../sys/inc/tfoot.php';
            exit;
        }
    }

    $my_voice = (isset($vote) && isset($user)) ? $db->query('SELECT COUNT(*) FROM `forum_vote_voices` WHERE `id_user` = '.$user['id'].' AND `id_vote` = '.$vote->id)->el() : NULL;

    if (isset($user)) {
        $last_post = $db->query('SELECT MAX(`id`) FROM `forum_posts` WHERE `id_theme` = '.$theme->id)->el();
        if (isset($_GET['delete_post']) && user_access('forum_post_ed')) {
            $_SESSION['success'] = '<div class = "msg">Комментарий успешно удалён.</div>';
            $files = $db->query('SELECT `name` FROM `forum_post_files` WHERE `id_post` = ?i', [$_GET['delete_post']]);
            while ($file = $files->object()) {
                unlink(H.FORUM.'/files/'.$file->name);
            }
            $db->query('DELETE FROM `forum_post_rating` WHERE `id_post` = ?i', [$_GET['delete_post']]);
            $db->query('DELETE FROM `forum_posts` WHERE `id` = ?i', [$_GET['delete_post']]);
            if ($k_post == 0) {
                $db->query('UPDATE `forum_themes` SET `time_post` = '.$theme->time.' WHERE `id` = '.$theme->id);
            }
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
            exit;
        } elseif (isset($_GET['hide']) && user_access('forum_post_ed')) {
            $_SESSION['success'] = '<div class = "msg">Комментарий успешно скрыт.</div>';
            $db->query('UPDATE `forum_posts` SET `id_admin` = ?i, `hide` = "1" WHERE `id` = ?i', [$user['id'], $_GET['hide']]);
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
            exit;
        } elseif (isset($_GET['display']) && user_access('forum_post_ed')) {
            $_SESSION['success'] = '<div class = "msg">Комментарий успешно показан вновь.</div>';
            $db->query('UPDATE `forum_posts` SET `id_admin` = "0", `hide` = "0" WHERE `id` = ?i', [$_GET['display']]);
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
            exit;
        } elseif (isset($_GET['vote']) && $my_voice == 0) {
            $_SESSION['success'] = '<div class = "msg">Ваш голос успешно учтён.</div>';
            $db->query('INSERT INTO `forum_vote_voices` (`id_vote`, `id_variant`, `id_user`) VALUES( ?i, ?i, ?i)',
                       [$vote->id, $_GET['vote'], $user['id']]);
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
            exit;
        } elseif (isset($_GET['del_file']) && ($db->query('SELECT `id_user` FROM `forum_post_files` WHERE `id` = ?i', [$_GET['del_file']])->el() == $user['id'] || user_access('forum_post_ed'))) {
            $_SESSION['success'] = '<div class = "msg">Файл успешно удалён.</div>';
            $file = $db->query('SELECT `id`, `id_post`,  `name` FROM `forum_post_files` WHERE `id` = ?i', [$_GET['del_file']])->object();
            if (is_file(H.FORUM.'/files/'.$file->name)) {
                unlink(H.FORUM.'/files/'.$file->name);
            }
            $id_post = $file->id_post;
            unset($_SESSION['sort_'.$user['id']]);
            $count = $db->query('SELECT COUNT(*) FROM `forum_posts` WHERE `id_theme` = '.$theme->id.' AND `id` < '.($id_post+1))->el();
            $count_posts = $db->query('SELECT COUNT(*) FROM `forum_posts` WHERE `id_theme` = '.$theme->id)->el();
            $this_pages = k_page($count, $set['p_str']);
            $pages = ($this_pages > 0) ? '/page='.$this_pages : '.html';
            $db->query('DELETE FROM `forum_post_files` WHERE `id` = '.$file->id);
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.$pages.'#'.$id_post);
            exit;
        }
        if (isset($_POST['comment']) && ($theme->reason_close == NULL || ($theme->reason_close != NULL && user_access('forum_post_close')))) {
            $text = $_POST['text'];
            if (isset($_POST['translit']) && $_POST['translit'] == 1) {
                $text = translit($text);
            }
            $mat = antimat($text);
            if ($mat) {
                ?>
                <div class = 'err'>В тексте сообщения обнаружен мат: <?php echo  $mat ?>.</div>
                <?php
            } elseif (mb_strlen($text) < $set['forum_new_them_min'] || mb_strlen($text) > $set['forum_new_them_max']) {
                ?>
                <div class = 'err'>Комментарий может содержать от <?php echo  $set['forum_new_them_min'] ?> до <?php echo  $set['forum_new_them_max'] ?> символов.</div>
                <?php
            } elseif ($db->query('SELECT COUNT(*) FROM `forum_posts` WHERE `id_user` = ?i AND `text` = ?', [$user['id'], $text])->el() != 0) {
                ?>
                <div class = 'err'>Вы уже писали это в данной теме. Будьте оригинальнее.</div>
                <?php
            } else {
                if (isset($_GET['cit']) && is_numeric($_GET['cit'])) {
                    $cit = intval($_GET['cit']);
                    $privat = 0;
                } elseif (isset($_GET['privat']) && is_numeric($_GET['privat'])) {
                    $privat = intval($_GET['privat']);
                    $cit = 0;
                } else {
                    $cit = 0;
                    $privat = 0;
                }
                $db->query('UPDATE `forum_themes` SET `time_post` = '.$time.' WHERE `id` = '.$theme->id);
                $data = [$theme->id, $user['id'], 0, $text, $cit, $privat, $time, 0, 0];
                $post_id = $db->query('INSERT INTO `forum_posts` (`id_theme`, `id_user`, `id_admin`, `text`, `cit`, `privat`, `time`, `hide`, `rating`)
VALUES( ?i, ?i, ?i, ?, ?i, ?i, ?i, ?i, ?i)', $data)->id();

                $db->query('UPDATE `user` SET `balls` = "'.($user['balls']+$set['forum_balls_add_post']).'" WHERE `id` = '.$user['id']);

				// Вычисляем рейтинг за сообщение.
				$rating_add_msg = rating :: msgnum($text);
				// Суммируем.
				$add_rating = ($rating_add_msg + 0.02);
				// Добавляем рейтингю
				rating :: add($user['id'], $add_rating, 1, null, 'forum');


                $count = $db->query('SELECT COUNT(*) FROM `forum_posts` WHERE `id_theme` = '.$theme->id.' AND `id` < '.($post_id+1))->el();
                $count_posts = $db->query('SELECT COUNT(*) FROM `forum_posts` WHERE `id_theme` = '.$theme->id)->el();
                $this_pages = k_page($count, $set['p_str']);
                $pages = ($this_pages > 0) ? '/page='.$this_pages : '.html';
                $j_f = 'Пользователь [url=/info.php?id='.$user['id'].']'.$user['nick'].'[/url] написал в теме "[url='.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.$pages.'#'.$post_id.']'.$theme->name.'[/url]", за которой Вы следите.
                Комментарий: [b]'.$text.'[/b]';
                $forum_js = $db->query('SELECT `id_user` FROM `forum_journal` WHERE `id_theme` = '.$theme->id);
                while ($forum_j = $forum_js->object()) {
                    if ($db->query('SELECT COUNT(*) FROM `forum_posts` WHERE `id` = '.$post_id.' AND `id_user` = '.$user['id'])->el() == 0 && ((isset($_GET['answer']) && $_GET['answer'] != $forum_j->id_user) || !isset($_GET['answer']))) {
                        $db->query('INSERT INTO `f_journal` (`id_user`, `type`, `text`, `time`) VALUES( ?i, ?, ?, ?i)',
                                   [$forum_j->id_user, 'themes', $j_f, $time]);
                    }
                }
                if (!isset($_GET['cit']) && !isset($_GET['privat']) && isset($_GET['answer'])) {
                    $j = 'Пользователь [url=/info.php?id='.$user['id'].']'.$user['nick'].'[/url] ответил Вам в теме "[url='.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.$pages.'#'.$post_id.']'.$theme->name.'[/url]".
                    Ответ: [b]'.$text.'[/b]';
                    $type = 'answers';
                } elseif (isset($_GET['cit']) && isset($_GET['answer'])) {
                    $j = 'Пользователь [url=/info.php?id='.$user['id'].']'.$user['nick'].'[/url] процитировал Ваш комментарий в теме [url='.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.$pages.'#'.$post_id.']'.$theme->name.'[/url].
                    Цитата: [b]'.$text.'[/b]';
                    $type = 'quotes';
                } elseif (isset($_GET['privat'])) {
                    $j = 'Пользователь [url=/info.php?id='.$user['id'].']'.$user['nick'].'[/url] оставил Вам приватное сообщение в теме [url='.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.$pages.'#'.$post_id.']'.$theme->name.'[/url].
                    Сообщение: [b]'.$text.'[/b]';
                    $type = 'privat';
                }
                if (isset($user) && $user['id'] != $theme->id_user && !isset($_GET['answer']) && !isset($_GET['cit']) && !isset($_GET['privat'])) {
                    $j_t = 'Пользователь [url=/info.php?id='.$user['id'].']'.$user['nick'].'[/url] написал комментарий в Вашей теме "[url='.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.$pages.'#'.$post_id.']'.$theme->name.'[/url]".
                    Комментарий: [b]'.$text.'[/b]';
                    $db->query('INSERT INTO `f_journal` (`id_user`, `type`, `text`, `time`) VALUES( ?i, ?,  ?, ?i)',
                               [$theme->id_user, 'my_themes', $j_t, $time]);
                }
                if (isset($_GET['answer'])) {
                    $db->query('INSERT INTO `f_journal` (`id_user`, `type`, `text`, `time`) VALUES( ?i, ?string, ?, ?i)',
                               [$_GET['answer'], $type, $j, $time]);
                } elseif (isset($_GET['privat'])) {
                    $db->query('INSERT INTO `f_journal` (`id_user`, `type`, `text`, `time`) VALUES( ?i, ?string, ?, ?i)',
                               [$_GET['privat'], $type, $j, $time]);
                }
                $_SESSION['success'] = '<div class = "msg">Комментарий успешно добавлен.</div>';
                $post = $db->query('SELECT `id` FROM `forum_posts` WHERE `id` = '.$post_id)->object();
                if (isset($_POST['add_file']) && $post) {
                    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/'.$post->id.'/add_file');
                } else {
                    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
                }
                exit;
            }
        } elseif (isset($user) && isset($_POST['edit_post'])) {
            $edit_post = trim($_POST['post']);
            //$post_edit = $db->query('SELECT `count_edit` FROM `forum_posts` WHERE `id` = ?i', [$_POST['id_post']])->object();
            $data = [$edit_post, $time, $user['nick'], 1, $_POST['id_post']];
            $db->query('UPDATE `forum_posts` SET `text`=?, `last_edit`=?i, `who_edit`=?, `count_edit`=`count_edit`+?i WHERE `id`=?i', $data);
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html#'.intval($_POST['id_post']));
            exit;
        } elseif (isset($_POST['cancel_edit'])) {
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
            exit;
        } elseif (isset($user) && isset($_GET['act_posts']) && (isset($_POST['delete_posts']) || isset($_POST['hide_posts']) || isset($_POST['display_posts']))) {
            include_once 'action/actions_posts.php'; // Действия над постами.
        }
    }
    if (isset($user) && isset($vote) && $db->query('SELECT COUNT(*) FROM `forum_votes_var` WHERE `variant` = "" AND `id_vote` = '.$vote->id)->el() != 0) {
        $db->query('DELETE FROM `forum_votes_var` WHERE `id_vote` = ?i', [$vote->id]);
        $db->query('DELETE FROM `forum_votes` WHERE `id` = ?i', [$vote->id]);
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
        exit;
    }
    if (isset($user) && isset($_GET['journal_yes'])) {
        if ($db->query('SELECT COUNT(*) FROM `forum_journal` WHERE `id_theme` = ?i AND `id_user` = ?i', [$theme->id, $user['id']])->el() == 0) {
            $db->query('INSERT INTO `forum_journal` (`id_theme`, `id_user`) VALUES( ?i, ?i)', [$theme->id, $user['id']]);
            $_SESSION['success'] = '<div class = "msg">Вы успешно подписались на эту тему.</div>';
        }
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
        exit;
    } elseif (isset($user) && isset($_GET['journal_no'])) {
        if ($db->query('SELECT COUNT(*) FROM `forum_journal` WHERE `id_theme` = ?i AND `id_user` = ?i', [$theme->id, $user['id']])->el() != 0) {
            $db->query('DELETE FROM `forum_journal` WHERE  `id_theme` = ?i AND `id_user` = ?i', [$theme->id, $user['id']]);
            $_SESSION['success'] = '<div class = "msg">Вы успешно отписались от этой темы.</div>';
        }
        header('Location: ' . FORUM . '/' . $forum->id . '/' . $razdel->id . '/' . $theme->id . '.html');
        exit;
    }
    if (isset($_SESSION['success'])) {
        echo $_SESSION['success'];
        unset($_SESSION['success']);
    }
    ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?php echo  FORUM ?>'>Форум</a> / <a href = '<?php echo  FORUM.'/'.$forum->id ?>/'><?php echo  output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?php echo  output_text($razdel->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?php echo  output_text($theme->name, 1, 1, 0, 0, 0) ?></a>
    </div>
    <?php
    if ($theme->reason_close != NULL) {
        ?>
        <div class = 'err'><?php echo  $theme->reason_close ?></div>
        <?php
    }

    $person = $db->query('SELECT `id`, `nick`,`mylink`, `pol` FROM `user` WHERE `id` = '.$theme->id_user)->object();

    $k_page = k_page($k_post, $set['p_str']);
    $page = page($k_page);
    $start = $set['p_str']*$page-$set['p_str'];
    ?>
    <table class = 'post'>
        <tr>
            <td class = 'icon14'>
                <img src = '<?php echo  FORUM ?>/icons/gr.png' alt = '' <?php echo  ICONS ?> />
            </td>

            <td class = 'p_t'>
				<a href = '/<?php echo $person->mylink;?>'> <?php echo  nick($person->id)?> </a> (<?php echo  vremja($theme->time) ?>)
			</td>

        </tr>
        <tr>
            <td class = 'p_m' colspan = '2' style = 'word-wrap: break-word'>
                <?php echo  output_text($theme->description) ?>
            </td>
        </tr>
        <?php
        if (isset($vote)) {
            $all_votes = $db->query('SELECT COUNT(*) FROM `forum_vote_voices` WHERE `id_vote` = '.$vote->id)->el();
            ?>
            <tr>
                <td class = 'p_m' colspan = '2'>
                    <?php
                    $i = 0;
                    echo output_text($vote->name).'<br />';
                    $vars = $db->query('SELECT `id`, `variant` FROM `forum_votes_var` WHERE `id_vote` = ?i ORDER BY `id` ASC', [$vote->id]);
                    while ($var = $vars->object()) {
                        $i++;
                        $vote_var = $db->query('SELECT COUNT(*) FROM `forum_vote_voices` WHERE `id_vote` = '.$vote->id.' AND `id_variant` = '.$var->id)->el();
                        $procent = ($all_votes == 0) ? 0 : $vote_var/$all_votes*100;
                        $procent = sprintf("%u", $procent);
                        echo output_text($var->variant).' ('.$procent.'%) - '.$vote_var.' чел.';
                        if (isset($user) && $my_voice == 0) {
                            ?>
                            <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/vote=<?php echo  $var->id ?>'>
                            <?php
                        }
                        ?>
                        <div class = 'votes'>
                            <!--img src = '<?php echo  FORUM ?>/icons/votes/poll<?php echo  $i ?>.gif' alt = '<?php echo  $i ?>' style = 'height: 13px; width: <?php echo  $procent ?>%; <?php echo  ($procent > 0) ? 'border-right: 1px solid black;' : NULL ?>' /-->
                            <span style="background: url('<?php echo  FORUM ?>/icons/votes/poll<?php echo  $i ?>.gif') repeat-x;width: <?php echo  $procent ?>%;"></span>
                        </div>
                        <?php
                        if (isset($user) && $my_voice == 0) {
                            echo '</a>';
                        }
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td class = 'p_m' colspan = '2'>
                    Начало голосования: <?php echo  vremja($vote->time) ?><br />
                    <?php
                    if ($vote->time_end > time()) {
                        echo 'Завершение голосования: '.vremja($vote->time_end).'<br />';
                    } elseif ($vote->time_end < time() && $vote->time_end != 0) {
                        echo 'Голосование завершено.<br />';
                    }
                    ?>
                    Проголосовало: <?php echo  $all_votes ?> чел.
                </td>
            </tr>
            <?php
        }
        if (isset($user) && $my_report == 0 && !user_access('forum_them_edit') && $user['id'] != $theme->id_user && $theme->reason_close == NULL) {
            ?>
            <tr>
                <td class = 'p_m' colspan = '2' style = 'text-align: right'>
                    <img src = '<?php echo  FORUM ?>/icons/report.png' alt = '' style = 'width: 16px; height: 16px' /> <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/report.html'>Жалоба на тему</a>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
    <?php
    if ($theme->time_edit != 0 && $theme->id_admin != 0) {
        $admin = $db->query('SELECT `id`, `nick`,`mylink` FROM `user` WHERE `id` = '.$theme->id_admin)->object();
        ?>
        <div class = 'p_t'>Последний раз редактировалось
		<a href = '/<?php echo $admin->mylink;?>'>
		<?php echo nick($admin->id,null,0,1)?>
		</a> (<?php echo  vremja($theme->time_edit) ?>)</div>
        <?php
    }
    ?>
    <div class = 'p_m'>
        Комментарии (<?php echo  $k_post ?>)<br />
        Новые: <?php echo  (!isset($_SESSION['sort_'.$user['id']])) ? '<b>Вверху</b> | <a href = "'.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/sort_1">Внизу</a>' : '<a href = "'.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/sort_0">Вверху</a> | <b>Внизу</b>' ?>
        <?php
        if (isset($user) && $user['id'] != $theme->id_user) {
            $journal = $db->query('SELECT COUNT(*) FROM `forum_journal` WHERE `id_theme` = "'.$theme->id.'" AND `id_user` = '.$user['id'])->el();
            $theme_journal = ($journal == 0) ? '<a href = "'.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/journal_yes">Да</a> | Нет' : 'Да | <a href = "'.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/journal_no">Нет</a>';
            ?>
            <div style = 'text-align: right'>Следить за темой: <?php echo  $theme_journal ?></div>
            <?php
        }
        ?>
    </div>
    <?php
    if ($k_post == 0) {
        ?>
        <div class = 'err'>Никто ещё не комментировал эту тему.</div>
        <?php
    }
    if (isset($user) && ($theme->reason_close == NULL || ($theme->reason_close != NULL && user_access('forum_post_close')))) {
        $cit = 0; $privat = 0; $text = '';

        if (isset($_GET['cit'])) {
            $cit = intval($_GET['cit']);
            $answer = intval($_GET['answer']);
            // TODO:  охренень над этим экспериментатором!
            $text = $db->query('SELECT `nick` FROM `user` WHERE `id` = ?i', [$answer])->el() . ', ';//var_dump($text);
            ?>
            <form method = 'post' action = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/cit=<?php echo  $cit ?>/answer=<?php echo  $answer ?>' class="p_m">
            <?php
        } elseif (isset($_GET['privat'])) {
            $privat = intval($_GET['privat']);
            $text = $db->query('SELECT `nick` FROM `user` WHERE `id` = ?i', [$privat])->el() . ', ';
            ?>
            <form method = 'post' action = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/privat=<?php echo  $privat ?>' class="p_m">
            <?php
        } elseif (isset($_GET['answer'])) {
            $answer = intval($_GET['answer']);
            $text = $db->query('SELECT `nick` FROM `user` WHERE `id` = ?i', [$answer])->el() . ', ';
            ?>
            <form method = 'post' action = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/answer=<?php echo  $answer ?>' class="p_m">
            <?php
        } else {
            ?>
            <form method = 'post' action = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html' class="p_m">
            <?php
        }
        $cit = $db->query('SELECT `text` FROM `forum_posts` WHERE `id` = ?i', [$cit])->el();
        if (isset($_GET['cit']) && isset($_GET['answer'])) {
            ?>
            <div class = 'cit'>Цитирование сообщения: "<?php echo  output_text($cit) ?>"</div>
            <?php
        }
        ?>
        <textarea name = 'text' style = 'width: 92%; height: 50px'><?php echo  $text ?></textarea><br />
        <?php
        if ($user['set_translit'] == 1) {
            ?>
            <label><input type = 'checkbox' name = 'translit' value = '1' /> Транслит</label><br />
            <?php
        }
        if ($user['set_files'] == 1) {
            ?>
            <label><input type = 'checkbox' name = 'add_file' value = '1' /> Добавить файл</label><br />
            <?php
        }
        ?>
        <input type = 'submit' name = 'comment' value = 'Отправить' />
        </form>
        <?php
    }
    if (isset($_GET['act_posts'])) {
        ?>
        <form action = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/act_posts' method = 'post' style = 'padding: 0px'>
        <?php
    }
    ?>
    <table class = 'post'>
        <?php
        $post_count = $start;
        $sort = (!isset($_SESSION['sort_'.$user['id']])) ? 'DESC' : 'ASC';
        $limit = (isset($_GET['act_posts'])) ? NULL : ' LIMIT '.$start.', '.$set['p_str'];
        $posts = $db->query('SELECT * FROM `forum_posts` WHERE `id_theme` = '.$theme->id.' ORDER BY `id` ' . $sort . $limit);

        while ($post = $posts->object()) {

            $man = ($post->id_user != 0) ? $db->query('SELECT `id`, `nick`, `pol` FROM `user` WHERE `id` = '.$post->id_user)->object() : array();
            $man_id = ($man && $post->id_user != 0) ? $man->id : 0;
            $man_pol = ($man && $post->id_user != 0) ? $man->pol : 0;
            $man_nick = ($man && $post->id_user != 0) ? $man->nick : 'Система';

            $privat = ($post->privat == $user['id'] || ($post->id_user == $user['id'] && $post->privat != 0)) ? '<span style = "color: blue">Приватно</span>' : NULL;
            $admin = ($post->hide == 1) ? $db->query('SELECT `id`, `nick`, `pol` FROM `user` WHERE `id` = '.$post->id_admin)->object() : NULL;

            if ($post->privat == 0 || ($post->privat == $user['id'] || $post->id_user == $user['id']) || $user['group_access'] > 14) {
                $post_count++;
                echo '<a id = "'.$post->id.'"></a>';
                ?>
                <tr>
                    <td class = 'icon14'>
                       #<?php echo $post_count ?>
                    </td>
                    <td class = 'p_t'>
                        <?php
                        if (isset($_GET['act_posts'])) {
                            ?>
                            <input type = 'checkbox' name = 'act[]' value = '<?php echo  $post->id ?>' />
                            <?php
                        }
                        ?>
                       <?php echo nick($man_id)?> (<?php echo  vremja($post->time) ?>) <?php echo  $privat ?>

				   </td>
                </tr>
                <tr>
                    <td class = 'p_m' colspan = '2'>
                        <?php
                        if (isset($_GET['edit']) && $_GET['edit'] == $post->id) {
                            $edit_post = $db->query('SELECT `id`, `id_user`, `text` FROM `forum_posts` WHERE `id` = ?i', [$_GET['edit']])->object();
                            if (isset($user) && (user_access('forum_post_ed') || ((time()-$post->time < $set['forum_edit_post_time']) && $theme->reason_close == NULL && $user['id'] == $edit_post->id_user && $last_post == $edit_post->id))) {
                                ?>
                                <form action = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html' method = 'post'>
                                    <input type = 'hidden' name = 'id_post' value = '<?php echo  intval($_GET['edit']) ?>' />
                                    <textarea name = 'post' style = 'width: 92%; height: 50px'><?php echo  $edit_post->text ?></textarea><br />
                                    <input type = 'submit' name = 'edit_post' value = 'Изменить' /> <input type = 'submit' name = 'cancel_edit' value = 'Отмена' />
                                </form>
                                <?php
                            } else {
                                header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
                                exit;
                            }
                        } else {
                            if ($post->hide == 1) {
                                ?>
                                <span style = 'color: Firebrick'><b>Комментарий скрыл<?php echo  ($admin->pol == 0) ? 'а' : NULL ?> <a href = '/info.php?id=<?php echo  $admin->id ?>'><?php echo  $admin->nick ?></a></b></span>
                                <?php
                                if (user_access('forum_post_ed')) {
                                    echo '<br /><s>'.output_text($post->text).'</s><br />';
                                }
                            } else {
                                if ($post->rating > 0 && $db->query('SELECT MAX(`rating`) FROM `forum_posts` WHERE `rating` > '.RATING.' AND `id_theme` = '.$theme->id)->el() == $post->rating) {
                                    ?>
                                    <span style = 'float: right; padding-left: 10px; color: <?php echo  COLOR ?>'>
                                        Лучший ответ
                                    </span>
                                    <?php
                                }
                                if ($post->cit != 0 && $db->query('SELECT COUNT(*) FROM `forum_posts` WHERE `id` = '.$post->cit)->el() != 0) {
                                    $cit_post = $db->query('SELECT `id_user`, `text`, `time` FROM `forum_posts` WHERE `id` = '.$post->cit)->object();
                                    $user_cit = $db->query('SELECT `id`, `nick` FROM `user` WHERE `id` = '.$cit_post->id_user)->object();
                                    ?>
                                    <div class = 'cit'>
                                        <b><?php echo  $user_cit->nick ?> (<?php echo  vremja($cit_post->time) ?>):</b><br />
                                        <?php echo  output_text($cit_post->text) ?>
                                    </div>
                                    <?php
                                }
                                echo output_text($post->text).'<br />';
                            }
                        }
                        if ($post->hide == 0 || ($post->hide == 1 && user_access('forum_post_ed'))) {

                            if ($db->query('SELECT COUNT(*) FROM `forum_post_files` WHERE `id_post` = '.$post->id)->el()) {
                                $files = $db->query('SELECT * FROM `forum_post_files` WHERE `id_post` = '.$post->id);
                                echo '<br /><b>Прикреплённые файлы:</b><br />';
                                while ($file = $files->object()) {
                                    $ras = strtolower(preg_replace('#^.*\.#', NULL, $file->name));
                                    if ($ras == 'jpg' || $ras == 'jpeg' || $ras == 'gif' || $ras == 'png' || $ras == 'bmp' || $ras == 'ico') {
                                        $icon = FORUM.'/files/'.$file->name;
                                    } elseif ($ras == '3gp' || $ras == 'mp4' || $ras == 'avi' || $ras == 'mpeg' || $ras == 'flv' || $ras == 'wmv' || $ras == 'mkv') {
                                        $icon = FORUM.'/icons/files/video.png';
                                    } elseif ($ras == 'docx' || $ras == 'doc' || $ras == 'docm' || $ras == 'dotx' || $ras == 'dot' || $ras == 'dotm') {
                                        $icon = FORUM.'/icons/files/word.png';
                                    } elseif ($ras == 'mp1' || $ras == 'mp2' || $ras == 'mp3' || $ras == 'wav' || $ras == 'aif' || $ras == 'ape' || $ras == 'flac' || $ras == 'ogg' || $ras == 'asf' || $ras == 'wma') {
                                        $icon = FORUM.'/icons/files/music.png';
                                    } elseif ($ras == 'zip' || $ras == 'rar' || $ras == 'tar' || $ras == '7-zip' || $ras == 'gzip' || $ras == 'jar' || $ras == 'jad' || $ras == 'war' || $ras == 'xar') {
                                        $icon = FORUM.'/icons/files/archive.png';
                                    } elseif ($ras == 'txt' || $ras == 'xml') {
                                        $icon = FORUM.'/icons/files/txt.png';
                                    } elseif ($ras == 'pdf') {
                                        $icon = FORUM.'/icons/files/pdf.png';
                                    } elseif ($ras == 'psd') {
                                        $icon = FORUM.'/icons/files/psd.png';
                                    } else {
                                        $icon = FORUM.'/icons/files/file.png';
                                    }
                                    ?>
                                    <table style = 'width: 100%' cellspacing = '0' cellpadding = '0' border = '0'>
                                        <tr>
                                            <td class = 'p_t' style = 'width: 50px; border-right: none'>
                                                <img src = '<?php echo  $icon ?>' alt = '' style = 'width: 50px; height: 50px;' />
                                            </td>
                                            <td class = 'p_t' style = 'border-left: none'>
                                                <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/download_<?php echo  $file->id ?>'>
                                                    <?php echo  output_text($file->real_name, 1, 1, 0, 0, 0) ?>
                                                </a> (<?php echo  size_file($file->size) ?>)<br />
                                                Скачали: <?php echo  $file->count_downloads ?> чел.
                                                <?php
                                                if ($user['id'] == $post->id_user || user_access('forum_post_ed')) {
                                                    ?>
                                                    <br />[ <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/del_file='.$file->id ?>'>Удалить файл</a> ]
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                    </table>
                                    <?php
                                }
                                echo '<br />';
                            }
                        }
                        if ($post->count_edit != 0) {
                            echo '('.$post->count_edit.') Посл. ред. '.vremja($post->last_edit).' - '.$post->who_edit.'<br />';
                        }
                        if (isset($user) && ((isset($_GET['edit']) && $post->id != $_GET['edit']) || !isset($_GET['edit']))) {
                            if ($user['id'] != $post->id_user && $post->id_user != 0 && $post->hide == 0 && ($theme->reason_close == NULL || ($theme->reason_close != NULL && user_access('forum_post_close')))) {
                                ?>
                                <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/answer=<?php echo  $man_id ?>'>Отв</a>
                                 | <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/cit=<?php echo  $post->id ?>/answer=<?php echo  $man_id ?>'>Цит</a>
                                 | <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/privat=<?php echo  $man_id ?>'>Прив</a>
                                <?php
                            }
                            if ($post->id_user != 0 && (user_access('forum_post_ed') || ((time()-$post->time < $set['forum_edit_post_time']) && $theme->reason_close == NULL && $user['id'] == $post->id_user && $last_post == $post->id))) {
                                $razd =  (user_access('forum_post_ed') && $post->hide == 0 && $post->id_user != $user['id']) ? ' | ' : NULL;
                                echo $razd.'<a href = "'.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/edit_'.$post->id.'#'.$post->id.'">Ред</a>';
                            }
                            if (user_access('forum_post_ed')) {
                                $razd = ($post->id_user == 0) ? NULL : ' | ';
                                echo $razd;
                                ?>
                                <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/delete_post=<?php echo  $post->id ?>'>Удал</a>
                                <?php
                                if ($post->hide == 0) {
                                    ?>
                                     | <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/hide=<?php echo  $post->id ?>'>Скрыть</a>
                                    <?php
                                } else {
                                    ?>
                                     | <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/display=<?php echo  $post->id ?>'>Показ</a>
                                    <?php
                                }
                            }
                        }
                        if ($user['id'] != $post->id_user && $post->id_user != 0) {
                            $_post = (isset($_GET['post'])) ? $db->query('SELECT `id`, `rating`, `text` FROM `forum_posts` WHERE `id` = ?i', [$_GET['post']])->object() : NULL;
                            if (isset($user) && isset($_GET['like']) && $db->query('SELECT COUNT(*) FROM `forum_post_rating` WHERE `id_user` = ?i AND `id_post` = ?i AND `type` = ?string', [$user['id'], $_GET['post'], 0])->el() == 0) {
                                $plus = ($db->query('SELECT COUNT(*) FROM `forum_post_rating` WHERE`id_user` = ?i AND `id_post` = ?i AND `type` = ?string', [$user['id'], $_GET['post'], 1])->el() != 0) ? 2 : 1;
                                if ($db->query('SELECT COUNT(*) FROM `forum_post_rating` WHERE `id_user` = ?i AND `id_post` = ?i', [$user['id'], $_GET['post']])->el() != 0) {
                                    $db->query('UPDATE `forum_post_rating` SET `type` = "0" WHERE `id_post` = ?i AND `id_theme` = ?i AND `id_user` = ?i', [$_GET['post'], $theme->id, $user['id']]);
                                } else {
                                    $db->query('INSERT INTO `forum_post_rating` (`id_theme`, `id_user`, `type`, `id_post`) VALUES( ?i, ?i, ?string, ?i)',
                                               [$theme->id, $user['id'], 0, $_GET['post']]);
                                }// ($_post->rating+$plus).' WHERE `id` = '.intval($_GET['post']
                                $db->query('UPDATE `forum_posts` SET `rating`=`rating`+?i WHERE `id` = ?i', [$plus, $_GET['post']]);
                                $_SESSION['success'] = '<div class = "msg">Вы успешно проголосвали за пост.</div>';
                                $page = (is_numeric($_GET['page'])) ? intval($_GET['page']) : trim($_GET['page']);
                                if ($page != 'end' && !is_numeric($page)) {
                                    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
                                } else {
                                    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/page='.$page.'#'.intval($_GET['post']));
                                }
                                exit;
                            } elseif (isset($user) && isset($_GET['dislike']) && $db->query('SELECT COUNT(*) FROM `forum_post_rating` WHERE `id_user` = ?i AND `id_post` = ?i AND `type` = ?string', [$user['id'], $_GET['post'], 1])->el() == 0) {
                                $minus = ($db->query('SELECT COUNT(*) FROM `forum_post_rating` WHERE `id_user` = ?i AND `id_post` = ?i AND `type` = ?string', [$user['id'], $_GET['post'], 0])->el() != 0) ? 2 : 1;
                                if ($db->query('SELECT COUNT(*) FROM `forum_post_rating` WHERE `id_user` = ?i AND `id_post` = ?i', [$user['id'], $_GET['post']])->el() != 0) {
                                    $db->query('UPDATE `forum_post_rating` SET `type` = "1" WHERE `id_post` = ?i AND `id_theme` = ?iAND `id_user` = ?i', [$_GET['post'], $theme->id, $user['id']]);
                                } else {
                                    $db->query('INSERT INTO `forum_post_rating` (`id_theme`, `id_user`, `type`, `id_post`) VALUES( ?i, ?i, ?string, ?i)',
                                               [$theme->id, $user['id'], 1, $_GET['post']]);
                                }
                                if (($_post->rating-$minus) < RATING2) {
                                    $persons = $db->query('SELECT `id`, `group_access` FROM `user` WHERE `group_access` > "2"');
                                    while ($person = $persons->object()) {
                                        $access = $db->query('SELECT COUNT(*) FROM `user_group_access` WHERE `id_group` = '.$person->group_access.' AND `id_access` = "forum_post_ed"')->el();
                                        if ($access != 0) {
                                            $msg = 'Комментарий в теме [url='.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/page='.$page.'#'.$_post->id.']'.$theme->name.'[/url] набрал много отрицательных отзывов. Есть подозрение, что он нарушает правила форума.
                                            [b]Сам комментарий:[/b]
                                            '.output_text($_post->text);
                                            $db->query('INSERT INTO `mail` (`id_user`, `id_kont`, `msg`, `time`) VALUES( ?i, ?i, ?, ?i)',
                                                       [0, $person->id, $msg, $time]);
                                        }
                                    }
                                }
                                $db->query('UPDATE `forum_posts` SET `rating`=`rating`-?i WHERE `id` = ?i', [$minus, $_GET['post']]);
                                $_SESSION['success'] = '<div class = "msg">Вы успешно проголосвали за пост.</div>';
                                $page = (is_numeric($_GET['page'])) ? intval($_GET['page']) : trim($_GET['page']);
                                if ($page != 'end' && !is_numeric($page)) {
                                    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
                                } else {
                                    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/page='.$page.'#'.intval($_GET['post']));
                                }
                                exit;
                            }
                            ?>
                            <span style = 'float: right; padding-left: 5px'>
                                <?php
                                if (isset($user) && $db->query('SELECT COUNT(*) FROM `forum_post_rating` WHERE `id_user` = '.$user['id'].' AND `id_post` = '.$post->id.' AND `type` = "0"')->el() == 0) {
                                    ?>
                                    <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/page='.$page.'/'.$post->id.'/like' ?>'><img src = '<?php echo  FORUM ?>/icons/like.png' alt = '' /></a>
                                    <?php
                                } else {
                                    ?>
                                    <img src = '<?php echo  FORUM ?>/icons/like.png' alt = '' />
                                    <?php
                                }
                                echo '('.$post->rating.')';
                                if (isset($user) && $db->query('SELECT COUNT(*) FROM `forum_post_rating` WHERE `id_user` = '.$user['id'].' AND `id_post` = '.$post->id.' AND `type` = "1"')->el() == 0) {
                                    ?>
                                    <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/page='.$page.'/'.$post->id.'/dislike' ?>'><img src = '<?php echo  FORUM ?>/icons/dislike.png' alt = '' /></a>
                                    <?php
                                } else {
                                    ?>
                                    <img src = '<?php echo  FORUM ?>/icons/dislike.png' alt = '' />
                                    <?php
                                }
                                ?>
                            </span>
                            <?php
                        } else {
                            ?>
                            <span style = 'float: right; padding-left: 5px'>
                                (<?php echo  $post->rating ?>)
                            </span>
                            <?php
                        }
                        ?>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </table>
    <?php
    if (isset($_GET['act_posts'])) {
        ?>
        <input type = 'submit' name = 'delete_posts' value = 'Удалить' /> <input type = 'submit' name = 'hide_posts' value = 'Скрыть' /> <input type = 'submit' name = 'display_posts' value = 'Показать' />
        </form>
        <?php
    }
    if ($k_page > 1) {
        str(FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/', $k_page, $page);
    }
    $who = $db->query('SELECT COUNT(*) FROM `user` WHERE `date_last` > ?i AND `url` LIKE "?e%"', [(time()-600), FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id])->el();
    $count_files = $db->query('SELECT COUNT(*) FROM `forum_post_files` WHERE `id_theme` = '.$theme->id)->el();
    ?>
    <div class = 'p_m'>
        <img src = '<?php echo  FORUM ?>/icons/who.png' alt = '' <?php echo  ICONS ?> /> <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/who.html'>Сейчас в теме</a> [<?php echo  $who ?>]
    </div>
    <div class = 'p_m'>
        <img src = '<?php echo  FORUM ?>/icons/files.png' alt = '' <?php echo  ICONS ?> /> <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/files.html'>Файлы в теме</a> [<?php echo  $count_files ?>]
    </div>
    <?php
    if ((user_access('forum_them_edit') || $user['id'] == $theme->id_user) && ($theme->reason_close == NULL || ($theme->reason_close != NULL && user_access('forum_post_close')))) {
        ?>
        <div class = 'p_m'>
            <img src = '<?php echo  FORUM ?>/icons/edit.png' alt = '' <?php echo  ICONS ?> /> <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/edit_theme.html'>Редактировать</a>
        </div>
        <?php
        if ($db->query('SELECT COUNT(*) FROM `forum_votes` WHERE `id_theme` = '.$theme->id)->el() == 0 && ($theme->reason_close == NULL || ($theme->reason_close != NULL && user_access('forum_post_close'))) && $user['id'] == $theme->id_user) {
            ?>
            <div class = 'p_m'>
                <img src = '<?php echo  FORUM ?>/icons/vote.png' alt = '' <?php echo  ICONS ?> /> <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/create_vote.html'>Создать голосование</a>
            </div>
            <?php
        } elseif ($db->query('SELECT COUNT(*) FROM `forum_votes` WHERE `id_theme` = '.$theme->id)->el() != 0 && ($theme->reason_close == NULL || ($theme->reason_close != NULL && user_access('forum_post_close'))) && $user['id'] == $theme->id_user) {
            ?>
            <div class = 'p_m'>
                <img src = '<?php echo  FORUM ?>/icons/vote.png' alt = '' <?php echo  ICONS ?> /> <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/edit_vote.html'>Редактировать голосование</a>
            </div>
            <?php
        }
        if (user_access('forum_them_edit')) {
            echo '<div class = "p_m">';
            if ($theme->reason_close == NULL && user_access('forum_post_close')) {
                ?>
                <img src = '<?php echo  FORUM ?>/icons/delete.png' alt = '' <?php echo  ICONS ?> /> <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/close_theme.html'>Закрыть</a>
                <?php
            } elseif ($theme->reason_close != NULL && user_access('forum_post_close')) {
                ?>
                <img src = '<?php echo  FORUM ?>/icons/delete.png' alt = '' <?php echo  ICONS ?> /> <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/open_theme'>Открыть</a>
                <?php
            }
            if (user_access('forum_post_ed')) {
                ?>
                 | <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/cleare_theme.html'>Очистить</a>
                <?php
            }
            echo ' тему</div>';
        }
        if (user_access('forum_post_ed') && user_access('forum_them_edit')) {
            ?>
            <div class = 'p_m'>
                <?php
                if (!isset($_GET['act_posts'])) {
                    ?>
                    <img src = '<?php echo  FORUM ?>/icons/edit.png' alt = '' <?php echo  ICONS ?> /> <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/act_posts'>Действия над постами</a>
                    <?php
                } else {
                    ?>
                    <img src = '<?php echo  FORUM ?>/icons/edit.png' alt = '' <?php echo  ICONS ?> /> <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'>Отменить действие</a>
                    <?php
                }
                ?>
            </div>
            <?php
        }
    }
    ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?php echo  FORUM ?>'>Форум</a> / <a href = '<?php echo  FORUM.'/'.$forum->id ?>/'><?php echo  output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?php echo  output_text($razdel->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?php echo  output_text($theme->name, 1, 1, 0, 0, 0) ?></a>
    </div>
    <?php
}

?>