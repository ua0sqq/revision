<?php

if (!user_access('forum_them_edit')) {
    header('Location: '.FORUM);
    exit;
} else {
    $set['title'] = 'Жалобы на темы';
    include_once H . 'sys/inc/thead.php';
    title().aut();

    if (isset($_GET['delete'])) {
        $this_report = intval($_GET['delete']);
        if ($db->query('SELECT COUNT(*) FROM `forum_reports` WHERE `id` = ?i', [$this_report])->el() != 0) {
            $db->query('DELETE FROM `forum_reports` WHERE `id` = ?i', [$this_report]);
        }
        header('Location: '.FORUM.'/reports.html');
        exit;
    }
    $k_post = $db->query('SELECT COUNT(*) FROM `forum_reports`')->el();
    $k_page = k_page($k_post, $set['p_str']);
    $page = page($k_page);
    $start = $set['p_str']*$page-$set['p_str'];
    if ($k_post == 0) {
        ?>
        <div class = 'err'>Жалоб на темы никто не оставлял.</div>
        <?php

    } ?>
    <table class = 'post'>
        <?php
        $reports = $db->query('SELECT * FROM `forum_reports` ORDER BY `id` ASC LIMIT ?i, ?i', [$start, $set['p_str']]);
    while ($report = $reports->object()) {
        $who = $db->query('SELECT `id`,`nick` FROM `user` WHERE `id` = '.$report->id_user)->object();
        $theme = $db->query('SELECT `id`, `id_razdel`, `name` FROM `forum_themes` WHERE `id` = '.$report->id_theme)->object();
        $razdel = $db->query('SELECT `id`, `id_forum` FROM `forum_razdels` WHERE `id` = '.$theme->id_razdel)->object();
        $forum = $db->query('SELECT `id` FROM `forum` WHERE `id` = '.$razdel->id_forum)->object(); ?>
            <tr>
                <td class = 'icon14'>
                    <img src = '<?php echo  FORUM ?>/icons/report.png' alt = '' <?php echo  ICONS ?> />
                </td>
                <td class = 'p_t'>
                    Тема: <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?php echo  output_text($theme->name, 1, 1, 0, 0, 0) ?></a>
                    (<a href = '/info.php?id=<?php echo  $who->id ?>'><?php echo  $who->nick ?></a>)
                </td>
                <td class = 'icon14'>
                    <a href = '<?php echo  FORUM ?>/reports/delete_<?php echo  $report->id ?>'><img src = '<?php echo  FORUM ?>/icons/delete.png' alt = '' <?php echo  ICONS ?> /></a>
                </td>
            </tr>
            <tr>
                <td class = 'p_m' colspan = '2'>
                    <?php echo  output_text($report->text) ?>
                </td>
            </tr>
            <?php

    } ?>
    </table>
    <?php
    if ($k_page > 1) {
        str(FORUM.'/reports/', $k_page, $page);
    } ?>
    <div class = 'p_m' style = 'text-align: right'><a href = '<?php echo  FORUM ?>'>Вернуться в форум</a></div>
    <?php

}

?>