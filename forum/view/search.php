<?php

$set['title'] = 'Поиск по форуму';
include_once H . 'sys/inc/thead.php';
title().aut();

if (isset($_POST['search']) || isset($_SESSION['search'])) {
    if (!isset($_POST['search']) && isset($_SESSION['search'])) {
        $search = $_SESSION['search'];
    } else {
        $search = trim($_POST['text']);
        $_SESSION['search'] = mb_strtolower($search);
    }
    if (mb_strlen($search) < 3) {
        ?>
        <div class = 'err'>Слишком короткий текст.</div>
        <?php

    } else {
        $k_post = $db->query('SELECT COUNT(*) FROM `forum_themes` WHERE `name` LIKE "%?e%" OR `description` LIKE "%?e%"', [$_SESSION['search'], $_SESSION['search']])->el();
        $k_page = k_page($k_post, $set['p_str']);
        $page = page($k_page);
        $start = $set['p_str']*$page-$set['p_str'];
        if ($k_post == 0) {
            ?>
            <div class = 'err'>По Вашему запросу ничего не найдено.</div>
            <?php

        } else {
            ?>
            <div class = 'p_m' style = 'text-align: right'>Вы ввели: <b><?php echo  output_text($_SESSION['search']) ?></b></div>
            <table class = 'post'>
                <?php
                $themes = $db->query('SELECT * FROM `forum_themes` WHERE `name` LIKE "%?e%" OR `description` LIKE "%?e%" ORDER BY `time_post` ASC LIMIT ?i, ?i',
                                     [$_SESSION['search'], $_SESSION['search'], $start, $set['p_str']]);
            while ($theme = $themes->object()) {
                $theme->name = str_replace($_SESSION['search'], '[color=yellow]'.$_SESSION['search'].'[/color]', mb_strtolower($theme->name));
                $theme->description = str_replace($_SESSION['search'], '[color=yellow]'.$_SESSION['search'].'[/color]', mb_strtolower($theme->description));
                $razdel = $db->query('SELECT `id`, `id_forum` FROM `forum_razdels` WHERE `id` = '.$theme->id_razdel)->object();
                $forum = $db->query('SELECT `id`, `access` FROM `forum` WHERE `id` = '.$razdel->id_forum)->object();
                if ($forum->access == 0 || (isset($user) && $forum->access == 1 && $user['group_access'] > 7) || (isset($user) && $forum->access == 2 && $user['group_access'] > 1)) {
                    $count_posts = $db->query('SELECT COUNT(*) FROM `forum_posts` WHERE `id_theme` = '.$theme->id)->el();
                    if ($theme->reason_close != null) {
                        $type = '_close';
                    } elseif ($theme->type == 1) {
                        $type = '_up';
                    } else {
                        $type = null;
                    } ?>
                        <tr>
                            <td class = 'icon14'>
                                <img src = '<?php echo  FORUM ?>/icons/theme<?php echo  $type ?>.png' alt = '' <?php echo  ICONS ?> />
                            </td>
                            <td class = 'p_t'>
                                <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?php echo  output_text($theme->name) ?></a> (<?php echo  $count_posts ?>) - <?php echo  vremja($theme->time) ?>
                            </td>
                        </tr>
                        <tr>
                            <td class = 'p_m' colspan = '2'>
                                <?php echo  output_text($theme->description) ?>
                            </td>
                        </tr>
                        <?php

                } else {
                    continue;
                }
            } ?>
            </table>
            <?php
            if ($k_page > 1) {
                str(FORUM.'/search/', $k_page, $page);
            }
        }
    }
}
?>
<form action = '<?php echo  FORUM ?>/search.html' method = 'post' class="p_m">
    <b>Введите текст (мин. 3 символа):</b><br />
    <input type = 'text' name = 'text' value = '<?php echo  (isset($_SESSION['search'])) ? output_text($_SESSION['search']) : null ?>' /><br />
    <input type = 'submit' name = 'search' value = 'Поиск' />
</form>
<div class = 'p_m' style = 'text-align: right'><a href = '<?php echo  FORUM ?>'>Вернуться в форум</a></div>