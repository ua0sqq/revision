<?php

$forum = $db->query('SELECT * FROM `forum` WHERE `id` = ?i', [$_GET['forum']])->object();

if (!$forum || ($forum->access == 1 && $user['group_access'] < 8) || ($forum->access == 2 && $user['group_access'] < 3)) {
    header('Location: '.FORUM);
    exit;
} else {
    $set['title'] = (isset($_GET['create_razdel'])) ? output_text($forum->name, 1, 1, 0, 0, 0).' - создание раздела' : 'Подфорум - '.output_text($forum->name, 1, 1, 0, 0, 0);
    include_once '../sys/inc/thead.php';
    title().aut();

    if (isset($_GET['create_razdel'])) {
        include_once 'action/create_razdel.php'; // Создание раздела.
    } elseif (isset($_GET['edit_razdel'])) {
        include_once 'action/edit_razdel.php'; // Редактирование раздела.
    } else {
        ?>
        <div class = 'menu_razd' style = 'text-align: left'>
            <a href = '<?php echo  FORUM ?>'>Форум</a> / <a href = '<?php echo  FORUM.'/'.$forum->id ?>/'><?php echo  output_text($forum->name, 1, 1, 0, 0, 0) ?></a>
        </div>
        <?php

    }
    if (isset($_SESSION['msg'])) {
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
    }
    if (user_access('forum_razd_create')) {
        ?>
        <div class = 'p_m' style = 'text-align: right'><a href = '<?php echo  FORUM.'/'.$forum->id ?>/create_razdel.html'>Создать раздел</a></div>
        <?php

    }
    $k_post = $db->query('SELECT COUNT(*) FROM `forum_razdels` WHERE `id_forum` = '.$forum->id)->el();
    $k_page = k_page($k_post, $set['p_str']);
    $page = page($k_page);
    $start = $set['p_str']*$page-$set['p_str'];
    if ($k_post == 0) {
        ?>
        <div class = 'err'>Разделы в этом подфоруме ещё не созданы.</div>
        <?php

    } else {
        ?>
        <table class = 'post'>
            <?php
            $razdels = $db->query('SELECT * FROM `forum_razdels` WHERE `id_forum` = '.$forum->id.' ORDER BY `number` ASC LIMIT '.$start.', '.$set['p_str']);
        while ($razdel = $razdels->object()) {
            $count_themes = $db->query('SELECT COUNT(*) FROM `forum_themes` WHERE `id_razdel` = '.$razdel->id)->el(); ?>
                <tr>
                    <td class = 'icon14'>
                        <img src = '<?php echo  FORUM ?>/icons/razdel.png' alt = '' <?php echo  ICONS ?> />
                    </td>
                    <td class = 'p_t'>
                        <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?php echo  output_text($razdel->name, 1, 1, 0, 0, 0) ?></a> (<?php echo  $count_themes ?>)
                    </td>
                    <?php
                    if (user_access('forum_razd_edit')) {
                        ?>
                        <td class = 'icon14'>
                            <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id ?>/edit_razdel.html'><img src = '<?php echo  FORUM ?>/icons/edit.png' alt = '' <?php echo  ICONS ?> /></a>
                        </td>
                        <?php

                    } ?>
                </tr>
                <?php
                if ($razdel->description != null && $razdel->output == 0) {
                    ?>
                    <tr>
                        <td class = 'p_m' colspan = '3'>
                            <?php echo  output_text($razdel->description, 1, 1, 0, 1, 1) ?>
                        </td>
                    </tr>
                    <?php

                } elseif ($razdel->output == 1 && $count_themes > 0) {
                    ?>
                    <tr>
                        <td class = 'p_m' colspan = '3'>
                            <?php
                            $themes = $db->query('SELECT `id`, `name`, `reason_close`, `type` FROM `forum_themes` WHERE `id_razdel` = '.$razdel->id.' ORDER BY `time` DESC LIMIT 3');
                    while ($theme = $themes->object()) {
                        if ($theme->type == 1) {
                            $type = '_up';
                        } elseif ($theme->reason_close != null) {
                            $type = '_close';
                        } else {
                            $type = null;
                        } ?>
                                <img src = '<?php echo  FORUM ?>/icons/theme<?php echo  $type ?>.png' alt = '' style = 'width: 16px; height: 16px' /> <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?php echo  output_text($theme->name, 1, 1, 0, 0, 0) ?></a><br />
                                <?php

                    } ?>
                        </td>
                    </tr>
                    <?php

                }
        } ?>
        </table>
        <?php
        if ($k_page > 1) {
            str(FORUM.'/'.$forum->id.'/', $k_page, $page);
        }
    } ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?php echo  FORUM ?>'>Форум</a> / <a href = '<?php echo  FORUM.'/'.$forum->id ?>/'><?php echo  output_text($forum->name, 1, 1, 0, 0, 0) ?></a>
    </div>
    <?php

}

?>