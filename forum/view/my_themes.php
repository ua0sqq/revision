<?php

$set['title'] = 'Мои темы';
include_once '../sys/inc/thead.php';
title().aut();

$k_post = $db->query('SELECT COUNT(*) FROM `forum_themes` WHERE `id_user` = ?i', [$user['id']])->el();

if ($k_post == 0) {
    ?>
    <div class = 'err'>За последние 24 часа не было создано ни одной темы.</div>
    <?php

} else {
    $k_page = k_page($k_post, $set['p_str']);
    $page = page($k_page);
    $start = $set['p_str']*$page-$set['p_str']; ?>
    <table class = 'post'>
        <?php
        $themes = $db->query('SELECT * FROM `forum_themes` WHERE `id_user` = ?i ORDER BY `id` DESC LIMIT ?i, ?i', [$user['id'], $start, $set['p_str']]);
    while ($theme = $themes->object()) {
        $creater = $db->query('SELECT `id`, `nick` FROM `user` WHERE `id` = '.$theme->id_user)->object();
        $razdel = $db->query('SELECT `id`, `id_forum` FROM `forum_razdels` WHERE `id` = '.$theme->id_razdel)->object();
        $forum = $db->query('SELECT `id` FROM `forum` WHERE `id` = '.$razdel->id_forum)->object();
        $hide = (user_access('forum_post_ed')) ? null : '`hide` = "0" AND';
        $last_post = $db->query('SELECT `id`, `hide`, `id_user`, `time` FROM `forum_posts` WHERE '.$hide.' `id_theme` = '.$theme->id.' ORDER BY `id` DESC')->object();

        $who = ($last_post && $last_post->id_user != 0) ? $db->query('SELECT `id`, `nick` FROM `user` WHERE `id` = '.$last_post->id_user)->object() : array();
        $who_id = ($last_post && $last_post->id_user != 0) ? $who->id : 0;
        $who_nick = ($last_post && $last_post->id_user != 0) ? $who->nick : 'Система'; ?>
            <tr>
                <td class = 'icon14'>
                    <img src = '<?php echo  FORUM ?>/icons/theme.png' alt = '' <?php echo  ICONS ?> />
                </td>
                <td class = 'p_t'>
                    <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?php echo  output_text($theme->name, 1, 1, 0, 0, 0) ?></a> (<?php echo  vremja($theme->time) ?>)
                </td>
            </tr>
            <tr>
                <?php
                if ($last_post) {
                    ?>
                    <td class = 'p_m' colspan = '2'>
                        <b>Автор: <a href = '/info.php?id=<?php echo  $theme->id_user ?>'><?php echo  $creater->nick ?></a> | Посл.:</b> <a href = '/info.php?id=<?php echo  $who_id ?>'><?php echo  $who_nick ?></a> (<?php echo  vremja($last_post->time) ?>)
                    </td>
                    <?php

                } else {
                    $continue = (mb_strlen($theme->description) > 150) ? '.....' : null; ?>
                    <td class = 'p_m' colspan = '2'>
                        <?php echo  output_text(mb_substr($theme->description, 0, 150), 1, 1, 0, 1, 1).$continue ?>
                    </td>
                    <?php

                } ?>
            </tr>
            <?php

    } ?>
    </table>
    <?php
    if ($k_page > 1) {
        str(FORUM.'/my_themes/', $k_page, $page);
    } ?>
    <div class = 'p_m' style = 'text-align: right'><a href = '<?php echo  FORUM ?>'>Вернуться в форум</a></div>
    <?php

}

?>