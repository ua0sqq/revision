<?php

$forum = $db->query('SELECT * FROM `forum` WHERE `id` = ?i', [$_GET['forum']])->object();
$razdel = $db->query('SELECT * FROM `forum_razdels` WHERE `id_forum` = ?i AND `id` = ?i', [$forum->id, $_GET['razdel']])->object();

if (!$razdel || !$forum || ($forum->access == 1 && $user['group_access'] < 8) || ($forum->access == 2 && $user['group_access'] < 3)) {
    header('Location: '.FORUM);
    exit;
} else {
    $set['title'] = (isset($_GET['create_theme'])) ? output_text($razdel->name, 1, 1, 0, 0, 0).' - создание темы' : 'Раздел - '.output_text($razdel->name, 1, 1, 0, 0, 0);
    include_once '../sys/inc/thead.php';
    title().aut();

    if (isset($user) && isset($_GET['create_theme'])) {
        include_once 'action/create_theme.php'; // Создание темы.
    } ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?php echo  FORUM ?>'>Форум</a> / <a href = '<?php echo  FORUM.'/'.$forum->id ?>/'><?php echo  output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?php echo  output_text($razdel->name, 1, 1, 0, 0, 0) ?></a>
    </div>
    <?php
    if (isset($_SESSION['msg'])) {
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
    }
    if (isset($user) && $razdel->type == 0 || ($razdel->type == 1) && $user['group_access'] > 2) {
        ?>
        <div class = 'p_m' style = 'text-align: right'><a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id ?>/create_theme.html'>Создать тему</a></div>
        <?php

    }
    $k_post = $db->query('SELECT COUNT(*) FROM `forum_themes` WHERE `id_razdel` = '.$razdel->id)->el();
    $k_page = k_page($k_post, $set['p_str']);
    $page = page($k_page);
    $start = $set['p_str']*$page-$set['p_str'];
    if ($k_post == 0) {
        ?>
        <div class = 'err'>Тем в этом разделе ещё не создавали.</div>
        <?php

    } else {
        ?>
        <table class = 'post'>
            <?php
            $themes = $db->query('SELECT * FROM `forum_themes` WHERE `id_razdel` = ?i ORDER BY `type` DESC, `time_post` DESC LIMIT ?i, ?i', [$razdel->id, $start, $set['p_str']]);
        while ($theme = $themes->object()) {
            $creater = $db->query('SELECT `id`, `nick` FROM `user` WHERE `id` = '.$theme->id_user)->object();
            $count_posts = $db->query('SELECT COUNT(*) FROM `forum_posts` WHERE `id_theme` = '.$theme->id)->el();
            $hide = (user_access('forum_post_ed')) ? null : '`hide` = "0" AND';
            $last_post = $db->query('SELECT `id`, `hide`, `id_user`, `time` FROM `forum_posts` WHERE '.$hide.' `id_theme` = '.$theme->id.' ORDER BY `id` DESC')->object();
            if ($last_post && $last_post->id_user != 0) {
                $who = $db->query('SELECT `id`, `nick` FROM `user` WHERE `id` = '.$last_post->id_user)->object();
                $who_id = $who ? $who->id : 0;
                $who_nick = $who ? $who->nick : 'Система';
            } elseif ($last_post && $last_post->id_user == 0) {
                $who_id = 0;
                $who_nick = 'Система';
            }
            if ($theme->reason_close != null) {
                $type = '_close';
            } elseif ($theme->type == 1) {
                $type = '_up';
            } else {
                $type = null;
            } ?>
                <tr>
                    <td class = 'icon14'>
                        <img src = '<?php echo  FORUM ?>/icons/theme<?php echo  $type ?>.png' alt = '' <?php echo  ICONS ?> />
                    </td>
                    <td class = 'p_t'>
                        <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?php echo  output_text($theme->name, 1, 1, 0, 0, 0) ?></a> (<?php echo  $count_posts ?>) - <?php echo  vremja($theme->time) ?>
                    </td>
                </tr>
                <tr>
                    <?php
                    if ($last_post) {
                        ?>
                        <td class = 'p_m' colspan = '2'>
                            <b>Автор: <a href = '/info.php?id=<?php echo  $theme->id_user ?>'><?php echo  $creater->nick ?></a> | Посл.:</b> <a href = '/info.php?id=<?php echo  $who_id ?>'><?php echo  $who_nick ?></a> (<?php echo  vremja($last_post->time) ?>)
                        </td>
                        <?php

                    } else {
                        $continue = (mb_strlen($theme->description) > 150) ? '...' : null; ?>
                        <td class = 'p_m' colspan = '2'>
                            <?php echo  output_text(mb_substr($theme->description, 0, 150), 1, 1, 0, 1, 1).$continue ?>
                        </td>
                        <?php

                    } ?>
                </tr>
                <?php

        } ?>
        </table>
        <?php
        if ($k_page > 1) {
            str(FORUM.'/'.$forum->id.'/'.$razdel->id.'/', $k_page, $page);
        }
    } ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?php echo  FORUM ?>'>Форум</a> / <a href = '<?php echo  FORUM.'/'.$forum->id ?>/'><?php echo  output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?php echo  output_text($razdel->name, 1, 1, 0, 0, 0) ?></a>
    </div>
    <?php

}

?>