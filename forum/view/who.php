<?php

$forum = $db->query('SELECT * FROM `forum` WHERE `id` = ?i', [$_GET['forum']])->object();
$razdel = $db->query('SELECT * FROM `forum_razdels` WHERE `id_forum` = ?i AND `id` = ?i', [$forum->id, $_GET['razdel']])->object();
$theme = $db->query('SELECT * FROM `forum_themes` WHERE `id_razdel` = ?i AND `id` = ?i', [$razdel->id, $_GET['theme']])->object();

if (!$theme || !$razdel || !$forum || ($forum->access == 1 && $user['group_access'] < 8) || ($forum->access == 2 && $user['group_access'] < 3)) {
    header('Location: '.FORUM);
    exit;
} else {
    $set['title'] = 'Сейчас в теме - '.output_text($theme->name, 1, 1, 0, 0, 0);
    include_once '../sys/inc/thead.php';
    title().aut(); ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?php echo  FORUM ?>'>Форум</a> / <a href = '<?php echo  FORUM.'/'.$forum->id ?>/'><?php echo  output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?php echo  output_text($razdel->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?php echo  output_text($theme->name, 1, 1, 0, 0, 0) ?></a>
    </div>
    <?php
    $data = [time() - 600, FORUM . '/' . $forum->id . '/' . $razdel->id . '/' . $theme->id];
    $k_post = $db->query('SELECT COUNT(*) FROM `user` WHERE `date_last` > ?i AND `url` LIKE "?e%"', $data)->el();
    $k_page = k_page($k_post, $set['p_str']);
    $page = page($k_page);
    $start = $set['p_str']*$page-$set['p_str'];
    if ($k_post == 0) {
        ?>
        <div class = 'err'>В теме никого нет.</div>
        <?php

    } ?>
    <table class = 'post'>
        <?php
        $data = [time() - 600, FORUM . '/' . $forum->id . '/' . $razdel->id . '/' . $theme->id, $start, $set['p_str']];
    $persons = $db->query('SELECT `id`, `pol`, `nick` FROM `user` WHERE `date_last` > ?i AND `url` LIKE "?e%" ORDER BY `date_last` LIMIT ?i, ?i', $data);
    while ($person = $persons->object()) {
        ?>
            <tr>

                <td class = 'p_t'>
                   <?php echo nick($person->id)?>
                </td>
            </tr>
            <?php

    } ?>
    </table>
    <?php
    if ($k_page > 1) {
        str(FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/who/', $k_page, $page);
    } ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?php echo  FORUM ?>'>Форум</a> / <a href = '<?php echo  FORUM.'/'.$forum->id ?>/'><?php echo  output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?php echo  output_text($razdel->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?php echo  output_text($theme->name, 1, 1, 0, 0, 0) ?></a>
    </div>
    <?php

}

?>