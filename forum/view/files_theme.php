<?php
$forum = $db->query('SELECT * FROM `forum` WHERE `id` = ?i', [$_GET['forum']])->object();
$razdel = $db->query('SELECT * FROM `forum_razdels` WHERE `id_forum` = ?i AND `id` = ?i', [$forum->id, $_GET['razdel']])->object();
$theme = $db->query('SELECT * FROM `forum_themes` WHERE `id_razdel` = ?i AND `id` = ?i', [$razdel->id, $_GET['theme']])->object();

if (!$theme || !$razdel || !$forum || ($forum->access == 1 && $user['group_access'] < 8) || ($forum->access == 2 && $user['group_access'] < 3)) {
    header('Location: '.FORUM);
    exit;
} else {
    $set['title'] = 'Файлы темы - '.output_text($theme->name, 1, 1, 0, 0, 0);
    include_once '../sys/inc/thead.php';
    title().aut();

    ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?php echo  FORUM ?>'>Форум</a> / <a href = '<?php echo  FORUM.'/'.$forum->id ?>/'><?php echo  output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?php echo  output_text($razdel->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?php echo  output_text($theme->name, 1, 1, 0, 0, 0) ?></a>
    </div>
    <?php
    $k_post = $db->query('SELECT COUNT(*) FROM `forum_post_files` WHERE `id_theme` = '.$theme->id)->el();
    $k_page = k_page($k_post, $set['p_str']);
    $page = page($k_page);
    $start = $set['p_str']*$page-$set['p_str'];
    if ($k_post == 0) {
        ?>
        <div class = 'err'>В этой теме нет файлов.</div>
        <?php
    }
    ?>
    <table class = 'post' cellspacing = '0' cellpadding = '0'>
        <?php
        $files = $db->query('SELECT * FROM `forum_post_files` WHERE `id_theme` = '.$theme->id.' ORDER BY `id` ASC LIMIT '.$start.', '.$set['p_str']);
        while ($file = $files->object()) {
            $ras = strtolower(preg_replace('#^.*\.#', NULL, $file->name));
            if ($ras == 'jpg' || $ras == 'jpeg' || $ras == 'gif' || $ras == 'png' || $ras == 'bmp' || $ras == 'ico') {
                $icon = FORUM.'/files/'.$file->name;
            } elseif ($ras == '3gp' || $ras == 'mp4' || $ras == 'avi' || $ras == 'mpeg' || $ras == 'flv' || $ras == 'wmv' || $ras == 'mkv') {
                $icon = FORUM.'/icons/files/video.png';
            } elseif ($ras == 'docx' || $ras == 'doc' || $ras == 'docm' || $ras == 'dotx' || $ras == 'dot' || $ras == 'dotm') {
                $icon = FORUM.'/icons/files/word.png';
            } elseif ($ras == 'mp1' || $ras == 'mp2' || $ras == 'mp3' || $ras == 'wav' || $ras == 'aif' || $ras == 'ape' || $ras == 'flac' || $ras == 'ogg' || $ras == 'asf' || $ras == 'wma') {
                $icon = FORUM.'/icons/files/music.png';
            } elseif ($ras == 'zip' || $ras == 'rar' || $ras == 'tar' || $ras == '7-zip' || $ras == 'gzip' || $ras == 'jar' || $ras == 'jad' || $ras == 'war' || $ras == 'xar') {
                $icon = FORUM.'/icons/files/archive.png';
            } elseif ($ras == 'txt' || $ras == 'xml') {
                $icon = FORUM.'/icons/files/txt.png';
            } elseif ($ras == 'pdf') {
                $icon = FORUM.'/icons/files/pdf.png';
            } elseif ($ras == 'psd') {
                 $icon = FORUM.'/icons/files/psd.png';
            } else {
                $icon = FORUM.'/icons/files/file.png';
            }
            ?>
            <tr>
                <td class = 'p_t' style = 'width: 50px; border-right: none'>
                    <a href = '<?php echo  $icon ?>'><img src = '<?php echo  $icon ?>' alt = '' style = 'width: 50px; height: 50px' /></a>
                </td>
                <td class = 'p_t' style = 'border-left: none'>
                    <?php echo  output_text($file->real_name, 1, 1, 0, 0, 0) ?><br />
                    Размер файла: <?php echo  size_file($file->size) ?>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
    <?php
    if ($k_page > 1) {
        str(FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/files/', $k_page, $page);
    }
    ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?php echo  FORUM ?>'>Форум</a> / <a href = '<?php echo  FORUM.'/'.$forum->id ?>/'><?php echo  output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?php echo  output_text($razdel->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?php echo  FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?php echo  output_text($theme->name, 1, 1, 0, 0, 0) ?></a>
    </div>
    <?php
}

?>