<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once  H . 'sys/inc/start.php';
include_once  H . 'sys/inc/sess.php';
include_once  H . 'sys/inc/settings.php';
include_once  H . 'sys/inc/db_connect.php';
include_once  H . 'sys/inc/ipua.php';
include_once  H . 'sys/inc/fnc.php';
include_once  H . 'sys/inc/user.php';

$args = [
         'type' => FILTER_SANITIZE_STRING,
         'all' => FILTER_SANITIZE_STRING,
         'new' => FILTER_SANITIZE_STRING
        ];
$inGet = filter_input_array(INPUT_GET, $args);

$set['title'] = 'Журнал форума';
include_once H . 'sys/inc/thead.php';
title().aut().only_reg();

if (!isset($inGet['all']) && !isset($inGet['new'])) {
    header('Location: journal.php?new');
    exit;
}

$isset = (isset($inGet['type'])) ? '?type='.htmlspecialchars($inGet['type']).'&amp;' : '?';
$type = (isset($inGet['all'])) ? 'all' : 'new';
$read = (isset($inGet['all'])) ? '' : 'AND `read` = "0"';

?>
<table class = 'post'>
	<tr>
		<td class = 'au' style = 'width: 50%; text-align: center'>
			<?php echo  (isset($inGet['all'])) ? 'Все' : '<a href = "journal.php'.$isset.'all" style="display: block;">Все</a>' ?>
		</td>
		<td class = 'au' style = 'width: 50%; text-align: center'>
			<?php echo  (!isset($inGet['all'])) ? 'Новые' : '<a href = "journal.php'.$isset.'new" style="display: block;">Новые</a>' ?>
		</td>
	</tr>
</table>
<div class = 'foot'>
	<?php echo  (isset($inGet['type'])) ? '<a href = "journal.php">Все</a>' : '<strong>Все</strong>' ?> |
	<?php echo  (isset($inGet['type']) && $inGet['type'] == 'my_themes') ? '<strong>Мои темы</strong>' : '<a href = "journal.php?type=my_themes&amp;'.$type.'">Мои темы</a>' ?> |
	<?php echo  (isset($inGet['type']) && $inGet['type'] == 'themes') ? '<strong>Темы</strong>' : '<a href = "journal.php?type=themes&amp;'.$type.'">Темы</a>' ?> |
	<?php echo  (isset($inGet['type']) && $inGet['type'] == 'answers') ? '<strong>Ответы</strong>' : '<a href = "journal.php?type=answers&amp;'.$type.'">Ответы</a>' ?> |
	<?php echo  (isset($inGet['type']) && $inGet['type'] == 'quotes') ? '<strong>Цитаты</strong>' : '<a href = "journal.php?type=quotes&amp;'.$type.'">Цитаты</a>' ?> |
	<?php echo  (isset($inGet['type']) && $inGet['type'] == 'privat') ? '<strong>Приват</strong>' : '<a href = "journal.php?type=privat&amp;'.$type.'">Приват</a>' ?>
</div>
<?php
if (isset($inGet['type'])) {
    $query = 'SELECT COUNT(*) FROM `f_journal` WHERE `type` = ? AND `id_user` = ?i ' . $read;
    $data = [$inGet['type'], $user['id']];
} else {
    $query = 'SELECT COUNT(*) FROM `f_journal` WHERE `id_user` = ?i ' . $read;
    $data = [$user['id']];
}

$type_j = (isset($inGet['type'])) ? '`type` = "'.addslashes($inGet['type']).'" AND' : '';
$k_post = $db->query($query, $data)->el();
$k_page = k_page($k_post, $set['p_str']);
$page = page($k_page);
$start = $set['p_str']*$page-$set['p_str'];
if ($k_post == 0) {
    ?>
    <div class = 'p_m'>
    	Записей нет.<br />
    	В журнале будут отображаться все ответы на Ваши комментарии и сообщения, оставленные пользователями в тех темах, за которыми Вы следите.
    </div>
    <?php

} else {
    if (isset($inGet['type'])) {
        $query2 = 'SELECT * FROM `f_journal` WHERE `type` = ? AND `id_user` = ?i ' . $read . ' ORDER BY `id` DESC LIMIT ?i, ?i';
        $data2 = [$inGet['type'], $user['id'], $start, $set['p_str']];
    } else {
        $query2 = 'SELECT * FROM `f_journal` WHERE `id_user` = ?i ' . $read . ' ORDER BY `id` DESC LIMIT ?i, ?i';
        $data2 = [$user['id'], $start, $set['p_str']];
    }
    $js = $db->query($query2, $data2);
    while ($j = $js->object()) {
        ?>
		<div class = 'p_m'>
			<?php echo  output_text($j->text) ?><br />
			<strong>Время отправки:</strong> <?php echo  vremja($j->time) ?>
		</div>
		<?php

    }
    if ($k_page > 1) {
        str('journal.php'.$isset.$type, $k_page, $page);
    }
    if (isset($inGet['new'])) {
        $db->query('UPDATE `f_journal` SET `read` = "1" WHERE `id_user` = '.$user['id']);
    }
}

include_once H . 'sys/inc/tfoot.php';

?>