<?php

include_once 'sys/inc/start.php';
include_once 'sys/inc/sess.php';
include_once 'sys/inc/home.php';
include_once 'sys/inc/settings.php';
include_once 'sys/inc/db_connect.php';
include_once 'sys/inc/ipua.php';
include_once 'sys/inc/fnc.php';
include_once 'sys/inc/shif.php';
$show_all = true; // показ для всех
$input_page = true;
include_once 'sys/inc/user.php';

only_unreg();

$_SESSION['aut_captcha']++;
lang::start('login');

if (isset($_GET['id']) && isset($_GET['pass']) && $set['avto_login_set'] == 1) {
    $cnt = $db->query('SELECT COUNT(*) FROM `user` WHERE `id`=?i AND `pass`=? LIMIT ?i',
                    [$_GET['id'], shif($_GET['pass']), 1])->el();
    if ((int)$cnt == 1) {
        $user = get_user($_GET['id']);
        $_SESSION['id_user'] = $user['id'];

        $db->query('UPDATE `user` SET `date_aut`=?i, `date_last`=?i WHERE `id`=?i LIMIT ?i', [time(), time(), $user['id'], 1]);
        $db->query('INSERT INTO `user_log` (`id_user`, `time`, `ua`, `ip`, `method`)
VALUES(?i, ?i, ?, ?i, ?)', [$user['id'], $time, $ua, $user['ip'], '0']);
    } else {
        $err[] = lang('Неправильный логин или пароль');
    }
} elseif (isset($_GET['id']) && isset($_GET['pass']) && $set['avto_login_set'] == 0) {
    $err[] = lang('Автологин отключен!');
}
/*elseif (isset($_POST['aut_save'],$_POST['nick']) and $_POST['chislo'] != $_SESSION['captcha'])
$err[] = lang('Код с картинки введен не верно');*/
elseif (isset($_POST['nick']) && isset($_POST['pass'])) {
    $cnt = $db->query('SELECT COUNT(*) FROM `user` WHERE `nick`=? AND `pass`=? LIMIT ?i',
                      [$_POST['nick'], shif($_POST['pass']), 1])->el();
    if ((int)$cnt == 1) {
        $user = $db->query('SELECT `id`, `hash_set`, `ua`, `ip`, `set_them`, `set_them2` FROM `user` WHERE `nick`=? AND `pass`=? LIMIT ?i',
                           [$_POST['nick'], shif($_POST['pass']), 1])->row();

        $_SESSION['id_user'] =$user['id'];

        $db->query('INSERT INTO `user_log` (`id_user`, `time`, `ua`, `ip`, `method`)
VALUES(?i, ?i, ?, ?i, ?string)', [$user['id'], $time, $ua, $user['ip'], '1']);

    // сохранение данных в COOKIE
    if (isset($_POST['aut_save']) && $_POST['aut_save']) {
        setcookie('id_user', $user['id'], time() + 60*60*24*365);
        setcookie('pass', cookie_encrypt($_POST['pass'], $user['id']), time() + 60*60*24*365);
    }

        if ($set['antihah_hash'] == 1 && $user['hash_set'] == 1) {
            $hash = ' `hash`="' . md5(md5($ip . md5($ua) . $user['id'])) . '",';
        } else {
            $hash = '';
        }

        $db->query('UPDATE `user` SET ?q `date_aut`=?i, `date_last`=?i  WHERE `id`=?i LIMIT 1',
               [$hash, $time, $time, $user['id']]);
    } else {
        $err[] = lang('Неправильный логин или пароль');
    }
} elseif (isset($_COOKIE['id_user'], $_COOKIE['pass']) && $_COOKIE['id_user'] && $_COOKIE['pass']) {
    cache_delete::user($_COOKIE['id_user']);
    $hash_set = $db->query('SELECT `hash_set` FROM `user` WHERE `id`=?i', [$_COOKIE['id_user']])->row();

    if ($set['antihah_hash'] == 1 && (int)$hash_set['hash_set'] == 1) {
        $hash = ' `hash`= "' . md5(md5($ip . md5($ua) . $_COOKIE['id_user'])) . '" AND ';
    } else {
        $hash = '';
    }
    $cnt = $db->query('SELECT COUNT(*) FROM `user` WHERE ?q `id`=?i AND `pass`=? LIMIT ?i',
                      [$hash, $_COOKIE['id_user'], shif(cookie_decrypt($_COOKIE['pass'], intval($_COOKIE['id_user']))), 1])->el();
    if ((int)$cnt == 1) {
        $user = get_user($_COOKIE['id_user']);
        $_SESSION['id_user'] = $user['id'];
        $db->query('UPDATE `user` SET `date_aut`=?i, `date_last`=?i WHERE `id`=?i LIMIT ?i', [time(), time(), $user['id'], 1]);
        $db->query('INSERT INTO `user_log` (`id_user`, `time`, `ua`, `ip`, `method`)
VALUES(?i, ?i, ?, ?i, ?)', [$user['id'], $time, $ua, $user['ip'], '2']);

        $user['type_input'] = 'cookie';
    } else {
        $err[] = lang('Ошибка авторизации по COOKIE');
        setcookie('id_user');
        setcookie('pass');
    }
} else {
    $err[] = lang('Ошибка авторизации');
}

if (!isset($user)) {
    $set['title'] = lang('Авторизация');
    include_once 'sys/inc/thead.php';
    title() . aut() . err();

    header('Refresh: 1; url=/aut.php');

    echo '<div class="p_m">';
    echo '<a href="/aut.php?">' . lang('Повторить попытку входа') . '</a><br />';
    echo '</div>';
    include_once H . 'sys/inc/tfoot.php';
    exit;
}

$set['title'] = lang('Дайджест');
$_SESSION['aut_captcha'] = 0;

if ($set['web']) { // для web темы
    if (is_dir(H. 'style/themes/' . $user['set_them2'])) {
        $set['set_them'] = $user['set_them2'];
    } else {
        $db->query('UPDATE `user` SET `set_them2`=? WHERE `id`=?i LIMIT ?i',
                   [$set['set_them2'], $user['id'], 1]);
    }
} else {
    if (is_dir(H . 'style/themes/' . $user['set_them'])) {
        $set['set_them'] = $user['set_them'];
    } else {
        $db->query('UPDATE `user` SET `set_them`=? WHERE `id`=?i LIMIT ?i',
                   [$set['set_them'], $user['id'], 1]);
    }
}

    //Загрузка дополнительных плагинов
    $Search = glob(H . 'sys/login/*.php');
    foreach ($Search as $load_plugins) {
        sort($Search);
        include_once $load_plugins;
    }

if (isset($_GET['return'])) {
    header('Location: ' . urldecode($_GET['return']));
} else {
    #	включаем модуль возврата
    if ($set['aut_ref'] == 1 && isset($_SESSION['ref_loc'])) {
        exit(header("Location: " . $_SESSION['ref_loc'] . "?&aut_ref"));
    } else {
        exit(header("Location: /user/start"));
    }
}

exit;
