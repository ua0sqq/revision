<?php

only_reg();
$set['title'] = 'Управление email';

if (isset($_POST['save'])) {
    include_once H.'sys/inc/shif.php';

    if (isset($_POST['pass']) && count::query('user', '`id`="' . $user['id'] . '" AND `pass`="' . shif($_POST['pass']) . '"') == 0) {
        $err[]= lang('Пароль не вверен');
    }
    
    if (isset($_POST['set_show_mail']) && $_POST['set_show_mail']==1) {
        $user['set_show_mail']=1;
        $db->query('UPDATE `user` SET `set_show_mail`=?string WHERE `id`= ?i LIMIT ?i', [$user['set_show_mail'], $user['id'], 1]);
    } else {
        $user['set_show_mail']=0;
        $db->query('UPDATE `user` SET `set_show_mail`=?string WHERE `id`= ?i LIMIT ?i', [$user['set_show_mail'], $user['id'], 1]);
    }

    if (isset($_POST['ank_mail']) && ($_POST['ank_mail']==null || preg_match('#^[A-z0-9-\._]+@[A-z0-9]{2,}\.[A-z]{2,4}$#ui', $_POST['ank_mail']))) {
        $user['ank_mail']=$_POST['ank_mail'];
        $db->query('UPDATE `user` SET `ank_mail`=? WHERE `id`= ?i LIMIT ?i', [$user['ank_mail'], $user['id'], 1]);
    } else {
        $err[] = lang('Неверный E-mail');
    }

    #	удаляем кэш файл
    cache_delete::user($user['id']);

    if (!isset($err)) {
        $_SESSION['message'] = lang('Изменения успешно приняты');
        exit(header('Location: ?'));
    }
}
err();

$div_edit = 'p_m';

    echo "<div class='div_edit_fon'><form method='post' action=''>
	<div class='$div_edit'>
	".lang('E-mail').":	<br /><input type='text' name='ank_mail' value='$user[ank_mail]' maxlength='32' /><br />
	<label><input type='checkbox' name='set_show_mail'".($user['set_show_mail']==1?' checked="checked"':null)." value='1' />".lang('Показывать E-mail в анкете')."</label>
	<hr />
	".lang('Пароль от профиля').":<br /><input type='text' name='pass' value='' /></div>
	</div>
	<div class='p_m'>
	<input type='submit' name='save' value='".lang('Сохранить')."' />
	</form>
	</div>";
