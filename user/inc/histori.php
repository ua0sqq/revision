<?php

$set['title'] = 'История авторизаций';
only_reg();

$k_post  = count::query('user_log', '`id_user`="' . $user['id'] . '"');
$k_page = k_page($k_post, $set['p_str']);
$page    = page($k_page);
$start    = $set['p_str']*$page-$set['p_str'];
echo "<table class='post'>\n";
if ($k_post==0) {
    echo "   <tr>\n";
    echo "  <td class='p_t'>\n";
    echo lang('Нет записанных авторизаций');
    echo "  </td>\n";
    echo "   </tr>\n";
}
$q = $db->query('SELECT * FROM `user_log` WHERE `id_user`=?i ORDER BY `id` DESC  LIMIT ?i, ?i', [$user['id'], $start, $set['p_str']]);
while ($post = $q->row()) {
    echo "<td class='p_t'>";
    if ($post['method'] == 1) {
        echo lang("Ввод логина и пароля");
    } elseif ($post['method'] == 2) {
        echo lang("Авторизация по кукам");
    } elseif ($post['method'] == 3) {
        echo lang("Авторизация через соц сеть");
    } else {
        echo lang("Автозакладка");
    }
    echo '  ['.vremja($post['time']);
    echo "] </td>";
    echo "   </tr>";
    echo "   <tr>";

    echo "  <td class='p_m'>\n";
    echo "IP: 
	 ".long2ip($post['ip'])."<br />";
    echo lang("Браузер").": ".output_text($post['ua']);
    echo "  </td>\n";
    echo "   </tr>\n";
}
echo "</table>\n";
if ($k_page>1) {
    str("?", $k_page, $page); // Вывод страниц
}
