<?php

only_reg();

$set['title'] = 'Информация о себе';


if (isset($_POST['save'])) {
    if (isset($_POST['ank_name']) && preg_match('#^([A-zА-я \-]*)$#ui', $_POST['ank_name'])) {
        $user['ank_name']=$_POST['ank_name'];
        $db->query('UPDATE `user` SET `ank_name`=? WHERE `id` =?i LIMIT ?i', [$user['ank_name'], $user['id'], 1]);
    } else {
        $err[] = lang('Неверный формат имени');
    }

    if (isset($_POST['ank_d_r']) && (in_array((int)$_POST['ank_d_r'], range(1, 31)) || $_POST['ank_d_r']==null)) {
        $user['ank_d_r'] = (int)$_POST['ank_d_r'];
        if ($user['ank_d_r'] == null) {
            $user['ank_d_r'] = 'null';
        }
        $db->query('UPDATE `user` SET `ank_d_r`=?i WHERE `id`=?i LIMIT ?i', [$user['ank_d_r'], $user['id'], 1]);
        if ($user['ank_d_r']=='null') {
            $user['ank_d_r']=null;
        }
    } else {
        $err[]=lang('Неверный формат дня рождения');
    }

    if (isset($_POST['ank_m_r']) && (in_array((int)$_POST['ank_m_r'], range(1, 12)) || $_POST['ank_m_r']==null)) {
        $user['ank_m_r']= (int) $_POST['ank_m_r'];
        if ($user['ank_m_r']==null) {
            $user['ank_m_r']='null';
        }
        $db->query('UPDATE `user` SET `ank_m_r`=?i WHERE `id`=?i LIMIT ?i', [$user['ank_m_r'], $user['id'], 1]);
        if ($user['ank_m_r']=='null') {
            $user['ank_m_r']=null;
        }
    } else {
        $err[]=lang('Неверный формат месяца рождения');
    }

    if (isset($_POST['ank_g_r']) && (in_array((int)$_POST['ank_g_r'], range(date('Y')-75, date('Y')-10)) || $_POST['ank_g_r']==null)) {
        $user['ank_g_r']= (int) $_POST['ank_g_r'];
        if ($user['ank_g_r']==null) {
            $user['ank_g_r']='null';
        }
        $db->query('UPDATE `user` SET `ank_g_r`=?i WHERE `id`=?i LIMIT ?i', [$user['ank_g_r'], $user['id'], 1]);
        if ($user['ank_g_r']=='null') {
            $user['ank_g_r']=null;
        }
    } else {
        $err[]=lang('Неверный формат года рождения');
    }

    if (isset($_POST['ank_city']) && preg_match('#^([A-zА-я \-]*)$#ui', $_POST['ank_city'])) {
        $user['ank_city'] = $_POST['ank_city'];
        $db->query('UPDATE `user` SET `ank_city`=? WHERE `id`=?i LIMIT ?i', [$user['ank_city'], $user['id'], 1]);
    } else {
        $err[]=lang('Неверный формат названия города');
    }

    if (isset($_POST['ank_icq']) && (is_numeric($_POST['ank_icq']) && mb_strlen($_POST['ank_icq'])>=5 && mb_strlen($_POST['ank_icq'])<=9 || $_POST['ank_icq']==null)) {
        $user['ank_icq']=$_POST['ank_icq'];
        if ($user['ank_icq']==null) {
            $user['ank_icq']=null;//var_dump($user['ank_icq']);exit;
        }
        $db->query('UPDATE `user` SET `ank_icq`=?n WHERE `id`=?i LIMIT ?i', [$user['ank_icq'], $user['id'], 1]);
        //if ($user['ank_icq']=='null')
        //{
        //	$user['ank_icq']=NULL;
        //}
    } else {
        $err[]=lang('Неверный формат ICQ');
    }

    if (isset($_POST['ank_skype']) && mb_strlen($_POST['ank_skype']) <= 32) {
        $user['ank_skype'] = $_POST['ank_skype'];
        $db->query('UPDATE `user` SET `ank_skype`=? WHERE `id`=?i LIMIT ?i', [$user['ank_skype'], $user['id'], 1]);
    }

    if (isset($_POST['pol']) && $_POST['pol']==1) {
        $user['pol']=1;
        $db->query('UPDATE `user` SET `pol` =?string WHERE `id` =?i LIMIT ?i', [$user['pol'], $user['id'], 1]);
    }
    if (isset($_POST['pol']) && $_POST['pol']==0) {
        $user['pol']=0;
        $db->query('UPDATE `user` SET `pol` =?string WHERE `id` =?i LIMIT ?i', [$user['pol'], $user['id'], 1]);
    }

    if (isset($_POST['ank_o_sebe']) && strlen2($_POST['ank_o_sebe']) <= 512) {
        $db->query('UPDATE `user` SET `ank_o_sebe`=? WHERE `id`=?i LIMIT ?i', [$_POST['ank_o_sebe'], $user['id'], 1]);
    } else {
        $err[]=lang('О себе нужно писать меньше');
    }

    if (isset($_POST['ank_countr']) && preg_match('#^([A-zА-я \-]*)$#ui', $_POST['ank_countr'])) {
        $db->query('UPDATE `user` SET `ank_countr`=? WHERE `id`=?i LIMIT ?i', [$_POST['ank_countr'], $user['id'], 1]);
    }

    if (isset($_POST['ank_family']) && preg_match('#^([A-zА-я \-]*)$#ui', $_POST['ank_family'])) {
        $db->query('UPDATE `user` SET `ank_family`=? WHERE `id`=?i LIMIT ?i', [$_POST['ank_family'], $user['id'], 1]);
    }

    #	удаляем кэш файл
    cache_delete::user($user['id']);

    if (!isset($err)) {
        $_SESSION['message'] = lang('Изменения успешно приняты');
        exit(header('Location: ?'));
    }
}
err();


$div_edit = 'p_m';

?>
<div class="div_edit_fon">
    <form method="post" action="">
        <div class="<?php echo $div_edit?>">
	       <?php echo lang('Имя')?>:<br />
            <input type="text" name="ank_name" value="<?php echo output_text($user['ank_name'], false)?>" maxlength="32"/>
        </div>
        <div class="<?php echo $div_edit?>">
	       <?php echo lang('Фамилия')?>:<br />
            <input type="text" name="ank_family" value="<?php echo output_text($user['ank_family'], false)?>" maxlength="32"/>
	   </div>
        <div class="<?php echo $div_edit?>">
            <?php echo lang('Дата рождения')?>:<br /><!-- День -->
	           <select name="ank_d_r">
                    <option selected="<?php echo $user['ank_d_r']?>" value="<?php echo $user['ank_d_r']?>"><?php echo $user['ank_d_r']?></option>
<?php
    for ($i = 1; $i < 32; ++$i) {
        ?>
                    <option value="<?php echo $i?>"><?php echo $i?></option>
<?php

    }
?>
            </select><!-- Месяц -->
            <select name="ank_m_r">
                <option selected="<?php echo $user['ank_m_r']?>" value="<?php echo $user['ank_m_r']?>"><?php echo $user['ank_m_r']?></option>
<?php
    for ($i = 1; $i < 13; ++$i) {
        ?>
                <option value="<?php echo $i?>"><?php echo $i?></option>
<?php

    }
?>
            </select><!-- Год -->
            <select name="ank_g_r">
                <option selected="<?php echo $user['ank_g_r']?>" value="<?php echo $user['ank_g_r']?>"><?php echo $user['ank_g_r']?></option>
<?php
    for ($i = date('Y')-75; $i < date('Y')-10; ++$i) {
        ?>
                <option value="<?php echo $i?>"><?php echo $i?></option>
<?php

    }
?>
            </select>
        </div>
        <div class="<?php echo $div_edit?>">
            <?php echo lang('Пол')?>: 
            <input name="pol" type="radio" <?php echo($user['pol']==1 ? ' checked="checked"' : null)?> value="1"/>
            <?php echo lang('Муж.')?>
            <input name="pol" type="radio" <?php echo($user['pol']==0 ? ' checked="checked"' : null)?> value="0"/>
            <?php echo lang('Жен.')?>
	   </div>
	   <div class="<?php echo $div_edit?>">
            <?php echo lang('Город')?>:<br />
            <input type="text" name="ank_city" value="<?php echo output_text($user['ank_city'], false)?>" maxlength="32"/>
	   </div>
	   <div class="<?php echo $div_edit?>">
            <?php echo lang('Регион')?>:<br />
            <input type="text" name="ank_countr" value="<?php echo output_text($user['ank_countr'], false)?>" maxlength="32"/>
	   </div>
	   <div class="<?php echo $div_edit?>">
            <?php echo lang('ICQ')?>:<br />
            <input type="text" name="ank_icq" value="<?php echo $user['ank_icq']?>" maxlength="9"/>
	   </div>
	   <div class="<?php echo $div_edit?>">
            <?php echo lang('Skype логин')?>:<br />
            <input type="text" name="ank_skype" value="<?php echo output_text($user['ank_skype'], false)?>" maxlength="32"/>
	   </div>
	   <div class="<?php echo $div_edit?>">
            <?php echo lang('О себе')?>:<br />
            <textarea name="ank_o_sebe"><?php echo output_text($user['ank_o_sebe'], false)?></textarea>
	   </div>
	       <input type="submit" name="save" value="<?php echo lang('Сохранить')?>"/>
	</form>
 </div>