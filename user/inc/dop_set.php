<?php

$set['title'] = 'Дополнительные настройки';
only_reg();
$back_link_set = true;

if (isset($_POST['save'])) {
    // TODO: желательно собрать запросы в один
    if (isset($_POST['set_show_icon']) && ($_POST['set_show_icon'] == 2 || $_POST['set_show_icon'] == 1 || $_POST['set_show_icon'] == 0)) {
        // $user['set_show_icon'] = intval($_POST['set_show_icon']);
        $db->query('UPDATE `user` SET `set_show_icon`=? WHERE `id`=?i LIMIT ?i', [$_POST['set_show_icon'], $user['id'], 1]);
    } else {
        $err = 'Ошибка режима показа иконок';
    }

    if (isset($_POST['add_konts']) && ($_POST['add_konts'] == 2 || $_POST['add_konts'] == 1 || $_POST['add_konts'] == 0)) {
        $user['add_konts'] = intval($_POST['add_konts']);
        $db->query('UPDATE `user` SET `add_konts`=?i WHERE `id`=?i LIMIT ?i', [$_POST['add_konts'], $user['id'], 1]);
    } else {
        $err = 'Ошибка режима добавления контактов';
    }

    if (isset($_POST['set_translit']) && ($_POST['set_translit'] == 1 || $_POST['set_translit'] == 0)) {
        $user['set_translit'] = intval($_POST['set_translit']);
        $db->query("UPDATE `user` SET `set_translit` = '$user[set_translit]' WHERE `id` = '$user[id]' LIMIT 1");
    } else {
        $err = 'Ошибка режима транслита';
    }

    if (isset($_POST['set_files']) && ($_POST['set_files'] == 1 || $_POST['set_files'] == 0)) {
        $user['set_files'] = intval($_POST['set_files']);
        $db->query("UPDATE `user` SET `set_files` = '$user[set_files]' WHERE `id` = '$user[id]' LIMIT 1");
    } else {
        $err = 'Ошибка режима файлов';
    }

    if (isset($_POST['show_url']) && ($_POST['show_url'] == 1 || $_POST['show_url'] == 0)) {
        $user['show_url']=intval($_POST['show_url']);
        $db->query("UPDATE `user` SET `show_url` = '$user[show_url]' WHERE `id` = '$user[id]' LIMIT 1");
    } else {
        $err = 'Ошибка режима местоположения';
    }

    if (isset($_POST['set_news_to_mail']) && $_POST['set_news_to_mail'] == 1) {
        $user['set_news_to_mail'] = 1;
        $db->query("UPDATE `user` SET `set_news_to_mail` = '1' WHERE `id` = '$user[id]' LIMIT 1");
    } else {
        $user['set_news_to_mail'] = 0;
        $db->query("UPDATE `user` SET `set_news_to_mail` = '0' WHERE `id` = '$user[id]' LIMIT 1");
    }

    if ($set['antihah_hash'] == 1) {
        if (isset($_POST['hash_set']) && $_POST['hash_set'] == 1) {
            $user['hash_set'] = 1;
            $db->query("UPDATE `user` SET `hash_set` = '1' WHERE `id` = '$user[id]' LIMIT 1");
        } else {
            $user['hash_set'] = 0;
            $db->query("UPDATE `user` SET `hash_set` = '0' WHERE `id` = '$user[id]' LIMIT 1");
        }
    }

    if (isset($_POST['set_p_str']) && is_numeric($_POST['set_p_str']) && $_POST['set_p_str'] > 0 && $_POST['set_p_str'] <= $set['p_str_set'] == 1 ? 30 : 10) {
        $user['set_p_str']=intval($_POST['set_p_str']);
        $set['p_str']=$user['set_p_str'];
        $db->query("UPDATE `user` SET `set_p_str` = '$user[set_p_str]' WHERE `id` = '$user[id]' LIMIT 1");
    } else {
        $err = 'Неправильное количество пунктов на страницу';
    }

    if (isset($_POST['set_timesdvig']) && (is_numeric($_POST['set_timesdvig']) && $_POST['set_timesdvig'] >= -12 && $_POST['set_timesdvig'] <= 12)) {
        $user['set_timesdvig'] = intval($_POST['set_timesdvig']);
        $db->query("UPDATE `user` SET `set_timesdvig` = '$user[set_timesdvig]' WHERE `id` = '$user[id]' LIMIT 1");
    } else {
        $err = 'Неправильное количество пунктов на страницу';
    }

    if (!isset($err)) {
        $_SESSION['message'] = lang('Изменения успешно приняты');
        //Удаляем кэш $user
        cache_delete::user($user['id']);
        exit(header('Location: ?rand=' . mt_rand(0, 9999)));
    }
}

err();

echo '<form method="post" action="?' . $passgen . '">';
echo "<div class='p_m'>Пунктов на страницу:<br /><select name='set_p_str'>";
echo "<option value='5'" . ($user['set_p_str'] == 5 ? " selected='selected'" : null) . "> 5 </option>";
echo "<option value='7'" . ($user['set_p_str'] == 7 ? " selected='selected'" : null) . "> 7 </option>";
echo "<option value='10'" . ($user['set_p_str'] == 10 ? " selected='selected'" : null) . "> 10 </option>";

if ($set['p_str_set'] == 1) {
    echo "<option value='15'" . ($user['set_p_str'] == 15 ? " selected='selected'" : null) . "> 15 </option>";
    echo "<option value='20'" . ($user['set_p_str'] == 20 ? " selected='selected'" : null) . "> 20 </option>";
    echo "<option value='25'" . ($user['set_p_str'] == 25 ? " selected='selected'" : null) . "> 25 </option>";
    echo "<option value='30'" . ($user['set_p_str'] == 30 ? " selected='selected'" : null) . "> 30 </option>";
}

echo "</select></div>";

echo "<div class='p_m'>Иконки:<br />\n<select name='set_show_icon'>\n";
echo "<option value='2'" . ($user['set_show_icon'] == 2 ? " selected='selected'" : null) . ">Большие</option>\n";
echo "<option value='1'" . ($user['set_show_icon'] == 1 ? " selected='selected'" : null) . ">Маленькие</option>\n";
echo "<option value='0'" . ($user['set_show_icon'] == 0 ? " selected='selected'" : null) . ">Скрывать</option>\n";
echo "</select></div>";

echo "<div class='p_m'>Транслит:<br />\n<select name='set_translit'>\n";
echo "<option value='1'" . ($user['set_translit'] == 1 ? " selected='selected'" : null) . ">По выбору</option>\n";
echo "<option value='0'" . ($user['set_translit'] == 0 ? " selected='selected'" : null) . ">Никогда</option>\n";
echo "</select></div>";

echo "<div class='p_m'>Выгрузка файлов:<br />\n<select name='set_files'>\n";
echo "<option value='1'" . ($user['set_files'] == 1 ? " selected='selected'" : null) . ">Показывать поле</option>\n";
echo "<option value='0'" . ($user['set_files'] == 0 ? " selected='selected'" : null) . ">Не использовать выгрузку</option>\n";
echo "</select></div>";

echo "<div class='p_m'>Местоположение:<br />\n<select name='show_url'>\n";
echo "<option value='1'" . ($user['show_url'] == 1 ? " selected='selected'" : null) . ">Показывать</option>\n";
echo "<option value='0'" . ($user['show_url'] == 0 ? " selected='selected'" : null) . ">Скрывать</option>\n";
echo "</select></div>";

echo "<div class='p_m'>Добавление контактов:<br />\n<select name='add_konts'>\n";
echo "<option value='2'" . ($user['add_konts'] == 2 ? " selected='selected'" : null) . ">При чтении сообщений</option>\n";
echo "<option value='1'" . ($user['add_konts'] == 1 ? " selected='selected'" : null) . ">При написании сообщения</option>\n";
echo "<option value='0'" . ($user['add_konts'] == 0 ? " selected='selected'" : null) . ">Только вручную</option>\n";
echo "</select></div>";

echo "<div class='p_m'>Время<br />\n<select name=\"set_timesdvig\">\n";
for ($i = -12; $i < 12; $i++) {
    echo "<option value='$i'" . ($user['set_timesdvig'] == $i ? " selected='selected'" : null) . ">" . date("G:i", $time + $i*60*60) . "</option>\n";
}
echo "</select></div>";

if ($set['antihah_hash'] == 1) {
    echo "<div class='p_m'><label><input type='checkbox' name='hash_set'" . ($user['hash_set'] ? " checked='checked'" : null) . " value='1' /> Дополнительный токен</label></div>";
}

if ($user['ank_mail'] != null) {
    echo "<div class='p_m'><label><input type='checkbox' name='set_news_to_mail'" . ($user['set_news_to_mail'] ? " checked='checked'" : null) . " value='1' /> Получать новости на E-mail</label></div>";
}

echo "<div class='p_m'><input type='submit' name='save' value='Сохранить' /></div>";
echo "</form>\n";
