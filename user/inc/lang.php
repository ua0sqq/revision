<?php

$back_link_set = true;
$set['title'] = 'Смена языка';
if ($set['lang_test'] == 0)
    exit(header('Location: /'));
if (isset($_POST['save']))
{
    //если адрес пуст
    if ($_GET['returnUrl'] == null)
        $_GET['returnUrl'] = base64_encode('?');

    if (count::query('lang_list' , '`name_g`="' . $_POST['lang'] . '"') == 1)
	{
		//если гость
		if (empty($user))
		{
			$_SESSION['lang'] = $_POST['lang'];
		}
		else
		{
			//если юзер
			$db->query('UPDATE `user` SET `lang`=? WHERE `id`=?i LIMIT ?i', [$_POST['lang'], $user['id'], 1]);
			//Удаляем кэш
			cache_delete::user($user['id']);
		}
    }

    $_SESSION['message'] = lang('Язык установлен');
    exit(header('Location: ' . base64_decode($_GET['returnUrl'])));
}
?>
<div class="p_m" >
    <form method="post" action="">
        <fieldset>
            <legend><?php echo lang('Смена языка')?></legend>
                <select name="lang"><?php
$lang_list = $db->query('SELECT * FROM `lang_list`');
while ($post = $lang_list->row())
{
?>
                <option value="<?php echo output_text($post['name_g'])?>" <?php echo ($post['name_g'] ==
        $set['lang'] ? ' selected="selected"' : null)?>><?php echo output_text($post['name'])?></option>
        <?php
}
?>              </select> 
            <input type="submit" name="save" value="<?php echo lang('Сохранить')?>" />
        </fieldset>
	</form>
</div>
<?php

?>