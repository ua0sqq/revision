<?php

only_reg();
$back_link_set = true;
$set['title'] = 'Смена пароля';

#------>Обработка введенных данных
include_once H . 'sys/inc/shif.php';

if (isset($_POST['save'])) {
    if (isset($_POST['pass']) and
        count::query('user', '`id`="' . $user['id'] . '" AND `pass`="' . shif($_POST['pass']) . '"') == 1) {
        if (isset($_POST['pass1']) && isset($_POST['pass2'])) {
            if ($_POST['pass1'] == $_POST['pass2']) {
                if (mb_strlen($_POST['pass1']) < 6) {
                    $err = 'По соображениям безопасности новый пароль не может быть короче 6-ти символов';
                }
                if (mb_strlen($_POST['pass1']) > 32) {
                    $err = 'Длина пароля превышает 32 символа';
                }
            } else {
                $err = 'Новый пароль не совпадает с подтверждением';
            }
        } else {
            $err = 'Введите новый пароль';
        }
    } else {
        $err = 'Старый пароль неверен';
    }

        
    //Поисковое слово
    $world_set_update = $_POST['pass1'];
    //файл где ищем
    $file_default = file_get_contents(H.'sys/dat/if_password.txt');

    //если  нету записываем
    if (stripos($file_default, $world_set_update) !== false) {
        $err = lang('Новый пароль слишком простой');
    }

    if (!isset($err)) {
        $db->query('UPDATE `user` SET `pass`=?, `pass_time`=?i WHERE `id`=?i LIMIT ?i',
                   [shif($_POST['pass1']), time(), $user[id], 1]);
        setcookie('pass', cookie_encrypt($_POST['pass1'], $user['id']), time()+60*60*24*365);
        $_SESSION['message'] = lang('Пароль успешно изменен');
        exit(header('Location: ?'));
    }
}
#------>Обработка введенных данных

err();
 
#------>Форма ввода
echo "<div class='p_m'>";
echo "<form method='post' action=''>";
echo "Старый пароль:<br /><input type='text' name='pass' value='' /><br />";
echo "Новый пароль:<br /><input type='password' name='pass1' value='' /><br />";
echo "Подтверждение:<br /><input type='password' name='pass2' value='' /><br />";
echo "<input type='submit' name='save' value='Изменить' />";
echo "</form>";
echo "</div>";
#------>Конец Формы
