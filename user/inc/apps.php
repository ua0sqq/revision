<?php

only_reg();
$set['title'] = 'Приложения';


$k_post  = count::query('apps');
$k_page = k_page($k_post, $set['p_str']);
$page    = page($k_page);
$start    = $set['p_str']*$page-$set['p_str'];
echo "<table class='post'>\n";
if ($k_post == 0) {
    echo "   <tr>\n";
    echo "  <td class='p_t'>\n";
    echo lang('Еще не добавлено');
    echo "  </td>\n";
    echo "   </tr>\n";
}
$z = 2;
$q = $db->query('SELECT * FROM `apps` ORDER BY `id` DESC  LIMIT ?i, ?i', [$start, $set['p_str']]);
while ($apps = $q->row()) {
    echo "<td class='z_".($z % 2 ? 1 : 2)."'>";
    ++$z;
    echo '<a href="/apps/'.($apps['name_lat'] != null ? $apps['name_lat'] : $apps['id']).'">'.output_text($apps['name']).'</a>';
    echo " </td>";
    echo "   </tr>";
    echo "   <tr>";
    echo "  <td class='p_m'>\n";
    echo output_text($apps['opis']).'<br/>';
    echo '<a href="/apps/'.($apps['name_lat'] != null ? $apps['name_lat'] : $apps['id']).'">'.lang('Запуск').'</a>';
    echo "  </td>";
    echo "   </tr>";
}
echo "</table>";
if ($k_page>1) {
    str("?", $k_page, $page); // Вывод страниц
}
