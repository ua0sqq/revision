<?php

only_reg();
$back_link_set = true;
$set['title'] = 'персональная ссылка';

#------>Обработка введенных данных
if (isset($_POST['save'])) {
    $mylink = trim($_POST['mylink']);
    
    $mylink = str_replace('\\', null, $mylink);

    if ($mylink == $user['mylink']) {
        $err = lang('Эта ссылка уже занята вами');
    }
    $count = count::query('user', ' `mylink` = "' . $mylink . '"');

    if ((int)$count > 0) {
        $err = lang('Ссылка уже занята');
    }
    //файл где ищем
    $file_default = file_get_contents(H . 'sys/dat/if_user_link.txt');

    //если  нету записываем
    if (stripos($file_default, $mylink) !== false) {
        $err = lang('Такие ссылки запрещены');
    }
    
    
    $cache_file = H.'sys/dat/if_user_link_dir.txt';
    $file_err_c = @file_get_contents($cache_file);

    $cache_time = 86400;

    if (!is_file($cache_file) or  time() - $cache_time < filemtime($cache_file)) {
        $dir_err = opendir(H.'/');
        while ($file_err = readdir($dir_err)) {
            if (preg_match("#^([A-z0-9\_])+$#ui", $file_err)) {
                if (is_dir(H.'/') && $file_err != '.' && $file_err != '..' && $file_err != '.htaccess') {
                    if (!stripos($file_err_c, $file_err) !== false) {
                        file_put_contents($cache_file, $file_err ."\n", FILE_APPEND);
                    }
                } else {
                    continue;
                }
            }
        }
    }

    if (stripos($file_err_c, $mylink) !== false) {
        $err = lang('Такие ссылки использовать нельзя');
    }
    
    //if id(num) что б не-было некоректных ссылок в анкетах
    if (preg_match("#^[^\s]*id[0-9][^\s]*$#ui", $mylink)) {
        $err = lang('Используются запрещенные символы#2');
    }

    if (!preg_match("#^([A-z0-9_])+$#ui", $mylink)) {
        $err = lang('Используются запрещенные символы');
    }


    if (strlen2($mylink) <= 4) {
        $err = lang('Ссылка должна быть не короче 5 символов');
    }
    
    if (strlen2($mylink) > 13) {
        $err = lang('Ссылка должна не  больше 9 символов');
    }

    if (!isset($err)) {
        $db->query('UPDATE `user` SET `mylink`=? WHERE `id`=?i LIMIT ?i', [$mylink, $user['id'], 1]);
        #	удаляем кэш файл
        cache_delete::user($user['id']);
        $_SESSION['message'] = lang('Успешно');
        exit(header('Location: ?'));
    }
}
#------>Обработка введенных данных
err();
if ($user['mylink'] == 'id' . $user['id']) {
    echo '<div class="msg">
	Вы еще не устанавливали свою персональную ссылку 
</div>';
}
#------>Форма ввода
echo "<div class='p_m'>";
echo "<form method='post' action=''>";
echo lang('Ваша персональная ссылка').":<br /> http://".htmlspecialchars($_SERVER['SERVER_NAME'])."/<input type='text' name='mylink' value='".$user['mylink']."' />";
echo "<input type='submit' name='save' value='".lang('Изменить')."' /></form></div>
<div class='p_m'>";
echo lang('Вы можете создать свою короткую ссылку на страницу').'<br />';
echo lang('Пример').': fiera , fiera1992 ,fiera_1992, 1992_fiera.<br />';
echo "</div>";
#------>Конец Формы
