<?php

only_reg();
$set['title'] = 'Журнал';

unset($_COOKIE['jurnal_count']);

if (isset($_GET['type_jurnal'])) {
    $types = $_GET['type_jurnal'];
    $data =  ['jurnal', ['id_user' => 0, 'id_kont' => $user['id'], 'type' => $types]];
    $upData = [['read' => '1'],  ['id_kont' => $user['id'], 'type' => $types]];
} else {
    $types = '';
    $data =  ['jurnal', ['id_user' => 0, 'id_kont' => $user['id']]];
    $upData = [['read' => '1'],  ['id_kont' => $user['id']]];
}

$db->query('UPDATE `jurnal` SET ?s WHERE ?w', $upData);

$k_post  = $db->query('SELECT COUNT(*) FROM ?c WHERE ?w', $data)->el();
?>
<div class="p_m" style="text-align:center;">
 	<a href="?type_jurnal=my_chat"><?php echo lang('Гостевая');?></a> ::
	<a href="?type_jurnal=obmen"><?php echo lang('Обменник');?></a> ::
	<a href="?type_jurnal=my_foto"><?php echo lang('Фото');?></a> ::
	<a href="?type_jurnal=rating"><?php echo lang('Оценки');?></a> ::
	<a href="?"><?php echo lang('Все действия');?></a>
</div>
<?php

if ($k_post == 0) {
    msg('Журнал пуст!');
} else {
    $k_page = k_page($k_post, $set['p_str']);
    $page    = page($k_page);
    $start    = $set['p_str']*$page - $set['p_str'];

    $jurnal_sql = $db->query('SELECT * FROM ?c WHERE ?w ORDER BY id DESC LIMIT ' . $start . ', ' . $set['p_str'],
                             $data);
    while ($row = $jurnal_sql->row()) {
        echo "<div class='p_m'><span class='ank_n'>(".vremja($row['time']).")</span> ".output_text($row['msg'])."</div>";
    }
    $type_p = isset($_GET['type_jurnal']) ? '?type_jurnal=' . htmlspecialchars($_GET['type_jurnal']) . '&amp;' : '?';

    if ($k_page > 1) {
        str($type_p, $k_page, $page); // Вывод страниц
    }
}
