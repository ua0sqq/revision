<?php

only_reg();

$cache_file = H . 'sys/cache/user_menu/index_' . md5($user['id'] . $_SERVER['REMOTE_ADDR']) . '.html';
$cache_time = $set['user_cab_time'];
if (is_file($cache_file) && time() - $cache_time < filemtime($cache_file)) {
    echo '<!--  dcms-fiera ' . date('H:i', filemtime($cache_file)) . ' Время кэша))-->';
    require_once($cache_file);
} else {
    ob_start();

    $q_menu = $db->query('SELECT * FROM `user_menu`  WHERE `type_set`=? ORDER BY `pos` ASC', ['index']);
    while ($post_menu = $q_menu->row()) {
        if ($post_menu['type'] != 'inc') {
            if ($post_menu['type']=='link') {
                echo '<a class="menu_s" style="display: block;" href="' . $post_menu['url'] . '">';
            } else {
                echo '<div class="menu_razd">';
            }

            echo lang($post_menu['name']);

            if ($post_menu['type'] == 'link') {
                echo '</a>';
            } else {
                echo '</div>';
            }
        } else {
            if ($post_menu['type'] == 'inc' and is_file(H . 'sys/user/index/'.$post_menu['url'])) {
                include_once H . 'sys/user/index/' . $post_menu['url'];
            }
        }
    }
    if (is_writeable($cache_file)) {
        $cached = fopen($cache_file, 'w');
        fwrite($cached, ob_get_contents());
        fclose($cached);
    }
    ob_end_flush(); # Отправялем вывод в браузер
}
