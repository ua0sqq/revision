<?php
$set['title'] = 'Пользователи';

//Считаем число юзеров
$us_counter = count::query('user');

$us_guest = count::query('guests', "`date_last` > '".(time()-$set['user_online'])."'");

if ($us_guest > 0) {
    $us_guest = '[<span style="color:#802000">'. $us_guest.'</span>]';
} else {
    $us_guest = null;
}



$us_new = count::query('user', " `date_reg` > '".(time()-$set['user_reg_time'])."'");
if ($us_new > 0) {
    $us_new = '[<span style="color:red">+'. $us_new.'</span>]';
} else {
    $us_new = null;
}


$us_online = count::query('user', " `date_last` > '".(time()-$set['user_online'])."'");
if ($us_online > 0) {
    $us_online = '[<span style="color:#c06020">'. $us_online.'</span>]';
} else {
    $us_online = null;
}




include_once H.'sys/user/other/users_inc.php';
?>
<div class="<?= $div_mod_fon?>">
	<div class="<?=$div_mod?>">
        <a href="people_search" style="display: block;"><img src="img/search.png" alt="*"/>&nbsp;<?= lang('Поиск людей')?></a>
    </div>
</div>
<div class="<?= $div_mod_fon?>">
    <div class="<?=$div_mod?>">
        <a href="users_list" style="color:gray;display: block;"><img src="img/b.png" alt="*"/>&nbsp;<?= lang('Пользователи')?>&nbsp;[<?= $us_counter?>]</a>
    </div>
    <div class="<?=$div_mod?>">
        <a href="online" style="display: block;"><img src="img/b.png" alt="*"/>&nbsp;<?= lang('Онлайн пользователи')?>&nbsp;<?= $us_online?></a>
    </div>
</div>
<div class="<?= $div_mod_fon?>">
    <div class="<?=$div_mod?>">
        <a href="users_new" style="display: block;"><img src="img/b.png" alt="*"/>&nbsp;<?= lang('Новые')?>&nbsp;<?= $us_new?></a>
    </div>
    <div class="<?=$div_mod?>">
        <a href="online_guest" style="display: block;"><img src="img/b.png" alt="*"/>&nbsp;<?= lang('Гости сайта')?>&nbsp;<?= $us_guest?></a>
    </div>
</div>