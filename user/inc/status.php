<?php

only_reg();
$set['title'] = 'Статус';

#------>Обработка введенных данных
include_once H.'sys/inc/shif.php';
//включаем ссылку возврата назад
$back_link = true;

if (isset($_POST['save'])) {
    if (isset($_POST['status_ank']) and strlen2($_POST['status_ank']) <= 512) {
        $db->query('UPDATE `user` SET `status_ank`=? WHERE `id`=?i LIMIT ?i', [$_POST['status_ank'], $user['id'], 1]);
    } else {
        $err[]=lang('О Статусе нужно писать меньше :)');
    }
    $_SESSION['message'] = lang('Успешно');
     
     //Удаляем кэш
     cache_delete::user($user['id']);
    exit(header('Location: /'.$user['mylink']));
}
#------>Обработка введенных данных

err();
 
#------>Форма ввода
echo "<form method='post' action=''>
<div class='p_m'>".lang('Статус').":<br />	
<div class='status_o_s'> </div><div class='status_o' > 			
<textarea name='status_ank' maxlength='512'>".outform($user['status_ank'])."</textarea>
</div></div>
<div class='p_m'>
<input type='submit' name='save' value='".lang('Сохранить')."' />";
echo "</form>";
echo "</div>";
#------>Конец Формы
