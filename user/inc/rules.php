<?php

$rules_lang = $set['lang'];
$set['title'] = 'Правила';

if (!preg_match('#^[A-z0-9-\._]$#ui', $rules_lang)) {
    echo "<div class='p_m'>";
    if (is_file(H.'sys/lang/'.$rules_lang.'/rules.txt')) {
        $f=file(H.'sys/lang/'.$rules_lang.'/rules.txt');
        $k_page=k_page(count($f), $set['p_str']);
        $page=page($k_page);
        $start = $set['p_str']*($page-1);
        $end = $set['p_str']*$page;
        for ($i= $start; $i < $end and $i<count($f);$i++) {
            echo output_text($f[$i])."<br />";
        }

        echo "</div>";
        if ($k_page>1) {
            str("?", $k_page, $page);
        } // Вывод страниц
    }

    if (!is_file(H.'sys/lang/'.$rules_lang.'/rules.txt')) {
        msg(lang('Не найден файл правил'));
    }
}
