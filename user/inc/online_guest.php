<?php

only_reg();
$set['title'] = 'Гости на сайте';
$guest_show_ip = user_access('guest_show_ip');
$k_post  = $db->query('select count(*) from `guests` where `date_last`>?i AND `pereh`>?i',
                      [time() - $set['user_online'], 0])->el();
$k_page =k_page($k_post, $set['p_str']);
$page    =page($k_page);
$start    =$set['p_str']*$page-$set['p_str'];
$q = $db->query('SELECT * FROM `guests` WHERE `date_last`>?i AND `pereh`>?i ORDER BY `date_aut` DESC LIMIT ?i, ?i',
                [(time() - $set['user_online']), 0, $start, $set['p_str']]);
echo "<table class='post'>\n";

if ($k_post==0) {
    echo msg(lang('Нет гостей на сайте'));
} else {
    while ($guest = $q->row()) {
        echo "   <tr>";
        echo "  <td class='p_t'>";
        echo lang('Гость');
        echo "  </td>";
        echo "   </tr>";
        echo "   <tr>";
        echo "  <td class='p_m'>";
        echo "<span class=\"ank_n\">".lang('Посл. посещение').":</span> <span class=\"ank_d\">". vremja($guest['date_last']) ."</span><br />\n";
        echo "<span class=\"ank_n\">".lang('Переходов').":</span> <span class=\"ank_d\">$guest[pereh]</span><br />\n";
        if ($guest['ua']!=null) {
            echo "<span class=\"ank_n\">UA:</span> <span class=\"ank_d\">$guest[ua]</span><br />\n";
        }

        if (isset($user) && $user['level'] > 0) {
            if ($guest_show_ip && $guest['ip']!=0) {
                echo '<span class="ank_n">IP:</span> <span class="ank_d">' . long2ip($guest['ip']) . '</span><br />';
            }

            if ($guest_show_ip && $opsos = opsos($guest['ip'])) {
                echo "<span class=\"ank_n\">".lang('Пров').":</span> <span class=\"ank_d\">".$opsos."</span><br />\n";
            }
            if (otkuda($guest['url'])) {
                echo "<span class=\"ank_n\">URL:</span> <span class=\"ank_d\"><a href='$guest[url]'>".otkuda($guest['url'])."</a></span><br />\n";
            }
        }
        echo "  </td>\n";
        echo "   </tr>\n";
    }

    echo "</table>\n";

    if ($k_page>1) {
        str("?", $k_page, $page); // Вывод страниц
    }
}
