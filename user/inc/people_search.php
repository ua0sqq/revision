<?php

only_reg();
$set['title'] = 'Поиск людей';

if (isset($_SESSION['save_z']) && !empty($_SESSION['save_z'])) {
    $_SESSION['usearchs'] = '';
    $_SESSION['usearchs_pol'] = '';
} else {
    $_SESSION['save_z'] = 0;
}

//если поисковой запрос не содержит сессии ,выбираем противоположный пол по умолчанию
 if (empty($_SESSION['usearchs_pol']) or $_SESSION['usearchs_pol'] == null) {
     $_SESSION['usearchs_pol'] = $user['pol'] ?: 0;
 }

if (!isset($_POST['search'])) {
    echo '
	<div class="p_m">
	<form method="post" action="">
	<p><input type="text" name="search" value="' . (isset($_SESSION['usearchs']) ? $_SESSION['usearchs'] : '') . '" required="required"/>

	<p><select name="pol">
	<option value="0" '.($_SESSION['usearchs_pol'] == 0 ?" selected='selected'": false).'> '.lang('Женский').'</option>
	<option value="1" '.($_SESSION['usearchs_pol'] == 1 ?" selected='selected'": false).'> '.lang('Мужской').'</option>
	</select>

	<p><button>'.lang('Искать').'</button></p>

	<div class="p_m">
	<select name="save_z">
	<option value="0" '.($_SESSION['save_z'] == 0 ?" selected='selected'": false).'> '.lang('Сохранять поисковой запрос').'</option>
	<option value="1" '.($_SESSION['save_z'] == 1 ?" selected='selected'": false).'> '.lang('Не сохранять').'</option>
	</select>
	</div>
	</form>
	<div class="p_m">
   * Пример: Saint Андрей Волгоград <br/>
   * Пример: Saint 17.04.1992 <br/>
	</div>
	</div>
	';
}
$search = filter_input(INPUT_POST, 'search', FILTER_DEFAULT);

if ($search) {
    $_SESSION['usearchs'] = $_POST['search'];
    $arg = array();

    $_SESSION['usearchs_pol'] = $pol = intval($_POST['pol']);
    $_SESSION['save_z'] = intval($_POST['save_z']);


    $pattern = 'FROM `user`
WHERE (`id`=?i OR `nick` LIKE "%?e%" OR `ank_city` LIKE "%?e%" OR `ank_name` LIKE "%?e%" OR `ank_icq` LIKE "%?e%" OR `ank_o_sebe` LIKE "%?e%" OR `ank_mail` LIKE "%?e%" OR `mylink` LIKE "%?e%") AND `pol`=?string';
    $data = [$search, $search, $search, $search, $search, $search, $search, $search, $pol];
    $selCount = 'SELECT COUNT(*) AS cnt ';
    $select = 'SELECT * ';

    $k_post = $db->query($selCount . $pattern, $data)->row();//var_dump($k_post); exit;

if (!$k_post = $k_post['cnt']) {
    msg('Ничего не найдено');
    echo '<div class="menu_s"><a href="?">&nbsp;Искать снова</a></div>' . PHP_EOL;
} else {
    echo '<table class="post">' . PHP_EOL;
    $sql_s = $db->query($select . $pattern, $data);
    while ($ank = $sql_s->row()) {
        echo '  <tr>' .
PHP_EOL . '     <td class="avar" rowspan="2">' . PHP_EOL;
        avatar($ank['id'], 110, 110);
        echo '      </td>' . PHP_EOL;
        echo '  <tr>' .
PHP_EOL . '     <td class="p_m">' . PHP_EOL;

        echo nick($ank['id']);
        echo($ank['ank_name'] != null ? ' , <strong>'.output_text($ank['ank_name']).'</strong>' : false).'
   '.($ank['ank_city'] != null ? ' , <strong>'.output_text($ank['ank_city']).'</strong>' : false);


        if ($ank['status_ank'] != null) {
            $text = output_text($ank['status_ank']);
            $res = mb_substr($text, 0, 99);
            if ($text != $res) {
                $res .= '...';
            }

            echo '<div class="status_o_s"> </div>' .
PHP_EOL . '<div class="status_o"> ' . $res . ' </div>' . PHP_EOL;
        }
        echo '      </td>' .
PHP_EOL . ' </tr>' . PHP_EOL;
    }
    echo '</table>' . PHP_EOL;

    echo '<div class="menu_s"><a href="?">&nbsp;Искать снова</a></div>' . PHP_EOL;
}
}
