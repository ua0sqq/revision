<?php

$k_post  = count::query('user');
$k_page = k_page($k_post, $set['p_str']);
$page    = page($k_page);
$start    = $set['p_str']*$page-$set['p_str'];
echo "<table class='post'>";


$q=$db->query('SELECT * FROM  `user`  ORDER BY `date_last`  DESC LIMIT ?i, ?i', [$start, $set['p_str']]);
while ($ank = $q->row()) {
    echo "<tr><td class='avar' rowspan='2'>";
    avatar($ank['id'], 110, 110);
    echo "</td>";

    echo "<tr><td class='p_m'>";
    if (user_access('user_prof_edit') and $user['group_access'] > $ank['group_access'] and $user['id'] != $ank['id']) {
        echo "<span style='float:right' class='ank_span_m'> <a href='".APANEL."/user.php?id=$ank[id]'>".lang('Редактировать профиль')."</a></span>";
    }
 
    echo nick($ank['id']);
    echo($ank['ank_name'] != null ? ' , <b>'.output_text($ank['ank_name']).'</b>' : false)."
   ".($ank['ank_city'] != null ? ' , <b>'.output_text($ank['ank_city']).'</b>' : false);


    if ($ank['status_ank'] != null) {
        $text = output_text($ank['status_ank']);
        $res = mb_substr($text, 0, 99);
        if ($text != $res) {
            $res .= '...';
        }

        echo "
<div class='status_o_s'> </div>
<div class='status_o' > ".$res." </div>

";
    }

    echo "</td></tr>";
}
echo "</table>";
if ($k_page > 1) {
    str('?', $k_page, $page);
}
