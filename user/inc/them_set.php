<?php

only_reg();
$back_link_set = true;
$set['title'] = 'Настройки тем оформления';

if (isset($_POST['save'])) {
    if (isset($_POST['set_them']) && preg_match('#^([A-z0-9\-_\(\)]+)$#ui', $_POST['set_them']) && is_dir(H.'style/themes/'.$_POST['set_them'])) {
        $db->query('UPDATE `user` SET `set_them`=? WHERE `id`=?i LIMIT ?i', [$_POST['set_them'], $user['id'], 1]);
    } elseif (isset($_POST['set_them2']) && preg_match('#^([A-z0-9\-_\(\)]+)$#ui', $_POST['set_them2']) && is_dir(H.'style/themes/'.$_POST['set_them2'])) {
        $db->query('UPDATE `user` SET `set_them2`=? WHERE `id`=?i LIMIT ?i', [$_POST['set_them2'], $user['id'], 1]);
    } else {
        $err = lang('Ошибка применения темы');
    }

//Удаляем кэш
cache_delete::user($user['id']);

    if (!isset($err)) {
        $_SESSION['message'] = lang('Успешно');
        exit(header('Location: ?'));
    }
}

echo "<form method='post' action=''>";

echo "<div class='p_m'>Тема :<br /><select name='set_them".($webbrowser?'2':null)."'>\n";
$opendirthem=opendir(H.'style/themes');
while ($themes=readdir($opendirthem)) {
    // пропускаем корневые папки и файлы
if ($themes=='.' || $themes=='..' || !is_dir(H."style/themes/$themes")) {
    continue;
}
// пропускаем темы для определенных браузеров
if (file_exists(H."style/themes/$themes/.only_for_".($webbrowser?'wap':'web'))) {
    continue;
}

    echo "<option value='$themes'".($user['set_them'.($webbrowser?'2':null)]==$themes?" selected='selected'":null).">".trim(file_get_contents(H.'style/themes/'.$themes.'/them.name'))."</option>";
}
closedir($opendirthem);
echo "</select></div>";
echo "<div class='p_m'><input type='submit' name='save' value='Выбрать' /></div>";
echo "</form>";
