<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once  H . 'sys/inc/start.php';
include_once  H . 'sys/inc/sess.php';
include_once  H . 'sys/inc/settings.php';
include_once  H . 'sys/inc/db_connect.php';
include_once  H . 'sys/inc/ipua.php';
include_once  H . 'sys/inc/fnc.php';
$banpage = true;
//$show_all = true; // показ для всех
include_once  H . 'sys/inc/user.php';
$set['title'] = lang('Кабинет');
include_once  H . 'sys/inc/thead.php';
title();
aut();

$div_mod = 'user_dir';
$div_mod_fon = 'user_fon';

//включаем ссылку возврата назад
$back_link = false;

//включаем ссылку возврата назад в настройки
$back_link_set = null;

//для дебилов (это не уязвимость ,юзайте php - su,net,ru)
$module_loads = isset($_GET['type_modules']) ? $_GET['type_modules'] : false;

if (preg_match('/\.php$/i', $module_loads)) {
    $module_s = true;
} else {
    $module_s = false;
}

if (file_exists(H . 'user/inc/' . $module_loads . '.php') and $module_s == false) {
    include_once 'inc/' . $module_loads . '.php';

    if ($back_link_set != false and isset($user)) {
        echo '<div class="p_m"><a href="/user/settings" style="color:#777;display:block;"><img src="img/1.png" alt="*" /> ' . lang('К Настройкам') . '</a></div>';
    }

    if ($back_link == false and isset($user)) {
        echo '<div class="p_m"><a href="/user" style="color:#777;display:block;"><img src="img/home.png" alt="*" /> ' . lang('В кабинет') . '</a></div>';
    }
} else {
    include_once 'inc/index.php';
}
/*
не-было желания прописывать в каждом файле инклуд функции ,aut,title и переменную заголовка .
пришлось решать проблему вот таким образом ,знаю ,знаю опера мини.
*/
?>
<script type="text/javascript">
    document.title = '<?php echo lang($set['title']); ?>';
</script>
<?php

include_once  H . 'sys/inc/tfoot.php';

?>