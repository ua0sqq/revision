<?php
$set['web'] = false;

header("Content-type: text/html");
?><!DOCTYPE HTML>
<html>
<head>
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
    <link rel="shortcut icon" href="/style/themes/<?php echo $set['set_them'];?>/favicon.ico" />
    <link rel="stylesheet" href="/style/themes/<?php echo $set['set_them'];?>/style.css" type="text/css" />
    <link rel="yandex-tableau-widget" href="/sys/js/yandex_tablo/conf.json" />
    <?php echo($set['bb_youtube'] == 1 ? $css : '');?>
    <title>
      <?php echo $set['title'];?>
    </title>
</head>
<body>
    <div class="logos">
        <a href="/">
<?php
#	Включаем время в шапке сайта
    echo ($set['times_head'] == 1)  ? ' <span class="time_head"> '.date('H:i:s').' </span> ':(false);
?>
<img src="/style/themes/<?php echo $set['set_them'];?>/logos.png" alt="logo"/>
</a>
    </div>
<?php

if (isset($_SESSION['message'][0])) {
    echo '<div class="msg">
    <img src="/style/icons/msg.png" style="float: left;margin-bottom: 0px;" alt="*" />
    ' . output_text($_SESSION['message']) . '
    </div>' . PHP_EOL;
    unset($_SESSION['message']);
}
