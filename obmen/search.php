<?php

if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}
include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/obmen.php';
include_once H . 'sys/inc/user.php';

$set['title'] = 'Поиск файлов'; // заголовок страницы
include_once H . 'sys/inc/thead.php';
title();
aut();

$search = null;
if (isset($_SESSION['search'])) {
    $search=$_SESSION['search'];
}
if (isset($_POST['search'])) {
    $search=$_POST['search'];
}
$_SESSION['search'] = $search;

$search = preg_replace("#( ){2,}#", " ", $search);
$search = preg_replace("#^( ){1,}|( ){1,}$#", "", $search);



if (isset($_GET['go']) && $search != null) {
    $search_a = explode(' ', $search);

    for ($i = 0; $i < count($search_a); $i++) {
        $search_a2[$i] = '<span class="search_c">' . stripcslashes(htmlspecialchars($search_a[$i])) . '</span>';
        $search_a[$i] = stripcslashes(htmlspecialchars($search_a[$i]));
    }

    $q_search = str_replace('%', '', $search);
    $q_search = str_replace(' ', '%', $q_search);

    $k_post = $db->query('SELECT COUNT(*) FROM `obmennik_files` WHERE `opis` LIKE "%?e%" OR `name` LIKE "%?e%"', [$q_search, $q_search])->el();
    $k_page=k_page($k_post, $set['p_str']);
    $page=page($k_page);
    $start=$set['p_str']*$page-$set['p_str'];
    if ($k_post == 0) {
        echo "<div class=\"err\">\nНет результатов</div>\n";
    }

    $q = $db->query('SELECT * FROM `obmennik_files` WHERE `opis` LIKE "%?e%" OR `name` LIKE "%?e%" ORDER BY `time` DESC LIMIT ?i, ?i',
                [$q_search, $q_search, $start, $set['p_str']]);
    $i = 0;
    while ($post = $q->row()) {
        $ras = $post['ras'];
        $file = H . "sys/obmen/files/$post[id].dat";
        $name = $post['name'];
        $size = $post['size'];
        $dir = $db->query('SELECT `dir`, `name` FROM `obmennik_dir` WHERE `id`=?i LIMIT ?i', [$post['id_dir'], 1])->row();
        echo "<table class='post'>\n";
        if ($set['set_show_icon'] == 2) {
            echo "  <td class='icon48' rowspan='2'>\n";
            include 'inc/icon48.php';
            echo "  </td>\n";
        } elseif ($set['set_show_icon'] == 1) {
            echo "  <td class='icon14'>\n";
            include 'inc/icon14.php';
            echo "  </td>\n";
        }

        echo "  <td class='p_t'>\n";



        if ($set['echo_rassh']==1) {
            $ras = ".$post[ras]";
        } else {
            $ras = null;
        }

        echo "<a href='/obmen$dir[dir]$post[name].$post[ras]?showinfo'>$post[name]$ras</a><br />\n";

        echo "  </td>\n";
        echo "   </tr>\n";
        echo "   <tr>\n";
        if ($set['set_show_icon']==1) {
            echo "  <td class='p_m' colspan='2'>\n";
        } else {
            echo "  <td class='p_m'>\n";
        }

        include 'inc/opis.php';
        echo ' Папка: <a href="/obmen'.$dir['dir'].'?">'.$dir['name'].'</a>';
        echo "</table>\n";
    }
    if ($k_page>1) {
        str("search.php?go&amp;", $k_page, $page);
//print'<br>';
    } // Вывод страниц
} else {
    echo"<div class='foot'>";
}
echo " Поиск файлов";
echo'</div>';
echo "<form method=\"post\" action=\"search.php?go\" class=\"search\">\n";
$search=stripcslashes(htmlspecialchars($search));
echo "<input type=\"text\" name=\"search\" maxlength=\"64\" value=\"$search\" /><br />\n";
echo "<input type=\"submit\" value=\"Поиск\" />\n";
echo "</form>\n";

echo "<div class='foot'>";
echo "&laquo;<a href='/obmen/'>Обменник</a><br />\n";
echo "</div>\n";

include_once H . 'sys/inc/tfoot.php';
