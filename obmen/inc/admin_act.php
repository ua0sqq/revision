<?php

if (user_access('obmen_dir_delete') && isset($_GET['act']) && $_GET['act'] == 'delete' && isset($_GET['ok']) && $l != '/') {
    $q = $db->query('SELECT * FROM `obmennik_dir` WHERE `dir_osn` like "?e%"', [$l]);

    while ($post = $q->row()) {
        $q2 = $db->query('SELECT * FROM `obmennik_files` WHERE `id_dir` =?i', [$post['id']]);
        while ($post2 = $q2->row()) {
            if (is_file(H . 'sys/obmen/files/' . $post2['id'] . '.dat')) {
                unlink(H . 'sys/obmen/files/' . $post2['id'] . '.dat');
            }
            array_map('unlink', glob(H . 'sys/obmen/screens/128/' . $post2['id'] . '.*'));
            array_map('unlink', glob(H . 'sys/obmen/screens/48/' . $post2['id'] . '.*'));
        }
        $db->query('DELETE FROM obmennik_dir, obmennik_files, obmennik_komm
USING
	obmennik_dir
	INNER JOIN obmennik_files ON obmennik_files.id_dir = obmennik_dir.id
	LEFT JOIN obmennik_komm ON obmennik_komm.id_file = obmennik_files.id
WHERE obmennik_dir.`id`=?i ', [$post['id']]);
        $db->query('OPTIMIZE TABLE `obmennik_dir`, `obmennik_files`, `obmennik_komm`');
    }

    $q2 = $db->query('SELECT `id` FROM `obmennik_files` WHERE `id_dir`=?i', [$dir_id['id']]);

    while ($post = $q2->row()) {
        if (is_file(H . 'sys/obmen/files/' . $post['id'] . '.dat')) {
            unlink(H . 'sys/obmen/files/' . $post['id'] . '.dat');
        }
        array_map('unlink', glob(H . 'sys/obmen/screens/128/' . $post['id'] . '.*'));
        array_map('unlink', glob(H . 'sys/obmen/screens/48/' . $post['id'] . '.*'));
    }

    $db->query('DELETE FROM obmennik_dir, obmennik_files, obmennik_komm
USING
	obmennik_dir
	INNER JOIN obmennik_files ON obmennik_files.id_dir = obmennik_dir.id
	LEFT JOIN obmennik_komm ON obmennik_komm.id_file = obmennik_files.id
WHERE obmennik_dir.`id`=?i ', [$dir_id['id']]);
    $db->query('OPTIMIZE TABLE `obmennik_dir`, `obmennik_files`, `obmennik_komm`');

    $l = $dir_id['dir_osn'];
    msg('Папка успешно удалена');
    admin_log('Обменник', 'Удаление папки', 'Папка ' . $dir_id['name'] . ' удалена');

    $dir_id = $db->query('SELECT * FROM `obmennik_dir` WHERE `dir`=? OR `dir`=? OR `dir`=? LIMIT ?i', ['/' . $l, $l . '/', $l, 1])->row();
    $id_dir=$dir_id['id'];
}

if (user_access('obmen_dir_edit') && isset($_GET['act']) && $_GET['act'] == 'mesto' && isset($_GET['ok']) && isset($_POST['dir_osn']) && $l != '/') {
    if (!isset($_POST['dir_osn'])) {
        $err= "Не выбран коненый путь";
    } else {
        $q = $db->query('SELECT * FROM `obmennik_dir` WHERE `dir_osn` LIKE "?e%"', [$l]);
        while ($post = $q->row()) {
            $new_dir_osn=preg_replace("#^$l/#", $_POST['dir_osn'], $post['dir_osn']).$dir_id['name'].'/';
            $new_dir=$new_dir_osn.$post['name'];// $new_dir/', `dir_osn`='$new_dir_osn' WHERE `id` = '$post[id]' LIMIT 1
        $db->query('UPDATE `obmennik_dir` SET `dir`=?, `dir_osn`=? WHERE `id`=?i LIMIT ?i',
                   [$new_dir. '/', $new_dir_osn, $post['id'], 1]);
        }

        $l = trim($_POST['dir_osn']);
//'".$l."$dir_id[name]/', `dir_osn`='".$l."' WHERE `id` = '$dir_id[id]' LIMIT 1
    $db->query('UPDATE `obmennik_dir` SET `dir`=?, `dir_osn`=? WHERE `id`=?i LIMIT ?i', [$l . $dir_id['name'] . '/', $l, $dir_id['id'], 1]);
        admin_log('Обменник', 'Изменение папки', "Папка '$dir_id[name]' перемещена");
        msg('Папка успешно перемещена');
        $dir_id = $db->query('SELECT * FROM `obmennik_dir` WHERE `id`=?i LIMIT ?i', [$dir_id['id'], 1])->row();
        $id_dir = $dir_id['id'];
    }
}

if (user_access('obmen_dir_edit') && isset($_GET['act']) && $_GET['act']=='rename' && isset($_GET['ok']) && isset($_POST['name']) && $l!='/') {
    if ($_POST['name']==null) {
        $err[] = 'Введите название папки';
    } elseif (!preg_match("#^([A-zА-я0-9\-\_\(\)\ ])+$#ui", $_POST['name'])) {
        $err[]='В названии присутствуют запрещенные символы';
    } else {
        $newdir = retranslit(esc($_POST['name'], 1));

        if (!isset($err)) {
            if ($l != '/') {
                $l .= '/';
            }
            $downpath = preg_replace('#[^/]*/$#', null, $l);
// esc($_POST['name'],1)."' WHERE `dir` = '/$l' OR `dir` = '$l/' OR `dir` = '$l' LIMIT 1
            $db->query('UPDATE `obmennik_dir` SET `name`=? WHERE `dir`=? OR `dir`=? OR `dir`=? LIMIT ?i',
                       [esc($_POST['name'], 1), '/' . $l, $l . '/', $l, 1]);
            msg('Папка успешно переименована');
            admin_log('Обменник', 'Изменение папки', "Папка '$dir_id[name]' переименована в '".esc($_POST['name'], 1)."'");

            $l = $downpath . $newdir;
            $dir_id = $db->query('SELECT * FROM `obmennik_dir` WHERE `dir`=? OR `dir`=? OR `dir`=? LIMIT ?i', ['/' . $l, $l . '/', $l, 1])->row();
            $id_dir = $dir_id['id'];
        }
    }
}

if (user_access('obmen_dir_create') && isset($_GET['act']) && $_GET['act']=='mkdir' && isset($_GET['ok']) && isset($_POST['name'])) {
    if (!isset($_POST['name'])) {
        $err[] = 'Введите название папки';
    } elseif (!preg_match("#^([A-zА-я0-9\-\_\(\)\ ])+$#ui", $_POST['name'])) {
        $err[] = 'В названии присутствуют запрещенные символы';
    } else {
        $newdir = retranslit(esc($_POST['name'], 1));

        if (isset($_POST['upload']) && $_POST['upload']=='1') {
            $upload =1;
        } else {
            $upload = 0;
        }

        if (!isset($_POST['ras']) || $_POST['ras']==null) {
            $upload = 0;
        }

        $size = 0;
        if ($upload == 1 && isset($_POST['size']) && isset($_POST['mn'])) {
            $size = intval($_POST['size']) * intval($_POST['mn']);
            if ($upload_max_filesize<$size) {
                $size = $upload_max_filesize;
            }
        } else {
            $upload=0;
        }

        $ras = trim($_POST['ras']);

        if (!isset($err)) {
            if ($l != '/') {
                $l .= '/';
            }
            $db->query('INSERT INTO `obmennik_dir` (`name` , `ras` , `maxfilesize` , `dir` , `dir_osn` , `upload` ) VALUES ( ?, ?, ?i, ?, ?, ?string)',
                       [esc($_POST['name'], 1), $ras, $size, $l . $newdir . '/', $l, $upload]);
            msg('Папка (' . esc($_POST['name'], 1) . ') успешно создана');
            admin_log('Обменник', 'Создание папки', 'Создана папка (' . esc($_POST['name'], 1) . ')');
        }
    }
}









if (user_access('obmen_dir_edit') && isset($_GET['act']) && $_GET['act']=='set' && isset($_GET['ok'])) {
    if (isset($_POST['upload']) && $_POST['upload']=='1') {
        $upload=1;
    } else {
        $upload=0;
    }

    if (!isset($_POST['ras']) || $_POST['ras']==null) {
        $upload=0;
    }
    $size=0;
    if ($upload==1 && isset($_POST['size']) && isset($_POST['mn'])) {
        $size=intval($_POST['size'])*intval($_POST['mn']);
        if ($upload_max_filesize<$size) {
            $size=$upload_max_filesize;
        }
    } else {
        $upload=0;
    }

// ShaMan
$ras=esc(stripcslashes(htmlspecialchars($_POST['ras'], 1)));
// Тут конец моих дум

if (!isset($err)) {
    if ($l!='/') {
        $l.='/';
    }
    $db->query("UPDATE `obmennik_dir` SET `ras`='$ras', `maxfilesize`='$size', `upload`='$upload' WHERE `id` = '$dir_id[id]'");
    msg('Параметры папки успешно изменены');
    admin_log('Обменник', 'Изменение папки', "Изменены параметры папки '$dir_id[name]'");
    $dir_id = $db->query('SELECT * FROM `obmennik_dir` WHERE `id`=?i LIMIT ?i', [$dir_id['id'], 1])->row();
    $id_dir=$dir_id['id'];
}
}
