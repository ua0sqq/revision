<?php

if ((user_access('obmen_file_delete') || $user['id'] == $file_id['id_user'])  && isset($_GET['act']) && $_GET['act'] == 'edit' && isset($_GET['ok']) && $l != '/') {
    $name=trim($_POST['name_file']);
    $opis = isset($_POST['opis']) ? trim($_POST['opis']) : '';

    if (mb_strlen($name) < 2) {
        $err[]='Короткое Название';
    }
    if (mb_strlen($name) > 32) {
        $err[]='Длинное Название';
    }

    if (!isset($err)) {
        $db->query('UPDATE `obmennik_files` SET `name_file`=?, `opis`=? WHERE `id`=?i LIMIT ?i', [$name, $opis, $file_id['id'], 1]);
        $_SESSION['message'] = 'Файл успешно отредактирован!';
        admin_log('Обменник', 'Редактирование файла', 'Редактирование файла [url=/obmen' . $dir_id['dir'] . ']' . $file_id['name'] . '[/url]');
        exit(header('Location: /obmen' . $dir_id['dir']));
    }
}

if ((user_access('obmen_file_delete') || $user['id'] == $file_id['id_user']) && isset($_GET['act']) && $_GET['act'] == 'delete' && isset($_GET['ok']) && $l != '/') {
    $db->query('DELETE FROM `obmennik_files` WHERE `id` =?i', [$file_id['id']]);
    if (is_file(H . 'sys/obmen/files/' . $file_id['id'] . '.dat')) {
        unlink(H . 'sys/obmen/files/' . $file_id['id'] . '.dat');
    }

    array_map('unlink', glob(H . 'sys/obmen/screens/128/' . $file_id['id'] . '.*'));
    array_map('unlink', glob(H . 'sys/obmen/screens/48/' . $file_id['id'] . '.*'));

    $_SESSION['message'] = 'Файл удален с сервера!';
    exit(header("Location: /obmen$dir_id[dir]?".SID));
}
