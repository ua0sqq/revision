<?php

if (user_access('obmen_file_edit') || $user['id']==$file_id['id_user']) {
    if (isset($_GET['act']) && $_GET['act']=='edit' && $l!='/') {
        echo "<form method=\"post\"  action=\"?showinfo&amp;act=edit&amp;ok\">\n";
        echo "Название файла:<br />\n";
        echo "<input name='name_file' type='text' maxlength='32' value='".output_text($file_id['name_file'])."' /><br />\n";
        echo "Описание:<br />\n";
        echo "<textarea name=\"opis\">".output_text($file_id['opis'])."</textarea><br />\n";
        if ($user['set_translit']==1) {
            echo "<label><input type=\"checkbox\" name=\"translit1\" value=\"1\" /> Транслит</label><br />\n";
        }
        echo "<input value=\"Изменить\" type=\"submit\" /><br />\n";
        echo "<div class=\"foot\">
	&laquo;<a href='?showinfo'>Отмена</a><br />
	</div>\n";
        include_once '../sys/inc/tfoot.php';
        exit;
    } else {
        echo "<div class=\"foot\">
	&raquo;<a href='?showinfo&amp;act=edit'>Редактировать файл</a><br />
	</div>\n";
    }
}

if (user_access('obmen_file_delete') || $user['id']==$file_id['id_user']) {
    if (isset($_GET['act']) && $_GET['act']=='delete' && $l!='/') {
        echo "<div class=\"err\">";
        echo "Удалить файл \"".output_text($file_id['name_file'])."\"?<br />\n";
        echo "<a href='?showinfo&amp;act=delete&amp;ok'>Да</a> \n";
        echo "<a href='?showinfo'>Нет</a><br />\n";
        echo "</div>";
    } else {
        echo "<div class=\"foot\">\n";
        echo "&raquo;<a href='?showinfo&amp;act=delete'>Удалить файл</a><br />\n";
        echo "</div>\n";
    }
}
