<?php

if (user_access('obmen_dir_edit') && isset($_GET['act']) && $_GET['act'] == 'set') {
    echo "<form class=\"foot\" action='?act=set&amp;ok&amp;page=$page' method=\"post\">";
    echo "Название папки:<br />\n";
    echo "<input type='text' name='name' value='$dir_id[name]' /><br />\n";
    if ($dir_id['upload'] == 1) {
        $check=' checked="checked"';
    } else {
        $check=null;
    }
    echo "<label><input type=\"checkbox\"$check name=\"upload\" value=\"1\" /> Выгрузка</label><br />\n";
    echo "Расширения через \";\":<br />\n";
    echo "<input type='text' name='ras' value='$dir_id[ras]' /><br />\n";
    echo "Максимальный размер файлов:<br />\n";
    if ($dir_id['maxfilesize'] < 1024) {
        $size = $dir_id['maxfilesize'];
    } elseif ($dir_id['maxfilesize'] >= 1024 && $dir_id['maxfilesize'] < 1048576) {
        $size = intval($dir_id['maxfilesize'] / 1024);
    } elseif ($dir_id['maxfilesize'] >= 1048576) {
        $size = intval($dir_id['maxfilesize'] / 1048576);
    }

    echo "<input type='text' name='size' size='4' value='$size' />\n";
    echo "<select name='mn'>\n";
    if ($dir_id['maxfilesize'] < 1024) {
        $sel=' selected="selected"';
    } else {
        $sel=null;
    }
    echo "<option value='1'$sel>B</option>\n";
    if ($dir_id['maxfilesize'] >= 1024 && $dir_id['maxfilesize'] < 1048576) {
        $sel=' selected="selected"';
    } else {
        $sel=null;
    }
    echo "<option value='1024'$sel>KB</option>\n";
    if ($dir_id['maxfilesize']>=1048576) {
        $sel=' selected="selected"';
    } else {
        $sel=null;
    }
    echo "<option value='1048576'$sel>MB</option>\n";
    echo "</select><br />\n";
    echo "*настройки сервера не позволяют выгружать файлы объемом более: ".size_file($upload_max_filesize)."<br />\n";
    echo "<input class='submit' type='submit' value='Принять изменения' /><br />\n";
    echo "&laquo;<a href='?'>Отмена</a><br />\n";
    echo "</form>";
}

if (user_access('obmen_dir_create') && isset($_GET['act']) && $_GET['act'] == 'mkdir') {
    echo "<form class=\"foot\" action='?act=mkdir&amp;ok&amp;page=$page' method=\"post\">";
    echo "Название папки:<br />\n";
    echo "<input type='text' name='name' value='' /><br />\n";
    echo "<label><input type=\"checkbox\" name=\"upload\" value=\"1\" /> Выгрузка</label><br />\n";
    echo "Расширения через \";\":<br />\n";
    echo "<input type='text' name='ras' value='' /><br />\n";
    echo "Максимальный размер файлов:<br />\n";
    echo "<input type='text' name='size' size='4' value='500' />\n";
    echo "<select name='mn'>\n";
    echo "<option value='1'>B</option>\n";
    echo "<option value='1024' selected='selected'>KB</option>\n";
    echo "<option value='1048576'>MB</option>\n";
    echo "</select><br />\n";
    echo "*настройки сервера не позволяют выгружать файлы объемом более: ".size_file($upload_max_filesize)."<br />\n";
    echo "<input class='submit' type='submit' value='Создать папку' /><br />\n";
    echo "&laquo;<a href='?'>Отмена</a><br />\n";
    echo "</form>";
}

if (user_access('obmen_dir_edit') && isset($_GET['act']) && $_GET['act'] == 'rename' && $l != '/') {
    echo "<form class='foot' action='?act=rename&amp;ok&amp;page=$page' method=\"post\">";
    echo "Название папки:<br />\n";
    echo "<input type=\"text\" name=\"name\" value=\"$dir_id[name]\"/><br />\n";
    echo "<input class=\"submit\" type=\"submit\" value=\"Переименовать\" /><br />\n";
    echo "&laquo;<a href='?'>Отмена</a><br />\n";
    echo "</form>";
}

if (user_access('obmen_dir_edit') && isset($_GET['act']) && $_GET['act'] == 'mesto' && $l != '/') {
    echo "<form class=\"foot\" action='?act=mesto&amp;ok&amp;page=$page' method=\"post\">";
    echo "Новый путь:<br />\n";
    echo "<select class=\"submit\" name=\"dir_osn\">";
    echo "<option value='/'>[в корень]</option>\n";
    $q= $db->query('SELECT DISTINCT `dir` FROM `obmennik_dir` WHERE `dir` NOT LIKE "?e%" ORDER BY `dir` ASC', [$l]);
    while ($post = $q->row()) {
        echo "<option value='$post[dir]'>$post[dir]</option>\n";
    }
    echo "</select><br />\n";
    echo "<input class=\"submit\" type=\"submit\" value=\"Переместить\" /><br />\n";
    echo "&laquo;<a href='?page=$page'>Отмена</a><br />\n";
    echo "</form>";
}

if (user_access('obmen_dir_delete') && isset($_GET['act']) && $_GET['act'] == 'delete' && $l != '/') {
    echo "<div class=\"err\">";
    echo "Удалить текущую папку ($dir_id[name])?<br />\n";
    echo "<a href='?act=delete&amp;ok&amp;page=$page'>Да</a> \n";
    echo "<a href='?page=$page'>Нет</a><br />\n";
    echo "</div>";
}


if (user_access('obmen_dir_edit') || user_access('obmen_dir_delete') || user_access('obmen_dir_create')) {
    echo "<div class=\"foot\">\n";

    if (user_access('obmen_dir_create')) {
        echo "&raquo;<a href='?act=mkdir&amp;page=$page'>Создать папку</a><br />\n";
    }

    if ($l != '/') {
        if (user_access('obmen_dir_edit')) {
            echo "&raquo;<a href='?act=rename&amp;page=$page'>Переименовать папку</a><br />\n";
            echo "&raquo;<a href='?act=set&amp;page=$page'>Параметры папки</a><br />\n";
            echo "&raquo;<a href='?act=mesto&amp;page=$page'>Переместить папку</a><br />\n";
        }

        if (user_access('obmen_dir_delete')) {
            echo "&raquo;<a href='?act=delete&amp;page=$page'>Удалить папку</a><br />\n";
        }
    }
    echo "</div>\n";
}
