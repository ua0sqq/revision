<?php

if (user_access('obmen_komm_del') && isset($_GET['del_post']) &&
    $db->query('SELECT COUNT(*) FROM `obmennik_komm` WHERE `id` =?i AND `id_file` =?i', [$_GET['del_post'], $file_id['id']])->el()) {
    $db->query('DELETE FROM `obmennik_komm` WHERE `id`=?i LIMIT ?i', [$_GET['del_post'], 1]);
    msg('Комментарий успешно удален');
}
