<?php

//$k_post  = $db->query('SELECT COUNT(*) FROM `obmennik_komm` WHERE `id_file` =?i', [$file_id['id']])->el();

if (!$k_post  = $db->query('SELECT COUNT(*) FROM `obmennik_komm` WHERE `id_file` =?i', [$file_id['id']])->el()) {
    echo '<div class="err">Нет комментариев</div>';
} else {
    $k_page = k_page($k_post, $set['p_str']);
    $page    = page($k_page);
    $start     = $set['p_str']*$page-$set['p_str'];
    echo "<table class='post'>\n";
    $q = $db->query('SELECT * FROM `obmennik_komm` WHERE `id_file`=?i ORDER BY `id` ASC LIMIT ?i, ?i',
                   [$file_id['id'], $start, $set['p_str']]);
    while ($post = $q->row()) {
        echo "   <tr>\n";
        if ($set['set_show_icon']==2) {
            echo "  <td class='icon48' rowspan='2'>\n";
            avatar($post['id_user']);
            echo "  </td>\n";
        }

        echo "  <td class='p_t'>\n";
        echo user($post['id_user'])." (".vremja($post['time']).")\n";
        echo "  </td>\n";
        echo "   </tr>\n";
        echo "   <tr>\n";
        if ($set['set_show_icon']==1) {
            echo "  <td class='p_m' colspan='2'>\n";
        } else {
            echo "  <td class='p_m'>\n";
        }

        echo output_text($post['msg'])."<br />\n";

        if (user_access('obmen_komm_del')) {
            echo "<a href='?komm&amp;page=$page&amp;del_post=$post[id]'>Удалить</a><br />\n";
        }

        echo "  </td>\n";
        echo "   </tr>\n";
    }
    echo "</table>\n";

    if ($k_page>1) {
        str('?komm&amp;', $k_page, $page); // Вывод страниц
    }
}

if (isset($user)) {
    panel_form :: head();
    echo "  <div class='p_m'>";
    echo "<form method=\"post\" name='message' action=\"?komm\">\n";
    if ($set['web'] && is_file(H.'style/themes/'.$set['set_them'].'/altername_post_form.php')) {
        include_once H.'style/themes/'.$set['set_them'].'/altername_post_form.php';
    } else {
        echo "Сообщение:<br />\n<textarea name=\"msg\"></textarea><br />\n";
    }
    if ($user['set_translit']==1) {
        echo "<label><input type=\"checkbox\" name=\"translit\" value=\"1\" /> Транслит</label><br />\n";
    }
    echo "<input value=\"Отправить\" type=\"submit\" />\n";
    echo "</form></div>";
    panel_form :: foot();
}
