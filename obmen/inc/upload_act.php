<?php

if ($dir_id['upload'] == 1 and $user['id'] and (!isset($set['obmen_limit_up']) ||
    $set['obmen_limit_up'] <= $user['balls'])) {
    if (isset($_GET['act']) && $_GET['act'] == 'upload' && isset($_GET['ok']) && $l !=
        '/') {
        if (!isset($_FILES['file'])) {
            $err[] = 'Ошибка при выгрузке файла';
        }
        if (!isset($_FILES['file']['tmp_name']) || filesize($_FILES['file']['tmp_name']) ==
            0) {
            $err[] = 'Ошибка нулевой размер файла !';
        } elseif (!isset($_FILES['file']['tmp_name']) || filesize($_FILES['file']['tmp_name']) >
            $dir_id['maxfilesize']) {
            $err[] = 'Размер файла превышает установленные ограничения';
        } else {
            $file = esc(stripcslashes(htmlspecialchars($_FILES['file']['name'])));
            $file = preg_replace('(\#|\?)', null, $file);
            $name = preg_replace('#\.[^\.]*$#', null, $file); // имя файла без расширения
            $ras = strtolower(preg_replace('#^.*\.#', null, $file));
            $type = $_FILES['file']['type'];
            $size = filesize($_FILES['file']['tmp_name']);
            $rasss = explode(';', $dir_id['ras']);
            $ras_ok = false;
            for ($i = 0; $i < count($rasss); $i++) {
                if ($rasss[$i] != null && $ras == $rasss[$i]) {
                    $ras_ok = true;
                }
            }
            if (!$ras_ok) {
                $err = 'Неверное расширение файла';
            }
        }
        $name_file = $name;
        $name = retranslit(esc($name . '_' . time()));
        $opis = null;
        if (isset($_POST['opis'])) {
            $opis = esc($_POST['opis']);
        }
        if ($db->query('SELECT COUNT(*) FROM `obmennik_files` WHERE `id_dir` =?i AND `name` =?', [$dir_id['id'], $name])->el() != 0) {
            $err[] = 'Файл с таким названием уже есть в этой папке';
        }
        if (!isset($err)) {
            $data = [$dir_id['id'], $name, $ras, $type, $size, $time, $time, $user['id'], $opis, $name_file];
            $pattern = 'INSERT INTO `obmennik_files` (`id_dir`, `name`, `ras`, `type`, `size`, `time`, `time_last`, `id_user`, `opis` ,`name_file`) VALUES ( ?i, ?, ?, ?, ?i, ?i, ?i, ?i, ?, ?)';
            $id_file = $db->query($pattern, $data)->id();

            if (!move_uploaded_file($_FILES['file']['tmp_name'], H . 'sys/obmen/files/' . $id_file . '.dat')) {
                $db->query('DELETE FROM `obmennik_files` WHERE `id` =?i LIMIT ?i', [$id_file, 1]);
                $err[] = 'Ошибка при выгрузке';
            }
        }
        if (!isset($err)) {
            if (isset($_FILES['screen']) && $imgc = @imagecreatefromstring(file_get_contents($_FILES['screen']['tmp_name']))) {
                $img_x = imagesx($imgc);
                $img_y = imagesy($imgc);
                if ($img_x == $img_y) {
                    $dstW = $set['obmen_foto_128_w']; // ширина
                    $dstH = $set['obmen_foto_128_H']; // высота
                } elseif ($img_x > $img_y) {
                    $prop = $img_x / $img_y;
                    $dstW = $set['obmen_foto_128_w'];
                    $dstH = ceil($dstW / $prop);
                } else {
                    $prop = $img_y / $img_x;
                    $dstH = $set['obmen_foto_128_H'];
                    $dstW = ceil($dstH / $prop);
                }
                $screen = imagecreatetruecolor($dstW, $dstH);
                imagecopyresampled($screen, $imgc, 0, 0, 0, 0, $dstW, $dstH, $img_x, $img_y);
                imagedestroy($imgc);
                $screen = img_copyright($screen); // наложение копирайта
                imagegif($screen, H . 'sys/obmen/screens/128/' . $id_file . '.gif');
                imagedestroy($screen);
            }
        }
    }
}
if (isset($_GET['act']) && $_GET['act'] == 'import' && isset($_GET['ok']) && $l != '/') {
    if (!isset($_POST['file'])) {
        $err[] = 'Ошибка при выгрузке файла';
    } else {
        $newfile = basename($_POST["file"]);
        $name = preg_replace('#\.[^\.]*$#i', null, urldecode($newfile));
        $ras = strtolower(preg_replace('#^.*\.#i', null, $newfile));
        $name_file = $name;
        $file = esc(stripcslashes(htmlspecialchars($_POST['file'])));
        $file = preg_replace('(\#|\?)', null, $file);
        $rasss = explode(';', $dir_id['ras']);
        $ras_ok = false;
        for ($i = 0; $i < count($rasss); $i++) {
            if ($rasss[$i] != null && $ras == $rasss[$i]) {
                $ras_ok = true;
            }
        }
        if (!$ras_ok) {
            $err = 'Неверное расширение файла';
        }
    }
    $opis = null;
    if (isset($_POST['opis'])) {
        $opis = trim($_POST['opis']);
    }
    if ($db->query('SELECT COUNT(*) FROM `obmennik_files` WHERE `id_dir`=?i AND `name`=?', [$dir_id['id'], $name])->el() != 0) {
        $err[] = 'Файл с таким названием уже есть в этой папке';
    }
    if (!isset($err)) {
        $data = [$dir_id['id'], $name, $ras, $time, $time, $user['id'], $opis, $name_file];
        $pattern = 'INSERT INTO `obmennik_files` (`id_dir`, `name`, `ras`, `time`, `time_last`, `id_user`, `opis`, `name_file`) VALUES ( ?i, ?, ?, ?i, ?i, ?i, ?, ?)';
        $id_file = $db->query($pattern, $data)->id();

        if (!copyRemoteFile($_POST['file'], H . 'sys/obmen/files/' . $id_file . '.dat')) {
            $db->query('DELETE FROM `obmennik_files` WHERE `id`=?i LIMIT ?i', [$id_file, 1]);
            $err[] = 'Ошибка при выгрузке';
        } else {
            $data = [filesize(H . 'sys/obmen/files/' . $id_file . '.dat'), $id_file, 1];
            $db->query('UPDATE `obmennik_files` SET `size`=?i WHERE `id`=?i LIMIT ?i', $data);
        }
    }
    if (!isset($err)) {
        //chmod(H . 'sys/obmen/files/' . $id_file . '.dat', 0666);
        if (isset($_POST['screen'])) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $_POST['screen']);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            $data = curl_exec($ch);
            curl_close($ch);
            if ($data) {
                $imgc = imagecreatefromstring($data);
                $img_x = imagesx($imgc);
                $img_y = imagesy($imgc);
                if ($img_x == $img_y) {
                    $dstW = $set['obmen_foto_128_w']; // ширина
                    $dstH = $set['obmen_foto_128_H']; // высота
                } elseif ($img_x > $img_y) {
                    $prop = $img_x / $img_y;
                    $dstW = $set['obmen_foto_128_w'];
                    $dstH = ceil($dstW / $prop);
                } else {
                    $prop = $img_y / $img_x;
                    $dstH = $set['obmen_foto_128_H'];
                    $dstW = ceil($dstH / $prop);
                }
                $screen = imagecreatetruecolor($dstW, $dstH);
                imagecopyresampled($screen, $imgc, 0, 0, 0, 0, $dstW, $dstH, $img_x, $img_y);
                imagedestroy($imgc);
                $screen = img_copyright($screen); // наложение копирайта
                imagegif($screen, H . 'sys/obmen/screens/128/' . $id_file . '.gif');
                imagedestroy($screen);
            }
        }
    }
}
