<?php

$list = null;

if ($l == '/') {
    $set['title'] = 'Файловый обменник'; // заголовок страницы
} else {
    $set['title'] = 'Обменник - ' . $dir_id['name']; // заголовок страницы
}

$_SESSION['page'] = 1;
include_once H . 'sys/inc/thead.php';

title();
include 'inc/upload_act.php';

include 'inc/admin_act.php';

err();
aut(); // форма авторизации
echo "<div class='foot'>";
echo "&raquo;<a href='/obmen/search.php'>Поиск файлов</a><br />\n";
echo "</div>\n";

if ($l != '/') {
    echo "<div class='foot'>";
    echo "<a href='/obmen/'>Обменник</a> &gt; " . obmen_path($l) . "<br />\n";
    echo "</div>\n";
}

echo "<table class='post'>\n";

// '/$l' OR `dir_osn` = '$l/' OR `dir_osn` = '$l' ".(!user_access('obmen_dir_edit') ? " and `system` = '0' " : false)."

$q = $db->query("SELECT * FROM `obmennik_dir`
WHERE `dir_osn` = '/$l' OR `dir_osn` = '$l/' OR `dir_osn` = '$l' " . (!user_access('obmen_dir_edit') ? " and `system` = '0' " : false) . " ORDER BY `name`,`num` ASC");

while ($post = $q->row()) {
    $list[] = array(
        'dir' => 1,
        'post' => $post
    );
}

$q = $db->query('SELECT * FROM `obmennik_files` WHERE `id_dir`=?i ORDER BY `time` DESC', [$id_dir]);

while ($post = $q->row()) {
    $list[] = array(
        'dir' => 0,
        'post' => $post
    );
}

$k_post = sizeof($list);
$k_page = k_page($k_post, $set['p_str']);
$page = page($k_page);
$start = $set['p_str'] * $page - $set['p_str'];

if ($k_post == 0) {
    echo "   <tr>\n";
    echo "  <td class='p_t'>\n";
    echo "Папка пуста\n";
    echo "  </td>\n";
    echo "   </tr>\n";
}

for ($i = $start; $i < $k_post && $i < $set['p_str'] * $page; $i++) {
    if ($list[$i]['dir'] == 1) { // папка
        $post = $list[$i]['post'];
        echo "   <tr>\n";
        if ($set['set_show_icon'] == 2) {
            echo "  <td rowspan='2' class='icon48'>\n";
            echo "<img src='/style/themes/$set[set_them]/loads/48/dir.png' alt='' />\n";
            echo "  </td>\n";
        } elseif ($set['set_show_icon'] == 1) {
            echo "  <td class='icon14'>\n";
            echo "<img src='/style/themes/$set[set_them]/loads/14/dir.png' alt='' />\n";
            echo "  </td>\n";
        }

        echo "  <td class='p_t'>\n";
        echo "<a href='/obmen$post[dir]'>" . $post['name'] . "</a>";
        echo "  </td>\n";
        echo "   </tr>\n";
        echo "   <tr>\n";
        if ($set['set_show_icon'] == 1) {
            echo "  <td class='p_m' colspan='2'>\n";
        } else {
            echo "  <td class='p_m'>\n";
        }

        $k_f  = 0;
        $k_n = 0;
        $q3  = $db->query('SELECT t1 . * , (
    SELECT count( * ) FROM `obmennik_files` WHERE id_dir = t1.id ) AS cnt, (
    SELECT count( * ) FROM `obmennik_files` WHERE id_dir = t1.id  AND `time`>?i) AS cnt2
FROM `obmennik_dir` t1
WHERE t1.`dir_osn` LIKE "?e%"', [(time() - 60 * 60 * $set['loads_new_file_hour']), $post['dir']]);
        while ($post2 = $q3->row()) {
            $k_f += $post2['cnt'];
            //$k_f = $k_f + $db->query('SELECT COUNT(*) FROM `obmennik_files` WHERE `id_dir`=?i', [$post2['id']])->el();
            $k_n += $post2['cnt2'];
            //$k_n = $k_n + $db->query('SELECT COUNT(*) FROM `obmennik_files` WHERE `id_dir`=?i AND `time`>?i', [$post2['id'], (time() - 60 * 60 * $set['loads_new_file_hour'])])->el();
        }
        $cnt = $db->query('SELECT * FROM (
                          SELECT COUNT(*) as kf FROM `obmennik_files` WHERE `id_dir`=?i) t1, (
                          SELECT COUNT(*) as kn FROM `obmennik_files` WHERE `id_dir`=?i AND `time` >?i) t2',
                          [$post['id'], $post['id'], (time() - 60 * 60 * $set['loads_new_file_hour'])])->row();
        $k_f += $cnt['kf'];
        //$k_f = $k_f + $db->query('SELECT COUNT(*) FROM `obmennik_files` WHERE `id_dir`=?i', [$post['id']])->el();
        $k_n += $cnt['kn'];
        //$k_n = $k_n + $db->query('SELECT COUNT(*) FROM `obmennik_files` WHERE `id_dir`=?i AND `time` >?i', [$post['id'], (time() - 60 * 60 * $set['loads_new_file_hour'])])->el();
        if ($k_n == 0) {
            $k_n = null;
        } else {
            $k_n = '/+' . $k_n;
        }
        echo "Файлов: $k_f$k_n<br />\n";
        echo "  </td>\n";
        echo "   </tr>\n";
    } else {
        $post = $list[$i]['post'];
        $ras = $post['ras'];
        $file = H . "sys/obmen/files/$post[id].dat";
        $name = $post['name'];
        $size = $post['size'];
        echo "   <tr>\n";
        if ($set['set_show_icon'] == 2) {
            echo "  <td class='icon48' rowspan='2'>\n";
            if (is_file(H . "sys/obmen/screens/128/$post[id].gif")) {
                echo "<img src='/sys/obmen/screens/128/" . $post['id'] . ".gif' alt='Скрин...' height='48' width='48' />";
            } else {
                include 'inc/icon48.php';
            }

            echo "  </td>\n";
        } elseif ($set['set_show_icon'] == 1) {
            echo "  <td class='icon14'>\n";
            if (is_file(H . "sys/obmen/screens/128/$post[id].gif")) {
                echo "<img src='/sys/obmen/screens/128/" . $post['id'] . ".gif' alt='Скрин...' height='48' width='48' />";
            } else {
                include 'inc/icon48.php';
            }

            echo "  </td>\n";
        }

        echo "  <td class='p_t'>\n";
        if ($set['echo_rassh'] == 1) {
            $ras = ".$post[ras]";
        } else {
            $ras = null;
        }
        echo "<a href='/obmen$dir_id[dir]$post[name].$post[ras]?showinfo'>" . output_text($post['name_file']) . "</a><br />\n";
        echo "  </td>\n";
        echo "   </tr>\n";
        echo "   <tr>\n";
        if ($set['set_show_icon'] == 1) {
            echo "  <td class='p_m' colspan='2'>\n";
        } else {
            echo "  <td class='p_m'>\n";
        }
        include 'inc/opis.php';

        echo "  </td>\n";
        echo "   </tr>\n";
    }
}

echo "</table>\n";

if ($k_page > 1) {
    str('?', $k_page, $page); // Вывод страниц
}

if ($l != '/') {
    echo "<div class='foot'>";
    echo "<a href='/obmen/'>Обменник</a> &gt; " . obmen_path($l) . "<br />\n";
    echo "</div>\n";
}

include 'inc/upload_form.php';

include 'inc/admin_form.php';
