<?php

$count = go\DB\query('SELECT * FROM (
SELECT COUNT( * ) AS files_posts FROM `obmennik_files`) AS q1, (
SELECT COUNT( * ) AS files_posts_new FROM `obmennik_files` WHERE `time` >?i) AS q2
', [(time()-60*60*$set['loads_new_file_hour'])])->row();

$c_g = $count['files_posts_new'];
if ($c_g==0) {
    $c_g=null;
} else {
    $c_g='/+'.$c_g;
}
echo $count['files_posts'] . $c_g;
