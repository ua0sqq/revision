<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
if (isset($_GET['showinfo']) || !isset($_GET['f']) || isset($_GET['komm'])) {
    include_once H . 'sys/inc/compress.php';
}
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/obmen.php';
include_once H . 'sys/inc/user.php';

if (isset($_GET['d']) && esc($_GET['d']) != null) {
    $l = preg_replace("#\.{2,}#", null, esc($_GET['d']));
    $l = preg_replace("#\./|/\.#", null, $l);
    $l = preg_replace("#(/){1,}#", "/", $l);
    $l = '/' . preg_replace("#(^(/){1,})|((/){1,}$)#", "", $l);
} else {
      $l = '/';
  }

if ($l == '/') {
    $dir_id['upload'] = 0;
    $id_dir = 0;
    $l = '/';
} elseif ($db->query('SELECT COUNT(*) FROM `obmennik_dir` WHERE `dir`=? OR `dir`=? OR `dir`=? LIMIT ?i', ['/' . $l, $l . '/', $l, 1])->el() != 0) {
    $dir_id = $db->query('SELECT * FROM `obmennik_dir` WHERE `dir`=? OR `dir`=? OR `dir`=? LIMIT ?i', ['/' . $l, $l . '/', $l, 1])->row();
    $id_dir = $dir_id['id'];
} else {
      $dir_id['upload'] = 0;
      $id_dir = 0;
      $l = '/';
  }

if (isset($_GET['f'])) {
    $f = esc(urldecode($_GET['f']));
    $name = preg_replace('#\.[^\.]*$#', null, $f); // имя файла без расширения
    $ras = strtolower(preg_replace('#^.*\.#', null, $f));
    $ras = str_replace('jad', 'jar', $ras);

    if ($db->query('SELECT COUNT(*) FROM `obmennik_files` WHERE `id_dir`=?i AND `name`=? AND `ras`=? LIMIT ?i', [$id_dir, $name, $ras, 1])->el() != 0) {
        $file_id = $db->query('SELECT * FROM `obmennik_files` WHERE `id_dir`=?i AND `name`=? AND `ras`=? LIMIT ?i', [$id_dir, $name, $ras, 1])->row();
        $ras = $file_id['ras'];
        $file = H . "sys/obmen/files/$file_id[id].dat";
        $name = $file_id['name'];
        $size = $file_id['size'];
        if (!isset($_GET['showinfo']) && !isset($_GET['komm']) && is_file(H . 'sys/obmen/files/' . $file_id['id'] . '.dat')) {
            if ($ras == 'jar' && strtolower(preg_replace('#^.*\.#', null, $f)) == 'jad') {
                include_once H . 'sys/inc/zip.php';

                $zip = new PclZip(H . 'sys/obmen/files/' . $file_id['id'] . '.dat');
                $content = $zip->extract(PCLZIP_OPT_BY_NAME, "META-INF/MANIFEST.MF", PCLZIP_OPT_EXTRACT_AS_STRING);
                $jad = preg_replace("#(MIDlet-Jar-URL:( )*[^(\n|\r)]*)#i", null, $content[0]['content']);
                $jad = preg_replace("#(MIDlet-Jar-Size:( )*[^(\n|\r)]*)(\n|\r)#i", null, $jad);
                $jad = trim($jad);
                $jad.= "\r\nMIDlet-Jar-Size: " . filesize(H . 'sys/obmen/files/' . $file_id['id'] . '.dat') . "";
                $jad.= "\r\nMIDlet-Jar-URL: /obmen$dir_id[dir]$file_id[name].$file_id[ras]";
                $jad = br($jad, "\r\n");
                header('Content-Type: text/vnd.sun.j2me.app-descriptor');
                header('Content-Disposition: attachment; filename="' . $file_id['name'] . '.jad";');
                echo $jad;
                exit;
            }

            @$db->query('UPDATE `obmennik_files` SET `k_loads`=`k_loads`+?i WHERE `id`=?i LIMIT ?i', [1, $file_id['id'], 1]);
            include_once H . 'sys/inc/downloadfile.php';

            DownloadFile(H . 'sys/obmen/files/' . $file_id['id'] . '.dat', $_SERVER['SERVER_NAME'] . '_' . $name . '.' . $ras, ras_to_mime($ras));
            exit;
        }

        $set['title'] = 'Обменник - ' . $file_id['name_file']; // заголовок страницы
        include_once H . 'sys/inc/thead.php';

        title();
        if (isset($_POST['msg']) && isset($user)) {
            $msg = $_POST['msg'];
            if (isset($_POST['translit']) && $_POST['translit'] == 1) {
                $msg = translit($msg);
            }

            $mat = antimat($msg);
            if ($mat) {
                $err[] = 'В тексте сообщения обнаружен мат: ' . $mat;
            }

            if (strlen2($msg) > 1024) {
                $err[] = 'Сообщение слишком длинное';
            } elseif (strlen2($msg) < 2) {
                $err[] = 'Короткое сообщение';
            } elseif ($db->query('SELECT COUNT(*) FROM `obmennik_komm` WHERE `id_file`=?i AND `id_user`=?i AND `msg`=?', [$file_id['id'], $user['id'], $msg])->el() != 0) {
                $err = 'Ваше сообщение повторяет предыдущее';
            } elseif (!isset($err)) {
                $ank = get_user($file_id['id_user']);
                if ($ank['id'] != $user['id']) { // уведомление
                    $msg = $user['nick'] . ' оставил комментарий к [url=/obmen' . $dir_id['dir'] . urlencode($file_id['name']) . '.' . $file_id['ras'] . '?showinfo&page=end]Вашему файлу[/url]';
                    $db->query('INSERT INTO `mail` (`id_user`, `id_kont`, `msg`, `time`) VALUES ( ?i, ?i, ?, ?i)', [0, $ank['id'], $msg, $time]);
                }

                $db->query('INSERT INTO `obmennik_komm` (`id_file`, `id_user`, `time`, `msg`) VALUES( ?i, ?i, ?i, ?)', [$file_id['id'], $user['id'], $time, $msg]);
                $db->query('UPDATE `user` SET `balls`=`balls`+?i WHERE `id`=?i LIMIT ?i', [1, $user['id'], 1]);
                msg('Сообщение успешно добавлено');
            }
        }

        include 'inc/file_act.php';

        err();
        aut(); // форма авторизации
        include_once 'inc/komm_act.php';

 // действия с комментариями

        echo "<div class='p_m'>";
        if (is_file("inc/file/$ras.php")) {
            include "inc/file/$ras.php";
        } else {
            include_once 'inc/file.php';
        }

        echo '</div>';
        echo "<div class='foot'>";
        if ($file_id['ras'] == 'jar') {
            echo "&raquo;<a href='/obmen$dir_id[dir]" . urlencode($file_id['name']) . ".jad'>Скачать</a> <a href='/obmen$dir_id[dir]" . urlencode($file_id['name']) . ".$file_id[ras]'>JAR</a> ($file_id[k_loads])<br />\n";
        } else {
            echo "&raquo;<a href='/obmen$dir_id[dir]" . urlencode($file_id['name']) . ".$file_id[ras]'>Скачать</a> ($file_id[k_loads])<br />\n";
        }
        echo "</div>\n";
        include 'inc/file_form.php';

        echo "<div class='foot'>";
        echo "<input type='text' value='http://$_SERVER[SERVER_NAME]/obmen$dir_id[dir]" . urlencode($file_id['name']) . ".$file_id[ras]' /><br />\n";
        echo "</div>\n";
        $_SESSION['page'] = 1;
        include_once H . 'sys/inc/thead.php';

        include_once 'inc/komm.php';

        echo "<div class='foot'>";
        echo "&laquo;<a href='/obmen$dir_id[dir]'>В папку</a><br />\n";
        echo "</div>\n";
        include_once H . 'sys/inc/tfoot.php';
    }
}

include_once 'inc/dir.php';

include_once H . 'sys/inc/tfoot.php';
