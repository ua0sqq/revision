<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/user.php';
//подключаем языковой пакет
lang::start('mini_chat');
$set['title'] = lang('Мини чат');
include_once H . 'sys/inc/thead.php';
title();
aut();

if ($_SERVER['REQUEST_URI'] != '/pages/mini_chat/?refresh') {
    $_SESSION['page_m'] = $_SERVER['REQUEST_URI'];
}

if (isset($_GET['refresh'])) {
    header("Location: " . $_SESSION['page_m']);
}


#--------------------------#
if (isset($_POST['msg']) && isset($user)) {
    $msg = $_POST['msg'];
    if (isset($_POST['translit']) and $_POST['translit'] == 1) {
        $msg = translit($msg);
    }
    $mat = antimat($msg);
    if ($mat) {
        $err[] = lang('В тексте сообщения обнаружен мат:') . $mat;
    }
    if (strlen2($msg) > $set['guest_max_post']) {
        $err[] = lang('Сообщение слишком длинное');
    } elseif (strlen2($msg) < 2) {
        $err[] = lang('Короткое сообщение');
    } elseif ($db->query('select count(*) from `guest` where `id_user`=?i AND `msg`=? AND `time`>?i LIMIT ?i',
                       [$user['id'], $msg, (time() - 60), 1])->el() != 0) {
        $err = lang('Ваше сообщение повторяет предыдущее');
    } elseif (!isset($err)) {
        #-------------------------------------#
            //вычисляем рейтинг за сообщение
            $rating_add_msg = rating::msgnum($msg);
            //сумируем
            $add_rating = ($rating_add_msg + 0.01);
            //добавляем рейтинг
            rating::add($user['id'], $add_rating, 1, null, 'mini_chat');
            #-------------------------------------#
            $db->query('INSERT INTO `guest` (id_user, `time`, msg) VALUES( ?i, ?i, ?)', [$user['id'], $time, $msg]);
        $db->query('UPDATE `user` SET `balls`=`balls`+?i WHERE `id`=?i LIMIT ?i', [$set['balls_guest'], $user['id'], 1]);
        $_SESSION['message'] = lang('Сообщение успешно добавлено');
        exit(header("Location: ?"));
    }
} elseif (isset($_POST['msg']) && !isset($user) && isset($set['write_guest']) &&
$set['write_guest'] == 1 && isset($_SESSION['captcha']) && isset($_POST['chislo'])) {
    $msg = $_POST['msg'];

    if (isset($_POST['translit']) && $_POST['translit'] == 1) {
        $msg = translit($msg);
    }
    $mat = antimat($msg);
    if ($mat) {
        $err[] =  lang('Обнаружен мат').': ' . $mat;
    }
    if (strlen2($msg) > 1024) {
        $err =  lang('Сообщение слишком длинное');
    } elseif ($_SESSION['captcha'] != $_POST['chislo']) {
        $err = lang('Неверное проверочное число');
    } elseif (isset($_SESSION['antiflood']) && $_SESSION['antiflood'] > $time - 300) {
        $err =  lang('Для того чтобы чаще писать нужно авторизоваться');
    } elseif (strlen2($msg) < 2) {
        $err =  lang('Короткое сообщение');
    }
    if ($db->query('select count(*) from `guest` where `id_user`=?i AND `msg`=? AND `time`>?i LIMIT ?i',
                   [$user['id'], $msg, (time() - 60), 1])->el() != 0) {
        $err =  lang('Ваше сообщение повторяет предыдущее');
    } elseif (!isset($err)) {
        $_SESSION['antiflood'] = $time;
        $db->query('INSERT INTO `guest` (id_user, `time`, msg) VALUES( ?i, ?i, ?)',
                   [0, time(), '[i](IP:' . $_SERVER['REMOTE_ADDR']. ')[/i]' . PHP_EOL . ' ' . $msg]);
        msg(lang('Успешно'));
    }
}
#-------------------------------------------#
err();
switch (empty($_GET['type']) ? false : $_GET['type']) {
    default:
        if (user_access('guest_clear')) {
            echo '<div class="guestbook"><a href="?type=deletmessages"> ' . lang('Полная очистка мини чата') .
                '</a></div>' . PHP_EOL;
        }
        echo '<div class="guestbook"><a href="?type=who">' . lang('Кто в чате?') .
            '</a> :: <a href="?refresh">' . lang('Обновить') . '</a></div>';
        if (isset($user) || (isset($set['write_guest']) && $set['write_guest'] == 1)) {
            panel_form::head();
            echo '<div class="guestbook">' . PHP_EOL;
            if (isset($user) || (isset($set['write_guest']) && $set['write_guest'] == 1 &&
                (!isset($_SESSION['antiflood']) || $_SESSION['antiflood'] < time() - 300))) {
                echo '<form method="post" name="message" action="?' . $passgen . '">' . PHP_EOL;
                echo lang('Сообщение') . ':<br /><textarea name="msg"></textarea><br />' . PHP_EOL;
                if (isset($user) && $user['set_translit'] == 1) {
                    echo '<label><input type="checkbox" name="translit" value="1" /> ' . lang('Транслит') .
                        '</label><br />' . PHP_EOL;
                }
                if (!isset($user)) {
                    echo '<img src="/captcha.php?SESS=' . $sess . '" width="100" height="30" alt="captcha" /><br />
                    <input name="chislo" size="5" maxlength="5" value="" type="text" /><br />' . PHP_EOL;
                }
                echo '<input value="' . lang('Отправить') . '" type="submit" />' . PHP_EOL;
                echo '</form>' . PHP_EOL;
            } elseif (isset($set['write_guest']) && $set['write_guest'] == 1 && isset($_SESSION['antiflood']) &&
            $_SESSION['antiflood'] > time() - 300) {
                echo '<div class="foot">' . PHP_EOL;
                echo '* ' . lang('Гостем вы можете писать только по 1 сообщению в 5 минут') .
                    '<br />' . PHP_EOL;
                echo '</div>' . PHP_EOL;
            }
            echo '</div>' . PHP_EOL;
            panel_form::foot();
        }
        $k_post = $db->query('SELECT COUNT(*) FROM `guest`')->el();
        $k_page = k_page($k_post, $set['p_str']);
        $page = page($k_page);
        $start = $set['p_str'] * $page - $set['p_str'];
        echo '<table class="post">' . PHP_EOL;
        if ($k_post == 0) {
            echo '<tr><td class="err"><span>' . lang('Тут никто, ничего, еще не писал') . '</span></td></tr>';
        }
        $q = $db->query('
SELECT a . * , b.id AS id_u, b.nick, b.pol, b.level, (
    SELECT COUNT( * ) FROM  ban WHERE id_user=b.id AND `time`>?i GROUP BY id_user) as ban
FROM `guest` a
LEFT JOIN user b ON a.id_user = b.id
ORDER BY a.id DESC LIMIT ?i, ?i', [time(), $start, $set['p_str']]);
        $z = 2;
        while ($post = $q->row()) {
            if ($post['id_user'] == 0) {
                $post['id_u'] = 0;
                $post['pol'] = 'guest';
                $post['level'] = 0;
            }
            if ($set['avatar_show'] == 1) {
                echo '<tr><td class="avar" style="" rowspan="2">' . PHP_EOL;
                avatar($post['id_u'], 90, 90);
                echo '</td>' . PHP_EOL;
            }
            echo '<tr><td class="z_'.($z % 2 ? 1 : 2).'">' . PHP_EOL;
            ++$z;

            if ($post['id_u'] == 0) {
                echo lang('Гость') . ' (' . vremja($post['time']) . ')' . PHP_EOL;
            } else {
                echo nick($post['id_u']) . ' (' . vremja($post['time']) . ') <br />' . PHP_EOL;
            }
            echo '<span class="status" style="float:right;margin-top: -17px;">' . (user_access('guest_delete') ? '<a href="?type=delete&amp;id=' . $post['id'] . '" class="trash" ></a>' : false) .
                ' </span>' . PHP_EOL;
            #-----------------------------------------#
            //Блокируем сообщение если человек в бане
            if ($post['ban'] == 0 && $set['msg_ban_set'] == 1) {
                echo output_text($post['msg']);
                if (isset($user) && $user['id'] != $post['id_user']) {
                    echo '
		<br /><br />
		<span class="ank_span_m" ><a href="?type=otvet&amp;id=' . $post['id'] . '">' . lang('Ответить') .
                        ' </a></span>' . ($user['level'] < 1 && $ank['level'] < 1 ?
                        '<span class="ank_span_m">
		 <a href="?type=jaloba&amp;id=' . $post['id'] . '"> ' . lang('Жалоба') . '</a></span>' : false);
                }
            } else {
                echo($user['group_access'] >= 1 ? '<span style="color:red">' . $set['msg_ban'] .
                    '</span> : <br />' . output_text($post['msg']) : $set['msg_ban']);
            }
            #-----------------------------------------#
            echo '</td></tr>' . PHP_EOL;
        }
        echo '</table>';
        if ($k_page > 1) {
            str('?', $k_page, $page);
        }
        break;
    case 'otvet':
        if (!isset($user)) {
            $_SESSION['message'] = lang('Доступ закрыт');
            exit(header('Location: ?'));
        }
        $post = $db->query('
        SELECT A.*, B.id AS id_u, B.nick, B.group_access FROM `guest` A
        LEFT JOIN user B ON A.id_user = B.id
        WHERE A.id =?i', [$_GET['id']])->row();

            if ($post['id_user'] == 0) {
                $post['id_u'] = 0;
                $post['pol'] = 'guest';
                $post['nick'] = lang('Гость');
            }

        if ($user['id'] != $post['id_user']) {
            if (isset($_POST['save'])) {
                $msg = $post['msg'] . '
--------
				' . $_POST['jaloba_msg'];
                $db->query('INSERT INTO `guest` (`id_user` ,`time` ,`msg`) VALUES (?i,  ?i,  ?);', [$user['id'],  time(), $msg]);
                if ($post['id_u'] != $user['id'] && $post['id_u'] !== 0) {// у гостя нет журнала
                    $jurnal = $user['nick'] . ' Ответил на ваше сообщение
                    ' . $msg . ' [url=/pages/mini_chat/?type=post&id=' . $post['id'] . ']Подробнее[/url]';
                    $db->query('INSERT INTO `jurnal` (`id_user`, `id_kont`, `msg`, `time`, `type`) VALUES( ?i, ?i, ?, ?i, ?string)',
                               [0, $post['id_u'], $jurnal, time(), 'my_chat']);
                }
                $_SESSION['message'] = lang('Успешно');
                exit(header('Location: ?'));
            }
        }
        echo '
	<div class="p_m">' . PHP_EOL . '
	<!--div class="status_o_s"> </div-->
	<div class="status_o">' . PHP_EOL . '
	' . output_text($post['msg']) . '
	</div>
	<br />' . PHP_EOL . '
	<span class="ank_span_m"><a href="?type=otvet&amp;id=' . $post['id'] . '"> ' . lang('Ответ') .
            '</a></span>
	' . ($post['group_access'] <= 1 ?
            '<span class="ank_span_m"><a href="?type=jaloba&amp;id=' . $post['id'] . '"> ' . lang('Жалоба') .
            '</a></span>' : false) . '
	<span class="ank_span_m"><a href="?"> ' . lang('Назад') . '</a></span>
	</div>'. PHP_EOL;
        echo '<div class="guestbook">' . PHP_EOL;
        echo '<form method="post" action="">' . PHP_EOL;
        echo lang('Ответ') . ':<br /><textarea name="jaloba_msg">' . $post['nick'] .
            ', </textarea><br />' . PHP_EOL;
        if (isset($user) && $user['set_translit'] == 1) {
            echo '<label><input type="checkbox" name="translit" value="1" /> ' . lang('Транслит') .
                '</label><br />' . PHP_EOL;
        }
        echo '
<input value="' . lang('Отправить') . '" type="submit"  name="save"/>' . PHP_EOL . '
<span class="ank_span_m"><a href="?type=post&amp;id=' . $post['id'] . '"> ' . lang('Отмена') .
            '</a></span>' . PHP_EOL;
        echo '</form></div>' . PHP_EOL;
        break;
    case 'post':
        $post = $db->query('SELECT id, msg FROM `guest` WHERE id=?i', [$_GET['id']])->row();
        echo '
	<div class="p_m">' . PHP_EOL . '
	<!--div class="status_o_s"> </div-->
	<div class="status_o">' . PHP_EOL . output_text($post['msg']) . '
	</div>
	<br />' . PHP_EOL . '
	<span class="ank_span_m"><a href="?type=otvet&amp;id=' . $post['id'] . '"> ' . lang('Ответ') .
            '</a></span>
	' . ($user['level'] < 1 ? '<span class="ank_span_m"><a href="?type=jaloba&amp;id=' . $post['id'] . '"> ' .
            lang('Жалоба') . '</a></span>' : false) . '
	<span class="ank_span_m"><a href="?"> ' . lang('Назад') . '</a></span>
	</div>' . PHP_EOL;
        break;
    case 'jaloba':
        if (!isset($user)) {
            $_SESSION['message'] = lang('Доступ закрыт');
            exit(header('Location: ?'));
        }
        $post = $db->query('SELECT * FROM `guest` WHERE id=?i', [$_GET['id']])->row();
        if ($user['id'] != $post['id_user']) {
            if (isset($_POST['save'])) {
                $msg = $_POST['jaloba_msg'];
                $prich = lang('Категория') . ' = ' . $_POST['prich'];
                $msg .= ' ' . lang('Подана жалоба на сообщение') . '
[url=/pages/mini_chat/?type=post&id=' . intval($_GET['id']) . ']' . $post['msg'] . '[/url]';
                $db->query('INSERT INTO `jurnal_system` (`time` ,`type` ,`read` ,`id_user`,`msg`,`id_kont`) VALUES ( ?i,  ?string,  ?string,  ?i, ?, ?i);',
                           [time(),  'spam_chat',  0,  $post['id_user'], $msg . ' ' . $prich, $user['id']]);

                $_SESSION['message'] = lang('Успешно');
                exit(header('Location: ?'));
            }
            echo '
	<div class="p_m">' . PHP_EOL .
    lang('Вы пытаетесь подать жалобу на сообщение') . ' : <br />
	<!--div class="status_o_s"> </div-->
	<div class="status_o">' . PHP_EOL .
    output_text($post['msg']) . '
	</div>
	</div>' . PHP_EOL;
            echo '<div class="guestbook">' . PHP_EOL;
            echo '<form method="post" action="">' . PHP_EOL;
            echo lang('Комментарий') .
                ':<br /><textarea name="jaloba_msg"></textarea><br />' . PHP_EOL;
            if (isset($user) && $user['set_translit'] == 1) {
                echo '<label><input type="checkbox" name="translit" value="1" /> ' . lang('Транслит') .
                    '</label><br />' . PHP_EOL;
            }
            echo lang('Категория') . ' :<select name="prich">';
            echo '<option value="' . lang('Мат') . '"> ' . lang('Мат') . '</option>';
            echo '<option value="' . lang('Оскорбления') . '"> ' . lang('Оскорбления') .
                '</option>';
            echo '<option value="' . lang('Спам') . '">' . lang('Спам') . ' </option>';
            echo '<option value="' . lang('Мошенничество') . '"> ' . lang('Мошенничество') .
                '</option>';
            echo '<option value="' . lang('Флуд,офтоп,тролинг') . '"> ' . lang('Флуд,офтоп,тролинг') .
                '</option>';
            echo '<option value="' . lang('Другое') . '"> ' . lang('Другое') . '</option>';
            echo '</select><br />' . PHP_EOL;
            echo lang('Вы уверены ?') . '
<input value="' . lang('Да') . '" type="submit"  name="save"/>
<span class="ank_span_m"><a href="?"> ' . lang('Нет') . '</a></span>
</form>' . PHP_EOL;
            echo '</div>' . PHP_EOL;
        } else {
            $_SESSION['message'] = lang('Нельзя подавать жалобы на свои сообщения');
            exit(header('Location: ?'));
        }
        break;
    case 'who':
        #----------------------------------------------------------#
        echo '<div class="foot"><a href="?">' . lang('Назад') . '  </a></div>' . PHP_EOL;
        $k_post = $db->query('select count(*) from `user` where `date_last`>?i AND `url` like "?e%"',
                             [(time() - 300), '/pages/mini_chat/'])->el();
        $k_page = k_page($k_post, $set['p_str']);
        $page = page($k_page);
        $start = $set['p_str'] * $page - $set['p_str'];
        $data = [(time() - 300), '/pages/mini_chat/%', $start, $set['p_str']];
        $pattern = 'SELECT * FROM `user` WHERE `date_last`>?i AND `url` like ? ORDER BY `date_last` DESC LIMIT ?i, ?i';
        $q = $db->query($pattern, $data);
        if ($k_post == 0) {
            msg(lang('Нi кого нету'));
        }
        while ($guest = $q->row()) {
            echo '  <div class="p_m">' . PHP_EOL;
            echo nick($guest['id']);
            echo '</div>' . PHP_EOL;
        }
        if ($k_page > 1) {
            str("?", $k_page, $page);
        }
        #----------------------------------------------------------#
        break;
    case 'deletmessages':
        if (user_access('guest_clear')) {
            if (isset($_GET['deletyes'])) {
                if ($cols = $db->query('DELETE FROM `guest` WHERE  1;')->ar()) {
                    $md = ('Удалено') . ' ' . $cols . ' ' . ('сообщений');
                    admin_log(('Мини чат'), ('Полная очистка'), $md);
                    if ($params['_adapter'] == 'mysql') { // TODO: чет тут не то ???
                        $db->query('ALTER TABLE `guest` AUTO_INCREMENT =1;');
                    } else {
                        $db->query('UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME="guest";');
                    }
                    msg(lang('Удалено') . ' ' . lang('сообщений') . ' ' . $cols);
                    echo "<div class='foot'><a href='?'>" . lang('Вернуться в мини чат') .
                    " </a><br /></div>";
                } else {
                    exit(header('Location: ?'));
                }
            }
            if (!isset($_GET['deletyes'])) {
                echo '<form method="post" class="foot" action="?type=deletmessages&amp;deletyes">' . PHP_EOL;
                echo lang('Удалить все сообщения') . '? <br />
* <span style="color:red;">' . lang('Будут удалены все сообщения') .
                    ' </span><br />' . PHP_EOL;
                echo '<input value="' . lang('Согласен на очистку') . '" type="submit" /><br />' . PHP_EOL;
                echo '<a href="?">' . lang('Отказываюсь') . '</a><br />';
                echo '</form>' . PHP_EOL;
            }
        } else {
            msg(lang('Доступ закрыт'));
        }
        break;
        break;
    case 'delete':
        if (user_access('guest_delete')) {
            $post = $db->query('
            SELECT a.*, b.id AS id_u, b.nick, b.level, b.pol FROM `guest` a
            LEFT JOIN user b ON a.id_user = b.id
            WHERE a.`id`=?i LIMIT ?i', [$_GET['id'], 1])->row();
            if ($post['id_user'] == 0) {
                $post['id_u'] = 0;
                $post['pol'] = 'guest';
                $post['level'] = 0;
                $post['nick'] = lang('Гость');
            }
            $md = lang('Удаление сообщения от') . ' ' . $post['nick'];
            admin_log(lang('Мини чат'), lang('Удаление сообщения'), $md);
            $db->query('DELETE FROM `guest` WHERE `id`=?i', [$post['id']]);
            if ($params['_adapter'] == 'mysql') { // TODO: чет тут не то ???
                        $db->query('OPTIMIZE TABLE `guest`;');
            } else {
                //  хрен знает ???
            }
            $_SESSION['message'] = lang('сообщение удалено');
            exit(header("Location: ?"));
        }
        break;
}
include_once H . 'sys/inc/tfoot.php';
