<?php

$count = go\DB\query('SELECT * FROM (
SELECT COUNT( * ) AS guest_posts FROM `guest`) AS q1, (
SELECT COUNT( * ) AS guest_posts_new FROM `guest` WHERE `time`>?i) AS q2
', [(time()-60*60*24)])->row();

$c_g = $count['guest_posts_new'];
if ($c_g==0) {
    $c_g=null;
} else {
    $c_g='/+'.$c_g;
}

echo $count['guest_posts'] . $c_g;
