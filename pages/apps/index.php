<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/compress.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/user.php';

if (!$user) {
    $_SESSION['message'] = lang('Нужно авторизоваться');
    exit(header('Location: /'));
}

$app = $db->query('SELECT `name_lat`, `link`, `id` FROM `apps` WHERE `name_lat`=? OR `id`=?i LIMIT ?i',
                  [$_GET['app'], $_GET['app'], 1])->row();

if ($app['link']) {
    $db->query('UPDATE `apps` SET `count_aut`=`count_aut`+1 WHERE `id`=?i LIMIT ?i', [$app['id'], 1]);
    exit(header("Location: ".$app['link']));
} else {
    $_SESSION['message'] = lang('Такой игры или приложения не обнаружено');
    exit(header("Location: /user/apps"));
}
