<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/compress.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/user.php';

$set['title'] = lang('Вход в RENIGADS');
include_once H . 'sys/inc/thead.php';
title() . aut();

if (!isset($user)) {
    msg(lang('Нужно авторизоваться'));
    include_once H . 'sys/inc/tfoot.php';
    exit;
}

define('API_SITE_RENIGADS_URL', 'fiera.renigads.ru/api');
//домен к которому подключен api (Адрес вашего сайта)
define('API_SITE_RENIGADS_DOMAINS', 'fiera');
//дополнительный хэш (0-off/1-on)
$rg_hash = 0;
//id юзера
$renigads['id_soc'] = $user['id'];
//домен партнера
$renigads['api_token_domain'] = API_SITE_RENIGADS_URL;
//Время
$renigads['time'] = time();
//--- не обязательные
//ник юзера (для переноса его в игру,если ник не занят)
$renigads['nick_soc'] = $user['nick'];
//почтовый ящик
$renigads['email'] = $user['ank_mail'];
//телефон
$renigads['phone'] = $user['ank_n_tel'];
//пол юзера
$renigads['pol'] = $user['pol'];
//при оосбых видах партнерства (промо код)
$renigads['promo'] = 'fiera';
//хэши
$renigads['hash'] = ($rg_hash == 1 ? md5($renigads['time']) : false) . $renigads['id_soc'] .'_'. mt_rand(11111, 99999);
$renigads['hash_id'] = ($rg_hash == 1 ? md5($renigads['time']) : false) . ($renigads['nick_soc'] .'_'. $renigads['id_soc'].'_'. $renigads['time']);
var_dump($db->query('SELECT COUNT(*) FROM `renigads` WHERE `id_user` =?i', [$renigads['id_soc']])->el());

if (!$db->query('SELECT COUNT(*) FROM `renigads` WHERE `id_user` =?i', [$renigads['id_soc']])->el()) {
    $db->query('INSERT INTO `renigads` (`id_user`, `api_token_rg_id`, `api_token_rg`, `time`, `count`) VALUES( ?i, ?, ?, ?i, ?i);',
               [$renigads['id_soc'], $renigads['hash_id'], $renigads['hash'], $renigads['time'], 0]);
    //кэш del.
    cache_delete::user($user['id']);
    //авторизоваем  если не возвращало на ошибки
    exit(header('location: ?'));
}

//Массив данных с юзером
$arr_rg = $db->query('SELECT `id_user`,`api_token_rg`,`api_token_rg_id` FROM `renigads` WHERE `id_user` =?i LIMIT ?i',
                     [$renigads['id_soc'], 1])->row();

//данные для авторизации
$renigads['token'] = $arr_rg['api_token_rg'];
$renigads['token_id'] = $arr_rg['api_token_rg_id'];

//если не пусто
if ($arr_rg['api_token_rg_id'] != null) {

//полученные данные в любом случаи будет снова зищифрованы ,открытыми их в базе renigads не занести .
//по этому можете и не шифровать (по желанию)
if (empty($_GET['err'])) {
    $url_enter = '//' .
API_SITE_RENIGADS_URL . '/site/' .
API_SITE_RENIGADS_DOMAINS .
'/enter.php?token=' . $renigads['token'] .
'&token_id=' . $renigads['token_id'] .
'&id_soc=' . $renigads['id_soc'] .
'&nick_soc=' . $renigads['nick_soc'] .
'&email=' . $renigads['email'] .
'&phone=' . $renigads['phone'] .
'&time=' . $renigads['time'] .
'&promo=' . $renigads['promo'] .
'&api_token_domain=' . $renigads['api_token_domain'] .
'&url=' . $_SERVER['HTTP_HOST'] .
'&pol=' . $renigads['pol'];

    //авторизоваем  если не возвращало на ошибки
    exit(header('location: '.$url_enter));
}
}

if (isset($_GET['err'])) {
    if ($_GET['err'] == 1) {
        $err_msg = 'Не верный рефер клиента,включите эту возможность у себя в браузере <a href="?">попробовать еще раз </a>';
    }
    if ($_GET['err'] == 2) {
        $err_msg = 'Ошибка партнерский сайт не доступен или какой то сбой,попробуйте позже =) <a href="?">попробовать еще раз </a>';
    }
    if ($_GET['err'] == 3) {
        $err_msg = 'Индификатор не был передан ,попробуйте обновить страницу и попробовать снова <a href="?">попробовать еще раз </a>';
    }
    if ($_GET['err'] == 4) {
        echo $err_msg = 'Неизвестная ошибка <a href="?">попробовать еще раз  </a> ,если не помогло обратитесь в тех.поддержку';
    }
    if ($_GET['err'] == 5) {
        echo $err_msg = 'Сервер api ограничен <a href="?">попробовать еще раз  </a> ,если не помогло нужно создать свой акк. партнера <a href="//renigads.ru/info/partners">';
    }
    echo msg($err_msg);
}

include_once H . 'sys/inc/tfoot.php';
