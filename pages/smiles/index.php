<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/compress.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/user.php';

$set['title']='Смайлы';
include_once H . 'sys/inc/thead.php';
title() . aut();

// ??? убить Сайнта мало за эту свалку
#	права доступа
$if_accept = user_access('smiles');

#	сообщение при переходе без прав
$if_accept_msg='Доступ запрещен';
$cnt =  $db->query('SELECT * FROM (
                   SELECT COUNT(*) as koll FROM `smiles`)q1, (
                   SELECT COUNT(*) as dir FROM `smiles_dir`)q2
')->row();
#	счетчик сколько всего смайлов
$smiles_koll = $cnt['koll'];

#	счетчик сколько всего категорий
$smiles_dir = $cnt['dir'];


#	switch - type - переключатель
switch (empty($_GET['type'])? false : $_GET['type']) {
#	условие по умолчанию
    default:
        $k_page = k_page($smiles_dir, $set['p_str']);
        $page = page($k_page);
        $start = $set['p_str']*$page-$set['p_str'];
        #	если категорий = 0
        if ($smiles_dir==0) {
            msg('Категории не созданы еще');
        }

        #	Выводим последние созданные категории вверх
        $smiles_dir = $db->query('SELECT * FROM `smiles_dir` ORDER BY `id` ASC LIMIT ?i, ?i', [$start, $set['p_str']]);

        while ($post = $smiles_dir->row()) {
            $smile = $db->query('SELECT COUNT(*) FROM `smiles` WHERE `id_dir` =?i', [$post['id']])->el();
            echo '<div class="p_m"><a href="?type=dir&amp;id='.$post['id'].'"> <img src="/style/icons/default.png" alt="*"/>
    	'.output_text($post['name']).' ('.$smile.')</a></div>
    	<div class="p_m">'.output_text($post['opis']).'</div>
    	<div class="p_m">'.($if_accept?'<span style="float: right">
    	<a href="?type=delet_dir&amp;id='.$post['id'].'">'.lang('Удалить').'</a></span><a href="?type=dir_edit&amp;id='.$post['id'].'">'.lang('Редактировать').'</a>':false).'</div>';
        }
        if ($if_accept) {
            echo '<div class="p_m"> <a href="?type=dir_add">[+] '.lang('Добавить категорию').'</a></div><div class="p_m"> <a href="?type=add_smile">[+] '.lang('Добавить Смайл').'</a>		</div>';
        }
        echo '<div class="p_m"> <a href="?type=smile_list">['.$smiles_koll.'] '.lang('Всего смайлов').'</a></div>';
        if ($k_page>1) {
            str('?', $k_page, $page);
        }

    break;
    #	выводим категорию со смайлами
    case 'dir':
        echo '<a href="?"><div class="p_m">'.lang('Назад').'</div></a>';
        if (isset($_GET['id'])) {
            $id = intval($_GET['id']);
        } else {
            exit(header("Location: ?type=null"));
        }

        $k_post = $db->query('SELECT COUNT(*) FROM `smiles` WHERE `id_dir`=?i', [$id])->el();
        $k_page = k_page($k_post, $set['p_str']);
        $page = page($k_page);
        $start = $set['p_str']*$page-$set['p_str'];

        if ($k_post == 0) {
            msg(lang('Нет смайлов'));
        }
        $smiles = $db->query('SELECT * FROM `smiles` WHERE `id_dir`=?i ORDER BY `id` DESC LIMIT ?i, ?i',
                             [$id, $start, $set['p_str']]);
        while ($post = $smiles->row()) {
            echo '<div class="p_m">
            <img src="/style/smiles/'.output_text($post['name']) . '" alt="' . output_text($post['name']).'" title="' .
            output_text($post['name']).'"/> <> ' . esc(stripcslashes(htmlspecialchars($post['zamena']))) . '' .
            ($if_accept ? ' [<a href="?type=edit_smile&id=' . $post['id'] . '">' .
            lang('Редактировать') . '</a>] :: [<a href="?type=delet_smiles&id=' . $post['id'].'">' .
            lang('Удалить') . '</a>]' : false) . '</div>';
        }
        if ($k_page > 1) {
            str('?type=dir&amp;id=' . $id . '&amp;', $k_page, $page);
        }
        echo '<div class="foot"><a href="?">' . lang('Все катерогии') . '</div></a>';

    break;

    case 'null':

        echo '<a href="?"><div class="p_m">' . lang('Назад') . '</div></a>';
        msg(lang('Произошла ошибка'));

    break;

    #	создаем категорию
    case 'dir_add':

        if ($if_accept) {
            echo '<a href="?"><div class="p_m">  '.lang('Назад').'</div></a>';
            if (isset($_POST['name'])) {
                $name = trim($_POST['name']);
                $opis = trim($_POST['name']);
                if (mb_strlen($name)>99) {
                    $err=lang('Слишком большое название категории');
                } elseif (mb_strlen($name)<1) {
                    $err=lang('Слишком короткое название категории');
                } else {
                    $db->query('INSERT INTO `smiles_dir` (`name`, `opis`) VALUES( ?, ?)', [$name, $opis]);
                    //query("OPTIMIZE TABLE `smiles_dir`");
                    msg(lang('Директория').' <b>' . $name . '</b> добавлена</br> ' . lang('Описание') . ' : ' . $opis);
                }
            }
            err();
            echo '<div class="p_m">
            <form method="post" action="?type=dir_add&">'.lang('Название').':(99 max)<br/>
            <input name="name" maxlength="99"/><br/>
            '.lang('Описание').':<br/><textarea name="opis"></textarea>
            <input value="'.lang('Создать').'" type="submit"/></form></div>';
        } else {
            msg($if_accept_msg);
        }

    break;

    #	Редактируем категорию
    case 'dir_edit':

        if ($if_accept) {
            echo '<a href="?"><div class="p_m">  '.lang('Назад').'</div></a>';

            if (isset($_GET['id'])) {
                $id = intval($_GET['id']);
            } else {
                header("Location: ?type=null");
            }

            if (isset($_POST['name'])) {
                $name = trim($_POST['name']);
                $opis = trim($_POST['opis']);

                if (mb_strlen($name)>99) {
                    $err=lang('Слишком большое название категории');
                } elseif (mb_strlen($name)<1) {
                    $err=lang('Слишком короткое название категории');
                } else {
                    $db->query('UPDATE `smiles_dir` SET `name`=?, `opis`=? WHERE `id`=?i LIMIT ?i',
                       [$name, $opis, $id, 1]);
                    msg('Директория <b>'.$name.'</b> отредактирована</br><b>'.$opis.'</b>');
                }
            }

            err();

            $dir = $db->query('SELECT * FROM `smiles_dir` WHERE `id`=?i', [$id])->row();
            echo '<div class="p_m"><form method="post" action="?type=dir_edit&id='.$id.'&">';
            echo lang('Название').':(99 max)<br/><input name="name" maxlength="99" value="'.output_text($dir['name']).'"/><br/>';
            echo '</div><div class="p_m">
            '.lang('Описание').'<br/>
            <textarea name="opis">'.output_text($dir['opis']).'</textarea></div><div class="p_m">
            ';
            echo '<input value="'.lang('Изменить').'" type="submit"/></form></div>';
        } else {
            msg($if_accept_msg);
        }

    break;
        #	Добовления смайла
        case 'add_smile':

            if ($if_accept) {
                echo '<a href="?"><div class="p_m">  '.lang('Назад').'</div></a>';

                if ($smiles_dir==0) {
                    msg(lang('Сначала создайте категории').' <a href="?type=dir_add">'.lang('Создать').'</a> ');
                    include_once H . 'sys/inc/tfoot.php';
                    exit;
                }

                if (isset($_FILES['file']) && isset($_POST['name']) && isset($_POST['zamena']) &&    isset($_POST['dir'])) {
                    $name = $_SERVER['SERVER_NAME'] . '_' . $_POST['name'].'.png';
                    $dir_s = intval($_POST['dir']);
                    $zamena = trim($_POST['zamena']);
                    if ($imgc=@imagecreatefromstring(file_get_contents($_FILES['file']['tmp_name']))) {
                        $name = retranslit($name);
                        if ($name==null) {
                            $err = lang('Название файла не заполнено');
                        } elseif (mb_strlen($name)>99) {
                            $err = lang('Название слишком большое. max 99');
                        } elseif (mb_strlen($name) < 2) {
                            $err = lang('Название слишком маленькое. min 2');
                        } else {
                            $db->query('INSERT INTO `smiles` (`name`, `id_dir`, `zamena`) VALUES ( ?, ?, ?)',
                                   [$name, $dir_s, $zamena]);
                            @copy($_FILES['file']['tmp_name'], H . 'style/smiles/' . $name);
                        //@chmod(H . 'style/smiles/'.$name,0666);
                        msg(lang("Успешно"));
                        }
                    } else {
                        msg(lang("Ошибка"));
                    }
                }

                err();

                echo '<div class="p_m">'.lang('Форматы').' *.png\*.jpg\*.gif<br/>';
                echo '<form method="post" enctype="multipart/form-data" action="?type=add_smile">';
                echo '<input type="file" name="file" accept="image/*,image/gif,image/png,image/jpeg"/><br/>';
                echo '<b>'.lang('Название').':</b><br/><input name="name"><br/>';
                echo '<b>'.lang('Как вызывать смайл').':</b><br/><input name="zamena"> ('.lang('Пример').' :privet:)<br/>';
                echo '<b>'.lang('Категория').'</b>:<br/><select name="dir">';
                $q = $db->query('SELECT * FROM `smiles_dir`');
                while ($dir = $q->row()) {
                    echo '<option value="'.$dir['id'].'">'.output_text($dir['name']).'</option>';
                }
                echo '</select><br/>';
                echo '<input value="'.lang('Загрузить').'" type="submit"/></form></div>';
            } else {
                msg($if_accept_msg);
            }

    break;

    #	редактирования смайла
    case 'edit_smile':

        if ($if_accept) {
            echo '<a href="?"><div class="p_m">'.lang('Назад').'  </div></a>';
            if (isset($_GET['id'])) {
                $id = intval($_GET['id']);
            } else {
                header("Location: ?type=null");
            }

            if (isset($_POST['zamena'])) {
                $zamena = trim($_POST['zamena']);
                $db->query('UPDATE `smiles` SET `zamena` =?  WHERE `id` =?i LIMIT ?i', [$zamena, $id, 1]);
                msg(lang('Смайл отредактирован'));
            }

            $smile = $db->query('SELECT * FROM `smiles` WHERE `id` =?i', [$id])->row();
            echo '<div class="p_m"><img src="/style/smiles/'.output_text($smile['name']).'" alt="'.output_text($smile['name']).'" title="'.output_text($smile['name']).'"/>
            '.output_text($smile['name']).' - '.output_text($smile['zamena']).'<br/>';
            echo '<form method="post" action="?type=edit_smile&id='.$id.'&">';
            echo '<b>Текст вызова</b>:<br/><input name="zamena" value="'.esc(stripcslashes(htmlspecialchars($smile['zamena']))).'"/> <br/>
            * '.lang('перечеслять можно через').' "|" , '.lang('пример').' .test.|.test2.|.test3. и т.п
            <br/>';
            echo '<input value="'.lang('Сохранить').'" type="submit"/></form></div>';
        } else {
            msg($if_accept_msg);
        }

    break;


    #	вЫВОДИМ ПОЛНЫЙ СПИСОК ВСЕХ СМАЙЛОВ
    case 'smile_list':
        echo '<a href="?"><div class="p_m">  '.lang('Назад').'</div></a>';


    $k_page = k_page($smiles_koll, $set['p_str']);
    $page = page($k_page);
    $start = $set['p_str']*$page-$set['p_str'];
    if ($smiles_koll==0) {
        msg(lang('Нет смайлов'));
    }
    echo '<div class="p_m">'.lang('Всего смайлов').' : ('. $smiles_koll.')</div>';
    $smiles = $db->query('SELECT * FROM `smiles` ORDER BY `id` DESC LIMIT ?i, ?i', [$start, $set['p_str']]);

    while ($post = $smiles->row()) {
        echo '<div class="p_m">
	<img src="/style/smiles/'.output_text($post['name']).'" alt="'.output_text($post['name']).'" title="'.output_text($post['name']).'"/>
	<>	'.esc(stripcslashes(htmlspecialchars($post['zamena']))).'</div>';
    }
    if ($k_page>1) {
        str('?', $k_page, $page);
    }

    break;
#	УДАЛЯЕМ КАТЕГОРИЮ СО ВСЕМИ ПОТРОХАМИ
        case 'delet_dir':

    if ($if_accept) {
        if (isset($_GET['id'])) {
            $id = intval($_GET['id']);
        } else {
            header("Location: ?type=null");
        }
        $q = $db->query('SELECT id, name FROM `smiles` WHERE `id_dir` =?i', [$id]);
        while ($sm = $q->row()) {
            if (is_file(H . 'style/smiles/' . $sm['name'])) {
                unlink(H . 'style/smiles/' . $sm['name']);
            }
        }

        $db->query('DELETE FROM `smiles` WHERE `id_dir` =?i', [$id]);
        $db->query('DELETE FROM `smiles_dir` WHERE `id` =?i', [$id]);
        $db->query('OPTIMIZE TABLE `smiles`, `smiles_dir`');
        exit(header('Location: ?type=delet_msg'));
    } else {
        msg($if_accept_msg);
    }

        break;

        #	удаляем смайл
        case 'delet_smiles':

    if ($if_accept) {
        if (isset($_GET['id'])) {
            $id = intval($_GET['id']);
        } else {
            header("Location: ?type=null");
        }
        $smile = $db->query('SELECT * FROM `smiles` WHERE `id` =?i', [$id])->row();
        @unlink(H . 'style/smiles/'.$smile['name']);
        $db->query('DELETE FROM `smiles` WHERE `id`=?i LIMIT ?i', [$id, 1]);
        $db->query('OPTIMIZE TABLE `smiles`');
        exit(header('Location: ?type=delet_msg'));
    } else {
        msg($if_accept_msg);
    }

        break;

        #	Сообщение успешно при некоторых действиях
        case 'delet_msg':
        echo '<a href="?"><div class="p_m"> '.lang('Назад').' </div></a>';
        msg(lang('Успешно'));
        break;


}

include_once H . 'sys/inc/tfoot.php';
