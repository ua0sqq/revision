<?php

/**
 * @const FORUM Название папки модуля.
 */
define('FORUM', '/forum');

/**
 * @const ICONS Ширина и высота иконок.
 */
define('ICONS', 'style = "width: 22px; height: 22px"');
if (!isset($set['set_show_forum_nw_coll'])) {
    $set['set_show_forum_nw_coll'] = 0;
}

$k_post = go\DB\query('select count(*) from `forum_themes`')->el();
$k_page = k_page($k_post, $set['p_str']);
$page = page($k_page);
$start = $set['p_str']*$page-$set['p_str'];
if ($k_post != 0) {
    ?>
	<div class="act_them"><?php echo lang('Последние темы'); ?></div>
	<table class="act_them_fon">
        <?php
        $themes = go\DB\query('SELECT th.*, us.`id` AS id_creator, us.`nick` AS nick_creator, us.`mylink`, (
    SELECT COUNT(*) FROM `forum_posts` WHERE id_theme=th.id GROUP BY id_theme) AS count_posts
FROM `forum_themes` th
JOIN `user` us ON th.id_user=us.id
ORDER BY `time_post` DESC LIMIT ?i', [$set['set_show_forum_nw_coll']]);

    while ($theme = $themes->row()) {
        $creater = ['id' => $theme['id_creator'], 'nick' => $theme['nick_creator'], 'mylink' => $theme['mylink']];
        $count_posts = (int)$theme['count_posts'];
        $hide = (user_access('forum_post_ed')) ? null : '`hide` = "0" AND';
        $last_post = go\DB\query('SELECT `id`, `hide`, `id_user`, `time` FROM `forum_posts` WHERE '.$hide.' `id_theme` = '.$theme['id'].' ORDER BY `id` DESC')->row();

        $who = ($last_post && $last_post['id_user'] != 0) ? go\DB\query('SELECT `id`, `nick` FROM `user` WHERE `id` = '.$last_post['id_user'])->row() : array();
        $who_id = ($who && $last_post && $last_post['id_user'] != 0) ? $who['id'] : 0;
        $who_nick = ($who && $last_post && $last_post['id_user'] != 0) ? $who['nick'] : $set['nick_system'];

        if ($theme['reason_close'] != null) {
            $type = '_close';
        } elseif ($theme['type'] == 1) {
            $type = '_up';
        } else {
            $type = null;
        } ?>
            <tr>
                <td class="icon14">
                    <img src="<?php echo  FORUM ?>/icons/theme<?php echo  $type ?>.png" alt="*" <?php echo  ICONS ?> />
                </td>
                <td class="p_t">
                    <a href="<?php echo  FORUM.'/'.$theme['id_forum'].'/'.$theme['id_razdel'].'/'.$theme['id'] ?>.html"><?php echo  output_text($theme['name'], 1, 1, 0, 0, 0) ?></a> (<?php echo  $count_posts ?>)
                </td>
            </tr>
            <tr>
                <?php
                if ($last_post) {
                    ?>
                    <td class="p_m" colspan="2">
                        <b><?php echo lang('Автор'); ?>: <a href="/id<?php echo  $theme['id_user'] ?>"><?php echo  $creater['nick'] ?></a> | <?php
                        echo lang('Посл.'); ?>:</b> <a href="/id<?php echo  $who_id ?>"><?php echo  $who_nick ?></a> (<?php echo  vremja($last_post['time']) ?>)
                    </td>
                    <?php

                } else {
                    ?>
                    <td class="p_m" colspan="2">
                        <b><?php echo lang('Автор'); ?>: <a href="/id<?php echo  $theme['id_user'] ?>"><?php echo  $creater['nick'] ?></a></b>
                    </td>
                    <?php

                } ?>
            </tr>
            <?php

    } ?>
    </table>
    <?php

}
