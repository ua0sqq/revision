<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once  H . 'sys/inc/start.php';
include_once  H . 'sys/inc/sess.php';
include_once  H . 'sys/inc/settings.php';
include_once  H . 'sys/inc/db_connect.php';
include_once  H . 'sys/inc/ipua.php';
include_once  H . 'sys/inc/fnc.php';
include_once  H . 'sys/inc/user.php';
$set['title'] = lang('Анкета');
include_once  H . 'sys/inc/thead.php';
title();
aut();
only_reg();

if (isset($user)) {
    $ank['id'] = $user['id'];
}

if (isset($_GET['user'])) {
    if (isset($user) and $_GET['user'] == null) {
        $mylink = $user['mylink'];
    } else {
        $mylink = $_GET['user'];
    }
     
    if (count::query('user', ' `mylink`="' . $mylink . '"') != 0) {
        $link = $db->query('SELECT `id`, `mylink` FROM `user` WHERE `mylink`=? LIMIT ?i', [$mylink, 1])->row();
        $ank['id'] = $link['id'];
    } else {
        $ank['id'] = $user['id'];
    }
}
   
 
if (isset($_GET['id'])) {
    $ank['id'] = intval($_GET['id']);
}

$ank = get_user($ank['id']);





//для сео META данные
#-----------------------------------------------------------------------------#
$meta_keywords = mb_substr($ank['status_ank'], 0, 160);
#--->
$set['meta_keywords'] = meta_out_keywords($meta_keywords);
#---------------#
$ank['_ank_city']   = $ank['ank_city'] != null ? ','.$ank['ank_city'] : null;
$ank['_mylink']     = $ank['mylink'] != null ? ','.$ank['mylink'] : null;
$ank['_ank_family'] = $ank['ank_family'] != null ? ','.$ank['ank_family'] : null;
$ank['_ank_name']   = $ank['ank_name'] != null ? ','.$ank['ank_name'] : null;
$ank['_ank_countr'] = $ank['ank_countr'] != null ? ','.$ank['ank_countr'] : null;
$ank['_ank_icq']    = $ank['ank_icq'] != null ? ','.$ank['ank_icq'] : null;
$ank['_ank_mail']   = $ank['ank_mail'] != null  && $ank['set_show_mail'] != 0 ? ','.$ank['ank_mail'] : null;
$ank['_ank_skype']  = $ank['ank_skype'] != null ? ','.$ank['ank_skype'] : null;
#--->
$set['meta_description'] = meta_out_description($ank['nick'].''.$ank['_mylink'].''.$ank['_ank_city'].''.$ank['_ank_family'].''.$ank['_ank_name'].''.$ank['_ank_countr'].''.$ank['_ank_icq'].''.$ank['_ank_mail'].''.$ank['_ank_skype']);
#-----------------------------------------------------------------------------#







echo "<div class='user_panel'>";
echo nick($ank['id'], 'text');
if ($ank['group_access'] > 1) {
    echo " <span class='status' style='float:right;'>".lang($ank['group_name'])." </span>  ";
}
echo "</div>";

echo "<div class='ank_fon' style='line-height:1.6em;'>";

if ($ank['ank_name']!=null) {
    echo "<span class=\"ank_n\"><img src='/style/icons/st_a.png' alt='' /> ". lang('Имя').":</span> 
<span class=\"ank_d\"><a href='/user/search_ank_get?type=name&amp;q=".urlencode($ank['ank_name'])."' title='".lang('Найти всех людей по имени')." ".$ank['ank_name']."'>".$ank['ank_name']."</a></span><br />\n";
}

if ($ank['ank_family']!=null) {
    echo "<span class=\"ank_n\"><img src='/style/icons/st_a.png' alt='' /> ". lang('Фамилия').":</span> 
<span class=\"ank_d\"><a href='/user/search_ank_get?type=fam&amp;q=".urlencode($ank['ank_family'])."' title='".lang('Найти всех людей по фамилии')." ".$ank['ank_family']."'>".$ank['ank_family']."</a></span><br />\n";
}

echo "<span class=\"ank_n\"><img src='/style/icons/st_a.png' alt='' /> ". lang('Пол').":</span> <span class=\"ank_d\">".(($ank['pol']==1) ? lang('Мужской') : lang('Женский'))."</span><br />\n";

 
 
if ($ank['ank_skype']!=null) {
    echo "<span class=\"ank_n\"><img src='/style/icons/st_a.png' alt='' /> ". lang('Skype').":</span> 
<span class=\"ank_d\"> ".$ank['ank_skype']."</span><br />";
}

 
 
if ($ank['ank_city']!=null) {
    echo "<span class=\"ank_n\"><img src='/style/icons/st_a.png' alt='' /> ". lang('Город').":</span> 
<span class=\"ank_d\"> <a href='/user/search_ank_get?type=city&amp;q=".urlencode($ank['ank_city'])."' title='".lang('Найти всех людей из города')." ".$ank['ank_city']."'>".$ank['ank_city']."</a></span><br />\n";
}



if ($ank['ank_countr']!=null) {
    echo "<span class=\"ank_n\"><img src='/style/icons/st_a.png' alt='' /> ". lang('Регион').":</span> 
<span class=\"ank_d\"> <a href='/user/search_ank_get?type=countr&amp;q=".urlencode($ank['ank_countr'])."' title='".lang('Найти всех по региону')." ".$ank['ank_countr']."'>".$ank['ank_countr']."</a></span><br />\n";
}

#-----------------------------------------------#
if ($ank['ank_m_r'] != null) {
    $arr_mes = array( null, lang('Января'), lang('Февраля'), lang('Марта'), lang('Апреля'), lang('Мая'), lang('Июня'), lang('Июля'), lang('Августа'), lang('Сентября'), lang('Октября'), lang('Ноября'), lang('Декабря'));
    if ($ank['ank_m_r'] != null) {
        $ank['mes'] = $arr_mes[$ank['ank_m_r']];
    }
    echo "<span class=\"ank_n\"><img src='/style/icons/st_a.png' alt='' /> ". lang('Дата рождения').":</span>
 <span class=\"ank_d\">$ank[ank_d_r] $ank[mes] $ank[ank_g_r]г.</span>";
    $ank['ank_age']=date("Y")-$ank['ank_g_r'];
    if (date("n")<$ank['ank_m_r']) {
        $ank['ank_age'] = $ank['ank_age']-1;
    } elseif (date("n")==$ank['ank_m_r']&& date("j")<$ank['ank_d_r']) {
        $ank['ank_age']=$ank['ank_age']-1;
    }
    echo ",<span class=\"ank_d\"> $ank[ank_age] ". sclon_value($ank['ank_age'], array(lang('год'),lang('года'),lang('лет')))."</span><br />";
    if ($ank['ank_icq']!=null && $ank['ank_icq']!=0) {
        echo "<span class=\"ank_n\"><img src='/style/icons/st_a.png' alt='' /> Icq</span><span class=\"ank_d\"> $ank[ank_icq]</span><br />\n";
    }
}
#-----------------------------------------------#


if ($ank['ank_mail']!=null && ($ank['set_show_mail']==1 ||
 isset($user) && ($user['level']>$ank['level'] ||
 $user['level']==4))) {
    if ($ank['set_show_mail']==0) {
        $hide_mail = '['.lang('скрыт').']';
    } else {
        $hide_mail=null;
    }

    if (preg_match("#(@mail\.ru$)|(@bk\.ru$)|(@inbox\.ru$)|(@list\.ru$)#", $ank['ank_mail'])) {
        echo "<img src=\"http://status.mail.ru/?$ank[ank_mail]\" width=\"13\" height=\"13\" alt=\"\" /> <a href=\"mailto:$ank[ank_mail]\" title=\"\" class=\"ank_d\">$ank[ank_mail]</a>$hide_mail<br />\n";
    } else {
        echo "<span class=\"ank_n\"> <img src='/style/icons/st_a.png' alt='' /> E-mail:</span> <a href=\"mailto:$ank[ank_mail]\" title=\"\" class=\"ank_d\">$ank[ank_mail]</a>$hide_mail<br />\n";
    }
}

if ($ank['ank_n_tel']!=null) {
    echo "<span class=\"ank_n\"><img src='/style/icons/st_a.png' alt='' /> ".lang('Телефон').":</span> <span class=\"ank_d\">$ank[ank_n_tel]</span><br />\n";
}



if ($ank['ank_o_sebe']!=null) {
    echo "<span class=\"ank_n\"><img src='/style/icons/st_a.png' alt='' /> ".lang('О себе')." :</span> 
<div class='status_o_s'> </div>
<div class='status_o'> ".output_text($ank['ank_o_sebe'])."</div>

";
}


echo "</div>";

include_once  H . 'sys/inc/tfoot.php';
