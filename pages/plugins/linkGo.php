<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/compress.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/user.php';

$set['title']='Перенаправление';
include_once H . 'sys/inc/thead.php';
title();

if (!isset($_GET['go']) || (!$db->query('SELECT COUNT(*) FROM `rekl` WHERE `id`=?i', [$_GET['go']])->el() && !preg_match('#^https?://#', @base64_decode($_GET['go'])))) {
    exit(header("Location: /"));
}



if (preg_match('#^(ht|f)tps?://#', base64_decode($_GET['go']))) {
    if (isset($_SESSION['adm_auth'])) {
        unset($_SESSION['adm_auth']);
    }
    exit(header('Location: ' . base64_decode($_GET['go'])));
} else {
    $rekl = $db->query('SELECT * FROM `rekl` WHERE `id` =?i', [$_GET['go']])->row();
    $db->query('UPDATE `rekl` SET `count`=`count`+?i WHERE `id` =?i', [1, $rekl['id']]);

    if (isset($_SESSION['adm_auth'])) {
        unset($_SESSION['adm_auth']);
    }
    header('Refresh: 99; url=' . $rekl['link']);



    echo "<div class='p_m'> За содержание рекламируемого ресурса<br />";
    echo "администрация сайта " . strtoupper($_SERVER['HTTP_HOST']) . " ответственности не несёт.</div>";
    echo "<div class='p_m'><b><a href='/'>Отмена</a></b> :: ";

    echo '<a href="' . $rekl['link'] . '">Переход </a> [' . $rekl['count'] . ']</div>';
}
include_once H . 'sys/inc/tfoot.php';
