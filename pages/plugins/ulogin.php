<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
$show_all = true;
$input_page = true;
$reg_set = 2;
include_once H . 'sys/inc/user.php';
include_once H . 'sys/inc/shif.php';

/**
* @param string $first_name
* @param string $last_name
* @param string $nickname
* @param string $bdate (string in format: dd.mm.yyyy)
* @param array $delimiters
* @return string
*/

function userExist($nick = null)
{
    $nick_count = count::query('user', "`nick` = '".  $nick ."'");
    if ($nick_count == 1) {
        return true;
    } else {
        return null;
    }
}

function nick_gen($first_name, $last_name="", $nickname="", $bdate="", $delimiters = array('.', '_'))
{
    $delim = array_shift($delimiters);

    $first_name = retranslit($first_name);
    $first_name_s = substr($first_name, 0, 1);

    $variants = array();
    if (!empty($nickname)) {
        $variants[] = $nickname;
    }
    $variants[] = $first_name;
    if (!empty($last_name)) {
        $last_name = retranslit($last_name);
        $variants[] = $first_name.$delim.$last_name;
        $variants[] = $last_name.$delim.$first_name;
        $variants[] = $first_name_s.$delim.$last_name;
        $variants[] = $first_name_s.$last_name;
        $variants[] = $last_name.$delim.$first_name_s;
        $variants[] = $last_name.$first_name_s;
    }
    if (!empty($bdate)) {
        $date = explode('.', $bdate);
        $variants[] = $first_name.$date[2];
        $variants[] = $first_name.$delim.$date[2];
        $variants[] = $first_name.$date[0].$date[1];
        $variants[] = $first_name.$delim.$date[0].$date[1];
        $variants[] = $first_name.$delim.$last_name.$date[2];
        $variants[] = $first_name.$delim.$last_name.$delim.$date[2];
        $variants[] = $first_name.$delim.$last_name.$date[0].$date[1];
        $variants[] = $first_name.$delim.$last_name.$delim.$date[0].$date[1];
        $variants[] = $last_name.$delim.$first_name.$date[2];
        $variants[] = $last_name.$delim.$first_name.$delim.$date[2];
        $variants[] = $last_name.$delim.$first_name.$date[0].$date[1];
        $variants[] = $last_name.$delim.$first_name.$delim.$date[0].$date[1];
        $variants[] = $first_name_s.$delim.$last_name.$date[2];
        $variants[] = $first_name_s.$delim.$last_name.$delim.$date[2];
        $variants[] = $first_name_s.$delim.$last_name.$date[0].$date[1];
        $variants[] = $first_name_s.$delim.$last_name.$delim.$date[0].$date[1];
        $variants[] = $last_name.$delim.$first_name_s.$date[2];
        $variants[] = $last_name.$delim.$first_name_s.$delim.$date[2];
        $variants[] = $last_name.$delim.$first_name_s.$date[0].$date[1];
        $variants[] = $last_name.$delim.$first_name_s.$delim.$date[0].$date[1];
        $variants[] = $first_name_s.$last_name.$date[2];
        $variants[] = $first_name_s.$last_name.$delim.$date[2];
        $variants[] = $first_name_s.$last_name.$date[0].$date[1];
        $variants[] = $first_name_s.$last_name.$delim.$date[0].$date[1];
        $variants[] = $last_name.$first_name_s.$date[2];
        $variants[] = $last_name.$first_name_s.$delim.$date[2];
        $variants[] = $last_name.$first_name_s.$date[0].$date[1];
        $variants[] = $last_name.$first_name_s.$delim.$date[0].$date[1];
    }
    $i=0;

    $exist = true;
    while (true) {
        if ($exist = userExist($variants[$i])) {
            foreach ($delimiters as $del) {
                $replaced = str_replace($delim, $del, $variants[$i]);
                if ($replaced !== $variants[$i]) {
                    $variants[$i] = $replaced;
                    if (!$exist = userExist($variants[$i])) {
                        break;
                    }
                }
            }
        }
        if ($i >= count($variants)-1 || !$exist) {
            break;
        }
        $i++;
    }

    if ($exist) {
        while ($exist) {
            $nickname = $first_name.mt_rand(1, 999);
            $exist = userExist($nickname);
        }
        return $nickname;
    } else {
        return $variants[$i];
    }
}

if (isset($user)) {
    $_SESSION['message'] = lang('Вы уже авторизованы');
    exit(header("Location: /"));
}

if (isset($_POST['token'])) {
    $arr = api_Login::arr(@$_POST['token']);
}

if ($arr == null) {
    $_SESSION['message'] = lang('Ошибка сервер ulogin не доступен');
    exit(header("Location: /"));
}

if ($arr['error'] != null) {
    $_SESSION['message'] = lang('При авторизации произошла ошибка');
    exit(header("Location: /"));
}



$us_count = $db->query('SELECT COUNT(*) FROM `ulogin` WHERE `identity` =?', [$arr['identity']])->el();


if ($us_count == 1) {
    $uid = $db->query('SELECT `id_user` FROM `ulogin` WHERE `identity`=? AND `network`=? LIMIT ?i',
                      [$arr['identity'], $arr['network'], 1])->row();

    $user = get_user($uid['id_user']);
    $_SESSION['id_user'] = $user['id'];


    //Загрузка дополнительных плагинов
    $Search = glob(H . 'sys/login/*.php');
    foreach ($Search as $load_plugins) {
        include_once $load_plugins;
    }

    if (!is_file(H . 'files/avatars/'.$user['id'].'.png') and $set['soc_ava_reset'] == 1) {
        //загружаем класс для работы с изображениями
        require_once  H . "sys/inc/ImgType.class.php";

        //класс для работы с файлом изображения
       $image = new ImgType();
       //сам файл
       $image->load($arr['photo_big']);
       //размеры
       //  $image->resize($wPic,$hPic);
       $image->resizeToWidth($set['avatar_s']);
       //сохраняем под ...
       $image->save(H . 'files/avatars/'. $user['id'] . '.png');
    }

    $db->query('INSERT INTO `user_log` (`id_user`, `time`, `ua`, `ip`, `method`) VALUES( ?i, ?i, ?, ?i, ?string)',
               [$user['id'], time(), $user['ua'], $user['ip'], 3]);
    cache_delete::user($user['id']);
    exit(header("Location: /user/start"));
}


$nick = nick_gen($arr['first_name'], $arr['last_name'], $arr['nickname'], $arr['bdate']);
$nick = str_replace(array('.', '-', '—', ' '), '_', trim($nick));
$nick_count = $db->query('SELECT COUNT(*) FROM `user` WHERE `nick`=?', [$nick])->el();
if ($nick_count == 1) {
    $nick = $nick . '_' . time();
}
$pass =  md5(passgen(9));

if ($arr['sex'] == 2) {
    $pol = 1;
} else {
    $pol = 0;
}

$data = [$nick, $pass, time(), time(), $pol, (time() + 99999999999), $arr['first_name'], $arr['last_name'], $arr['email'], $arr['city'], $arr['country'], 1];
$id = $db->query('INSERT INTO `user` (`nick`, `pass`, `date_reg`, `date_last`, `pol`,`pass_time`,`ank_name`,`ank_family`,`ank_mail`,`ank_city`,`ank_countr`,`soc_nick`)
VALUES ( ?, ?, ?i, ?i, ?string, ?i, ?, ?, ?, ?, ?, ?)', $data);

$id = $id->id();
$key = $pass;

if ($arr['manual'] == null) {
    $arr['manual'] = null;
}

$db->query('INSERT INTO `ulogin` (`time`, `id_user`, `ip`, `ank_name`, `ank_family`, `key`, `network`, `identity`, `profile`, `uid`, `manual`)
VALUES(?i, ?i, ?, ?, ?, ?, ?, ?, ?, ?, ?);',
[time(), $id, $ip, $arr['first_name'], $arr['last_name'], $key, $arr['network'], $arr['identity'], $arr['profile'], $arr['uid'], $arr['manual']]);

$user = get_user($id);
$_SESSION['id_user'] = $user['id'];

setcookie('id_user', $user['id'], time()+60*60*24*365);
setcookie('pass', cookie_encrypt($pass, $user['id']), time()+60*60*24*365);

    $db->query('UPDATE `user` SET `mylink`=?, `browser`=? WHERE `id`=?i',
               ['id' . $user['id'], ($webbrowser == 'web' ? 'web':'wap'), $user['id']]);

    $_SESSION['message'] = lang('Регистрация прошла успешно,добро пожаловать на сайт');

    $Search = glob(H . 'sys/reg/*.php');
    foreach ($Search as $load_plugins) {
        sort($Search);
        include_once $load_plugins;
    }


    //загружаем класс для работы с изображениями
    require_once  H . 'sys/inc/ImgType.class.php';
    if (is_file(H . 'files/avatars/' . $user['id'] . '.png')) {
        unlink(H . 'files/avatars/' . $user['id'] . '.png');
    }

    //класс для работы с файлом изображения
   $image = new ImgType();
   //сам файл
   $image->load($arr['photo_big']);
   //размеры
   $image->resizeToWidth($set['avatar_s']);
   //сохраняем под ...
   $image->save(H.'files/avatars/'. $user['id'] . '.png');

    cache_delete::user($user['id']);
    exit(header("Location: /"));
