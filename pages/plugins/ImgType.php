<?php

    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
$_GET['imgLink'] = base64_decode($_GET['imgLink']);
$imgLink = $_GET['imgLink'] != null ? $_GET['imgLink'] : H . 'style/icons/non_bb_img.png';
$imgType = isset($_GET['imgType']) ? intval($_GET['imgType']) : false;
//переключение шаблонов
switch ($imgType) {
    default:
        $imgType = 0;
        break;
    case 1:
        $imgTypeSet = 50;
        break;
    case 2:
        $imgTypeSet = 80;
        break;
    case 3:
        $imgTypeSet = 150;
        break;
}
//если выбран режим шаблонов
if ($imgType == 0) {
    $width = isset($_GET['width']) ? $_GET['width'] : 50;
    $height = isset($_GET['height']) ? $_GET['height'] : 50;
}
//загружаем класс
include H . 'sys/inc/ImgType.class.php';
//задаем мемо типы
header('Content-Type: image/png');
//обьявляем класс
$image = new ImgType();
//загружаем картинку
//если режим бб кода
if (isset($_GET['imgLink_bb'])) {
    $image->load(base64_decode($_GET['imgLink_bb']));
} else {
    $image->load(H . $imgLink);
}
//если тип не 0
if ($imgType > 0) {
    $image->resizeToWidth($imgTypeSet);
}
//если ноль то можно задать свои размеры
if ($imgType == 0) {
    $image->resize($width, $height);
}
//выводим
$image->output();
