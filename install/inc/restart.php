<?php


$db->query("TRUNCATE `forum`");
$db->query("TRUNCATE `forum_journal`");
$db->query("TRUNCATE `forum_posts`");
$db->query("TRUNCATE `forum_post_files`");
$db->query("TRUNCATE `forum_post_rating`");
$db->query("TRUNCATE `forum_razdels`");
$db->query("TRUNCATE `forum_reports`");
$db->query("TRUNCATE `forum_themes`");

$db->query("TRUNCATE `forum_votes`");
$db->query("TRUNCATE `forum_votes_var`");
$db->query("TRUNCATE `forum_vote_voices`");
$db->query("TRUNCATE `f_journal`");

##------------>
dir_clear(H . 'forum/files/');
##------------>
dir_clear(H . 'sys/cache/images');
dir_clear(H . 'sys/cache/users');
dir_clear(H . 'sys/cache/other');
dir_clear(H . 'sys/cache/user_menu/');
dir_clear(H . 'sys/cache');


dir_clear(H . 'sys/tmp');
dir_clear(H.'files/avatars_list', null);
dir_clear(H.'files/avatars', null);
dir_clear(H.'files/foto', null);


dir_clear(H . 'sys/modules/log');
dir_clear(H . 'sys/modules/tmp');
dir_clear(H . 'sys/modules/files');

dir_clear(H . 'sys/sql_update');

##------------------------------------##
dir_clear(H . 'sys/obmen/files');
dir_clear(H . 'sys/obmen/screens/14');
dir_clear(H . 'sys/obmen/screens/48');
dir_clear(H . 'sys/obmen/screens/128');
##------------------------------------##

$db->query("TRUNCATE `admin_log`");
$db->query("TRUNCATE `admin_log_act`");
$db->query("TRUNCATE `admin_log_mod`");

$db->query("TRUNCATE `ban_ip`");
$db->query("TRUNCATE `reg_mail`");
$db->query("TRUNCATE `ulogin`");


$db->query("TRUNCATE `gallery`");
$db->query("TRUNCATE `gallery_foto`");
$db->query("TRUNCATE `gallery_komm`");
$db->query("TRUNCATE `gallery_rating`");

$db->query("TRUNCATE `guest`");
$db->query("TRUNCATE `guests`");

$db->query("TRUNCATE `ban`");
$db->query("TRUNCATE `jurnal`");
$db->query("TRUNCATE `jurnal_system`");
$db->query("TRUNCATE `license_support`");

$db->query("TRUNCATE `mail`");
$db->query("TRUNCATE `mail_to_send`");

$db->query("TRUNCATE `modules`");

$db->query("TRUNCATE `news`");
$db->query("TRUNCATE `news_komm`");

$db->query("TRUNCATE `obmennik_dir`");
$db->query("TRUNCATE `obmennik_files`");
$db->query("TRUNCATE `obmennik_komm`");

$db->query("TRUNCATE `rekl`");

$db->query("TRUNCATE `users_konts`");
$db->query("TRUNCATE `user_collision`");
$db->query("TRUNCATE `user_files`");
$db->query("TRUNCATE `user_log`");
$db->query("TRUNCATE `user_ref`");

$db->query("TRUNCATE `visit_everyday`");
$db->query("TRUNCATE `visit_today`");

$db->query('UPDATE `user` SET `license_time` =?i WHERE `id` =?i', [(time() + 2592000), 1]);

$text = 'Привет :)
Установка прошла успешно ,теперь я советовал бы тебе посетить админку сайта и настроить  все как тебе нужно.
А так же хочу заметить что к данной версии будут на форуме выложены новые модули приложения ,
игры и многое другое.Уже совсем скоро ,а так же советовал бы следить за обновлениями на форуме :)
еще раз спасибо за пользование Fiera
Кстати на форуме иногда разыгрываются бесплатные лицензии :)
Удачи Мастер :)
';

mail_send(0, 1, $text);
