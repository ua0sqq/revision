<?php

function install()
{
    $msg = false;
    if (is_file(H . 'sys/dat/settings.conf')) {
        $msg .= ' settings.conf';
    }
    if (is_file(H . 'sys/dat/shif.conf')) {
        $msg .= ' shif.conf';
    }
    if (is_file(H . 'sys/ini/db.ini')) {
        $msg .= ' db.ini';
    }
    if (false !== $msg) {
        return ('Удалите старые файлы конфигурации' . $msg . '!');
    } else {
        return false;
    }
}
// функция обрабатывает текстовые строки перед выводом в браузер
// настоятельно не рекомундуется тут что-либо менять
function output_text($str, $br = 1, $html = 1, $smiles = 1, $links = 1, $bbcode = 1)
{
    global $theme_ini;
    // преобразуем все к нормальному перевариванию браузером
    if ($html) {
        $str = htmlentities($str, ENT_QUOTES, 'UTF-8');
    }
    // обработка ссылок
    if ($links) {
        $str = links($str);
    }
    // обработка bbcode
    if ($bbcode) {
        $tmp_str = $str;
        $str = bbcode($str);
    }
    // переносы строк
    if ($br) {
        $str = br($str);
    }
    // возвращаем обработанную строку
    return stripslashes($str);
}
function links_preg1($arr)
{
    global $set;
    if (preg_match('#^http://' . preg_quote($_SERVER['HTTP_HOST']) . '#', $arr[1]) ||
        !preg_match('#://#', $arr[1])) {
        return '<a href="' . $arr[1] . '">' . $arr[2] .
            '</a>';
    } else {
        return '<a' . ($set['web'] ? ' target="_blank"' : null) . ' href="http://' .
            $_SERVER['HTTP_HOST'] . '/linkGo?go=' . base64_encode(html_entity_decode($arr[1])) .
            '">' . $arr[2] . '</a>';
    }
}
function links_preg2($arr)
{
    global $set;
    if (preg_match('#^http://' . preg_quote($_SERVER['HTTP_HOST']) . '#', $arr[2])) {
        return $arr[1] . '<a href="' . $arr[2] . '">' . $arr[2] . '</a>' . $arr[4];
    } else {
        return $arr[1] . '<a' . ($set['web'] ? ' target="_blank"' : null) .
            ' href="http://' . $_SERVER['HTTP_HOST'] . '/linkGo?go=' . base64_encode(html_entity_decode($arr[2])) . '">' . $arr[2] . '</a>' . $arr[4];
    }
}
function links($msg)
{
    global $set;
    if ($set['bb_img']) {
        $msg = preg_replace_callback('/\[img\](.+)\[\/img\]/isU',
            'img_preg', $msg);
    }
    if ($set['bb_url']) {
        $msg = preg_replace_callback('/\[url=(.+)\](.+)\[\/url\]/isU',
            'links_preg1', $msg);
    }
    if ($set['bb_http']) {
        $msg = preg_replace_callback('~(^|\s)([a-z]+://([^ \r\n\t`\'"]+))(\s|$)~iu',
            'links_preg2', $msg);
    }
    return $msg;
}
function dir_clear($path, $conf = 1)
{
    if ($handle = opendir($path)) {
        while (false !== ($file = readdir($handle))) {
            if ($file != '.' && $file != '..' && $file != '.htaccess' && $file !=
                'clear_conf.txt' && $file != '0.png' && $file != 'guest.png') {
                if (is_file($path . '/' . $file)) {
                    unlink($path . '/' . $file);
                }
            }
        }
        if ($conf == 1) {
            $text = 'Последняя зачистка папки была [' . date("F j, Y, H:i:s") . ']';
            $files = fopen($path . 'clear_conf.txt', 'w');
            fwrite($files, $text);
            fclose($files);
            closedir($handle);
        }
    }
}
function mail_send($user = 0, $user_id = 0, $msg = null)
{
    go\DB\query('INSERT INTO `mail` (`id_user`, `id_kont`, `msg`, `time`) VALUES ( ?i, ?i, ?, ?i)', [$user, $user_id, $msg, time()]);
}
//помните внутрености? вот вот
function strlen2($str)
{
    if (function_exists('mb_substr')) {
        return mb_strlen($str, 'UTF-8');
    }
    if (function_exists('iconv')) {
        return iconv_strlen($string, 'UTF-8');
    }
    return strlen(utf8_decode($string));
}
# Подсветка кода
function bbcodehightlight($arr)
{
    $arr[0] = html_entity_decode($arr[0], ENT_QUOTES, 'UTF-8');
    return '<div class="cit" style="overflow:scroll;clip:auto;max-width:480px;">' .
        preg_replace('#<code>(.*?)</code>#si', '\\1', highlight_string($arr[0], 1)) .
        '</div>';
}
# ВВ коды
function bbcode($msg)
{
    global $set, $user;
    $bbcode = array();
    $bbcode['/\[url=(.+)\](.+)\[\/url\]/isU'] =
        '<a class="links_t" href="$1">$2</a>';
    //Куксив
    $bbcode['/\[i\](.+)\[\/i\]/isU'] = '<em>$1</em>';
    //Жирный
    $bbcode['/\[b\](.+)\[\/b\]/isU'] = '<strong>$1</strong>';
    //Подчеркнутый
    $bbcode['/\[u\](.+)\[\/u\]/isU'] =
        '<span style="text-decoration:underline;">$1</span>';
    //большой
    $bbcode['/\[big\](.+)\[\/big\]/isU'] =
        '<span style="font-size:large;">$1</span>';
    //малый
    $bbcode['/\[small\](.+)\[\/small\]/isU'] =
        '<span style="font-size:small;">$1</span>';
    //цветной текст
    $bbcode['/\[color=(.+)\](.+)\[\/color\]/isU'] =
        '<span style="color:$1;">$2</span>';
    //рег. размеры текста
    $bbcode['/\[size=([0-9]+)\](.+)\[\/size\]/isU'] =
        '<span style="font-size:$1px;">$2</span>';
    //видео с ютуба
    $bbcode['/\[youtube\](.+)\[\/youtube\]/isU'] =
        '<br /><center><iframe width="400" height="250" src="//www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe></center>';
    //видео
    $bbcode['/\[video\](.+)\[\/video\]/isU'] =
        "<center><object type=application/x-shockwave-flash data=/sys/js/video/v.swf width=550 height=400><param name=bgcolor value=#ffffff /><param name=allowFullScreen value=true /><param name=allowScriptAccess value=always /><param name=wmode value=transparent /><param name=movie value=/sys/js/video/v.swf/><param name=flashvars value=file=$1/></object></center>";
    //мп3
    $bbcode['/\[mp3\](.+)\[\/mp3\]/isU'] =
        '<center><object type="application/x-shockwave-flash" data="/sys/js/mp3/i.swf" width="200" height="20" id="dewplayer" name="dewplayer"><param name="movie" value="/sys/js/mp3/i.swf" /><param name="flashvars" value="mp3=$1" /><param name="wmode" value="transparent" /></object></center>';
    //админ текст
    $bbcode['/\[admin\](.+)\[\/admin\]/isU'] = $user['group_access'] >= 1 ?
        'Админ текст :<br/>  <span style="color:red">$1 </span> ' :
        ' <span style="color:red">Текст доступный только Администрации </span>';
    //если найдены
    if (count($bbcode)) {
        $msg = preg_replace(array_keys($bbcode), array_values($bbcode),
            $msg);
    }
    //Подсветка php кода
    if ($set['bb_code'] == 1) {
        $msg = preg_replace_callback('#&lt;\?(.*?)\?&gt;#sui',
            'bbcodehightlight', $msg);
    }
    return $msg;
}
function lang($text)
{
    return $text;
}
function br($msg, $br = '<br />')
{
    return preg_replace("~((<br( ?/?)>)|\n|\r)+~i", $br, $msg);
} // переносы строк
function esc($text, $br = null)
{ // Вырезает все нечитаемые символы
    if ($br != null) {
        for ($i = 0; $i <= 31; $i++) {
            $text = str_replace(chr($i), null, $text);
        }
    } else {
        for ($i = 0; $i < 10; $i++) {
            $text = str_replace(chr($i), null, $text);
        }
        for ($i = 11; $i < 20; $i++) {
            $text = str_replace(chr($i), null, $text);
        }
        for ($i = 21; $i <= 31; $i++) {
            $text = str_replace(chr($i), null, $text);
        }
    }
    return $text;
}
function msg($msg)
{
    echo "<div class='msg'>$msg</div>\n";
} // вывод сообщений
function passgen($k_simb = 8, $types = 3)
{
    $password = "";
    $small = "abcdefghijklmnopqrstuvwxyz";
    $large = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $numbers = "1234567890";
    mt_srand((double)microtime() * 1000000);
    for ($i = 0; $i < $k_simb; $i++) {
        $type = mt_rand(1, min($types, 3));
        switch ($type) {
            case 3:
                $password .= $large[mt_rand(0, 25)];
                break;
            case 2:
                $password .= $small[mt_rand(0, 25)];
                break;
            case 1:
                $password .= $numbers[mt_rand(0, 9)];
                break;
        }
    }
    return $password;
}
$passgen = passgen();
// сохранение настроек системы
function save_settings($set)
{
    unset($set['web']);
    if ($fopen = fopen(H . 'sys/dat/settings.conf', 'w')) {
        fputs($fopen, serialize($set));
        fclose($fopen);
        chmod(H . 'sys/dat/settings.conf', 0777);
        return true;
    } else {
        return false;
    }
}
// рекурсивное удаление папки
function delete_dir($dir)
{
    if (is_dir($dir)) {
        $od = opendir($dir);
        while ($rd = readdir($od)) {
            if ($rd == '.' || $rd == '..') {
                continue;
            }
            if (is_dir("$dir/$rd")) {
                chmod("$dir/$rd", 0777);
                delete_dir("$dir/$rd");
            } else {
                chmod("$dir/$rd", 0777);
                unlink("$dir/$rd");
            }
        }
        closedir($od);
        chmod("$dir", 0777);
        return rmdir("$dir");
    } else {
        chmod("$dir", 0777);
        unlink("$dir");
    }
}
