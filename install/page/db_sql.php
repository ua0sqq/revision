    <div class="div">
      <form method="post" action="">
        <fieldset>
            <input type="submit" class="div" name="refresh" value="Обновить"/>
        </fieldset>
      </form>
    </div>
<?php

if (isset($_SESSION['mysql_ok']) && $_SESSION['mysql_ok'] == true) {
    if (isset($_GET['step']) && $_GET['step'] == '4') {
        header('Location: ?type=creater&step=4');
        exit;
    }
} elseif (isset($_POST['host']) && isset($_POST['user']) && isset($_POST['pass']) && isset($_POST['db'])) {
    error_reporting(E_ALL);
    $db = @new mysqli($_POST['host'], $_POST['user'], $_POST['pass'], $_POST['db']);
    if ($db->connect_error) {
        $err[] = '' . $db->connect_error;
        $err[] = 'Невозможно подключиться к серверу ' . $_POST['host'];
    } else {
        $set['mysql_db_name'] = $_SESSION['db'] = $_POST['db'];
        $set['mysql_host'] = $_SESSION['host'] = $_POST['host'];
        $set['mysql_user'] = $_SESSION['user'] = $_POST['user'];
        $set['mysql_pass'] = $_SESSION['pass'] = $_POST['pass'];
        $set['charset_names'] = 'utf8';
        $db->query('set charset utf8');

        $db_tables = array();
        $tab = $db->query('SHOW TABLES FROM `' . $_SESSION['db'] . '`');
        if ($tab->num_rows > 0) {
            while ($row = $tab->fetch_row()) {
                $db_tables[] = $row[0];
            }
        }
        $opdirtables = opendir(H . 'install/db');
        while ($filetables = readdir($opdirtables)) {
            if (preg_match('#\.sql$#i', $filetables)) {
                $table_name = preg_replace('#\.sql$#i', null, $filetables);
                if (in_array($table_name, $db_tables)) {
                    if (isset($_POST['rename']) && $_POST['rename'] == 1) {
                        $db->query('ALTER TABLE `' .$table_name . '` RENAME `~' . time() . '_' . $table_name . '`');
                    } else {
                        $db_not_null = true;
                    }
                }
            }
        }
        if (isset($db_not_null)) {
            $err[] = 'В выбранной базе данных (' . $_SESSION['db'] .
                ') содержатся таблицы с идентичными названиями. Очистите или выберите другую базу данных.';
        } else {
            include_once H . 'install/inc/ver_tables.php';
            $msg[] = 'Успешно выполнено ' . $ok_sql . ' из ' . $k_sql . ' запросов';
            $_SESSION['mysql_ok'] = true;
        }
    }
}
if (isset($_SESSION['mysql_ok']) && $_SESSION['mysql_ok'] == true) {
    echo '<div class="msg">Подключение к базе данных успешно выполнено</div>' . PHP_EOL;
    if (isset($msg)) {
        foreach ($msg as $key => $value) {
            echo '<div class="msg">' . $value . '</div>' . PHP_EOL;
        }
    }
    if (isset($err)) {
        foreach ($err as $key => $value) {
            echo '<div class="err">' . $value . '</div>' . PHP_EOL;
        }
    }
    if (is_file(H . 'sys/ini/db.ini')) {
        unlink(H . 'sys/ini/db.ini');
    }
    if (!isset($err)) {
        $dbSET = '[MYSQL]
;Адаптер для работы с базой данных
_adapter = "mysql";
;Хост базы данных
mysql_host = "' . $_SESSION['host'] . '";
;Имя пользователя базы данных
mysql_user = "' . $_SESSION['user'] . '";
;Пароль пользователя базы данных
mysql_pass = "' . $_SESSION['pass'] . '";
;Имя базы данных
mysql_db_name = "' . $_SESSION['db'] . '";
;Кодировка базы данных и соединения
charset_names = "utf8";
charset = "";
charset_client = "";
charset_connection = "";
charset_result = "";
';
        file_put_contents(H . 'sys/ini/db.ini', $dbSET, FILE_APPEND);
        $tmp_set['shif'] = passgen(8);
        if (save_settings($tmp_set)) {
        }
        include_once H . 'sys/inc/shif.php'; ?>
<script type="text/javascript">
   document.location.href = "?type=creator&step=4";
</script><noscript><div class="p_m"><a href="?type=creator&amp;step=4">Далее...</a></div></noscript>
   <?php

    }
} else {
    if (isset($err)) {
        foreach ($err as $key => $value) {
            echo '<div class="err">' . $value . '</div>' . PHP_EOL;
        }
    } ?>
<div class="div">
    <form method="post" action="">
        <fieldset>
        <legend>Подключение к базе</legend>
        <p>Хост:
        <p><input name="host" class="div" value="<?php echo(isset($_SESSION['host']) ? $_SESSION['host'] : 'localhost')?>" type="text"/>
        <p>Пользователь:
        <p><input name="user" class="div" value="<?php echo(isset($_SESSION['user']) ? $_SESSION['user'] : '')?>" type="text"/>
        <p>Пароль:<br />
        <p><input name="pass" class="div" value="<?php echo(isset($_SESSION['pass']) ? $_SESSION['pass'] : '')?>" type="text"/>
        <p>Имя базы:
        <p><input name="db" class="div" value="<?php echo(isset($_SESSION['db']) ? $_SESSION['db'] : '')?>" type="text"/></p>

<?php

    if (isset($db_not_null)) {
        ?>
        <p><label><input type="checkbox" checked="checked" name="rename" value="1" />
        Переименовать существующие таблицы</label></p>
<?php

    } ?>
        <p><input class="links" value="Далее" type="submit" /></p>
        </fieldset>
    </form>
</div>
<?php

}

?>
