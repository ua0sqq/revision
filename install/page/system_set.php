<?php

include_once H . 'sys/inc/testing.php';
if (isset($err)) {
    if (is_array($err)) {
        foreach ($err as $key => $value) {
            echo '<div class="err">' . $value . '</div>' . PHP_EOL;
        }
    } else {
        echo '<div class="err">' . $err . '</div>' . PHP_EOL;
    }
}
if (!isset($err)) {
    echo
        '<div class="div">Система совместима <a class="links" href="?type=cmod&amp;step=2">Продолжить</a></div>';
} else {
    echo '<div class="err">Система не совместима ,исправьте ошибки и обновите страницу</div>';
    echo '<div class="div">Вы можите <a class="links" href="?type=cmod&amp;step=2">Пропустить это шаг </a>(возможны ошибки на сайте)</div>';
}
