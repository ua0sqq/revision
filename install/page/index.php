<?php

if (is_file(H . 'sys/docs/version_update.txt')) {
    $int = isset($_GET['update']) ? 50 : 10;
    echo '<div class="div">' . PHP_EOL;
    $f = file(H . 'sys/docs/version_update.txt');
    for ($i = 0; $i < $int && $i < count($f); $i++) {
        echo output_text($f[$i]);
    }
    if (!isset($_GET['update'])) {
        echo '<br /><a class="links" href="?update">Подробнее...</a>' . PHP_EOL;
    }
    echo '</div>' . PHP_EOL;
}
if (is_file(H . 'sys/docs/core.txt')) {
    echo '<div class="div">' . PHP_EOL;
    $f = file(H . 'sys/docs/core.txt');
    for ($i = 0; $i < 99999 && $i < count($f); $i++) {
        echo output_text($f[$i]);
    }
    echo '</div>' . PHP_EOL;
}
echo '<div class="div">' . PHP_EOL;
echo '  <a class="links" href="?type=copy&amp;step=0"><img src="img/b1.png" alt="*"/>&nbsp;Начать установку</a>' .
    PHP_EOL;
echo '</div>' . PHP_EOL;
