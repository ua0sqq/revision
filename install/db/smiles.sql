-- phpMyAdmin SQL Dump
-- version 4.3.4
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 03 2015 г., 03:43
-- Версия сервера: 5.6.22-log
-- Версия PHP: 5.5.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test_local`
--

-- --------------------------------------------------------

--
-- Структура таблицы `smiles`
--

CREATE TABLE IF NOT EXISTS `smiles` (
  `id` int(11) NOT NULL,
  `id_dir` int(11) NOT NULL,
  `name` varchar(99) NOT NULL,
  `zamena` varchar(99) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `smiles`
--

INSERT DELAYED INTO `smiles` (`id`, `id_dir`, `name`, `zamena`) VALUES
(29, 1, 'rad_subdomain.test1.ru', '.rad.'),
(30, 1, 'jazyk_subdomain.test1.ru', '.yaz.'),
(31, 1, 'pechal_subdomain.test1.ru', '.pesh.'),
(32, 1, 'uzhas_subdomain.test1.ru', '.yjas.'),
(33, 1, 'zuby_subdomain.test1.ru', '.зубы.'),
(34, 1, 'rad_subdomain.test1.ru', '.рад.'),
(35, 1, 'bez_zvuka_subdomain.test1.ru', '.безЗвука.');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `smiles`
--
ALTER TABLE `smiles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `smiles`
--
ALTER TABLE `smiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
