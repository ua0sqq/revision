<?php
// ???  надо еще раз просмотреть
include_once 'sys/inc/start.php';
include_once 'sys/inc/sess.php';
include_once 'sys/inc/home.php';
include_once 'sys/inc/settings.php';
include_once 'sys/inc/db_connect.php';
include_once 'sys/inc/ipua.php';
include_once 'sys/inc/fnc.php';
include_once 'sys/inc/user.php';
only_reg();
unset($_COOKIE['mail_count']);

if (!isset($_GET['id'])) {
    exit(header('Location: /konts.php' . SID));
}

$ank = $db->query('select id, nick, group_access, mylink from `user` where `id`=?i limit ?i', [$_GET['id'], 1])->row();

if ($ank['id'] == $user['id']) {
    exit(header('Location: /konts.php'));
}


if (!isset($ank['nick'])) {
    $ank['nick'] = $set['nick_system'];
    $ank['group_access'] = 999;
    $ank['id'] = $set['id_system'];
}

if (!$ank) {
    exit(header("Location: /konts.php?us_dell"));
}

$set['title'] = lang('Почта') . ' : ' . $ank['nick'];
include_once 'sys/inc/thead.php';
title();

$time_flood = $user['level'] == 0 and $user['date_last'] > time() - $set['flood_mail_time_s'];

// добавляем в контакты
if ($user['id'] != $ank['id'] && $user['add_konts'] == 2 &&
    !$db->query('SELECT COUNT(*) FROM `users_konts` WHERE `id_user`=?i AND `id_kont`=?i', [$user['id'], $ank['id']])->el()) {
    $db->query('INSERT INTO `users_konts` (`id_user`, `id_kont`, `time`) VALUES (?i, ?i, ?i)', [$user['id'], $ank['id'], $time]);
}

// обновление сведений о контакте
$db->query('UPDATE `users_konts` SET `new_msg`=?string WHERE `id_kont`=?i AND `id_user`=?i LIMIT ?i', [0, $ank['id'], $user['id'], 1]);

// помечаем сообщения как прочитанные
$db->query('UPDATE `mail` SET `read`=?string WHERE `id_kont`=?i AND `id_user`=?i', [1, $user['id'], $ank['id']]);

if (isset($_POST['refresh'])) {
    exit(header('Location: ?id=' . $ank['id']));
}

if (isset($_POST['msg']) && $_POST['send'] && $ank['id'] != 0) {
    if ($user['flood_time'] > time()) {
        $db->query('UPDATE `user` SET `flood_count`=`flood_count`+?i WHERE `id`=?i', [1, $user['id']]);
        $_SESSION['message'] = lang('Нельзя слать сообщения слишком часто');
        exit(header("Location: ?id=$ank[id]"));
    }

    if ($user['flood_count'] > $set['flood_mail_count'] and $set['flood_mail_on'] == 1) {
        $_SESSION['message'] = lang('Вы временно отправлены на карантин, из-за подозрения в спаме');
        $prich = 'Слишком высокая активность в почте';
        $timeban = time() + 60 * $set['flood_mail_ban']; // бан на час
        $db->query('INSERT INTO `ban` (`id_user`, `id_ban`, `prich`, `time`) VALUES (?i, ?i, ?, ?i)', [$user['id'], 0, $prich, $timeban]);
        $db->query('UPDATE `user` SET `flood_count`=?i WHERE `id`=?i', [0, $user['id']]);

        if ($set['flood_mail_adm_on'] == 1) {
            //сообщаем админам (в разработке)
        }

        exit(header("Location: ?id=$ank[id]"));
    }

    if ($time_flood) {
        if (!isset($_SESSION['captcha'])) {
            $err[] = lang('Ошибка проверочного числа');
        }
        if (!isset($_POST['chislo'])) {
            $err[] = lang('Введите проверочное число');
        } elseif ($_POST['chislo'] == null) {
            $err[] = lang('Введите проверочное число');
        } elseif ($_POST['chislo'] != $_SESSION['captcha']) {
            $err[] = lang('Проверьте правильность ввода проверочного числа');
        }
    }

    $msg = $_POST['msg'];
    if (isset($_POST['translit']) && $_POST['translit'] == 1) {
        $msg = translit($msg);
    }
    if (mb_strlen($msg) > 1024) {
        $err[] = lang('Сообщение превышает 1024 символа');
    }
    if (mb_strlen($msg) < 2) {
        $err[] = lang('Слишком короткое сообщение');
    }
    $mat = antimat($msg);
    if ($mat) {
        $err[] = lang('В тексте сообщения обнаружен мат') . ': ' . $mat;
    }

    if (!isset($err)) {
        if (!$db->query('SELECT COUNT(*) FROM `mail` WHERE `id_user` =?i AND `id_kont` =?i AND `time`>?i AND `msg`=?',
                     [$user['id'], $ank['id'], ($time - 360), $msg])->el()) {
            // отправка сообщения
                $db->query('INSERT INTO `mail` (`id_user`, `id_kont`, `msg`, `time`) VALUES(?i, ?i, ?, ?i)',
                           [$user['id'], $ank['id'], $msg, $time]);
                // добавляем в контакты
                if ($user['add_konts'] == 1) {
                    if (!$db->query('SELECT COUNT(*) FROM `users_konts` WHERE `id_user`=?i AND `id_kont`=?i',
                                      [$user['id'], $ank['id']])->el()) {
                        $db->query('INSERT INTO `users_konts` (`id_user`, `id_kont`, `time`) VALUES (?i, ?i, ?i)',
                                   [$user['id'], $ank['id'], $time]);
                    }
                }
                // обновление сведений о контакте
                $db->query('UPDATE `users_konts` SET `time`=?i WHERE `id_user`=?i AND `id_kont`=?i OR `id_user`=?i AND `id_kont`=?i',
                           [$time, $user['id'], $ank['id'], $ank['id'], $user['id']]);

            if ($set['flood_mail_on'] == 1 && $time_flood) {
                $db->query('UPDATE `user` SET `flood_time`=?i WHERE `id`=?i',
                               [(time() + $set['flood_mail_time']), $user['id']]);
            }
                #-------------------------------------#
                //вычисляем рейтинг за сообщение
                $rating_add_msg = rating::msgnum($msg);
                //сумируем
                $add_rating = ($rating_add_msg + 0.01);
                //добавляем рейтинг
                rating::add($user['id'], $add_rating, 1, null, 'mail');
                #-------------------------------------#
                $db->query('UPDATE `user` SET `balls`=`balls`+?i WHERE `id`=?i LIMIT ?i',
                           [$set['balls_mail'], $user['id'], 1]);
                //Удаляем кэш
                cache_delete::user($user['id']);
            $_SESSION['message'] = lang('Сообщение успешно отправлено');
            exit(header("Location: ?id=$ank[id]"));
        }
    }
}

aut();
err();

if (isset($_GET['Block_act'])) {
    msg(lang('Вы уверены?'));
    echo "<div class='p_m' style='text-align:center;'><a href='?id=" . $ank['id'] . "&Block=" . $post['id'] . "&Block_act=1'><b>" . lang('Да') . "</b></a> ::  <a href='?id=" . $ank['id'] . "'>" . lang('Нет') . "</a></div>";
}

if ($user['id'] != $ank['id'] and $ank['group_access'] == 0 and $user['group_access'] == 0 and isset($_GET['Block']) and $_GET['Block_act'] == 1) {
    $msg = lang('Новая жалоба в почте на пользователя') . ' [url=/' . $ank['mylink'] . ']' . nick($ank['id'], null, 0) . '[/url]';
    $msgt = $db->query('SELECT `msg` FROM `mail` WHERE `id`=?i LIMIT ?i', [$_GET['Block'], 1])->el();
    $msg .= '
-----------
' . $msgt['msg'] . '
-----------
';

    $msg .= lang('Жалоба подана от') . ' : [url=/' . $user['mylink'] . ']' . nick($user['id'], null, 0) . '[/url] ';

    $db->query('INSERT INTO `jurnal_system` (`time`, `type`, `id_user`, `msg`, `id_kont`)
VALUES (?i,  ?string, ?i, ?, ?i)', [time(), 'spam_mail', $ank['id'], $msg, 0]);
    $_SESSION['message'] = lang('Успешно');
//Удаляем кэш
    cache_delete::user($ank['id']);
    exit(header('Location: ?id=' . $ank['id']));
}

echo "<div class='p_m'>&raquo;
	<a href='/konts.php?" . (isset($kont) ? 'type=' . $kont['type'] : null) . "'>" . lang('Мои контакты') . "</a></div>";

if ($user['group_access'] == 0) {
    $b_count = $db->query('SELECT COUNT(*) FROM `ban` WHERE `id_user`=?i AND `time`>?i', [$ank['id'], time()])->el();
} else {
    $b_count = 0;
}

if ($ank['id'] != 0 and $b_count == 0) {
    panel_form :: head();
    echo "<div class='p_m'>";
    echo "<form method='post' name='message' action='?id=$ank[id]&amp;$passgen'>\n";
    echo "<textarea name='msg'></textarea></div>";
    panel_form :: foot();

    echo "<div class='p_m'>";

    if ($time_flood) {
        echo "<img src='/captcha.php?SESS=$sess' width='100' height='30' /><br />
<input name='chislo' size='5' maxlength='5' value='' type='text' /><br/>
" . lang('Когда вы покажите свою активность код с картинки вводить не надо будет каждый раз') . "
<br/>
";
    }

    echo "<input type='submit' name='send' value='" . lang('Отправить') . "' />\n";
    echo "<input type='submit' name='refresh' value='" . lang('Обновить') . "' />";
    if ($user['set_translit'] == 1) {
        echo "<label><input type='checkbox' name='translit' value='1' /> " . lang('Транслит') . "</label><br />\n";
    }
    echo "</form></div>";
} else {
    msg('Этому контакту нельзя писать');
}

$k_post = $db->query('SELECT COUNT(*) FROM `mail` WHERE `id_user`=?i AND `id_kont` =?i OR `id_user` =?i AND `id_kont` =?i',
                   [$user['id'], $ank['id'], $ank['id'], $user['id']])->el();

if ($k_post == 0) {
    msg(lang('Нет сообщений'));
} else {
    $k_page = k_page($k_post, $set['p_str']);
    $page = page($k_page);
    $start = $set['p_str'] * $page - $set['p_str'];

    $q = $db->query('SELECT m.*, u.nick, COALESCE(b.cnt, 0) AS ban
FROM `mail` m
LEFT JOIN `user` u ON m.id_user=u.id
LEFT JOIN (SELECT id_user, COUNT(*) cnt FROM `ban` WHERE `time`>?i GROUP BY id_user) b ON b.`id_user`=u.id
WHERE m.`id_user`=?i AND `id_kont`=?i OR m.`id_user`=?i AND `id_kont`=?i ORDER BY id DESC LIMIT ?i, ?i',
[time(), $user['id'], $ank['id'], $ank['id'], $user['id'], $start, $set['p_str']]);

    while ($post = $q->row()) {
        //$ank2 = get_user($post['id_user']);
    $ank2['id'] = $post['id_user'];
        if ($ank2['id'] == 0) {
            $ank2 = ['id' => $set['id_system'], 'nick' => $set['nick_system'], 'group_access' => 999];
        }
        echo '<table class="p_m"  style="width:avto;"><tr><td class="avar">';
        avatar($ank2['id'], 80, 80);
        echo '</td><div class="p_m" >';
        if ($user['id'] == $ank2['id']) {
            echo $user['nick']; // Я
        } else {
            echo nick($ank2['id']);
        }

        echo '<td style="" class="">';
        if ($post['read'] == 0) {
            echo "<span class='' style='float:right;color:red;'>" . lang('не прочитано') . "</span></br>";
        }

        if (!isset($_GET['Block']) and $user['id'] != $post['id_user'] and $ank['group_access'] == 0 and $user['group_access'] == 0) {
            echo "<span class='ank_span' style='float:right'>";
            echo "<a href='?id=" . $ank['id'] . "&Block=" . $post['id'] . "&Block_act'> " . lang('Жалоба') . "</a><br />";
            echo "</span>";
        }

        if ($user['id'] != $post['id_user']) {
            $div_mess = 'status_o_mail';
        } else {
            $div_mess = 'status_o';
        }

    //Блокируем сообщение если человек в бане
    if ($post['ban'] == 0 && $set['msg_ban_set'] == 1) {
        // if (count::query('ban', "`id_user` = '" . $ank['id'] . "' AND `time` > " . time()) == 0 && $set['msg_ban_set'] == 1) {
        echo "<div class='status_o_s_mail'> </div>
			<div class='$div_mess'> " . output_text($post['msg']) . " </div>";
    } else {
        echo($user['group_access'] >= 1 ? '
	<br /><span style="color:red">' . $set['msg_ban'] . "</span> : <br/>
	<div class='status_o_s_mail'> </div>
	<div class='$div_mess'> " . output_text($post['msg']) . " </div>
		" : $set['msg_ban']);
    }
        echo "	<span class='' style='float:right;color:green;font-size:9px;'>" . vremja($post['time']) . "</span>";
        echo '</td>';
        echo '</tr></table>';
    }
}
if ($k_page > 1) {
    str("?id=$ank[id]&amp;", $k_page, $page);
} // Вывод страниц

include_once 'sys/inc/tfoot.php';
