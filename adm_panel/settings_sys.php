<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
$temp_set=$set;
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';

user_access('adm_set_sys', null, 'index.php?'.SID);
adm_check();

$set['title']='Настройки системы';
include_once H . 'sys/inc/thead.php';
title();
if (isset($_POST['save'])) {
    // ShaMan
    $temp_set['title']=esc(stripcslashes(htmlspecialchars($_POST['title'])), 1);
    // Тут конец моих дум
    $temp_set['id_system']=intval($_POST['id_system']);
    $temp_set['p_str_set']=intval($_POST['p_str_set']);
    $temp_set['guest_max_post']=intval($_POST['guest_max_post']);
    $temp_set['balls_guest']=intval($_POST['balls_guest']);
    $temp_set['times_head']=intval($_POST['times_head']);
    $temp_set['panel_foot_sys']=intval($_POST['panel_foot_sys']);
    $temp_set['lang'] = htmlspecialchars($_POST['lang']);

    $db->query('ALTER TABLE `user` CHANGE `set_p_str` `set_p_str` INT( 11 ) DEFAULT ?', [$temp_set['p_str']]);

    $temp_set['nick_system'] = htmlspecialchars($_POST['nick_system']);

    if (!preg_match('#\.\.#', $_POST['set_them']) && is_dir(H.'style/themes/'.$_POST['set_them'])) {
        $temp_set['set_them']=$_POST['set_them'];
        $db->query('ALTER TABLE `user` CHANGE `set_them` `set_them` VARCHAR( 32 ) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT ?',
                   [$temp_set['set_them']]);
    }
    if (!preg_match('#\.\.#', $_POST['set_them2']) && is_dir(H.'style/themes/'.$_POST['set_them2'])) {
        $temp_set['set_them2']=$_POST['set_them2'];
        $db->query('ALTER TABLE `user` CHANGE `set_them2` `set_them2` VARCHAR( 32 ) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT ?',
                   [$temp_set['set_them2']]);
    }
    if ($_POST['set_show_icon']==2 || $_POST['set_show_icon']==1 || $_POST['set_show_icon']==0) {
        $temp_set['set_show_icon']=intval($_POST['set_show_icon']);
        $db->query('ALTER TABLE `user` CHANGE `set_show_icon` `set_show_icon` SET( "0", "1", "2" ) DEFAULT ?',
                   [$temp_set['set_show_icon']]);
    }

    if ($_POST['show_err_php']==1 || $_POST['show_err_php']==0) {
        $temp_set['show_err_php'] = intval($_POST['show_err_php']);
    }

    if ($_POST['set_show_forum_nw_coll']==1 or
      $_POST['set_show_forum_nw_coll']==3  or
      $_POST['set_show_forum_nw_coll']==5  or
      $_POST['set_show_forum_nw_coll']==7  or
      $_POST['set_show_forum_nw_coll']==9  or
      $_POST['set_show_forum_nw_coll']==15 or
      $_POST['set_show_forum_nw_coll']==20) {
        $temp_set['set_show_forum_nw_coll']=intval($_POST['set_show_forum_nw_coll']);
    }

    if ($_POST['frames_stop']==1 || $_POST['frames_stop']==0) {
        $temp_set['frames_stop']=intval($_POST['frames_stop']);
    }

    if ($_POST['set_show_forum_nw']==1 || $_POST['set_show_forum_nw']==0) {
        $temp_set['set_show_forum_nw']=intval($_POST['set_show_forum_nw']);
    }

    if (isset($_POST['antidos']) && $_POST['antidos']==1) {
        $temp_set['antidos']=1;
    } else {
        $temp_set['antidos']=0;
    }

    if (isset($_POST['antimat']) && $_POST['antimat']==1) {
        $temp_set['antimat']=1;
    } else {
        $temp_set['antimat']=0;
    }

    $temp_set['meta_keywords']=esc(stripcslashes(htmlspecialchars($_POST['meta_keywords'])), 1);
    $temp_set['meta_description']=esc(stripcslashes(htmlspecialchars($_POST['meta_description'])), 1);

    if (save_settings($temp_set)) {
        admin_log('Настройки', 'Система', 'Изменение системных настроек');
        $_SESSION['message'] = lang('Настройки успешно приняты');
        exit(header('Location: ?'));
    } else {
        $err='Нет прав для изменения файла настроек';
    }
}

err() . aut();

echo "<form method='post' action=''>";
echo "<div class='p_m'>Язык сайта по умолчанию:<br /><select name='lang'>";
// ------------------------------------------------
$lang_list = $db->query('SELECT * FROM `lang_list`');
while ($post = $lang_list->row()) {
    echo "<option value='" . $post['name_g'] . "'>" . $post['name'] . "</option>";
}
// ------------------------------------------------
echo '</select></div>';

echo "<div class='p_m'>Название сайта:<br />\n<input name=\"title\" value=\"$temp_set[title]\" type=\"text\" /></div>";

echo "<div class='p_m'>Дополнительные пункты на страницу:<br />\n<select name=\"p_str_set\">\n";
if ($temp_set['p_str_set']==1) {
    $sel=' selected="selected"';
} else {
    $sel=null;
}
echo "<option value=\"1\"$sel>Включить</option>\n";
if ($temp_set['p_str_set']==0) {
    $sel=' selected="selected"';
} else {
    $sel=null;
}
echo "<option value=\"0\"$sel>Отключить</option>\n";
echo "</select>
<br /> * Дает возможность юзерам ставить больше 10 пунктов в настройках максимум (30)
<br /> * Учитывайте запросы ,о которых описано выше
</div>";

echo "<div class='p_m'>Ник системы :<br /><input type='text' name='nick_system' value='$temp_set[nick_system]'/></div>";
echo "<div class='p_m'>ID системы :<br /><input type='text' name='id_system' value='$temp_set[id_system]'/></div>";

echo "<div class='p_m'>Запрет на фрейм сайта:<br />\n<select name=\"frames_stop\">\n";
if ($temp_set['frames_stop']==1) {
    $sel=' selected="selected"';
} else {
    $sel=null;
}
echo "<option value=\"1\"$sel>Включить</option>\n";
if ($temp_set['frames_stop']==0) {
    $sel=' selected="selected"';
} else {
    $sel=null;
}
echo "<option value=\"0\"$sel>Отключить</option>\n";
echo "</select><br /> * скрипт блокирующий доступ что б ваш сайт не использовался в качестве фрейма</div>";

echo "<div class='p_m'>Иконки:<br />\n<select name=\"set_show_icon\">\n";
if ($temp_set['set_show_icon']==2) {
    $sel=' selected="selected"';
} else {
    $sel=null;
}
echo "<option value=\"2\"$sel>Большие</option>\n";
if ($temp_set['set_show_icon']==1) {
    $sel=' selected="selected"';
} else {
    $sel=null;
}
echo "<option value=\"1\"$sel>Маленькие</option>\n";
if ($temp_set['set_show_icon']==0) {
    $sel=' selected="selected"';
} else {
    $sel=null;
}
echo "<option value=\"0\"$sel>Скрывать</option>\n";
echo "</select></div>";

echo "<div class='p_m'>Темы форума на главной (wap):<br />\n<select name=\"set_show_forum_nw\">\n";
echo "<option value='0'".($temp_set['set_show_forum_nw']==0?" selected='selected'":null).">Скрывать</option>\n";
echo "<option value='1'".($temp_set['set_show_forum_nw']==1?" selected='selected'":null).">Показывать</option>\n";
echo "</select><br /> * показ новых и последних обсуждаемых  тем на форуме</div>";

echo "<div class='p_m'>Число новых тем на главной  (wap):<br />\n<select name=\"set_show_forum_nw_coll\">\n";
echo "<option value='1'".($temp_set['set_show_forum_nw_coll']==1?" selected='selected'":null).">1 тема</option>\n";
echo "<option value='3'".($temp_set['set_show_forum_nw_coll']==3?" selected='selected'":null).">3 темы</option>\n";
echo "<option value='5'".($temp_set['set_show_forum_nw_coll']==5?" selected='selected'":null).">5 тем</option>\n";
echo "<option value='7'".($temp_set['set_show_forum_nw_coll']==7?" selected='selected'":null).">7 тем</option>\n";
echo "<option value='9'".($temp_set['set_show_forum_nw_coll']==9?" selected='selected'":null).">9 тем</option>\n";
echo "<option value='15'".($temp_set['set_show_forum_nw_coll']==15?" selected='selected'":null).">15 тем</option>\n";
echo "<option value='20'".($temp_set['set_show_forum_nw_coll']==20?" selected='selected'":null).">20 тем</option>\n";
echo "</select><br />* на соц сетях не рекомендуется более 3-5 </div>";

echo "<div class='p_m'>Дина максимального сообщения в гостевой(max 20000) :<br />
<input type='text' name='guest_max_post' value='$temp_set[guest_max_post]'  /></div>";

echo "<div class='p_m'>Давать баллов за сообщение в гостевой:<br /><select name=\"balls_guest\">";
echo "<option value='0'".($temp_set['balls_guest']==0?" selected='selected'":null).">Не давать баллы</option>\n";
echo "<option value='1'".($temp_set['balls_guest']==1?" selected='selected'":null).">1 балл</option>\n";
echo "<option value='3'".($temp_set['balls_guest']==3?" selected='selected'":null).">3 балла</option>\n";
echo "<option value='5'".($temp_set['balls_guest']==5?" selected='selected'":null).">5 баллов</option>\n";
echo "<option value='7'".($temp_set['balls_guest']==7?" selected='selected'":null).">7 баллов</option>\n";
echo "<option value='9'".($temp_set['balls_guest']==9?" selected='selected'":null).">9 баллов</option>\n";
echo "<option value='15'".($temp_set['balls_guest']==15?" selected='selected'":null).">15 баллов</option>\n";
echo "<option value='20'".($temp_set['balls_guest']==20?" selected='selected'":null).">20 баллов</option>\n";
echo "</select></div>";

echo "<div class='p_m'>Время в шапки сайта:<br /><select name=\"times_head\">";
echo "<option value='0'".($temp_set['times_head']==0?" selected='selected'":null).">Не показывать</option>\n";
echo "<option value='1'".($temp_set['times_head']==1?" selected='selected'":null).">Показывать</option>\n";
echo "</select></div>";

echo "<div class='p_m'>Нижняя панель статистики:<br /><select name=\"panel_foot_sys\">";
echo "<option value='0'".($temp_set['panel_foot_sys']==0?" selected='selected'":null).">Не показывать</option>";
echo "<option value='1'".($temp_set['panel_foot_sys']==1?" selected='selected'":null).">Показывать </option>";

echo "</select>
<br/>* Генирация ,трафик .число запросов на старницу
<br/>* Администрации показывать вся панель юзерам только часть : трафик и вес страницы .
</div>";


echo "<div class='p_m'>Тема (WAP):<br />\n<select name='set_them'>\n";
$opendirthem=opendir(H . 'style/themes');
while ($themes=readdir($opendirthem)) {
    // пропускаем корневые папки и файлы
    if ($themes=='.' || $themes=='..' || !is_dir(H."style/themes/$themes")) {
        continue;
    }
    // пропускаем темы для web браузеров
    if (file_exists(H."style/themes/$themes/.only_for_web")) {
        continue;
    }
    echo "<option value='$themes'".($temp_set['set_them']==$themes?" selected='selected'":null).">".trim(file_get_contents(H.'style/themes/'.$themes.'/them.name'))."</option>\n";
}
closedir($opendirthem);
echo "</select><br />\n";

echo "Тема (WEB):<br />\n<select name='set_them2'>\n";
$opendirthem=opendir(H.'style/themes');
while ($themes=readdir($opendirthem)) {
    // пропускаем корневые папки и файлы
    if ($themes=='.' || $themes=='..' || !is_dir(H."style/themes/$themes")) {
        continue;
    }
    // пропускаем темы для wap браузеров
    if (file_exists(H."style/themes/$themes/.only_for_wap")) {
        continue;
    }
    echo "<option value='$themes'".($temp_set['set_them2']==$themes?" selected='selected'":null).">".trim(file_get_contents(H.'style/themes/'.$themes.'/them.name'))."</option>\n";
}
closedir($opendirthem);
echo "</select>
<br /> * Эти темы будут установлены по умолчанию при регистрации ,те у кого уже выбраны темы (авторизованых) не каснется
</div>";

echo "<div class='p_m'>Ключевые слова (META):<br />\n";
echo "<textarea name='meta_keywords'>$temp_set[meta_keywords]</textarea></div>";
echo "<div class='p_m'>Описание (META):<br />\n";
echo "<textarea name='meta_description'>$temp_set[meta_description]</textarea></div>";

echo "<div class='p_m'><label><input type='checkbox'".($temp_set['antidos']?" checked='checked'":null)." name='antidos' value='1' /> Анти-Dos*</label><br />* Анти-Dos - защита от частых запросов с одного IP-адреса (не путать с анти ddos\"ом)</div>";
echo "<div class='p_m'><label><input type='checkbox'".($temp_set['antimat']?" checked='checked'":null)." name='antimat' value='1' /> Анти-Мат</label>
<br />* дает 3 предупреждения ,а затем банит
</div>";

echo "<div class='p_m'>Ошибки интерпретатора:<br />\n<select name=\"show_err_php\">\n";
echo "<option value='0'".($temp_set['show_err_php']==0?" selected='selected'":null).">Скрывать</option>";
echo "<option value='1'".($temp_set['show_err_php']==1?" selected='selected'":null).">Показывать администрации</option>";
echo "<option value='2'".($temp_set['show_err_php']==2?" selected='selected'":null).">Показывать Всем</option>";
echo "</select></div>";

echo "<div class='p_m'><input value=\"Сохранить\" name='save' type=\"submit\" />";
echo "</form></div>";

echo "<div class='foot'>\n";
echo "&laquo;<a href='/adm_panel/'>В админку</a><br />\n";
echo "</div>\n";

include_once H . 'sys/inc/tfoot.php';
