<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';

user_access('modules_edit', null, 'index.php?'.SID);
adm_check();

$set['title'] = lang('Управление админ разделами');
include_once H.'sys/inc/thead.php';
title();
  
lang::start('default');

if (isset($_POST['add']) && isset($_POST['name']) && $_POST['name']!=null && isset($_POST['url']) && $_POST['url']!=null && isset($_POST['counter'])) {
    $name= trim($_POST['name']);
    $url= trim($_POST['url']);
    $counter = trim($_POST['counter']);

    $pos = $db->query('SELECT MAX(`pos`) AS pos FROM `admin_menu`');//+1;

    $accesses = trim($_POST['accesses']);

    $icon=preg_replace('#[^a-z0-9 _\-\.]#i', null, $_POST['icon']);
    $db->query('INSERT INTO `admin_menu` (`name`, `url`, `counter`, `pos`, `img`,`time`,`accesses`) VALUES ( ?, ?, ?, ?i, ?, ?i, ?)',
               [$name, $url, $counter, $pos, $icon, time(), $accesses]);

    $_SESSION['message'] = lang('Ссылка успешно добавлена');
    exit(header('Location: ?'));
}


if (isset($_POST['change']) && isset($_GET['id']) && isset($_POST['name']) && $_POST['name']!=null) {
    $id=intval($_GET['id']);
    $name= trim($_POST['name']);
    $url= trim($_POST['url']);
    $counter = trim($_POST['counter']);
    $accesses = trim($_POST['accesses']);

    if (isset($_POST['icon'])) {
        $icon = preg_replace('#[^a-z0-9 _\-\.]#i', null, $_POST['icon']);
    } else {
        $icon = 'default.png';
    }
    $data = [$name, $url, $counter, $icon, $accesses, $id, 1];
    $db->query('UPDATE `admin_menu` SET `name`=?, `url`=?, `counter`=?, `img`=?, `accesses`=? WHERE `id`=?i LIMIT ?i', $data);
    $_SESSION['message'] = lang('Пункт меню успешно изменен');
    exit(header('Location: ?'));
}

if (isset($_GET['id']) && isset($_GET['act']) &&
    $menu = $db->query('SELECT `pos` FROM `admin_menu` WHERE `id`=?i LIMIT ?i', [$_GET['id'], 1])->el()) {
    //$menu = $db->query('SELECT `pos` FROM `admin_menu` WHERE `id`=?i LIMIT ?i', [$_GET['id'], 1]);
    if ($_GET['act']=='up') { // ???
        $db->query("UPDATE `admin_menu` SET `pos` = '".($menu['pos'])."' WHERE `pos` = '".($menu['pos']-1)."' LIMIT 1");
        $db->query("UPDATE `admin_menu` SET `pos` = '".($menu['pos']-1)."' WHERE `id` = '".intval($_GET['id'])."' LIMIT 1");

        $_SESSION['message'] = lang('Пункт меню сдвинут на позицию вверх');
        exit(header('Location: ?'));
    }
    if ($_GET['act']=='down') {
        $db->query("UPDATE `admin_menu` SET `pos` = '".($menu['pos'])."' WHERE `pos` = '".($menu['pos']+1)."' LIMIT 1");
        $db->query("UPDATE `admin_menu` SET `pos` = '".($menu['pos']+1)."' WHERE `id` = '".intval($_GET['id'])."' LIMIT 1");

        $_SESSION['message'] = lang('Пункт меню сдвинут на позицию вниз');
        exit(header('Location: ?'));
    }
    if ($_GET['act']=='del') {
        $db->query('DELETE FROM `admin_menu` WHERE `id`=?i LIMIT ?i', [$_GET['id'], 1]);

        $_SESSION['message'] = lang('Пункт меню удален');
        exit(header('Location: ?'));
    }
}
 
err() . aut();

echo "<table class='post'>";

$res = $db->query('SELECT * FROM `admin_menu` ORDER BY `pos` ASC');
while ($post = $res->row()) {
    echo "   <tr>\n";
    echo "  <td class='adm_panel'>";

    //Выводим иконку
    if ($post['type'] == 'link') {
        echo image::is("/style/icons_admpanel/". $post['img']);
    } else {
        echo "<img src='/style/icons_admpanel/razd.png'> ";
    }
    
    echo  lang($post['name']) . ($post['type'] == 'razd' ? ' <ins>(Раздел)</ins>' : '') . " <span style='float:right' class='adm_panel_span'>  <a href='?id=$post[id]&amp;act=edit'>".lang('Редактировать')." </a></span>";
    echo "  </td>\n";
    echo "   </tr>\n";
    echo "   <tr>\n";
    echo "  <td class='p_m'>\n";
    echo($post['type'] == 'link' ?   lang('Ссылка') . ': '.$post['url'].' <br/>': null);
 
    if (isset($_GET['id']) && $_GET['id']==$post['id'] && isset($_GET['act']) && $_GET['act']=='edit') {
        echo "<form action=\"?id=$post[id]\" method=\"post\">";
        echo lang('Тип') .': '.($post['type'] == 'link' ? lang('Ссылка') : lang('Разделитель'))."<br />";
        echo lang('Название')." :<br />";
        echo "<input type='text' name='name' value='".lang($post['name'])."' /><br />";
        echo lang('Права доступа').":<br />";
        $acc = $db->query('SELECT `type`, `name` FROM `all_accesses` ORDER BY name')->assoc();
        echo '<p><select name="accesses">';
        for ($i = 0; $i <= count($acc); $i++) {
            echo '<option value="' . $acc[$i]['type'] . '"' . ($post['accesses'] == $acc[$i]['type'] ? ' selected="selected"' : '') . '>' . $acc[$i]['name'] . '</option>';
        }
        echo '</select></p>';
        //echo "<input type=\"text\" name=\"accesses\" value=\"".($post['accesses'])."\"/><br />";

    if ($post['type']=='link') {
        echo lang('Ссылка').":<br />";
        echo "<input type='text' name='url' value='$post[url]' /><br />";
    } else {
        echo "<input type='hidden' name='url' value='' />";
    }

        echo lang('Счетчик').":<br />";
        echo "<input type='text' name='counter' value='$post[counter]' /><br />\n";
        if ($post['type']=='link') {
            echo lang('Иконка').": <br/><input type='text' name='icon' value='$post[img]' /><br />";
        }

        echo "<input class=\"submit\" name=\"change\" type=\"submit\" value='".lang('Изменить')."' /><br />\n";
        echo "</form>";
        echo "<a href='?'>".lang('Отмена')."</a><br />";
    } else {
        echo lang('Счетчик').': '.($post['counter'] == null ? lang('отсутствует') : $post['counter'])."<br /><br />";
        echo "<span class='adm_panel_span'><a href='?id=$post[id]&amp;act=up&amp;$passgen'>".lang('Выше')."</a> | ";
        echo "<a href='?id=$post[id]&amp;act=down&amp;$passgen'>".lang('Ниже')."</a> | ";
        echo "<a href='?id=$post[id]&amp;act=del&amp;$passgen'> ".lang('Удалить')."</a></span><br /><br />";
    }

    echo "  </td>";
    echo "   </tr>";
}

echo "</table>";

if (isset($_GET['add'])) {
    echo "  <div class='p_m'>";
    echo "<form action='?add=$passgen' method=\"post\">";
    echo "Тип:<br />";
    echo "<select name='type'>";
    echo "<option value='link'> ".lang('Ссылка')."</option>";
    echo "<option value='razd'> ".lang('Раздел')."</option>";
    echo "</select><br />";
    echo lang('Название').":<br />";
    echo "<input type=\"text\" name=\"name\" value=\"\"/><br />";
    echo lang('Права доступа').":<br />";
    echo "<input type=\"text\" name=\"accesses\" value=\"\"/><br />";
    echo lang('Ссылка').":<br />\n";
    echo "<input type=\"text\" name=\"url\" value=\"\"/><br />";
    echo lang('Счетчик')." (".APANEL."/count/*):<br />\n";
    echo "<input type=\"text\" name=\"counter\" value=\"\"/><br />";
    echo lang('Иконка')." :<br />\n";
    echo "<input type='text' name='icon' value='default.png' /><br />";
    echo "<input class='submit' name='add' type='submit' value='".lang('Добавить')."' /><br />";
    echo "<a href='?$passgen'>".lang('Отмена')."</a><br />\n";
    echo "</form></div>";
} else {
    echo "<div class='foot'><a href='?add=$passgen'>".lang('Добавить пункт')."</a></div>";
}

echo "<div class='foot'>";
echo "&laquo;<a href='".APANEL."/'>".lang('В админку')."</a><br />";
echo "</div>";


include_once '../sys/inc/tfoot.php';
