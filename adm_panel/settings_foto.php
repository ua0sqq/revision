<?php
include_once '../sys/inc/start.php';
include_once '../sys/inc/compress.php';
include_once '../sys/inc/sess.php';
include_once '../sys/inc/home.php';
include_once '../sys/inc/settings.php';
$temp_set=$set;
include_once '../sys/inc/db_connect.php';
include_once '../sys/inc/ipua.php';
include_once '../sys/inc/fnc.php';
include_once '../sys/inc/adm_check.php';
include_once '../sys/inc/user.php';
user_access('adm_set_foto', null, 'index.php?'.SID);
adm_check();
$set['title']='Настройки ИЗображений';
include_once '../sys/inc/thead.php';
title();
if (isset($_POST['save'])) {
    $temp_set['max_upload_foto_x']=intval($_POST['max_upload_foto_x']);
    $temp_set['max_upload_foto_y']=intval($_POST['max_upload_foto_y']);
    $temp_set['avatar_s']=intval($_POST['avatar_s']);
    $temp_set['avatar_w']=intval($_POST['avatar_w']);

    $temp_set['avatar_s_ank']=intval($_POST['avatar_s_ank']);
    $temp_set['avatar_w_ank']=intval($_POST['avatar_w_ank']);

    $temp_set['avatar_s_other']=intval($_POST['avatar_s_other']);
    $temp_set['avatar_w_other']=intval($_POST['avatar_w_other']);

    $temp_set['obmen_foto_128_H']=intval($_POST['obmen_foto_128_H']);
    $temp_set['obmen_foto_128_w']=intval($_POST['obmen_foto_128_w']);



#	фыр фыр фыр...
$temp_set['copy_path'] = esc(stripcslashes(htmlspecialchars($_POST['copy_path'])));



    if (save_settings($temp_set)) {
        admin_log('Настройки', 'ИЗображения', 'Изменение настроек фотогалереи');
        msg('Настройки успешно приняты');
    } else {
        $err='Нет прав для изменения файла настроек';
    }
}
err();
aut();



echo "<form method=\"post\" action=\"?\">\n";
echo " <div class='p_m'> Фотоальбомы <br />
 Ширина фото  (max):<br />\n<input type='text' name='max_upload_foto_x' value='$temp_set[max_upload_foto_x]' /><br />
Высота фото (max):<br />\n<input type='text' name='max_upload_foto_y' value='$temp_set[max_upload_foto_y]' /></div>


<div class='p_m'> Максимальный размер загруженных аватаров  <br />
Ширина (max):<br /><input type='text' name='avatar_s' value='$temp_set[avatar_s]'/><br />
Высота (max):<br /><input type='text' name='avatar_w' value='$temp_set[avatar_w]'/><br />
* рекомендуется не менять здесь что либо каждый раз например сделали 500 х 500 если вы сделаете больше или меньше отображение изменится 
(у тех кто загрузит новые аватары) 
</div>

<div class='p_m'> Высота выводимых аватаров в анкете  <br />

Ширина (max):<br /><input type='text' name='avatar_w_ank' value='$temp_set[avatar_w_ank]'/><br />
Высота (max):<br /><input type='text' name='avatar_s_ank' value='$temp_set[avatar_s_ank]'/><br /> 
* рекомендуется игнорировать высоту \"0\" (для наилучшего вывода фото )


</div>

<div class='p_m'> Высота выводимых аватаров в остальных частях сайта  <br />
* рекомендуется 40 х 40 (для наилучшего вывода фото )
<br /> 


Ширина (max):<br /><input type='text' name='avatar_s_other' value='$temp_set[avatar_s_other]'/><br />
Высота (max):<br /><input type='text' name='avatar_w_other' value='$temp_set[avatar_w_other]'/></div>


<div class='p_m'> Максимальная высота загружаемых фото в обменнике<br />


Ширина (max):<br /><input type='text' name='obmen_foto_128_w' value='$temp_set[obmen_foto_128_w]'/><br />
Высота (max):<br /><input type='text' name='obmen_foto_128_H' value='$temp_set[obmen_foto_128_H]'/>
<br /> * рекомендуется пропорциональное указание высот и сторон типа 200х200 и т.п
</div>






<div class='p_m'>
Файл копирайта (на картинки):<br />\n<input type='text' name='copy_path' value='$temp_set[copy_path]' /></div>




";




echo "<div class='p_m'><input value=\"Сохранить\" name='save' type=\"submit\" />";
echo "</form>


* будьте внимательны в параметрах  и настройки их
<br /> 


</div>";


echo "<div class='foot'>\n";
echo "&laquo;<a href='/adm_panel/'>В админку</a><br />\n";
echo "</div>\n";

include_once '../sys/inc/tfoot.php';
