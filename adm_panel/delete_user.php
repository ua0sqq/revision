<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/home.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';
user_access('user_delete', null, '/');
adm_check();

if (isset($_GET['id'])) {
    $ank['id']=intval($_GET['id']);
}
if (count::query('user', ' `id` = "' . $ank['id'] . '"') == 0) {
    exit(header("Location: /"));
}
$ank=get_user($ank['id']);
if ($user['level'] <= $ank['level']) {
    exit(header("Location: /"));
}


$set['title']='Удаление пользователя "'.$ank['nick'].'"';
include_once H.'sys/inc/thead.php';
title().aut();

echo "<div class='p_m'>";

if (isset($_POST['delete'])) {
    if (function_exists('set_time_limit')) {
        @set_time_limit(60);
    }

    //Загрузка дополнительных плагинов
    $Search = glob(H . 'sys/delete_user_inc/files/*.php');
    if ($Search != null) {
        foreach ($Search as $load_plugins) {
            include_once $load_plugins;
        }
    }

    $tab = $db->query('SHOW TABLE STATUS') ;
    while ($tables = $tab->row()) {
        if ($tables['Engine'] == 'MyISAM' && $tables['Data_free'] > '0') {
            $db->query('OPTIMIZE TABLE `' . $tables['Name'] . '`');
        }
    }


    admin_log('Пользователи', 'Удаление', 'Удаление пользователя ' . $ank['nick'] . ' (id#' . $ank['id'] . ')');
    msg('Все данные о пользователе ' . $ank['nick'] . ' удалены');

    $_SESSION['message'] = lang('Удаление завершено');
    exit(header('Location: ?'));
}


//Загрузка дополнительных плагинов
$Search = glob(H.'sys/delete_user_inc/opis/*.php');
if ($Search != null) {
    foreach ($Search as $load_plugins) {
        include_once $load_plugins;
    }
}


echo "<form method=\"post\" action=\"\">\n";
echo "<input value=\"Удалить\" type=\"submit\" name='delete' />\n";
echo "</form>\n";
echo "Удаленные данные невозможно будет восстановить<br />\n";
echo "</div>";

echo "<div class='foot'>";
echo "&laquo;<a href='/info.php?id=$ank[id]'>В анкету</a><br />\n";
echo "&laquo;<a href='/users.php'>Пользователи</a><br />\n";

echo "</div>";
include_once H . 'sys/inc/tfoot.php';
