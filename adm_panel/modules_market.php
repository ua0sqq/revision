<?php
include_once '../sys/inc/start.php';
include_once '../sys/inc/compress.php';
include_once '../sys/inc/sess.php';
include_once '../sys/inc/home.php';
include_once '../sys/inc/settings.php';
include_once '../sys/inc/db_connect.php';
include_once '../sys/inc/ipua.php';
include_once '../sys/inc/fnc.php';
include_once '../sys/inc/adm_check.php';
include_once '../sys/inc/user.php';
user_access('api_config', null, 'index.php?'.SID);
adm_check();

$set['title'] = lang('Поиск новых модулей');
include_once '../sys/inc/thead.php';
title();
aut();
msg(lang('В разработке'));
include_once '../sys/inc/tfoot.php';
