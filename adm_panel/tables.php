<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';

user_access('adm_mysql', null, 'index.php?' . SID);
adm_check();
protect_panel();

$set['title']='Залитие таблиц';
include_once H . 'sys/inc/thead.php';
title();

if (isset($_FILES['file'])) {
    $file = esc(stripcslashes(htmlspecialchars($_FILES['file']['name'])));
    $ras = strtolower(preg_replace('#^.*\.#i', null, $file));
    if ($ras != 'sql') {
        $err = lang('Не верный формат файла');
    }
    if (!isset($err)) {
        move_uploaded_file($_FILES['file']['tmp_name'], H . 'sys/sql_update/' . $_FILES['file']['name']);
// выполнение одноразовых запросов
$opdirtables = opendir(H . 'sys/sql_update/');
        while ($rd = readdir($opdirtables)) {
            if (preg_match('#^\.#', $rd)) {
                continue;
            }
            if (isset($set['update'][$rd])) {
                continue;
            }

            if (preg_match('#\.sql$#i', $rd)) {
                include_once H.'sys/inc/sql_parser.php';
                $sql = SQLParser::getQueriesFromFile(H.'sys/sql_update/'.$rd);

                for ($i = 0; $i < count($sql);$i++) {
                    $db->query($sql[$i], array());
                }

                $set['update'][$rd]=true;
                $save_settings=true;
            }
        }
        closedir($opdirtables);

        if (is_file(H . 'sys/sql_update/' . $_FILES['file']['name'])) {
            unlink(H . 'sys/sql_update/' . $_FILES['file']['name']);
        }

        $_SESSION['message'] = lang('Таблицы успешно залиты');
        exit(header('Location: ?'));
    }
}


if (isset($_GET['update_file'])) {
    $file = esc(stripcslashes(htmlspecialchars(base64_decode($_GET['update_file']))));
    $ras = strtolower(preg_replace('#^.*\.#i', null, $file));
    if ($ras != 'sql') {
        $err = lang('Не верный формат файла');
    }

    if (!isset($err)) {
        include_once H.'sys/inc/sql_parser.php';
        $sql = SQLParser::getQueriesFromFile(H.'sys/sql_update/'.$file);

        for ($i = 0; $i < count($sql);$i++) {
            $db->query($sql[$i]);
        }
//удаляем файл
@unlink(H."sys/sql_update/". $file);

        $count = lang('Выполнено').' : ('.count($sql).') '.lang('Запросов');
        $_SESSION['message'] = lang('Таблица успешно залита') . $count;
        exit(header('Location: ?'));
    }
}





err();
aut();


    echo "
	<div class='p_m'>
	<form method='post' enctype='multipart/form-data' action=''>
	".lang('Выгрузить').":<br />
	<input name='file' type='file' accept='sql' /><br /><input value='".lang('Залить')."' type='submit' />
	</form>
	<br /> ".lang('Внимание , После загрузки файла и выполнения запроса, он будет автоматически удален')."</div>
	";

  echo '<div class="p_m">';
  $scandirs = scandir(H.'sys/sql_update/');
  foreach ($scandirs as $file) {
      # Нам не нужно добавлять в новый zip-файл ранее созданные архивы
    if (
    $file == '.' or
    $file == '..' or
    $file == '.htaccess' or
    $file == 'clear_conf.txt') {
        continue;
    }
      $files[] = $file;
      echo '<div class="menu_razd">' . $file .' [<a href="?update_file='.base64_encode($file).'">'.lang('Залить в базу').'</a>]</div>';
  }
  echo "</div>";





echo "<div class='foot'>";
echo "&laquo;<a href='mysql.php'>".lang('MySQL запросы')."</a><br />";

echo "&laquo;<a href='".APANEL."/'>".lang('В админку')."</a><br />";
echo "</div>";
include_once H . 'sys/inc/tfoot.php';
