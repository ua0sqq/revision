<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once  H . 'sys/inc/start.php';
include_once  H . 'sys/inc/sess.php';
include_once  H . 'sys/inc/settings.php';
include_once  H . 'sys/inc/db_connect.php';
include_once  H . 'sys/inc/ipua.php';
include_once  H . 'sys/inc/fnc.php';
include_once  H . 'sys/inc/user.php';
include_once  H . 'sys/inc/adm_check.php';

user_access('adm_panel_show', null, '/');

if (user_access('no_captcha')) {
    $_SESSION['adm_auth'] = time() + 9999;
}

if (isset($_SESSION['adm_auth']) && $_SESSION['adm_auth'] > time() || isset($_SESSION['captcha']) && isset($_POST['chislo']) && $_SESSION['captcha'] == $_POST['chislo']) {
    $_SESSION['adm_auth'] = time() + 600;

    if (isset($_GET['go']) && $_GET['go'] != null) {
        exit(header('Location: '.base64_decode($_GET['go'])));
    }

    $set['title'] = lang('Админка');
    include_once H.'sys/inc/thead.php';
    title() . aut();

    include_once H.'sys/inc/api_config.php';

    echo "<div class='adm_panel_fon'>";

    if (user_access('api_config')) {
        ?>
        <div class='adm_panel'>
            <img src="/style/icons_admpanel/1.png" alt="" />
            <div style="text-align: center;"><b><span style='color:#4388C3'>Fiera</span> <span style='color:#A1AD9B'><?php echo  $set['fiera_vers'].'</span> '.$set['fiera_status'] .' ['. vremja($set['install_system'])?>]</b><hr />
                <a href='support.php'><?php echo  lang('Служба поддержки') ?></a> ::
                <a href='<?php echo  FIERA_URL.'/?update_fiera&amp;vers='.$set['fiera_vers'].'&amp;site='.$_SERVER['HTTP_HOST']?>'><?php echo  lang('Обновления') ?></a> ::
				<a href='fiera_news.php'><?php echo  lang('Новости fiera') ?></a>
            </div>
        </div>
        <?php

    }
    include 'fiera_news_inc.php';

    $admin_menu = $db->query('SELECT * FROM `admin_menu` ORDER BY `pos` ASC');
    if ($admin_menu == null) {
        msg(lang('Разделы еще не созданы'));
    } else {
        while ($post = $admin_menu->row()) {
            if ($post['accesses'] == user_access($post['accesses'])) {
                // Задаём позицию, если её не было ранее.
                if ($post['pos'] == 0) {
                    $pos = $db->query('SELECT MAX(`pos`) FROM `admin_menu`')->el();
                    $db->query('UPDATE `admin_menu` SET  `pos`=?i WHERE `id`=?i', [$pos+ 1, $post['id']]);
                }

                // Если модуль новый, то задаём ему время и считаем ссылку новой.
                if ($post['time'] == 0) {
                    $db->query('UPDATE `admin_menu` SET  `time`=?i WHERE `id`=?i', [time(), $post['id']]);
                }

                if ($post['type']=='link') {
                    // Если ссылка.
                    echo "<a class='adm_panel' style='display: block' href='$post[url]'>";
                } else {
                    // Если раздел.
                    echo "<div class='adm_panel_razd'> <img src='/style/icons_admpanel/razd.png' alt=\"*\"/> ";
                }

                // Выводим иконку.
                if ($post['type'] == 'link') {
                    $img = image::is("/style/icons_admpanel/". $post['img']);
                    echo $img.' ';
                }

                // Выводим название.
                echo lang($post['name']);

                // Если новая ссылка.
                if ($post['time'] + 86400 > time() and $post['type'] == 'link') {
                    echo ' <span class="adm_panel_span">NEW</span>';
                }

                // Счётчик.
                if ($post['counter'] != null and is_file('count/'. $post['counter']) and $post['type'] == 'link') {
                    echo '<span style="float:right" class="adm_panel_span"> ';
                    include_once('count/'. $post['counter']);
                    echo ' </span>';
                }
                // Закрывающие дивы.
                if ($post['type'] == 'link') {
                    echo '</a>';
                } else {
                    echo "</div>";
                }
            }
        }
    }

    if (user_access('modules_edit')) {
        echo "<div class='adm_panel'><a href='admin_menu_edit.php' style=\"color:#777;display:block;\"><img src='/style/icons_admpanel/edit.png' alt=\"*\"/>".lang('Редактировать админ меню')."</a></div>";
    }
    echo "</div>";
} else {
    $set['title'] = lang('Защита от автоматических изменений');
    include_once H.'sys/inc/thead.php';
    title() . aut();

    echo "
    <form method='post' action='?gen=$passgen&amp;". (isset($_GET['go']) ?"go={$_GET['go']}":null)."'>
        <img src='/captcha.php?$passgen&amp;SESS=$sess' width='100' height='30' alt='' /><br />
        ".lang('Введите число с картинки').":<br />
        <input name='chislo' size='5' maxlength='5' value='' type='text' /><br/>
        <input type='submit' value='".lang('Далее')."' />
    </form>
    ";
}

include_once H.'sys/inc/tfoot.php';

?>