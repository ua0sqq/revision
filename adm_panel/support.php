<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';

user_access('license_support', null, '/');
adm_check();

$set['title'] = lang('Служба поддержки');
include_once H . 'sys/inc/thead.php';
title() . aut();
echo '<div class="err">Не проверено</div>'; // ???
include_once H . 'sys/inc/api_config.php';

if ($set['license_mod'] == 1) {
    if (!is_file(H.'sys/ini/license_key.ini')) {
        msg('Отсутствует файл с ключем');
        echo "<div class='foot'>";
        echo "&laquo; <a href='".APANEL."/'>".lang('В админку')."</a><br />";
        echo "</div>";
        include_once H.'sys/inc/tfoot.php';
        exit;
    }

    $license_key = parse_ini_file(H . 'sys/ini/license_key.ini');

    if ($license_key['key'] == null) {
        msg('Лицензия еще не активирована');
        echo "<div class='foot'>";
        echo "&laquo; <a href='".APANEL."/'>".lang('В админку')."</a><br />";
        echo "&laquo; <a href='?activator'>".lang('Активация лицензии')."</a><br />";
        echo "</div>";
        include_once H.'sys/inc/tfoot.php';
        exit;
    }

    if (isset($_GET['delete'])) {
        $iddata = [$_GET['delete'], 1];
        $post = $db->query('SELECT * FROM `license_support` WHERE `id`=?i LIMIT ?i', $iddata)->row();
        if ($post['id'] > 0) {
            $db->query('DELETE FROM `license_support` WHERE `id`=?i LIMIT ?i', $iddata);
            $_SESSION['message'] = lang('Вопрос удален');
            exit(header('Location: ?'));
        } else {
            $_SESSION['message'] = lang('Вопрос не найден');
            exit(header('Location: ?'));
        }
    }

    if (isset($_GET['edit'])) {
        $_SESSION['message'] = lang('В разработке');
        exit(header('Location: ?'));
    }

    if (isset($_GET['tiket_add'])) {
        //$post = $db->query('SELECT * FROM `license_support` WHERE `id`=?i LIMIT ?i', [$_GET['tiket_add'], 1])->row();
         //$count_tiket = $db->query('SELECT COUNT(*) FROM `license_support` WHERE `id`=?i', [$post['id']])->el();

         if (!$post = $db->query('SELECT * FROM `license_support` WHERE `id`=?i LIMIT ?i', [$_GET['tiket_add'], 1])->row()) {
             $_SESSION['message'] = lang('Ошибка ,такого вопроса не существует или он удален');
             exit(header('Location: ?'));
         }

        $post['key'] = base64_encode($post['key']);
        $post['name'] = base64_encode($post['name']);
        $post['msg'] = base64_encode($post['msg']);
        $post['type'] = base64_encode($post['type']);

        $license_key = parse_ini_file(H.'sys/ini/license_key.ini');
        $key_license = $license_key['key'];
        $key_license = base64_encode($key_license);

        $arr = @json_decode(file_get_contents(API_SUPPORT_URL_I."?status={$post['status']}&key={$post['key']}&name={$post['name']}&msg={$post['msg']}&type={$post['type']}&key_license={$key_license}&site={$_SERVER['HTTP_HOST']}"), true);
        if ($arr != null) {
            foreach ($arr as $row) {
                if ($row['error'] != null) {
                    msg(output_text(lang($row['error'])));
                } else {
                    $db->query('UPDATE `license_support` SET  `act`= "1" WHERE `id` =?i', [$post['id']]);
                    $_SESSION['message'] = lang('Успешно');
                    exit(header('Location: ?'));
                }
            }
        }
    }


    //if (isset($_GET['license_support']))
    //{
    //	$types = ' WHERE  `type`="' . trim($_GET['license_support']) . '"';
    //}
    //else
    //{
    //	$types = '';
    //}

    if (isset($_GET['new_tiket'])) {
        if (isset($_POST['new_tiket'])) {
            $name = trim($_POST['name']);
            $opis = trim($_POST['opis']);
            $name = trim($_POST['name']);
            $type = trim($_POST['type']);
            $key  = passgen(32);
            $status = trim($_POST['status']);

            if (strlen2($name) > 999) {
                $err[] = lang('Название слишком длинное');
            }
            if (strlen2($name) < 3 or strlen2($name) == null) {
                $err[] = lang('Короткое Название');
            }
            if (strlen2($opis) > 999) {
                $err[] = lang('Описание слишком длинное');
            }
            if (strlen2($opis) < 3 or strlen2($opis) == null) {
                $err[] = lang('Короткое Описание');
            }

            $if = count::query('license_support', " `name` = '". $name ."'");

            if ($if >= 1) {
                $err[] = lang('Вы такой вопрос уже задавали');
            }

            if (!isset($err)) {
                query("INSERT INTO  `license_support` (`time` , `type` , `name` , `msg` , `key`,`status`)VALUES ( '".time()."',  '$type',    '$name',  '$opis',  '$key', '$status')");
                $_SESSION['message'] = lang('Вопрос Создан');
                exit(header("Location: ?"));
            }

            err();
        }

        echo "<div class='p_m'><form method='post' action=''>";
        echo lang('Тема вопроса').":<br />	<input type='text' name='name' value='' /><br />";
        echo lang('Вопрос').":<br />	<textarea name='opis'></textarea><br />";
        echo "<select name='type'>";
        echo "<option value='common'>".lang('Категория вопроса')." </option>";
        echo "<option value='common'>".lang('Общие вопросы')."</option>";
        echo "<option value='bags'>".lang('Баг или ошибка')."</option>";
        echo "<option value='plugins_new'>".lang('Заказ модуля')."</option>";
        echo "<option value='new_id'>".lang('Предложение по развитию')."</option>";
        echo "<option value='admin'>".lang('Разработчикам')."</option>";
        echo "</select><br />";
        echo "<select name='status'>";
        echo "<option value='0'>".lang('Важность')." </option>";
        echo "<option value='1'>".lang('Низкая')."</option>";
        echo "<option value='2'>".lang('Средняя')."</option>";
        echo "<option value='3'>".lang('Высокая')."</option>";
        echo "</select><br />";
        echo lang('Отправляя запрос вы соглашаетесь с')." <a href='".FIERA_URL."/license/rules'><b>".lang('правилами')."</b></a>
		".lang('И рекомендациями тех. поддержки')."<br />";

        echo "<br /><input value='".lang('Сохранить')."' name='new_tiket' type='submit' />";
        echo "<a href='?' class='adm_panel_span'>".lang('Отмена')."</a>	</form></div>";

        include_once H.'sys/inc/tfoot.php';
        exit;
    }

    if (isset($_GET['license_support'])) {
        $types = ' WHERE  `type`=?';
        $dtCnt = [$_GET['license_support']];
    } else {
        $types = '';
        $dtCnt = [];
    }

    $k_post = $db->query('SELECT COUNT(*) FROM `license_support`'.$types.';', [$dtCnt])->el();
    $k_page = k_page($k_post, $set['p_str']);
    $page = page($k_page);
    $start = $set['p_str'] * $page - $set['p_str'];

    echo '<div style="text-align:center;" class="foot">'.lang('История вопросов').' <br/>
 <a href="?license_support=common">'.lang('Общие вопросы').'</a> ::
 <a href="?license_support=bags">'.lang('Баг или ошибка').'</a> ::
 <a href="?license_support=plugins_new">'.lang('Заказ модуля').'</a> ::
 <a href="?license_support=new_id">'.lang('Предложение по развитию').' </a> ::
 <a href="?license_support=admin">'.lang('Разработчикам').' </a> ::
 <a href="?">'.lang('Все').'</a><br/>
 <a href="?new_tiket">'.lang('Задать вопрос вопрос в тех. поддержку').'</a>
 </div>
 ';

    if ($k_post == 0) {
        msg(lang('Вопросов нету'));
    }

    if (isset($_GET['update_t'])) {
        $key = $_GET['update_t'];
        $arr = @json_decode(file_get_contents(API_SUPPORT_URL.'?post_update&key_license='.$license_key['key'] .'&msg_key='.$_GET['update_t'].'&site='.$_SERVER['HTTP_HOST']), true);
        foreach ($arr as $otvet) {
            if ($otvet['error'] != null) {
                msg($otvet['error']);
            } else {
                $otvet['otvet'] = base64_decode($otvet['otvet']);
                $otvet['support_agent_name'] = base64_decode($otvet['support_agent_name']);
                $otvet['support_agent_id_fiera'] = base64_decode($otvet['support_agent_fiera_id']);
                $otvet['time_otvet'] = (int) $otvet['time_otvet'];
                $otvet['support_agent_pol'] = (int) $otvet['support_agent_pol'];

                if ($otvet['otvet'] != null) {
                    $data = [
                             $otvet['otvet'],
                             $otvet['time_otvet'],
                             $otvet['support_agent_pol'],
                             $otvet['support_agent_name'],
                             $otvet['support_agent_fiera_id'],
                             $key
                        ];
                    $db->query("UPDATE `license_support`
SET `support_agent_otvet` =  ?,
`time_otvet` = ?i' ,
`support_agent_pol` = ?,
`support_agent_name` = ?,
`support_agent_id_fiera` = ?i'
WHERE `key` = ?I");
                } else {
                    $_SESSION['message'] = lang('Ответ еще не обработан');
                    exit(header('Location: ?'));
                }
            }
        }
    }

    $p_sql = $db->query('SELECT * FROM `license_support`' . $types . ' ORDER BY id DESC LIMIT ?i, ?i', [$start, $set['p_str']]);
    while ($license = $p_sql->row()) {
        echo "<div class='license_fon'>" . output_text($license['name']) . "  -	<span class='ank_n'>(" .
        vremja($license['time']) . ")</span> <hr />" . output_text($license['msg']);
        $keys = $license['key'];

        if ($license['act'] != 0) {
            if ($license['time_otvet'] > 0) {
                echo ' <hr/> '.lang('Ответ дал') .($license['support_agent_pol'] == 1 ? false : 'а').' :  <a href="//dcms-fiera.ru/id'.intval($license['support_agent_id_fiera']) .'">'.output_text($license['support_agent_name']) .'</a>
		<span class="ank_n">('.vremja($license['time_otvet']).')</span>
		<br/><br/>';
                echo output_text($license['support_agent_otvet']);
            //echo ' <br/>[<a href="?update_t='.($keys).'">Обновить ответ</a>]';
            echo '<br/> [<a href="?delete='.$license['id'].'">'.lang('Удалить').'</a>]<br/>';
            } else {
                echo '<hr/>'.lang('Ожидание ,вопрос еще не был проверен администратором Fiera');
                echo '<br/> [<a href="?update_t='.($keys).'">'.lang('Проверить ответ').'</a>]';
            }
        } else {
            echo '<hr/>'.lang('Вопрос создан,отправить его в службу поддержки');
            echo '? <br/> [<a href="?tiket_add='.($license['id']).'">'.lang('Отправить').'</a>]';
            echo ' [<a href="?edit='.$license['id'].'">'.lang('Редактировать').'</a>]';
            echo ' [<a href="?delete='.$license['id'].'">'.lang('Удалить').'</a>]<br/>';
        }
        echo "</div>";
    }
    if ($k_page > 1) {
        str("?license_support=".trim($_GET['license_support'])."&", $k_page, $page);
    }
} else {
    msg(lang('Сервис API FIERA отключен'));
}

echo "<div class='foot'>";
echo "&laquo;<a href='".APANEL."/'>".lang('В админку')."</a><br />";
echo "</div>";

include_once H.'sys/inc/tfoot.php';
