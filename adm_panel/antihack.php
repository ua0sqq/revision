<?php
include_once '../sys/inc/start.php';
include_once '../sys/inc/compress.php';
include_once '../sys/inc/sess.php';
include_once '../sys/inc/home.php';
include_once '../sys/inc/settings.php';
$temp_set=$set;
include_once '../sys/inc/db_connect.php';
include_once '../sys/inc/ipua.php';
include_once '../sys/inc/fnc.php';
include_once '../sys/inc/adm_check.php';
include_once '../sys/inc/user.php';
user_access('all_guard', null, 'index.php?'.SID);
adm_check();
$set['title']= lang('Антихак модуль');
include_once '../sys/inc/thead.php';
title();

if (isset($_POST['save'])) {
    $temp_set['antihah_hash']=intval($_POST['antihah_hash']);
    $temp_set['antihack']=intval($_POST['antihack']);
    $temp_set['antihah_files_format']=intval($_POST['antihah_files_format']);
    $temp_set['antihah_upload_msg']=intval($_POST['antihah_upload_msg']);
    $temp_set['antihah_upload_ban']=intval($_POST['antihah_upload_ban']);
    $temp_set['antihah_upload_ban_ip']=intval($_POST['antihah_upload_ban_ip']);
    $temp_set['antihah_upload_ban_time']=intval($_POST['antihah_upload_ban_time']);
    $temp_set['antihah_upload_intansiv']=intval($_POST['antihah_upload_intansiv']);

    if (save_settings($temp_set)) {
        admin_log('Настройки', 'ИЗображения', 'Изменение настроек фотогалереи');
        msg('Настройки успешно приняты');
    } else {
        $err='Нет прав для изменения файла настроек';
    }
}
err();
aut();



echo "
<form method=\"post\" action=\"?\">

<div class='p_m'>Дополнительная HASH проверка:<br /><select name=\"antihah_hash\">
<option value='0'".($temp_set['antihah_hash']==0?" selected='selected'":null).">Выкл</option>
<option value='1'".($temp_set['antihah_hash']==1?" selected='selected'":null).">Вкл. </option>
</select>
<br />  * Повышает шанс что куки не будут украдены на 95%
</div>


<div class='p_m'>Анти шелл модуль:<br /><select name=\"antihack\">
<option value='0'".($temp_set['antihack']==0?" selected='selected'":null).">Выкл</option>
<option value='1'".($temp_set['antihack']==1?" selected='selected'":null).">Вкл. </option>
</select>
<br />  * Защита от выгрузки шеллов на сайте
</div>



<div class='p_m'>Интенсивность сигнатур:<br /><select name=\"antihah_upload_intansiv\">
<option value='2'".($temp_set['antihah_upload_intansiv']==2?" selected='selected'":null).">Высокая</option>
<option value='3'".($temp_set['antihah_upload_intansiv']==3?" selected='selected'":null).">Средняя </option>
<option value='4'".($temp_set['antihah_upload_intansiv']==5?" selected='selected'":null).">Умеренная </option>
<option value='6'".($temp_set['antihah_upload_intansiv']==7?" selected='selected'":null).">Низкая </option>
</select>
</div>

<div class='p_m'>Анти шелл модуль на выгружаемые форматы:<br /><select name=\"antihah_files_format\">
<option value='0'".($temp_set['antihah_files_format']==0?" selected='selected'":null).">Выкл</option>
<option value='1'".($temp_set['antihah_files_format']==1?" selected='selected'":null).">Вкл. </option>
</select>
<br />  * Защита от выгрузки шеллов на сайте
</div>

<div class='p_m'>Отправлять в журнал данные об атаке:<br /><select name=\"antihah_upload_msg\">
<option value='0'".($temp_set['antihah_upload_msg']==0?" selected='selected'":null).">Выкл</option>
<option value='1'".($temp_set['antihah_upload_msg']==1?" selected='selected'":null).">Вкл. </option>
</select>
</div>

<div class='p_m'>Банить ли если человек авторизован:<br /><select name=\"antihah_upload_ban\">
<option value='0'".($temp_set['antihah_upload_ban']==0?" selected='selected'":null).">Выкл</option>
<option value='1'".($temp_set['antihah_upload_ban']==1?" selected='selected'":null).">Вкл. </option>
</select>
<br />  * Защита от выгрузки шеллов на сайте
</div>

<div class='p_m'>Время бана :<br /><select name=\"antihah_upload_ban_time\">
<option value='86400'".($temp_set['antihah_upload_ban_time']==86400?" selected='selected'":null).">Сутки</option>
<option value='259200'".($temp_set['antihah_upload_ban_time']==3?" selected='selected'":null).">3-е суток </option>
<option value='604800'".($temp_set['antihah_upload_ban_time']==5?" selected='selected'":null).">Неделю </option>
<option value='2419200'".($temp_set['antihah_upload_ban_time']==5?" selected='selected'":null).">Месяц </option>
<option value='99999999999'".($temp_set['antihah_upload_ban_time']==99999999999?" selected='selected'":null).">Навсегда </option>
</select>
</div>

<div class='p_m'>Банить по ip:<br /><select name=\"antihah_upload_ban_ip\">
<option value='0'".($temp_set['antihah_upload_ban_ip']==0?" selected='selected'":null).">Выкл</option>
<option value='1'".($temp_set['antihah_upload_ban_ip']==1?" selected='selected'":null).">Вкл. </option>
</select>
<br />  * Защита от выгрузки шеллов на сайте
</div>

<div class='p_m'><input value=\"Сохранить\" name='save' type=\"submit\" /></form>
* будьте внимательны в параметрах  и настройки их
<br /> 

</div>";


echo "<div class='foot'>\n";
echo "&laquo;<a href='/adm_panel/'>В админку</a><br />\n";
echo "</div>\n";

include_once '../sys/inc/tfoot.php';
