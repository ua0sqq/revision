<?php
if ($set['license_mod'] == 1) {
    $break = Explode('/', $_SERVER['SCRIPT_NAME']);
    $file = $break[count($break) - 1];
    $cachefile = H.'sys/cache/other/news_fiera.html';

    if (file_exists($cachefile) and time() - 5  < filemtime($cachefile)) {
        require_once $cachefile;
    } else {
        ob_start(); # Запуск буфера вывода

$fiera_vers = parse_ini_file(H.'sys/ini/vers.ini');
        if (isset($fiera_vers['key'])) {
            $api_news = json_decode(copyRemoteFile(FIERA_API_NEWS_US.'?key='.$fiera_vers['key'].'&url='. $_SERVER['HTTP_HOST']), true);
        }

        if (isset($api_news)) {
            foreach ($api_news as $post) {
                if ($post['error'] != null) {
                    msg($post['error']);
                } else {
                    $post['msg'] = base64_decode($post['msg']);
                    $post['title'] = base64_decode($post['title']);

                    echo "
			<div class='p_m'>
			<span class='adm_panel_span'> ". output_text($post['title']) ."</span>   <span style='float:right' class='adm_panel_span'>". vremja($post['time']) ."</span>
			<br/><br/>
			". output_text($post['msg']) ."
			</div>
			";
                }
            }
        }
        if (is_writable($cachefile)) {
            $cached = fopen($cachefile, 'w');
            fwrite($cached, ob_get_contents());
            fclose($cached);
        }
        ob_end_flush(); # Отправялем вывод в браузер
    }
}
//else msg(lang('Сервис API FIERA отключен'));
