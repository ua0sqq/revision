<?php
include_once '../sys/inc/start.php';
include_once '../sys/inc/compress.php';
include_once '../sys/inc/sess.php';
include_once '../sys/inc/home.php';
include_once '../sys/inc/settings.php';
include_once '../sys/inc/db_connect.php';
include_once '../sys/inc/ipua.php';
include_once '../sys/inc/fnc.php';
include_once '../sys/inc/adm_check.php';
include_once '../sys/inc/user.php';
user_access('cmod', null, '/');
adm_check();
$set['title'] = lang('cmod'); // заголовок страницы
include_once '../sys/inc/thead.php';
title();
aut();

include_once H.'sys/inc/chmod_test.php';

echo "<div class='foot'>";
echo "&laquo;<a href='".APANEL."/'>".lang('В админку')."</a><br />";
echo "</div>";

include_once '../sys/inc/tfoot.php';
