<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';

user_access('adm_log_read', null, 'index.php?' . SID);
adm_check();

$set['title']='Действия администрации';
include_once '../sys/inc/thead.php';
title();
err();
aut();

if (isset($_GET['id'])) {
    $ank = get_user($_GET['id']);
} else {
    $ank=false;
}

echo "<div class='p_m'>";
if ($ank && user_access('adm_log_read') && ($ank['id'] == $user['id'] || $ank['level'] < $user['level'])) {
    echo user($ank['id']) . " (".lang($ank['group_name']).")<br />\n";
    $adm_log_c_all = $db->query('select count(*) from `admin_log` where `id_user`=?i', [$ank['id']])->el();
    $mes = mktime(0, 0, 0, date('m') - 1); // время месяц назад
    $adm_log_c_mes = $db->query('select count(*) from `admin_log` where `id_user`=?i AND `time`>?i', [$ank['id'], $mes])->el();
    echo "<span class='ank_n'>".lang('Вся активность').":</span> <span class='ank_d'>$adm_log_c_all</span><br />\n";
    echo "<span class='ank_n'>".lang('Вся Активность за месяц').":</span> <span class='ank_d'>$adm_log_c_mes</span><br />\n";
} else {
    $adm_log_c_all = $db->query('select count(*) from `admin_log`')->el();
    $mes = mktime(0, 0, 0, date('m') - 1); // время месяц назад
    $adm_log_c_mes = $db->query('select count(*) from `admin_log` where `time`>?i', [$mes])->el();
    echo "<span class='ank_n'>".lang('Вся активность').":</span> <span class='ank_d'>$adm_log_c_all</span><br />\n";
    echo "<span class='ank_n'>".lang('Активность за месяц').":</span> <span class='ank_d'>$adm_log_c_mes</span><br />\n";
}
echo "</div>";

if (isset($_GET['id_mod']) && isset($_GET['id_act']) &&
    $db->query('select count(*) from `admin_log` where `mod`=?i AND `act`=?i' . ($ank ? ' AND `id_user`="' . $ank['id'] . '"' : ''),
               [$_GET['id_mod'], $_GET['id_act']])->el()) {
    $mod = $db->query('SELECT * FROM `admin_log_mod` WHERE `id`=?i LIMIT ?i', [$_GET['id_mod'], 1])->row();
    $act   = $db->query('SELECT * FROM `admin_log_act` WHERE `id`=?i LIMIT ?i', [$_GET['id_act'], 1])->row();

    $k_post  = $db->query('select count(*) from `admin_log` where `mod`=?i AND `act`=?i' .
                          ($ank ? ' AND `admin_log`.`id_user`="' . $ank['id'] . '"' : ''), [$mod['id'], $act['id']])->el();
    $k_page = k_page($k_post, $set['p_str']);
    $page    = page($k_page);
    $start    = $set['p_str']*$page - $set['p_str'];
    echo "<table class='post'>\n";
    if ($k_post == 0) {
        msg(lang('Действий нету'));
    } else {
        $pattern = 'SELECT log.*, u.nick FROM `admin_log` log
JOIN `user` u ON log.id_user=u.id
WHERE `mod`=?i AND `act`=?i' . ($ank ? ' AND `id_user`=?i' : '') . ' ORDER BY log.id DESC LIMIT ?i, ?i';
        if ($ank) {
            $data = [$mod['id'], $act['id'], $ank['id'], $start, $set['p_str']];
        } else {
            $data = [$mod['id'], $act['id'], $start, $set['p_str']];
        }

        $res = $db->query($pattern, $data);

        while ($post = $res->row()) {
            //$ank2=get_user($post['id_user']);
        echo "   <tr>\n";

            echo "  <td class='icon14' rowspan='2'>\n";
            avatar($post['id_user'], 60, 60);
            echo "  </td>\n";

            echo "  <td class='p_t'>\n";
            echo  $post['nick'] . '  (' . vremja($post['time']) . ')';
            echo "  </td>\n";
            echo "   </tr>\n";
            echo "   <tr>\n";
            if ($set['set_show_icon']==1) {
                echo "  <td class='p_m' colspan='2'>\n";
            } else {
                echo "  <td class='p_m'>\n";
            }
            echo $post['opis']."<br />\n";
            echo "  </td>\n";
            echo "   </tr>\n";
        }
        echo "</table>\n";

        if ($k_page>1) {
            str('?id_mod='.$mod['id'].'&amp;id_act='.$act['id'].'&amp;', $k_page, $page); // Вывод страниц
        }
    }
    echo "<div class='p_m'> &laquo;<a href='?id_mod=$mod[id]".($ank?"&amp;id=$ank[id]":null)."'>".lang('Список действий')."</a><br />\n";
    echo "&laquo;<a href='?$passgen".($ank?"&amp;id=$ank[id]":null)."'>".lang('Список модулей')."</a></div>\n";
} elseif (isset($_GET['id_mod']) &&
        $db->query('select count(*) from `admin_log` where `mod`=?i' . ($ank ? ' AND `id_user`="' . $ank['id'] . '"' : ''), [$_GET['id_mod']])->el()) {
    // действия в модуле
    $mod = $db->query('SELECT * FROM `admin_log_mod` WHERE `id`=?i', [$_GET['id_mod']])->row();
    $res = $db->query('SELECT a.`name` , a.`id` , (
    SELECT COUNT( * ) FROM `admin_log` WHERE `act` = a.`id` AND `mod` =?i' .
    ($ank ? ' AND `admin_log`.`id_user` ="' . $ank['id'] . '"' : '') . ' GROUP BY `act`) AS cnt
FROM `admin_log_act` a', [$mod['id']]);
    echo "<div class='p_m'>\n";
    if (!count($res)) {
        echo lang('Нет действий в модуле')." '".lang($mod['name'])."'";
    }
    while ($act=$res->row()) {
        if ($act['cnt']) {
            echo '<p style="line-height:0.8em;"><a href="?id_mod=' . $mod['id'] . '&amp;id_act=' . $act['id'] . '' .
            ($ank ? '&amp;id=' . $ank['id'] : '') . '">' . lang($act['name']) . '</a> (' . $act['cnt'] . ')</p>';
        }
    }
    echo "</div>\n";

    echo "<div class='p_m'>";

    echo "&laquo;<a href='?$passgen".($ank?"&amp;id=$ank[id]":null)."'>".lang('Список модулей')."</a><br />\n";
    echo "</div>\n";
} else {
    // действия по модулям
    $pattern = 'SELECT m.`name` , m.`id` , (
    SELECT COUNT( * ) FROM `admin_log` WHERE `mod`= m.id ' . ($ank ? ' AND id_user=?i' : '') . ' GROUP BY `mod`) cnt
FROM `admin_log_mod` m ';
    if ($ank) {
        $data = [$ank['id']];
    } else {
        $data = [];
    }
    $res = $db->query($pattern, $data);

    echo "<div class='p_m'>\n";
    if (!count($res)) {
        echo msg(lang("Нет действий в модулях"));
    }
    while ($mod = $res->row()) {
        if ($mod['cnt']) {
            echo '<p style="line-height:0.8em;"><a href="?id_mod=' . $mod['id'] . ($ank ? '&amp;id=' . $ank['id'] : null) . '">' . lang($mod['name']) . '</a> (' . $mod['cnt'] . ')</p>';
        }
    }
    echo "</div>\n";
}





if (user_access('adm_panel_show')) {
    echo "<div class='foot'>\n";
    if (user_access('adm_show_adm')) {
        echo "&raquo;<a href='administration.php'>".lang('Администрация')."</a><br />\n";
    }
    echo "&laquo;<a href='".APANEL."/'>".lang('В админку')."</a><br />\n";
    echo "</div>\n";
}

include_once '../sys/inc/tfoot.php';
