<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';

user_access('adm_banlist', null, 'index.php?'.SID);
adm_check();

$set['title'] = lang('Банлист');
include_once '../sys/inc/thead.php';
title();
err();
aut();

$k_post  = $db->query('SELECT COUNT(*) FROM `ban` WHERE `time`>?i', [$time])->el();
$k_page = k_page($k_post, $set['p_str']);
$page    = page($k_page);
$start    = $set['p_str']*$page-$set['p_str'];

echo "<table class='post'>\n";
if ($k_post==0) {
    echo "   <tr>\n";
    echo "  <td class='p_t'>\n";
    echo lang("Нет активных нарушений");
    echo "  </td>\n";
    echo "   </tr>\n";
}
$q=$db->query('SELECT * FROM `ban` WHERE `time`>?i ORDER BY `id` DESC LIMIT ?i, ?i', [$time, $start, $set['p_str']]);
while ($ban = $q->row()) {
    echo "   <tr>\n";
    $ank=get_user($ban['id_user']);
    echo "  <td class='icon14' rowspan='2'>\n";
    avatar($ank['id'], 80, 80);
    echo "  </td>\n";
    echo "  <td class='p_t'>\n";
    echo nick($ank['id']);
    echo "  </td>\n";
    echo "   </tr>\n";
    echo "   <tr>\n";
    if ($set['set_show_icon']==1) {
        echo "  <td class='p_m' colspan='2'>\n";
    } else {
        echo "  <td class='p_m'>\n";
    }

    $user_ban=get_user($ban['id_ban']);

    echo "<span class=\"ank_n\">".lang('Бан до')." ".vremja($ban['time']).":</span> ($user_ban[nick])<br /> ";
    echo lang('Причина').':'.output_text($ban['prich'])."<br />
".lang('Админ причина')." :".output_text($ban['adm_pr'])."<br />
".lang('Категория')." :".lang(output_text($ban['cat']))."<br />";

    if ((isset($access['ban_set']) || isset($access['ban_unset'])) && ($ank['level']<$user['level'] || $user['level']==4)) {
        echo "<a href='/adm_panel/ban.php?id=$ank[id]'>".lang('Подробно')."</a><br />\n";
    }
    echo "  </td>\n";
    echo "   </tr>\n";
}
echo "</table>\n";


if ($k_page>1) {
    str("?", $k_page, $page); // Вывод страниц
}

echo "<div class='foot'>\n";
echo "&laquo;<a href='".APANEL."/'>".lang('В админку')."</a><br />\n";
echo "</div>\n";

include_once '../sys/inc/tfoot.php';
