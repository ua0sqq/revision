<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';

user_access('adm_show_adm', null, 'index.php?' . SID);
adm_check();

$set['title']='Администрация'; // заголовок страницы
include_once '../sys/inc/thead.php';
title();
aut();

$k_post  = count::query('user', '`group_access`>"1"');
$k_page = k_page($k_post, $set['p_str']);
$page    = page($k_page);
$start    = $set['p_str']*$page-$set['p_str'];
echo "<table class='post'>\n";
if ($k_post == 0) {
    echo "   <tr>\n";
    echo "  <td class='p_t'>\n";
    echo "Нет результатов\n";
    echo "  </td>\n";
    echo "   </tr>\n";
}

$q = $db->query('SELECT * FROM `user` WHERE `group_access`>?i ORDER BY `group_access` DESC LIMIT ?i, ?i',
                [1, $start, $set['p_str']]);
while ($ank = $q->row()) {
    $ank = get_user($ank['id']);
    
    echo "   <tr>\n";
    echo "  <td class='icon14' rowspan='2'>\n";
    avatar($ank['id'], 130, 130);
    echo "  </td>\n";
    echo "  <td class='p_t'>\n";
    if (user_access('adm_log_read') && $ank['level'] != 0 && ($ank['id'] == $user['id'] || $ank['level'] < $user['level'])) { // ???
        echo "<a href='adm_log.php?id=$ank[id]'>$ank[nick]</a> ($ank[group_name])";
    } else {
        echo  user($ank['id'])." ($ank[group_name])";
    }
    echo "  </td>\n";
    echo "   </tr>\n";
    echo "   <tr>\n";
    if ($set['set_show_icon']==1) {
        echo "  <td class='p_m' colspan='2'>\n";
    } else {
        echo "  <td class='p_m'>\n";
    }

    echo "<span class=\"ank_n\">Пол:</span> <span class=\"ank_d\">".(($ank['pol']==1)?'Мужской':'Женский')."</span><br />\n";

    $adm_log_c_all=count::query('admin_log', ' `id_user`="' . $ank['id'] . '"');
    $mes=mktime(0, 0, 0, date('m')-1); // время месяц назад
    $adm_log_c_mes=count::query('admin_log', ' `id_user`="' . $ank['id'] . '" AND `time`>"' . $mes . '"');
    echo "<span class='ank_n'>Вся активность:</span> <span class='ank_d'>$adm_log_c_all</span><br />\n";
    echo "<span class='ank_n'>Активность за месяц:</span> <span class='ank_d'>$adm_log_c_mes</span><br />\n";

    echo "<span class=\"ank_n\">Посл. посещение:</span> <span class=\"ank_d\">".vremja($ank['date_last'])."</span><br />\n";
    if (isset($user) && ($user['level']>$ank['level'] || $user['level']==4)) {
        echo "<a href='/adm_panel/user.php?id=$ank[id]'>Редактировать профиль</a><br />\n";
    }

    echo "  </td>\n";
    echo "   </tr>\n";
}
echo "</table>\n";
if ($k_page > 1) {
    str('?', $k_page, $page); // Вывод страниц
}

if (user_access('adm_panel_show')) {
    echo "<div class='foot'>\n";
    echo "&laquo;<a href='/adm_panel/'>В админку</a><br />\n";
    echo "</div>\n";
}

include_once '../sys/inc/tfoot.php';
