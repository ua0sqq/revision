<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';

user_access('modules_edit', null, 'index.php?'.SID);
adm_check();
protect_panel();

$set['title'] = lang('Установка модулей');
include_once H . 'sys/inc/thead.php';
title() . aut();
include_once H.'sys/inc/zip.php';

$dir = H . 'sys/modules/files';

//пока тут побудет
$set['modules_key'] = 1;

if (isset($_GET['delete'])) {
    $_SESSION['message'] = lang('Модуль').' '.lang('Удален');
    unlink($dir.'/'.base64_decode($_GET['delete']));
    exit(header('Location: ?type=list'));
}

if (isset($_GET['install'])) {
    $f = $dir.'/'.base64_decode($_GET['install']);
}

if (isset($_GET['install']) && filesize($f) > 1) {
    $file=esc(stripcslashes(htmlspecialchars(base64_decode($_GET['install']))));
    $file=preg_replace('#(|\?)#', null, $file);
    $name=esc(trim(retranslit(preg_replace('#\.[^\.]*$#', null, $file)))); // имя файла без расширения
    $ras=strtolower(preg_replace('#^.*\.#i', null, $file));

    if ($ras != 'zip') {
        $err = lang('Модуль должен находиться в ZIP архиве');
    }

    if (!isset($err)) {
        $zip=new PclZip($f);

    #	открываем архив и открываем файл конфигурации
    $content = $zip->extract(PCLZIP_OPT_BY_NAME, 'sys/tmp/config.ini', PCLZIP_OPT_EXTRACT_AS_STRING);

    #	разбиваем  массив данных  из строки
        $Them_ini = ini :: string_arr($content[0]['content']);
        $update_conf = $Them_ini['update_conf'];
        $update_status = $Them_ini['update_status'];
    #	читаем и записываем название
        $them_name = $Them_ini['name'];
        $author = $Them_ini['author'];
        $vers = $Them_ini['vers'];
        $opis = $Them_ini['opis'];
        $de_install = $Them_ini['de_install'];
        $sql = $Them_ini['sql'];
        $img = $Them_ini['img'];
        $set_update = $Them_ini['set_update'];
        $set_update_text = $Them_ini['set_update_text'];
        $chmod = $Them_ini['chmod'];
        $set_update_htaccess = $Them_ini['set_update_htaccess'];
        $set_htaccess_update_text = $Them_ini['set_htaccess_update_text'];
        $fileunset = $f;
        $key = $Them_ini['key'];
        $incset = $Them_ini['incset'];
        if (strlen2($them_name) == null) {
            $err = lang('Конфигурационный файл sys/tmp/config.ini пуст или не найден');
        }
    }

    if (!isset($err)) {
        $backup = H.'sys/modules/backup/'.$name.'-'.time().'.zip';

        $zipuu = new PclZip($f);
        $zipb = new ZipArchive;

        $listContent = $zipuu->listContent();
        $count_a = count($listContent);

        if ($listContent != 0) {
            $flist = '';
            foreach ($listContent as $keyi => $value) {
                $r = $zipb->open($backup, ZipArchive::CREATE);
                if ($r === true) {
                    $flist .= $value['filename'] . "\n";
                    $zipb->addFromString('backup-files-'.date("d.m.y").'.txt', "Debug hist #". $them_name ."\n->Резервная копия файлов (". $count_a .') '. date("d.m.y")."\n\n". $flist);
                    $zipb->addFile(H.$value['filename'], $value['filename']);
                    $zipb->setArchiveComment('Backup Files Fiera ');
                    @$zipb->close(); // php7
                }
            }
        }

        if ($zip->extract(PCLZIP_OPT_PATH, H.'/', PCLZIP_OPT_REMOVE_PATH, H .'/') == 0) {
            die("Error : ".$zip->errorInfo(true));
        }

        $db->query('INSERT INTO `modules` (`name` ,`opis` ,`time` ,`author_install` ,`author` ,`vers`, `de_install`, `img`)
VALUES ( ?,  ?,  ?i, ?i, ?, ?, ?, ?)', [$them_name, $opis, time(), $user['id'], $author, $vers, $de_install, $img]);

//пытаемся выставить права
if ($chmod != null) {
    $file = explode(',', $chmod);
    foreach ($file as $v) {
        $k = explode(':', $v);
        @chmod(H . $k[0], "0{$k[1]}");
    }

//Поисковое слово
$world_set_update = $chmod;
//файл где ищем
$file_default = file_get_contents(H.'sys/dat/chmod.txt');

//если  нету записываем
if (!stripos($file_default, $world_set_update) !== false) {
    file_put_contents(H.'sys/dat/chmod.txt', $world_set_update ."\n", FILE_APPEND);
}
}

/*
#	записываем в АдминЛог
admin_log('Админка','Модули',"Установил".($user['pol'] == 0 ? 'а': false)
    ." модуль [color=green]".$them_name."[/color]  [color=green]". $name ."[/color]");
*/

if ($update_conf == 1) {
    unlink(H.'sys/ini/vers.ini');
    $update_conf_text = "
	vers = '{$vers}';
	status = '{$update_status}';
	time_update = '". time() ."';
	";

    $msgup = lang('Версия движка Fiera была обновлена') .' '. $set['fiera_vers'].' '.$set['fiera_status'] .' -> '. $vers .' '. $update_status;
    file_put_contents(H.'sys/ini/vers.ini', $update_conf_text ."\n", FILE_APPEND);
    $db->query('INSERT INTO `jurnal_system` (`time` ,`type` ,`read` ,`id_user`,`msg`,`id_kont`)
	VALUES ( ?, ?, ?i, ?i, ?, ?)', [time(), 'system', 0, $user['id'], $msgup, 0]);
}

//если нужно добавить что-то в default.ini
if ($set_update == 1) {

//Поисковое слово
$world_set_update = $set_update_text;
//файл где ищем
$file_default = file_get_contents(H.'sys/dat/default.ini');

//если  нету записываем
if (!stripos($file_default, $world_set_update) !== false) {
    file_put_contents(H.'sys/dat/default.ini', $set_update_text ."\n", FILE_APPEND);
}
}

//если нужно добавить что-то в .htaccess
if ($set_update_htaccess == 1) {

//Поисковое слово
$world_htaccess = $set_htaccess_update_text;
//файл где ищем
$file_htaccess = file_get_contents(H.'.htaccess');

//если  нету записываем
if (!stripos($file_htaccess, $world_htaccess) !== false) {
    file_put_contents(H.'.htaccess', $set_htaccess_update_text ."\n", FILE_APPEND);
}
}

        if ($sql == 1) {
            // выполнение одноразовых запросов
$opdirtables=opendir(H.'sys/sql_update/');
            while ($rd=readdir($opdirtables)) {
                if (preg_match('#^\.#', $rd)) {
                    continue;
                }
                if (isset($set['update'][$rd])) {
                    continue;
                }
                if (preg_match('#\.sql$#i', $rd)) {
                    include_once H.'sys/inc/sql_parser.php';
                    $sql = SQLParser::getQueriesFromFile(H.'sys/sql_update/'.$rd);
                    for ($i=0;$i<count($sql);$i++) {
                        $db->plainQuery($sql[$i]);
                    }
                    $set['update'][$rd] = true;
                    $save_settings = true;
                    @delete_dir(H.'sys/sql_update/'.$rd);
                }
            }
            closedir($opdirtables);
        }
    }

#-------------------log add--------------------------------#
$log_install = '--------'.date("F j, Y, g:i a").'---------------
';
    $log_install .= 'Модуль: '.$them_name.'
';
    $log_install .= 'Время: '.date("H:i:s").'
';
    $log_install .= 'Автор: '.$author.' ['.$vers.']
';
    $log_install .= 'Описание: '.$opis.'
';
    if ($sql == 1) {
        $log_install .= 'SQL: '. $i .'
';
    }
    if ($set_update_htaccess == 1) {
        $log_install .= 'htaccess: '.$set_htaccess_update_text.'
';
    }
    if ($set_update == 1) {
        $log_install .= 'set_update: '.$set_update_text.'
';
    }
    if ($chmod != null) {
        $log_install .= 'chmod: '.$chmod.'
';
    }
    $log_install .= 'ip install: '.$ip.'
';

    if ($key != null) {
        $log_install .= 'key : '.$key.'
';
    }

    $log_install .= '--------'.date("F j, Y, g:i a").'---------------
';
    file_put_contents(H.'sys/modules/log/log_'. date("d.m.y") .'.txt', $log_install ."\n", FILE_APPEND);
    unset($log_install);
#------------------log end---------------------------------#

@copy($fileunset, H.'sys/modules/tmp/'.$name.'.zip');
    unlink($fileunset);

    unlink(H.'sys/tmp/config.ini');

    if ($incset != null) {
        include H.'sys/tmp/'. $incset;
        unlink(H.'sys/tmp/'. $incset);
    }

    $_SESSION['message'] = lang('Модуль').' "'. $them_name .'"  '.lang('успешно установлен');
    exit(header('Location: ?type=list'));
}

err();

#	switch - type - переключатель
switch (empty($_GET['type']) ? false : $_GET['type']) {
#	условие по умолчанию
    default:

    echo '<a href="?type=list"><div class="p_m">
	<span class="adm_panel_span" style="color:green">'.lang('Установка модулей').'</span>
	</div></a> ';

    echo '<a href="?type=upload_m"><div class="p_m">
	<span class="adm_panel_span" style="color:green">'.lang('Загрузка модулей').'</span>
	</div></a> ';

    break;
    case 'list':

$mod = glob($dir .'/*zip');
if ($mod == null) {
    msg(lang('Нету модулей для установки'));
}

if ($mod != null) {
    foreach ($mod as $arr) {
        $size = $arr;
        $fn = $arr;

        $arr = str_replace($dir .'/', null, $arr);

        $arr_link = base64_encode($arr);

        $name = null;
        $author = null;

        if ($fn) {
            $zip=new PclZip($fn);

    #	открываем архив и открываем файл конфигурации
    $content = $zip->extract(PCLZIP_OPT_BY_NAME, 'sys/tmp/config.ini', PCLZIP_OPT_EXTRACT_AS_STRING);
            if ($content == null) {
                continue;
            }

    #	разбиваем  массив данных  из строки
    $Them_ini = ini :: string_arr($content[0]['content']);

    #	читаем и записываем название
    $name = $Them_ini['name'];
            $author = $Them_ini['author'];
        }

        if (filesize($size) > 100) {
            echo '<div class="p_m">  '.output_text($name).' '.output_text($author).' ['. size_file(filesize($size)).'] <a  href="?type=install_info&mod='.$arr_link.'"><span class="adm_panel_span" style="color:green">'.lang('Установить').'</span></a> <a  href="?delete='.$arr_link.'"><span class="adm_panel_span">'.lang('Удалить').'</span></a></div>';
        } else {
            echo '<div class="p_m">  '.output_text($name).' '.output_text($author).' [<span style="color:red">'.lang('Файл поврежден').'</span>] <a  href="?delete='.$arr_link.'"><span class="adm_panel_span">'.lang('Удалить').'</span></a></div>';
        }
    }
}
    break;

    case 'upload_m':

//uploads module
if (isset($_FILES['file_m']) && filesize($_FILES['file_m']['tmp_name']) != 0) {
    $file_m=esc(stripcslashes(htmlspecialchars($_FILES['file_m']['name'])));
    $file_m=preg_replace('#(|\?)#', null, $file_m);
    $name=esc(trim(retranslit(preg_replace('#\.[^\.]*$#', null, $file_m)))); // имя файла без расширения
    $ras=strtolower(preg_replace('#^.*\.#i', null, $file_m));
    if ($ras != 'zip') {
        $err = lang('Модуль должен находиться в ZIP архиве');
    }
    $name = retranslit($name);
    $_SESSION['message'] = lang('Модуль').' '.$name.' '.lang('успешно загружен');
    $name = $name .'-'. time();
    copy($_FILES['file_m']['tmp_name'], H.'sys/modules/files/'.$name.'.zip');
    chmod(H.'sys/modules/files/'.$name.'.zip', 0777);
    exit(header('Location: ?type=upload_m'));
}

echo "
<form class='foot' enctype='multipart/form-data' action='?type=upload_m' method='post'>
<input name='file_m' type='file' accept='application/zip' /><br />
<input class='submit' type='submit' value='".lang('Загрузка')."' /><br />
*  ".lang('Модуль должен находится в')." <span style='color:red'>zip</span> ".lang('архиве')." <span style='color:red'>".lang('без папки')."</span><br />
*  ".lang('Присутствия в архиве файла')."<span style='color:green'> sys/tmp/config.ini</span> ".lang('обязательно')."<br />
*  ".lang('Архив должен содержать только латинские буквы и цифры')."<br />
</form>";

    break;

#---------------------------------------#
    case 'install_info':

    $f = $dir.'/'.base64_decode($_GET['mod']);
    $file=esc(stripcslashes(htmlspecialchars(base64_decode($_GET['mod']))));
    $file=preg_replace('#(|\?)#', null, $file);
    $name=esc(trim(retranslit(preg_replace('#\.[^\.]*$#', null, $file)))); // имя файла без расширения
    $ras=strtolower(preg_replace('#^.*\.#i', null, $file));

    if ($ras != 'zip') {
        $err = lang('Модуль должен находиться в ZIP архиве');
    }

    if (!isset($err)) {
        $zip=new PclZip($f);

    #	открываем архив и открываем файл конфигурации
    $content = $zip->extract(PCLZIP_OPT_BY_NAME, 'sys/tmp/config.ini', PCLZIP_OPT_EXTRACT_AS_STRING);

    #	разбиваем  массив данных  из строки
    $Them_ini = ini::string_arr($content[0]['content']);

    #	читаем и записываем название
    $update_conf = $Them_ini['update_conf'];
        $backup = $Them_ini['backup'];
    //default
    if ($backup == null) {
        $backup = 1;
    }
        $name = $Them_ini['name'];
        $author = $Them_ini['author'];
        $vers = $Them_ini['vers'];
        $opis = $Them_ini['opis'];
        $sql = $Them_ini['sql'];
        $incset = $Them_ini['incset'];
        $img = @$Them_ini['img'];
        $set_update = $Them_ini['set_update'];
        $set_update_text = $Them_ini['set_update_text'];
        $chmod = $Them_ini['chmod'];
        $set_update_htaccess = $Them_ini['set_update_htaccess'];
        $set_htaccess_update_text = $Them_ini['set_htaccess_update_text'];
        $fileunset = $f;

        if (@$Them_ini['key'] !=  null) {
            $key = $Them_ini['key'];
        } else {
            $key = null;
        }

        $output = null;

        if ($opis != null) {
            $output .= '<div class="p_m">
	'.output_text($name).'<br/>
	'.lang('Автор').' : <span style="color:green"> '.output_text($author).' ['.$vers.'] </span><br/>
    '.output_text($opis).'<br/>
	<br/>
	</div>';
        }

        if ($chmod != null) {
            $output .= '<div class="p_m">'.lang('Будет затронут файл конфигурации прав CMOD').' :<br/>
	'.lang('Будут добавлены новые строки').'<br/>
	<span style="color:green">'.output_text($chmod).'</span></div>';
        }

        if ($set_update == 1) {
            $output .= '<div class="p_m">'.lang('Будет затронут файл конфигурации настроек системы').' :<br/>
	'.lang('Будут добавлены новые строки').'<br/>
	<span style="color:green">'.output_text($set_update_text).'</span></div>';
        }

        if ($set_update_htaccess == 1) {
            $output .= '<div class="p_m">'.lang('Будет затронут файл конфигурации .htaccess').' :<br/>
	'.lang('Будут добавлены новые строки').'<br/>
	<span style="color:green">'.output_text($set_htaccess_update_text).'</span></div>';
        }

        if ($key == null and $set['modules_key'] == 1) {
            $output .= '<div class="p_m">'.lang('Отсуствует цифровая подпись модуля').'</div>';
        }

        if ($sql == 1) {
            $output .= '<div class="p_m"><span style="color:green">'.lang('будут добавлены таблицы в базу данных').'</span> </div>';
        }

        if ($update_conf == 1) {
            $output .= '<div class="p_m"><span style="color:green">'.lang('Будет изменена версия движка в админки').'</span> </div>';
        }

        if ($backup == 1) {
            $output .= '<div class="p_m"><span style="color:green">'.lang('Будет создана резевная копия файлов').'</span> </div>';
        }

        $key_c = count::query('modules', " `key` = '".$key."'");
        if ($key_c == 1) {
            $output .= '<div class="p_m"><span style="color:red">'.lang('Цифровая подпись совпадает с ранее установленным модулем').'</span></div>';
        }

        if ($incset != null) {
            $output .= '<div class="p_m"><span style="color:red">'.lang('Во время установки ,будет вызван инклуд').' -> sys/tmp/'.$incset.'</span> </div>';
        }

        echo '<div class="p_m"> '.lang('Будет совершено следующие').' :</div> '. $output;

        $zipuu = new PclZip($f);
        $listContent = $zipuu->listContent();
        $count_a = count($listContent);
        if ($listContent != 0) {
            echo '<div class="p_m">Содержимое : <br/>';
            foreach ($listContent as $keyi => $value) {
                echo output_text($value['filename']).'<br/>';
            }
            echo '</div>';
        }

        echo '<div class="p_m">'.lang('Вы уверены?')."<br />
	<span style='color:red'>".lang('Вы автоматичски соглашаетесь с правилами установки модулей')."</span>
	<a href='?type=rules'>[".lang('Правила')."]</a><br />
	<br />
	<a href='?install=".htmlspecialchars($_GET['mod'])."'>
	<span class='adm_panel_span' style='color:green'>".lang('Установка')."</span>
	</a></div>";
    }
    break;

    case 'rules':

if (is_file(H.'sys/docs/rules_modules.txt')) {
    $f=file(H.'sys/docs/rules_modules.txt');
    $k_page=k_page(count($f), $set['p_str']);
    $page=page($k_page);
    $start = $set['p_str']*($page-1);
    $end = $set['p_str']*$page;
    for ($i= $start; $i < $end and $i<count($f);$i++) {
        echo '<div class="p_m">'.output_text($f[$i])."</div>";
    }
}
    break;

}

echo "<div class='foot'>";
echo "&laquo; <a href='modules.php'>".lang('Модули')."</a><br />";
echo "&laquo;<a href='".APANEL."/'>".lang('В админку')."</a><br />";
echo "&laquo;<a href='modules_market.php'>".lang('Fiera Market')."</a><br />";
echo "</div>";

include_once H . 'sys/inc/tfoot.php';
