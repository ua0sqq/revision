<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';

user_access('user_prof_edit', null, 'index.php?'.SID);
adm_check();

if (isset($_GET['id'])) {
    $ank['id'] = intval($_GET['id']);
} else {
    exit(header("Location: /"));
}

if ($db->query('SELECT COUNT(*) FROM `user` WHERE `id`=?i LIMIT ?i', [$ank['id'], 1])->el() == 0) {
    header("Location: /index.php?".SID);
    exit;
}
//$ank = get_user($ank['id']);
$ank = $db->query('SELECT * FROM `user` WHERE `id`=?i', [$ank['id']])->row();

if ($user['group_access'] <= $ank['group_access']) {
    $_SESSION['message'] = lang('не хватает прав');
    exit(header('Location: /'));
}

$set['title'] = lang('Профиль пользователя') . ' ' . $ank['nick'];
include_once H . 'sys/inc/thead.php';
title();


if (isset($_POST['save'])) {
    if (isset($_POST['nick']) && $_POST['nick'] != $ank['nick']) {
        if ($db->query('SELECT COUNT(*) FROM `user` WHERE `nick`=?', [$_POST['nick']])->el() == 1) {
            $err = lang('Ник') . ' ' . output_text($_POST['nick']) .' ' . lang('уже занят');
        } elseif (user_access('user_change_nick')) {
            $nick=trim($_POST['nick']);
            if (!preg_match("#^([A-zА-я0-9\-\_\ ])+$#ui", $nick)) {
                $err[] = lang('В нике присутствуют запрещенные символы');
            }
            if (mb_strlen($nick) < 3) {
                $err[] = lang('Короткий ник');
            }
            if (mb_strlen($nick) > 32) {
                $err[] = lang('Длина ника превышает 32 символа');
            }
            if (!isset($err)) {
                admin_log(lang('Пользователи'), lang('Изменение ника'), lang('Ник') . ' ' . $ank['nick'] . ' ' . lang('изменен на') . ' ' . $nick);
                $ank['nick']=$nick;
                $db->query("UPDATE `user` SET `nick` = '$nick' WHERE `id` = '$ank[id]' LIMIT 1");
            }
        } else {
            $err[] = lang('У Вас нет привилегий на изменение ника пользователя');
        }
    }

    if (isset($_POST['set_show_icon']) && ($_POST['set_show_icon'] == 1 || $_POST['set_show_icon'] == 0)) {
        $db->query('UPDATE `user` SET `set_show_icon`=? WHERE `id`=?i LIMIT ?i', [$_POST['set_show_icon'], $ank['id'], 1]);
    } else {
        $err = lang('Ошибка режима аватара');
    }

    if (isset($_POST['set_translit']) && ($_POST['set_translit'] == 1 || $_POST['set_translit'] == 0)) {
        $db->query('UPDATE `user` SET `set_translit`=? WHERE `id`=?i LIMIT ?i', [$_POST['set_translit'], $ank['id'], 1]);
    } else {
        $err = lang('Ошибка режима транслита');
    }

    if (isset($_POST['set_files']) && ($_POST['set_files'] == 1 || $_POST['set_files'] == 0)) {
        $db->query('UPDATE `user` SET `set_files`=? WHERE `id`=?i LIMIT ?i', [$_POST['set_files'], $ank['id'], 1]);
    } else {
        $err = lang('Ошибка режима файлов');
    }

    if (isset($_POST['set_time_chat']) && (is_numeric($_POST['set_time_chat']) && $_POST['set_time_chat'] >= 0 && $_POST['set_time_chat'] <= 900)) {
        $db->query('UPDATE `user` SET `set_time_chat`=? WHERE `id`=?i LIMIT ?i', [$_POST['set_time_chat'], $ank['id'], 1]);
    } else {
        $err = lang('Ошибка во времени автообновления');
    }

    if (isset($_POST['set_p_str']) && (is_numeric($_POST['set_p_str']) && $_POST['set_p_str'] > 0 && $_POST['set_p_str'] <= 100)) {
        $db->query('UPDATE `user` SET `set_p_str`=? WHERE `id`=?i LIMIT ?i', [$_POST['set_p_str'], $ank['id'], 1]);
    } else {
        $err = lang('Неправильное количество пунктов на страницу');
    }

    if (isset($_POST['ank_name']) && preg_match('#^([A-zА-я \-]*)$#ui', $_POST['ank_name'])) {
        $ank['ank_name'] = esc(trim($_POST['ank_name'])) ;
        $db->query('UPDATE `user` SET `ank_name`=? WHERE `id`=?i LIMIT ?i', [$ank['ank_name'], $ank['id'], 1]);
    } else {
        $err= lang('Вы ошиблись в поле имя');
    }

    if (isset($_POST['ank_d_r']) && (is_numeric($_POST['ank_d_r']) && $_POST['ank_d_r'] > 0 && $_POST['ank_d_r'] <= 31 || $_POST['ank_d_r'] == null)) {
        $ank['ank_d_r'] = $_POST['ank_d_r'];
        if ($ank['ank_d_r'] == null) {
            $ank['ank_d_r'] = null;
        }
        $db->query('UPDATE `user` SET `ank_d_r`=? WHERE `id`=?i LIMIT ?i', [$ank['ank_d_r'], $ank['id'], 1]);
        if ($ank['ank_d_r'] == null) {
            $ank['ank_d_r'] = null;
        }
    } else {
        $err = lang('Неверный формат дня рождения');
    }

    if (isset($_POST['ank_m_r']) && (is_numeric($_POST['ank_m_r']) && $_POST['ank_m_r']>0 && $_POST['ank_m_r']<=12 || $_POST['ank_m_r']==null)) {
        $ank['ank_m_r'] = $_POST['ank_m_r'];
        if ($ank['ank_m_r'] == null) {
            $ank['ank_m_r'] = null;
        }
        $db->query('UPDATE `user` SET `ank_m_r`=? WHERE `id`=?i LIMIT ?i', [$ank['ank_m_r'], $ank['id'], 1]);
        if ($ank['ank_m_r'] == null) {
            $ank['ank_m_r'] = null;
        }
    } else {
        $err= lang('Неверный формат месяца рождения');
    }

    if (isset($_POST['ank_g_r']) && (is_numeric($_POST['ank_g_r']) && $_POST['ank_g_r']>0 && $_POST['ank_g_r']<=date('Y') || $_POST['ank_g_r']==null)) {
        $ank['ank_g_r'] = $_POST['ank_g_r'];
        if ($ank['ank_g_r'] == null) {
            $ank['ank_g_r'] = null;
        }
        $db->query('UPDATE `user` SET `ank_g_r`=? WHERE `id`=?i LIMIT ?i', [$ank['ank_g_r'], $ank['id'],  1]);
        if ($ank['ank_g_r'] == null) {
            $ank['ank_g_r'] = null;
        }
    } else {
        $err= lang('Неверный формат года рождения');
    }

    if (isset($_POST['ank_city']) && preg_match('#^([A-zА-я \-]*)$#ui', $_POST['ank_city'])) {
        $ank['ank_city'] = esc(trim($_POST['ank_city']));
        $db->query('UPDATE `user` SET `ank_city`=? WHERE `id`=?i LIMIT ?i', [$ank['ank_city'], $ank['id'], 1]);
    } else {
        $err= lang('Вы ошиблись в поле город');
    }

    if (isset($_POST['ank_icq']) && (is_numeric($_POST['ank_icq']) && mb_strlen($_POST['ank_icq']) >= 5 && mb_strlen($_POST['ank_icq']) <= 9 || $_POST['ank_icq'] == null)) {
        $ank['ank_icq'] = $_POST['ank_icq'];
        if ($ank['ank_icq'] == null) {
            $ank['ank_icq'] = null;
        }
        $db->query('UPDATE `user` SET `ank_icq`=? WHERE `id`=?i LIMIT ?i', [$ank['ank_icq'], $ank['id'], 1]);
        if ($ank['ank_icq'] == null) {
            $ank['ank_icq'] = null;
        }
    } else {
        $err= lang('Неверный формат ICQ');
    }

    if (isset($_POST['ank_skype']) && preg_match('#^([A-z0-9 \-]*)$#ui', $_POST['ank_skype'])) {
        $ank['ank_skype'] = $_POST['ank_skype'];
        if ($ank['ank_skype'] == null) {
            $ank['ank_skype'] = null;
        }
        $db->query('UPDATE `user` SET `ank_skype`=? WHERE `id`=?i LIMIT ?i', [$ank['ank_skype'], $ank['id'], 1]);
    } else {
        $err[]= lang('Неверный логин Skype');
    }

    if (isset($_POST['ank_n_tel']) && (is_numeric($_POST['ank_n_tel']) && mb_strlen($_POST['ank_n_tel']) >= 5 && mb_strlen($_POST['ank_n_tel']) <= 11 || $_POST['ank_n_tel'] == null)) {
        $ank['ank_n_tel'] = $_POST['ank_n_tel'];
        $db->query('UPDATE `user` SET `ank_n_tel`=?i WHERE `id`=?i LIMIT ?i', [$ank['ank_n_tel'], $ank['id'], 1]);
    } else {
        $err= lang('Неверный формат номера телефона');
    }

    if (isset($_POST['ank_mail']) && ($_POST['ank_mail'] == null || preg_match('#^[A-z0-9-\._]+@[A-z0-9]{2,}\.[A-z]{2,4}$#ui', $_POST['ank_mail']))) {
        $ank['ank_mail'] = $_POST['ank_mail'];
        $db->query('UPDATE `user` SET `ank_mail`=? WHERE `id`=?i LIMIT ?i', [$ank['ank_mail'], $ank['id'], 1]);
    } else {
        $err[] = lang('Неверный E-mail');
    }

    if (isset($_POST['ank_o_sebe']) && preg_match('#^([A-zА-я \-]*)$#ui', $_POST['ank_o_sebe'])) {
        $ank['ank_o_sebe']=esc(trim($_POST['ank_o_sebe']));
        $db->query('UPDATE `user` SET `ank_o_sebe`=? WHERE `id`=?i LIMIT ?i', [$ank['ank_o_sebe'], $ank['id'], 1]);
    } else {
        $err = lang('Вы ошиблись в поле о себе');
    }

    if (isset($_POST['new_pass']) && mb_strlen($_POST['new_pass']) > 5) {
        admin_log('Пользователи', 'Смена пароля', 'Пользователю ' . $ank['nick'] . ' установлен новый пароль');
        $db->query('UPDATE `user` SET `pass`=? WHERE `id`=?i LIMIT ?i',
                   [shif($_POST['new_pass']), $ank['id'], 1]);
    }

    if (isset($_POST['pol']) && ($_POST['pol'] == '0' || $_POST['pol'] == '1')) {
        $db->query('UPDATE `user` SET `pol`=?string WHERE `id`=?i LIMIT ?i', [$_POST['pol'], $ank['id'], 1]);
    }

    if (user_access('user_change_group') && isset($_POST['group_access'])) {
        if ($db->query('SELECT COUNT(*) FROM `user_group` WHERE `id`=?i AND `level` < ?string ',
                       [$_POST['group_access'], $user['level']])->el() == 1) {
            if ($ank['group_access'] != intval($_POST['group_access'])) {
                admin_log('Пользователи', 'Изменение статуса', 'Пользователь ' . $ank['nick'] . ': Статус ' . $ank['group_name'] . '
						  изменен на ' . $db->query('SELECT `name` FROM `user_group` WHERE `id`=?i', [$_POST['group_access']])->el());
                $ank['group_access'] = intval($_POST['group_access']);
                $db->query('UPDATE `user` SET `group_access`=?i WHERE `id`=?i LIMIT ?i', [$ank['group_access'], $ank['id'], 1]);
            }
        }
    }

    if (($user['level'] >= 3 || $ank['id'] == $user['id']) && isset($_POST['balls']) && is_numeric($_POST['balls'])) {
        $db->query('UPDATE `user` SET `balls`=?i WHERE `id`=?i LIMIT ?i', [$_POST['balls'], $ank['id'], 1]);
    }

    if ($user['level'] >= 3 || $ank['id'] == $user['id'] && isset($_POST['rating'])) {
        $db->query('UPDATE `user` SET `rating`=? WHERE `id`=?i LIMIT ?i', [number_format($_POST['rating'], 2), $ank['id'], 1]);
    }

    if ($user['level'] >= 3 || $ank['id'] == $user['id'] && isset($_POST['money']) && is_numeric($_POST['money'])) {
        $db->query('UPDATE `user` SET `money`=?i WHERE `id`=?i LIMIT ?i', [$_POST['money'], $ank['id'], 1]);
    }

    if ($user['level'] >= 3 || $ank['id'] == $user['id'] && isset($_POST['tester']) && is_numeric($_POST['tester'])) {
        $db->query('UPDATE `user` SET `tester`=?i WHERE `id`=?i LIMIT ?i', [$_POST['tester'], $ank['id'], 1]);
    }

    if (isset($_POST['mylink'])) {
        $mylink = trim($_POST['mylink']);
        $count = $db->query('SELECT COUNT(*) FROM `user` WHERE `mylink`=?', [$mylink])->el();

        if ($count == 1) {
            $err = lang('Ссылка уже занята');
        }
        if (!preg_match("#^([A-z0-9\-\_\ ])+$#ui", $mylink)) {
            $err = lang('Используются запрещенные символы');
        }
        if (mb_strlen($mylink) <= 4) {
            $err = lang('Ссылка должна быть не короче 5 символов');
        }
        if (mb_strlen($mylink) > 9) {
            $err = lang('Ссылка должна быть больше 9 символов');
        }
        if ($mylink == $ank['mylink']) {
            $err = null;
        }
        if ($err == null) {
            $db->query('UPDATE `user` SET `mylink`=? WHERE `id`=?i LIMIT ?i', [$mylink, $ank['id'], 1]);
        }
        $ank['mylink'] = $mylink;
    }

    admin_log('Пользователи', 'Профиль', 'Редактирование профиля пользователя ' . $ank['nick'] . ' (id#' . $ank['id']);
    #	удаляем кэш файл
    cache_delete::user($ank['id']);

    if (!isset($err)) {
        $_SESSION['message'] = lang('Изменения успешно приняты');
        exit(header('Location: /' . $ank['mylink']));
    }
}

err();
aut();

echo "<div class='p_m'><form method='post' action=''>
	".lang('Ник').":<br /><input" . (user_access('user_change_nick') ? null : ' disabled="disabled"') . " type='text' name='nick' value='$ank[nick]' maxlength='32' /><br />
	".lang('Имя в реале').":<br />\n<input type='text' name='ank_name' value='$ank[ank_name]' maxlength='32' /><br />";
    echo lang('Дата рождения').':<br />';
    //День
    echo '<select name="ank_d_r"><option selected="'.$ank['ank_d_r'].'" value="'.$ank['ank_d_r'].'" >'.$ank['ank_d_r'].'</option>';
    for ($i = 1; $i < 32; ++$i) {
        echo '<option value="'.$i.'">'.$i.'</option>';
    }
    echo '</select>';
    //Месяц
    echo '<select name="ank_m_r"><option selected="'.$ank['ank_m_r'].'" value="'.$ank['ank_m_r'].'" >'.$ank['ank_m_r'].'</option>';
    for ($i = 1; $i < 13; ++$i) {
        echo '<option value="'.$i.'">'.$i.'</option>';
    }
    echo '</select>';
    //Год
    echo '
	<select name="ank_g_r"><option selected="'.$ank['ank_g_r'].'" value="'.$ank['ank_g_r'].'" >'.$ank['ank_g_r'].'</option>';
    for ($i = 1920; $i < date('Y') - 7; ++$i) {
        echo '<option value="'.$i.'">'.$i.'</option>';
    }
    echo '</select><br />';
    echo lang('Город').":<br />\n<input type='text' name='ank_city' value='$ank[ank_city]' maxlength='32' /><br />
	".lang('Icq').":<br />\n<input type='text' name='ank_icq' value='$ank[ank_icq]' maxlength='9' /><br />
	".lang('Skype логин')."<br />
		<input type='text' name='ank_skype' value='$ank[ank_skype]' maxlength='16' /><br />
	".lang('E-mail').":<br />\n<input type='text' name='ank_mail' value='$ank[ank_mail]' maxlength='32' /><br />
	".lang('Номер телефона').":<br />\n<input type='text' name='ank_n_tel' value='$ank[ank_n_tel]' maxlength='11' /><br />
	".lang('О себе').":<br />\n<input type='text' name='ank_o_sebe' value='$ank[ank_o_sebe]' maxlength='512' /><br />\n";
echo lang('Автообновление в чате').":<br />\n<input type='text' name='set_time_chat' value='$ank[set_time_chat]' maxlength='3' /><br />\n";
echo lang('Пунктов на страницу').":<br />\n<input type='text' name='set_p_str' value='$ank[set_p_str]' maxlength='3' /><br />\n";
echo lang('Иконки').":<br />\n<select name=\"set_show_icon\">\n";

if ($ank['set_show_icon']==1) {
    $sel=' selected="selected"';
} else {
    $sel=null;
}
echo "<option value=\"1\"$sel>".lang('Показывать')."</option>\n";

if ($ank['set_show_icon']==0) {
    $sel=' selected="selected"';
} else {
    $sel=null;
}

echo "<option value=\"0\"$sel>".lang('Скрывать')."</option>\n";
echo "</select><br />\n";

echo lang('Транслит').":<br />\n<select name=\"set_translit\">\n";
if ($ank['set_translit']==1) {
    $sel=' selected="selected"';
} else {
    $sel=null;
}
echo "<option value=\"1\"$sel>".lang('По выбору')."</option>\n";
if ($ank['set_translit']==0) {
    $sel=' selected="selected"';
} else {
    $sel=null;
}
echo "<option value=\"0\"$sel>".lang('Никогда')."</option>\n";
echo "</select><br />\n";

echo lang('Выгрузка файлов').":<br />\n<select name=\"set_files\">\n";
if ($ank['set_files']==1) {
    $sel=' selected="selected"';
} else {
    $sel=null;
}
echo "<option value=\"1\"$sel>".lang('Показывать поле')."</option>\n";
if ($ank['set_files']==0) {
    $sel=' selected="selected"';
} else {
    $sel=null;
}
echo "<option value=\"0\"$sel>".lang('Не использовать выгрузку')."</option>\n";
echo "</select><br />\n";

echo lang('Пол').":<br />\n<select name=\"pol\">\n";
if ($ank['pol']==1) {
    $sel=' selected="selected"';
} else {
    $sel=null;
}
echo "<option value=\"1\"$sel>".lang('Мужской')."</option>\n";
if ($ank['pol']==0) {
    $sel=' selected="selected"';
} else {
    $sel=null;
}
echo "<option value=\"0\"$sel>".lang('Женский')."</option>\n";
echo "</select><br />\n";

if ($user['level']<3) {
    $dis=' disabled="disabled"';
} else {
    $dis=null;
}
echo lang('Баллы').":<br />\n<input type='text'$dis name='balls' value='$ank[balls]' /><br />\n";

if ($user['level']<3) {
    $dis=' disabled="disabled"';
} else {
    $dis=null;
}
echo lang('Монеты').":<br />\n<input type='text'$dis name='money' value='$ank[money]' /><br />\n";

if ($user['level']<3) {
    $dis=' disabled="disabled"';
} else {
    $dis=null;
}
echo lang('Рейтинг').":<br /><input type='text'$dis name='rating' value='".rating::output($ank['id'])."' /><br />\n";

echo lang('Группа').":<br />\n<select name='group_access'".(user_access('user_change_group')?null:' disabled="disabled"')."><br />\n";

$q = $db->query('SELECT * FROM `user_group` ORDER BY `level`,`id` ASC');
while ($post = $q->row()) {
    echo "<option value='$post[id]'".($post['level']>=$user['level']?" disabled='disabled'":null)."".($post['id']==$ank['group_access']?" selected='selected'":null).">".lang($post['name'])."</option>\n";
}

echo "</select><br />\n";

echo lang('Новый пароль').":<br />\n<input type='text' name='new_pass' value='' /><br />\n";

echo lang('Персональная ссылка').":<br/> http://".$_SERVER['SERVER_NAME']."/<input type='text' name='mylink' value='".$ank['mylink']."' /><br />";



echo lang('Режим тестера').":<br />\n<select name=\"tester\">\n";
if ($ank['tester'] == 1) {
    $sel=' selected="selected"';
} else {
    $sel=null;
}
echo "<option value=\"1\"$sel>".lang('Включен')."</option>\n";
if ($ank['tester'] == 0) {
    $sel=' selected="selected"';
} else {
    $sel=null;
}
echo "<option value=\"0\"$sel>".lang('Отключен')."</option>\n";
echo "</select>(";
echo lang('Бесконечные баллы и монеты').')<br />';




echo "<input type='submit' name='save' value='".lang('Сохранить')."' />";
echo "</form></div>";


echo "<div class='foot'>\n";
echo "&raquo;<a href=\"/mail.php?id=$ank[id]\">".lang('Написать сообщение')."</a><br />\n";
echo "&laquo;<a href=\"/$ank[mylink]\">".lang('В анкету')."</a><br />\n";
echo "&laquo;<a href='".APANEL."/'>".lang('В админку')."</a><br />\n";
echo "</div>\n";
include_once H . 'sys/inc/tfoot.php';
