<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';

user_access('ava_dell', null, '/');
adm_check();

//if (isset($_GET['id']))
//{
//	$ank['id'] = intval($_GET['id']);
//}
//
if (!$ank = $db->query('SELECT id, group_access, mylink FROM `user` WHERE `id` = ?i', [$_GET['id']])->row()) {
    exit(header("Location: /"));
}

//$ank = get_user($ank['id']);

if ($user['group_access'] < $ank['group_access'] and user_access('dell_avatar')) {
    $_SESSION['message'] = lang('Не хватает прав');
    exit(header("Location: /"));
}

if ($user['id'] == $ank['id']) {
    $_SESSION['message'] = lang('Свои аватары удалять нельзя');
    exit(header("Location: /"));
}

if (!is_file(H . 'files/avatars/' . $ank['id'] . '.png')) { // ??? почему только png?
    $_SESSION['message'] = lang('У пользователя не установлен аватар');
    exit(header("Location: /"));
}


$set['title'] = lang('Удаление аватара');
include_once H . 'sys/inc/thead.php';
title();
aut();

//блокировка статуса
if (isset($_POST['delete_avatar'])) {
    $msg = trim($_POST['prich']);

    if (mb_strlen($msg) < 3) {
        $_SESSION['message'] = lang('Нужно указать причину подробнее ');
        exit(header("Location: " . APANEL . "/avatar_del.php?id=" . $ank['id']));
    }

    if (isset($_POST['avtor'])) {
        $av = ($_POST['avtor'] == 1 ? ' Модератор [url=/' . $user['mylink'] . ']' . $user['nick'] . '[/url]' : false);
    } else {
        $av = null;
    }

    $text = 'Ваш Аватар Удален!
Причина удаления : 
'. $msg . $av;

    mail_send(0, $ank['id'], $text);
    $_SESSION['message'] = lang('Автар Удален');

    if (is_file(H . 'files/avatars/' . $ank['id'] . '.png')) {
        unlink(H . 'files/avatars/' . $ank['id'] . '.png');
    }
    $_SESSION['message'] = lang('Аватар удален');
    exit(header("Location: /id".$ank['id']));
}

echo "<center><div class='p_m'>";
avatar($ank['id'], 200, 200);
echo "</div></center>";

echo "<div class='p_m'><form action='' method='post'>";
echo lang('Причина')." <textarea name='prich' class='form_a'></textarea><br/>";
echo "<label><input type='checkbox' name='avtor' value='1' /> ".lang('Указать кто удалил')."</label><br/>";
echo "<input class='form_a_bottom'  name='delete_avatar' type='submit' value='".lang('Удалить Аватар')."' /> 
<a href='/{$ank['mylink']}'>".lang('Отмена')."</a>";
echo "</form></div>";

echo "<div class='foot'>";
echo "&laquo;<a href='".APANEL."/'>В админку</a><br />";
echo "</div>";

include_once H . 'sys/inc/tfoot.php';
