<?php
include_once '../sys/inc/start.php';
include_once '../sys/inc/compress.php';
include_once '../sys/inc/sess.php';
include_once '../sys/inc/home.php';
include_once '../sys/inc/settings.php';
include_once '../sys/inc/db_connect.php';
include_once '../sys/inc/ipua.php';
include_once '../sys/inc/fnc.php';
include_once '../sys/inc/adm_check.php';
include_once '../sys/inc/user.php';
user_access('api_config', null, 'index.php?'.SID);
adm_check();
$set['title']= lang('Новости fiera');
include_once '../sys/inc/thead.php';
title();
aut();

include_once H.'sys/inc/api_config.php';

$api_news = json_decode(@file_get_contents(FIERA_API_NEWS), true);


if ($api_news != null) {
    echo "<div class='p_m'>";
    foreach ($api_news as $post) {
        echo "
			<div class='p_m'>
			". output_text($post['title']) ." :: ". vremja($post['time']) ."
			<hr/>
			". output_text($post['msg']) ."
			</div><br/>
			";
    }
    echo '</div>';
} else {
    msg(lang('Сервис API NEWS  недоступен , попробуйте позже'));
}


    



include_once '../sys/inc/tfoot.php';
