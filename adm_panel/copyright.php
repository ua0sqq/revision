<?php
include_once '../sys/inc/start.php';
include_once '../sys/inc/compress.php';
include_once '../sys/inc/sess.php';
include_once '../sys/inc/home.php';
include_once '../sys/inc/settings.php';
include_once '../sys/inc/db_connect.php';
include_once '../sys/inc/ipua.php';
include_once '../sys/inc/fnc.php';
include_once '../sys/inc/adm_check.php';
include_once '../sys/inc/user.php';
user_access('adm_news', null, '/');
adm_check();

//подключаем языковой пакет
lang::start('news');

$set['title']= lang('Настройка copyright');
include_once '../sys/inc/thead.php';
title();
err();
aut();
msg('В разработке');
echo "<div class='foot'>\n";
echo "&laquo; <a href='".APANEL."'>".lang('В админку')."</a><br />\n";
echo "</div>\n";

include_once '../sys/inc/tfoot.php';
