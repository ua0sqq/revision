<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';

user_access('adm_mysql', null, '/');
adm_check();
protect_panel();

$set['title']='MySQL запрос';
include_once H.'sys/inc/thead.php';
title() . aut();

if (isset($_GET['set']) && $_GET['set'] == 'set' && isset($_POST['query'])) {
    $sql = trim($_POST['query']);

    if ($conf['phpversion'] >= 5) {
        include_once H . 'sys/inc/sql_parser.php';
        $sql=SQLParser::getQueries($sql);
    } else {
        $sql = preg_split('/;(\n|\r)*/', $sql);
    }

    $k_z = 0;
    $k_z_ok = 0;

    for ($i=0; $i < count($sql); $i++) {
        if ($sql[$i] != '') {
            $k_z++;
            if ($db->query($sql[$i])) {
                $k_z_ok++;
            }
        }
    }
    if ($k_z_ok > 0) {
        if ($k_z_ok == 1 && $k_z = 1) {
            msg('Запрос успешно выполнен');
        } else {
            msg('Выполнено ' . $k_z_ok . ' запросов из ' . $k_z);
        }

        admin_log('Админка', 'MySQL', 'Выполнено ' . $k_z_ok . ' запрос(ов)');
    }
}

err();

?>
<div class="p_m">
    <form method="post" action="?set=set">
        <textarea name="query" ></textarea>
        <input value="Выполнить" type="submit" />
    </form>
</div>
<div class="foot">
    &laquo;<a href="/adm_panel/">В админку</a>
    &laquo;<a href="tables.php">Залить файлом</a>
</div>
<?php

include_once '../sys/inc/tfoot.php';
