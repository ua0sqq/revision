<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';

user_access('system_jurnal', null, '/'.SID);
adm_check();

$set['title'] = lang('Системный  журнал');
include_once H.'sys/inc/thead.php';
title();
aut();

unset($_SESSION['spam_count']);
// TODO:  ??? блеать запутал меня Сайнт:-) убрать надо весь хлам

$count_jurnal = $db->query('select count(*) from jurnal_system where `read`=?', ['0'])->el();
$count_foto = $db->query("select count(*) from jurnal_system where `read`=? and `type`=?", ['0', 'spam_foto'])->el();
$count_forum = $db->query("select count(*) from jurnal_system where `read`=? and `type`=?", ['0', 'spam_forum'])->el();
$count_chat = $db->query("select count(*) from jurnal_system where `read`=? and `type`=?", ['0', 'spam_chat'])->el();
$count_system = $db->query("select count(*) from jurnal_system where `read`=? and `type`=?", ['0', 'system'])->el();
$count_vedro = $db->query("select count(*) from jurnal_system where `read`=?", ['1'])->el();
$count_profile = $db->query("select count(*) from jurnal_system where `read`=? and `type`=?", ['0', 'spam_profile'])->el();
$count_files = $db->query("select count(*) from jurnal_system where `read`=? and `type`=?", ['0', 'spam_files'])->el();
$count_spam_mail = $db->query("select count(*) from jurnal_system where `read`=? and `type`=?", ['0', 'spam_mail'])->el();


echo '
 <div class="p_m">
 <a href="?type_jurnal=spam_forum">'.lang('Жалоба на форуме').' ['.$count_forum.']</a> ::
 <a href="?type_jurnal=spam_chat">'.lang('Жалоба в чате').' ['.$count_chat.']</a> ::
 <a href="?type_jurnal=spam_files">'.lang('Жалоба на файл').' ['.$count_files.']</a> ::
  <a href="?type_jurnal=spam_mail">'.lang('Жалоба в почте').'. ['.$count_spam_mail.']</a> <br/>
 </div>
  <div class="p_m">
 <a href="?type_jurnal=spam_profile">'.lang('Жалоба на анкету').' ['.$count_profile.']</a> ::
 <a href="?type_jurnal=spam_foto">'.lang('Жалоба на фото').'. ['.$count_foto.']</a> <br/>
 </div>
 <div class="p_m">
 <a href="?type_jurnal=system">'.lang('Системные').' ['.$count_system.']</a> ::
 <a href="?old">'.lang('Корзина').' ['.$count_vedro.']</a> ::  <a href="?">'.lang('На модерации').'['.$count_jurnal.']</a>
 </div>';

if (isset($_GET['read'])) {
    $db->query('UPDATE `jurnal_system` SET `read`=?string where `id`=?i', [1, $_GET['read']]);
    $_SESSION['message'] = lang('Успешно проверено');
    exit(header('Location: ?'));
}

if (isset($_GET['read_sytem']) && $user['level'] > 2) {
    $db->query('UPDATE `jurnal_system` SET `read`=?string WHERE `type`=?', [1, 'system']);
    $_SESSION['message'] = lang('Системные сообщения отмечены как прочитанные');
    exit(header('Location: ?'));
}

if (isset($_GET['old'])) {
    echo '<div class="msg">'.lang('Корзина журнала - в ней хранятся старые оповещения или отмеченные модератором как проверенные').'
 <br />'.lang('Удаляются раз в 30 дней автоматически').'.</div> ';

    $k_post  = $db->query('select count(*) from jurnal_system where `read`="1"')->el();
    $k_page = k_page($k_post, $set['p_str']);
    $page    = page($k_page);
    $start     = $set['p_str']*$page-$set['p_str'];

    echo '<div class="p_m">
 <a href="?">'.lang('Выход из корзины').'</a>
 </div>';

    if ($k_post == 0) {
        msg(lang('Корзина пуста!'));
    } else {
        $jurnal_sql = $db->query('SELECT * FROM `jurnal_system` where `read`=?string ORDER BY id DESC LIMIT ?i, ?i', [1, $start, $set['p_str']])->assoc();
        foreach ($jurnal_sql as $row) {
            echo "<div class='p_m'>
	<span class='' style='float:right;'>".vremja($row['time'])." </span><br/>
	<div class='status_o_s'> </div>
	<div class='status_o'> ".output_text($row['msg'])." </div>
	</div>";
        }

        if ($k_page > 1) {
            str("?old&amp;", $k_page, $page); // Вывод страниц
        }
    }
}

if (!isset($_GET['old'])) {
    $types = '';
    if (isset($_GET['type_jurnal'])) {
        $types=$_GET['type_jurnal'];
    }

    $k_post  = $db->query('SELECT COUNT( * ) FROM jurnal_system WHERE `read`=?string AND `type`=?string', [0, $types])->el();
    $k_page = k_page($k_post, $set['p_str']);
    $page    = page($k_page);
    $start     = $set['p_str']*$page-$set['p_str'];

    if (isset($_GET['type_jurnal']) && $_GET['type_jurnal'] == 'system' && $user['level'] > 2 && $count_system > 0) {
        echo "<div class='p_m'>[<a href='?read_sytem'>".lang('Отметить все')."</a>]</div>";
    }

    if ($k_post == 0) {
        msg('Пусто');
    } else {
        $jurnal_sql=$db->query('SELECT * FROM `jurnal_system` WHERE  `read`=?string AND `type`=?string ORDER BY id DESC LIMIT ?i, ?i',
                               [0, $types, $start, $set['p_str']]);
        while ($row = $jurnal_sql->row()) {
            echo "<div class='p_m'><span class='' style='float:right;'>".vremja($row['time'])." </span><br/>
			<div class='status_o_s'> </div>
			<div class='status_o'> ".output_text($row['msg'])." </div>
			<br/>
			<a href='?read=$row[id]'><span class='ank_span_m' style='color:green;'>".lang('Проверенно')."</span></a>
			<br/></div>";
        }

        if (isset($_GET['type_jurnal'])) {
            $type_p = '?type_jurnal=' . $_GET['type_jurnal'] . '&amp;';
        } else {
            $type_p = '?';
        }

        if ($k_page > 1) {
            str($type_p, $k_page, $page); // Вывод страниц
        }
    }
}

include_once H.'sys/inc/tfoot.php';
