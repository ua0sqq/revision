<?php
include_once '../sys/inc/start.php';
include_once '../sys/inc/compress.php';
include_once '../sys/inc/sess.php';
include_once '../sys/inc/home.php';
include_once '../sys/inc/settings.php';
$temp_set=$set;
include_once '../sys/inc/db_connect.php';
include_once '../sys/inc/ipua.php';
include_once '../sys/inc/fnc.php';
include_once '../sys/inc/adm_check.php';
include_once '../sys/inc/user.php';
user_access('mailGuard', null, '/'.SID);
adm_check();

$set['title'] = 'Настройки mailGuard';
include_once '../sys/inc/thead.php';
title();
if (isset($_POST['save'])) {
    $temp_set['flood_mail_time']=intval($_POST['flood_mail_time']);
    $temp_set['flood_mail_count']=intval($_POST['flood_mail_count']);
    $temp_set['flood_mail_on']=intval($_POST['flood_mail_on']);
    $temp_set['flood_mail_ban']=intval($_POST['flood_mail_ban']);
    $temp_set['flood_mail_adm_on']=intval($_POST['flood_mail_adm_on']);
    $temp_set['flood_mail_time_s']=intval($_POST['flood_mail_time_s']);
    $temp_set['balls_mail']=intval($_POST['balls_mail']);





    if (save_settings($temp_set)) {
        admin_log('Настройки', 'Форум', 'Изменение настроек форума');
        msg('Настройки успешно приняты');
    } else {
        $err='Нет прав для изменения файла настроек';
    }
}
err();
aut();



echo "<form method=\"post\" action=\"?\">\n";





echo "<div class='p_m'>Активность модуля:<br /><select name='flood_mail_on'>";
echo "<option value='1'".($temp_set['flood_mail_on']==1?" selected='selected'":null)."> Включен</option>";
echo "<option value='0'".($temp_set['flood_mail_on']==0?" selected='selected'":null)."> Отключён</option>";
echo "</select></div>";


echo "<div class='p_m'>Сообщать администрации:<br /><select name='flood_mail_adm_on'>";
echo "<option value='1'".($temp_set['flood_mail_adm_on']==1?" selected='selected'":null)."> Сообщать</option>";
echo "<option value='0'".($temp_set['flood_mail_adm_on']==0?" selected='selected'":null)."> Не сообщать</option>";
echo "</select></div>";





echo "<div class='p_m'>Время анти флуда(писать не реже чем):<br /><select name='flood_mail_time'>";
echo "<option value='5'".($temp_set['flood_mail_time']==5?" selected='selected'":null)."> 5 сек</option>";
echo "<option value='10'".($temp_set['flood_mail_time']==10?" selected='selected'":null)."> 10 сек</option>";
echo "<option value='30'".($temp_set['flood_mail_time']==30?" selected='selected'":null)."> 30 сек</option>";
echo "<option value='40'".($temp_set['flood_mail_time']==40?" selected='selected'":null)."> 40 сек</option>";
echo "<option value='50'".($temp_set['flood_mail_time']==50?" selected='selected'":null)."> 50 сек</option>";
echo "<option value='60'".($temp_set['flood_mail_time']==60?" selected='selected'":null)."> 60 сек</option>";
echo "<option value='90'".($temp_set['flood_mail_time']==90?" selected='selected'":null)."> 90 сек</option>";
echo "</select></div>";

echo "<div class='p_m'>Число попыток до бана:<br /><select name='flood_mail_count'>";
echo "<option value='10'".($temp_set['flood_mail_count']==10?" selected='selected'":null)."> 10</option>";
echo "<option value='15'".($temp_set['flood_mail_count']==15?" selected='selected'":null)."> 15</option>";
echo "<option value='20'".($temp_set['flood_mail_count']==20?" selected='selected'":null)."> 20</option>";
echo "<option value='25'".($temp_set['flood_mail_count']==25?" selected='selected'":null)."> 25</option>";
echo "<option value='30'".($temp_set['flood_mail_count']==30?" selected='selected'":null)."> 30</option>";
echo "<option value='40'".($temp_set['flood_mail_count']==40?" selected='selected'":null)."> 40</option>";
echo "<option value='50'".($temp_set['flood_mail_count']==50?" selected='selected'":null)."> 50</option>";
echo "<option value='60'".($temp_set['flood_mail_count']==60?" selected='selected'":null)."> 60</option>";

echo "</select></div>";


echo "<div class='p_m'>Сколько начислять баллов за сообщение:<br /><select name='balls_mail'>";
echo "<option value='1'".($temp_set['balls_mail']==1?" selected='selected'":null)."> 1</option>";
echo "<option value='2'".($temp_set['balls_mail']==2?" selected='selected'":null)."> 2</option>";
echo "<option value='3'".($temp_set['balls_mail']==3?" selected='selected'":null)."> 3</option>";
echo "<option value='4'".($temp_set['balls_mail']==4?" selected='selected'":null)."> 4</option>";
echo "<option value='5'".($temp_set['balls_mail']==5?" selected='selected'":null)."> 5</option>";
echo "<option value='6'".($temp_set['balls_mail']==6?" selected='selected'":null)."> 6</option>";
echo "<option value='7'".($temp_set['balls_mail']==7?" selected='selected'":null)."> 7</option>";
echo "<option value='8'".($temp_set['balls_mail']==8?" selected='selected'":null)."> 8</option>";
echo "<option value='9'".($temp_set['balls_mail']==9?" selected='selected'":null)."> 9</option>";
echo "<option value='10'".($temp_set['balls_mail']==10?" selected='selected'":null)."> 10</option>";
echo "</select></div>";







echo "<div class='p_m'>На сколько минут банить (в минутах) :<br />
<input type='text' name='flood_mail_ban' value='$temp_set[flood_mail_ban]'/></div>";

echo "<div class='p_m'>Сколько минут надо провести что б не антиспам не коснулся пользователя  :<br />
<input type='text' name='flood_mail_time_s' value='$temp_set[flood_mail_time_s]'/></div>";





echo "<div class='p_m'><input value=\"Cохранить\" name='save' type=\"submit\" />\n";
echo "</form></div>";


echo "<div class='foot'>\n";

echo "&laquo;<a href='".APANEL."/'>В админку</a><br />\n";
echo "</div>\n";

include_once '../sys/inc/tfoot.php';
