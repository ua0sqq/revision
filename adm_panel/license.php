<?php
include_once '../sys/inc/start.php';
include_once '../sys/inc/compress.php';
include_once '../sys/inc/sess.php';
include_once '../sys/inc/home.php';
include_once '../sys/inc/settings.php';
include_once '../sys/inc/db_connect.php';
include_once '../sys/inc/ipua.php';
include_once '../sys/inc/fnc.php';
include_once '../sys/inc/adm_check.php';
include_once '../sys/inc/user.php';
user_access('license', null, 'index.php?'.SID);
adm_check();
$set['title'] = lang('Лицензия - информация');
include_once '../sys/inc/thead.php';
title();
aut();

include_once H.'sys/inc/api_config.php';


if (isset($_GET['activator'])) {
    if (isset($_FILES['file'])) {
        $file_is = $_FILES['file']['name'];

        if ($_FILES['file']['name'] == null) {
            $_SESSION['message'] = lang('Не выбран файл ,или он поврежден');
            exit(header('Location: ?activator'));
        }
    
        $file=esc(stripcslashes(htmlspecialchars($_FILES['file']['name'])));
        $file=preg_replace('(\#|\?)', null, $file);
        $ras = strtolower(preg_replace('#^.*\.#', null, $file));
    
        if ($ras != 'ini') {
            $_SESSION['message'] = lang('Ошибка не верный файл');
            exit(header('Location: ?activator'));
        }
    
    
        $text = file_get_contents($_FILES['file']['tmp_name']);
        $fp = fopen(H.'sys/ini/license_key.ini', "w");
        fwrite($fp, $text);
        fclose($fp);
    
        $_SESSION['message'] = lang('Файл лицензии загружен');
        exit(header('Location: ?'));
    }

    echo "
	    <form method='post' enctype='multipart/form-data' action=''>
				<div colspan='2' class='p_m'>
					<input type='file' name='file' accept='image/*,image/gif,image/png,image/jpeg' />
					<br /><input value='".lang('Обновить ключ лицензии')."' type='submit' />
				</div>
		</form>
		
		<div class='foot'>
			<a href='?'>".lang('Отмена')."</a>
		</div>
		
	   <div class='foot'>
			<a href='".FIERA_URL."/license/'>".lang('Где взять лицензию')."?</a>
		</div>";
        
    include_once H.'sys/inc/tfoot.php';
    exit;
}

if ($set['license_mod'] == 1) {
    if (!is_file(H.'sys/ini/license_key.ini')) {
        msg('Отсутствует файл с ключем');
        echo "<div class='foot'>";
        echo "&laquo; <a href='".APANEL."/'>".lang('В админку')."</a><br />";
        echo "</div>";
        include_once H.'sys/inc/tfoot.php';
        exit;
    }

    $license_key = parse_ini_file(H.'sys/ini/license_key.ini');

    if ($license_key['key'] == null) {
        msg('Лицензия еще не активирована');
        echo "<div class='foot'>";
        echo "&laquo; <a href='".APANEL."/'>".lang('В админку')."</a><br />";
        echo "&laquo; <a href='?activator'>".lang('Активация лицензии')."</a><br />";
        echo "</div>";
        include_once H.'sys/inc/tfoot.php';
        exit;
    }

    $arr = @json_decode(file_get_contents(API_SUPPORT_URL_LICENSE."?key={$license_key['key']}&site=".$_SERVER['HTTP_HOST']), true);

    if ($arr != null) {
        foreach ($arr as $api_license) {
            $error = api_fnc::output($api_license['eror']);

//если есть ошибки
if ($error != null) {
    echo api_fnc::err($error);
} else {

//обрабатываем данные
$key_status = output_text($api_license['key_opis']);
    $time_key = time_key($api_license['time_key'], 2, ' закончилась');
    $url = output_text($api_license['url']);
    $key = output_text($api_license['key']);
    $time_server = time_key($api_license['time_server']);
    $us_license = output_text($api_license['us_license']);

    echo "
	<div class='license'>".lang('Сайт').": {$url}</div>
	<div class='license'>".lang('Время доступности ключа').": {$time_key} </div>
	<div class='license'>".lang('Статус лицензии').": {$key_status} </div>
	<div class='license'>".lang('Владелиц').": {$us_license} </div>
	<div class='license'> <a href='".FIERA_URL."/license/?capabilities'>".lang('Возможности вашего ключа лицензии')."</a></div>
	";
}
        }
    }
} else {
    msg(lang('Сервис API FIERA отключен'));
}


echo "<div class='foot'>";
echo "&laquo; <a href='".APANEL."/'>".lang('В админку')."</a><br />";
echo "&laquo; <a href='?activator'>".lang('Активация лицензии')."</a><br />";
echo "&laquo; <a href='".APANEL."/copyright.php'>".lang('Настройки копирайта')."</a><br />";
echo "</div>";


include_once H.'sys/inc/tfoot.php';
