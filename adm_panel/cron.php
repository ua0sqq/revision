<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';

user_access('cron_system', null, '/');
adm_check();

$set['title'] = lang('Планировщик задач - Cron');
include_once H . 'sys/inc/thead.php';
title();
aut();


if (isset($_GET['edit'])) {
    if (isset($_POST['edit'])) {
        $id = intval($_GET['edit']);
        $time = trim($_POST['time']);
        $name = trim($_POST['name']);
        $opis = trim($_POST['opis']);
        $file = trim($_POST['file']);
        $act = trim($_POST['act']);

        $file = preg_replace('#[^a-z0-9 _\-\.]#i', null, $_POST['file']);

        if ($file == null) {
            $err[] = lang('Файл не выбран');
        }
        if (mb_strlen($name) > 999) {
            $err[] = lang('Название слишком длинное');
        }
        if (mb_strlen($name) < 3 or mb_strlen($name) == null) {
            $err[] = lang('Короткое Название');
        }
        if (mb_strlen($opis) > 999) {
            $err[] = lang('Описание слишком длинное');
        }
        if (mb_strlen($opis) < 3 or mb_strlen($opis) == null) {
            $err[] = lang('Короткое Описание');
        }

        if (!isset($err)) {
            $db->query('UPDATE `cron` SET  `name`=?, `opis`=?, `file`=?,`time_update`=?i WHERE  `id`=?i;',
                       [$name, $opis, $file, $time, $id]);
            $_SESSION['message'] = lang('Успешно');
            exit(header('Location: ?'));
        }
        err();
    }


    $post = $db->query('SELECT * FROM `cron` WHERE `id`=?i LIMIT ?i', [$_GET['edit'], 1]);

    $tmp = scandir(H . 'sys/cron/');
    foreach ($tmp as $file) {
        if ($file == '.' or $file == '..' or $file == '.htaccess') {
            continue;
        }
        $files[] = $file;
    }

    echo "<div class='p_m'><form method='post' action=''>";

    echo "<select name='file'>";
    echo "<option value=''>".lang('Выберете нужный файл')." </option>";
    for ($i=0;$i<sizeof($files);$i++) {
        echo "<option value='$files[$i]'".($post['file'] == $files[$i] ? " selected='selected'":null).">$files[$i]</option>";
    }
    echo "</select><br />";


    echo lang('Название').":<br />	<input type='text' name='name' value='$post[name]' /><br />";
    echo lang('Описание').":<br />	<textarea name='opis'>$post[opis]</textarea><br />";
    echo lang('Время выполнения')." (".lang('сек.')."):<br />	<input type='text' name='time' value='$post[time_update]' /><br />";
    echo "<br /><input value='".lang('Сохранить')."' name='edit' type='submit' />";
    echo "<a href='?' class='adm_panel_span'>".lang('Отмена')."</a>
	</form></div>";

    include_once H . 'sys/inc/tfoot.php';
    exit;
}


if (isset($_GET['add'])) {
    if (isset($_POST['add'])) {
        $id = intval($_GET['add']);
        $time_u = trim($_POST['time']);
        $name = trim($_POST['name']);
        $opis = trim($_POST['opis']);
        $file = trim($_POST['file']);
        $file = preg_replace('#[^a-z0-9 _\-\.]#i', null, $_POST['file']);
        if ($file == null) {
            $err[] = lang('Файл не выбран');
        }
        if (mb_strlen($name) > 999) {
            $err[] = lang('Название слишком длинное');
        }
        if (mb_strlen($name) < 3 or mb_strlen($name) == null) {
            $err[] = lang('Короткое Название');
        }
        if (mb_strlen($opis) > 999) {
            $err[] = lang('Описание слишком длинное');
        }
        if (mb_strlen($opis) < 3 or mb_strlen($opis) == null) {
            $err[] = lang('Короткое Описание');
        }
//$if = $db->query('SELECT COUNT(*) FROM `cron` WHERE `file`=?', [$file])->el();
        if ($db->query('SELECT COUNT(*) FROM `cron` WHERE `file`=?', [$file])->el() > 0) {
            $err[] = lang('Задача с таким файлом уже существует');
        }

        if (!isset($err)) {
            $id = $db->query('SELECT MAX(`id`) FROM `cron`')->el()+1;
            $db->query('INSERT INTO `cron` (`id`,`name` ,`opis` ,`file` ,`time_update` ,`time`) VALUES(?i, ?, ?, ?,  ?i, ?i);',
                       [$id, $name, $opis, $file, $time_u, 0]);
            $_SESSION['message'] = lang('Задача создана');
            exit(header('Location: ?'));
        }

        err();
    }


    $tmp = scandir(H . 'sys/cron/');
    foreach ($tmp as $file) {
        if ($file == '.' or $file == '..' or $file == '.htaccess') {
            continue;
        }
        $files[] = $file;
    }
    echo "<div class='p_m'><form method='post' action=''>";
    echo "<select name='file'>";
    echo "<option value=''>".lang('Выберете нужный файл')." </option>";
    for ($i=0;$i<sizeof($files);$i++) {
        echo "<option value='$files[$i]'>$files[$i]</option>";
    }
    echo "</select><br />";
    echo lang('Название').":<br />	<input type='text' name='name' value='' /><br />";
    echo lang('Описание').":<br />	<textarea name='opis'></textarea><br />";
    echo lang('Время выполнения')." (".lang('сек.')."):<br />	<input type='text' name='time' value='' /><br />";
    echo "<br /><input value='".lang('Сохранить')."' name='add' type='submit' />";
    echo "<a href='?' class='adm_panel_span'>".lang('Отмена')."</a>
	</form></div>";

    include_once H . 'sys/inc/tfoot.php';
    exit;
}

if (isset($_GET['act'])) {
    $db->query('UPDATE `cron` SET  `act`=?i WHERE  `id`=?i', [$_GET['act'], $_GET['id']]);
    $_SESSION['message'] = ($_GET['act'] == 0 ? lang('Процесс остановлен') : lang('Процесс запущен'));
    exit(header('Location: ?'));
}

if (isset($_GET['reset'])) {
    $db->query('UPDATE `cron` SET  `time`=0 WHERE  `id`=?i', [$_GET['id']]);
    $_SESSION['message'] = lang('Процесс перезапущен');
    exit(header('Location: ?'));
}

if (isset($_GET['del'])) {
    $id = $db->query('SELECT * FROM `cron` WHERE `id`=?i LIMIT ?i', [$_GET['del'], 1]);
    if ($id['system'] == 1) {
        $_SESSION['message'] = lang('Нельзя удалять системные задачи');
        exit(header('Location: ?'));
    }
    $db->query('DELETE FROM `cron` WHERE `id`=?i', [$id['id']]);
    $_SESSION['message'] = lang('Процесс удален');
    exit(header('Location: ?'));
}



$q_menu = $db->query('SELECT * FROM `cron` ORDER BY `time_update` ASC');
while ($cron = $q_menu->row()) {
    echo "<div class='p_m'>";
    echo $cron['name'] . '<br />';
    echo '<div class="status_o_s"> </div>
	<div class="status_o">'. $cron['opis'] .' </div>';
    echo '<img src="/style/icons/default.png"> '.lang('Последние выполнение').' :'.vremja($cron['time']).'<br/>';
    echo '<img src="/style/icons/default.png"> '.lang('След. выполнение').' :'.vremja($cron['time'] + $cron['time_update']).'<hr/>';
    echo '<img src="/style/icons/default.png"> '.lang('Задача выполнилось').'  ('.$cron['count'].' раз.) <br/><br/>';
    echo "<span class='adm_panel_span'>
<a href='?edit=".$cron['id']."'>".lang('Редактировать')."</a> ::
".($cron['act'] == 1  ? "
<a href='?act=0&id=".$cron['id']."'>".lang('Остановить')."</a>":"
<a href='?act=1&id=".$cron['id']."'>".lang('Запустить')."</a>")."
".($cron['system'] == 0 ? " :: <a href='?del=".$cron['id']."'>".lang('Удалить')."</a>":false)."
 :: <a href='?reset&id=".$cron['id']."'>".lang('Перезапустить')."</a>
</span></div>";
}
echo "<a href='?add'><div class='p_m'>".lang('Новая задача')."</div></a>";

echo "<div class='foot'>\n";
echo "&laquo;<a href='".APANEL."/'>".lang('В админку')."</a><br />\n";
echo "</div>\n";

include_once H . 'sys/inc/tfoot.php';
