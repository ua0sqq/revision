<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';

user_access('adm_accesses', null, 'index.php?'.SID);
adm_check();

if (isset($_GET['id_group']) && count::query('user_group', ' `id` = "' . intval($_GET['id_group']) . '"')) {
    $group = $db->query('SELECT * FROM `user_group` WHERE `id`=?i', [$_GET['id_group']])->row();

    $set['title']= output_text('Группа "'.$group['name'].'" - привилегии'); // заголовок страницы
    include_once H . 'sys/inc/thead.php';
    title();
    if (isset($_POST['accesses'])) {
        $db->query('DELETE FROM `user_group_access` WHERE `id_group` =?i', [$group['id']]);
        $q = $db->query('SELECT * FROM `all_accesses`');
        
        while ($post = $q->row()) {
            $type = $post['type'];
            if (isset($_POST[$type]) && $_POST[$type]==1) {
                $db->query('INSERT INTO `user_group_access` (`id_group`, `id_access`) VALUES ( ?i, ?string)',
                           [$group['id'], $post['type']]);
            }
        }
    
        msg('Привилегии успешно изменены');
    }
    
    aut();

    echo "<form method='post' action='?id_group=$group[id]&amp;$passgen'>\n";
    
    $q = $db->query('SELECT * FROM `all_accesses` ORDER BY `name` ASC');
    while ($post = $q->row()) {
        echo "<div class='adm_panel'>";
        echo "<label>";
        echo "<input type='checkbox'" .
        (count::query('user_group_access', ' `id_group`="' . $group['id'] . '" AND `id_access`="' . $post['type'] . '" LIMIT 1') == 1 ? " checked='checked'" : null) . " name='$post[type]' value='1' />";
        echo output_text(lang($post['name']));
        echo "</label></div>";
    }
    echo "<div class='adm_panel'>";
    echo "<input value='" . lang('Применить') . "' name='accesses' type='submit' /></div>";
    echo "</form>";
    echo "<div class='foot'>";
    echo "&laquo;<a href='?'>" . lang('Группы') . "</a><br />";
    echo "&laquo;<a href='" . APANEL . "/'>" . lang('Админка') . "</a><br />";
    echo "</div>\n";
    
    include_once H . 'sys/inc/tfoot.php';
}

$set['title']=lang('Группы пользователей'); // заголовок страницы
include_once H . 'sys/inc/thead.php';
title();

aut();

if (isset($_GET['add']) or isset($_GET['edit'])) {
    lang(msg('В разработке'));
}

$accesses = $db->query('SELECT * FROM `user_group` ORDER BY `id` ASC');
while ($res = $accesses->row()) {
    echo "<div class='adm_panel'>";
    echo "<a href='?id_group=$res[id]'>".lang($res['name'])." (L$res[level], ".
    count::query('user_group_access', ' `id_group`="' . $res['id'] . '"') . ")</a>";
    echo '<a href="?edit='.$res['id'].'"><span style="float:right" class="adm_panel_span"> <span style="color:#c00020">'.lang('Ред.').'</span> </span></a></div>';
}

if (!isset($_GET['add'])) {
    echo "<a href='?add'><div class='p_m'>".lang('Создать новую должность')."</div></a>";
}

echo "<div class='foot'>";
echo "&laquo;<a href='".APANEL."/'>".lang('Админка')."</a><br />";
echo "</div>\n";
include_once H . 'sys/inc/tfoot.php';
