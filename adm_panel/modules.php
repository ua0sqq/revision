<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';

user_access('modules_edit', null, 'index.php?'.SID);
adm_check();

$set['title'] = lang('Модули');
include_once H . 'sys/inc/thead.php';
title() . aut();

if (isset($_GET['detals'])) {
    $post = $db->query('SELECT t1.*, t2.nick
FROM `modules` t1
JOIN `user` t2 ON t1.author_install=t2.id
WHERE t1.`id`=?i', [$_GET['detals']])->row();
    if ($post) {
        echo "<table class='post'>";
        echo "   <tr>";
        echo "  <td class='p_t'>";
        echo $post['name'] .' :: '. $post['author'];
        if (is_file($_SERVER['DOCUMENT_ROOT'] . $post['de_install']))
        {
            echo '  <span class="button" style="float:right;padding:5px;
background:#C3C7C9;
border-radius:5px;
border:1px solid #b32828;"><a href="' . $post['de_install'] . '?mod=' . $post['id'] . '">Удалить</a></span>';
            echo '<span style="clear: both;"></span>';
        }
        echo "  </td>";
        echo "   </tr>";
        echo "   <tr>";
        echo "  <td class='p_m'>";
        echo output_text($post['opis']);
        echo "  </td>";
        echo "   </tr>";
        echo "</table>";
        echo '<div class="p_m">Установил <a href="/id' . $post['author_install'] . '">'.$post['nick'].'</a></div>';
    }
    echo '<a href="?"><div class="p_m">  Назад</div></a>';
    include_once H . 'sys/inc/tfoot.php';
    exit;
}

$k_post  = $db->query('SELECT COUNT(*) FROM `modules`')->el();

if ($k_post == 0) {
    echo "  <div class='err'>";
    echo lang("Список установленных модулей пуст");
    echo "  </div>";
} else {
    $k_page = k_page($k_post, $set['p_str']);
    $page    = page($k_page);
    $start    = $set['p_str']*$page-$set['p_str'];

    echo "<table class='post'>\n";

    $q = $db->query('SELECT * FROM `modules`  ORDER BY `id` DESC LIMIT ?i, ?i', [$start, $set['p_str']]);

    while ($post = $q->row()) {
        echo "   <tr>\n";
        echo "  <td class='icon14' rowspan='2'>";
        echo image::is('/style/modules/'. $post['img'], 100, 100, '/style/modules/null.png');
        echo '<a href="?detals='.$post['id'].'"><div class="ank_span" style="text-align:center;">  '.lang('Подробнее').'</div></a>';
        echo "  </td>";
        echo "  <td class='p_t'>";
        echo output_text($post['name']) . ' :: '. output_text($post['author']);
        echo "  </td>";
        echo "   </tr>";
        echo "   <tr>";
        echo "  <td class='p_m'>";

        //разбиваем на массив
        $arr_str = explode(" ", output_text($post['opis']));
        //берем первые 6 элементов
        $arr = array_slice($arr_str, 0, 15);
        //превращаем в строку
        $text = implode(" ", $arr);

        // Если необходимо добавить многоточие
        if (count($arr_str) > 15) {
            $text .= '...';
        }

        echo($text);
        echo "  </td>";
        echo "   </tr>";
    }
    echo "</table>";
    if ($k_page > 1) {
        str("?", $k_page, $page); // Вывод страниц
    }
}

echo "<div class='foot'>";
echo "&laquo;<a href='modules_install.php'>".lang('Установка модулей')."</a><br />";
echo "&laquo;<a href='modules_market.php'>".lang('Fiera Market')."</a><br />";
echo "&laquo;<a href='".APANEL."/'>".lang('В админку')."</a><br />";
echo "</div>";

include_once H . 'sys/inc/tfoot.php';
