<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
$temp_set=$set;
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';

user_access('adm_rekl', null, 'index.php?' . SID);
adm_check();

if (isset($_GET['sel']) && is_numeric($_GET['sel']) && $_GET['sel']>0 && $_GET['sel']<=4) {
    $sel = intval($_GET['sel']);
    $set['title']='Реклама';
    include_once '../sys/inc/thead.php';
    title();

    if (isset($_GET['add']) && isset($_POST['name']) && $_POST['name'] != null && isset($_POST['link']) && isset($_POST['img']) && isset($_POST['ch']) && isset($_POST['mn'])) {
        $ch = intval($_POST['ch']);
        $mn = intval($_POST['mn']);
        $time_last = time() + $ch*$mn*60*60*24;

        if (isset($_POST['dop_str']) && $_POST['dop_str'] == 1) {
            $dop_str = 1;
        } else {
            $dop_str = 0;
        }

        $link=stripcslashes(htmlspecialchars($_POST['link']));
        $name=stripcslashes(htmlspecialchars($_POST['name']));
        $img=stripcslashes(htmlspecialchars($_POST['img']));

        $db->query('INSERT INTO `rekl` (`time_last`, `name`, `img`,  `link`, `sel`, `dop_str`) VALUES ( ?i, ?, ?, ?, ?string, ?string)',
                   [$time_last, $name, $img, $link, $sel, $dop_str]);
        msg('Рекламная ссылка добавлена');
    } elseif (isset($_GET['set']) &&
        $db->query('SELECT COUNT(*) FROM `rekl` WHERE `sel`=?string AND `id`=?i', [$sel, $_GET['set']])->el() &&
        isset($_POST['name']) && isset($_POST['link']) && isset($_POST['img']) && isset($_POST['ch']) && isset($_POST['mn'])) {
        $rekl = $db->query('SELECT * FROM `rekl` WHERE `sel`=?string AND `id`=?i LIMIT ?i', [$sel, $_GET['set'], 1])->row();
        $ch=intval($_POST['ch']);
        $mn=intval($_POST['mn']);
        if ($rekl['time_last']>time()) {
            $time_last=$rekl['time_last']+$ch*$mn*60*60*24;
        } else {
            $time_last=time()+$ch*$mn*60*60*24;
        }

        $link=stripcslashes(htmlspecialchars($_POST['link']));
        $name=stripcslashes(htmlspecialchars($_POST['name']));
        $img=stripcslashes(htmlspecialchars($_POST['img']));

        if (isset($_POST['dop_str']) && $_POST['dop_str']==1) {
            $dop_str=1;
        } else {
            $dop_str=0;
        }
        $db->query('UPDATE `rekl` SET `time_last`=?i, `name`=?, `link`=?, `img`=?, `dop_str`=?string WHERE `id`=?i',
                   [$time_last, $name, $link, $img, $dop_str, $_GET['set']]);
        msg('Рекламная ссылка изменена');
    } elseif (isset($_GET['del']) &&
            $db->query('SELECT COUNT(*) FROM `rekl` WHERE `sel`=?string AND `id`=?i', [$sel, $_GET['del']])->el()) {
        $db->query('DELETE FROM `rekl` WHERE `id`=?i LIMIT ?i', [$_GET['del'], 1]);
        msg('Рекламная ссылка удалена');
    }

    err();
    aut();

    $k_post  = $db->query('SELECT COUNT(*) FROM `rekl` WHERE `sel`=?string', [$sel])->el();

    echo "<table class='post'>\n";
    if ($k_post==0) {
        echo "   <tr>\n";
        echo "  <td class='p_t'>\n";
        echo "Нет рекламы\n";
        echo "  </td>\n";
        echo "   </tr>\n";
    } else {
        $k_page = k_page($k_post, $set['p_str']);
        $page    = page($k_page);
        $start    = $set['p_str']*$page-$set['p_str'];
        $q = $db->query('SELECT * FROM `rekl` WHERE `sel`=?string ORDER BY `time_last` DESC LIMIT ?i, ?i',
                    [$sel,  $start, $set['p_str']]);
        while ($post = $q->row()) {
            echo "   <tr>\n";
            echo "  <td class='p_t'>\n";
            if ($post['img'] == null) {
                echo "$post[name]<br />\n";
            } else {
                echo "<a href='$post[img]'>[картинка]</a><br />\n";
            }
            if ($post['time_last'] > time()) {
                echo "(до " . vremja($post['time_last']) . ")\n";
            } else {
                echo "(срок показа истек)\n";
            }
            echo "  </td>\n";
            echo "   </tr>\n";
            echo "   <tr>\n";
            echo "  <td class='p_m'>\n";
            echo "Ссылка: $post[link]<br />\n";
            if ($post['img'] != null) {
                echo "Картинка: $post[img]<br />\n";
            }
            if ($post['dop_str'] == 1) {
                echo "Переходов: $post[count]<br />\n";
            }
            echo "<a href='rekl.php?sel=$sel&amp;del=$post[id]&amp;page=$page'>Удалить</a><br />\n";

            if (isset($_GET['set']) && $_GET['set']==$post['id']) {
                echo "<form method='post' action='rekl.php?sel=$sel&amp;set=$post[id]&amp;page=$page'>\n";
                echo "Ссылка:<br />\n<input type=\"text\" name=\"link\" value=\"$post[link]\" /><br />\n";
                echo "Название:<br />\n<input type=\"text\" name=\"name\" value=\"$post[name]\" /><br />\n";
                echo "Картинка:<br />\n<input type=\"text\" name=\"img\" value=\"$post[img]\" /><br />\n";

                if ($post['time_last'] > time()) {
                    echo "Продлить на:<br />\n";
                } else {
                    echo "Продлить до:<br />\n";
                }

                echo "<input type=\"text\" name=\"ch\" size='3' value=\"0\" />\n";
                echo "<select name=\"mn\">\n";
                echo "  <option value=\"1\" selected='selected'>Дней</option>\n";
                echo "  <option value=\"7\">Недель</option>\n";
                echo "  <option value=\"31\">Месяцев</option>\n";
                echo "</select><br />\n";
                if ($post['dop_str']==1) {
                    $dop=" checked='checked'";
                } else {
                    $dop=null;
                }
                echo "<label><input type=\"checkbox\"$dop name=\"dop_str\" value=\"1\" /> Доп. страница</label><br />\n";
                echo "<input value=\"Применить\" type=\"submit\" />\n";
                echo "</form>\n";
                echo "<a href='rekl.php?sel=$sel&amp;page=$page'>Отмена</a><br />\n";
            } else {
                echo "<a href='rekl.php?sel=$sel&amp;set=$post[id]&amp;page=$page'>Изменить</a><br />\n";
            }
            echo "  </td>\n";
            echo "   </tr>\n";
        }
    }
    echo "</table>\n";
    if ($k_page>1) {
        str("rekl.php?sel=$sel&amp;", $k_page, $page); // Вывод страниц
    }

    echo "<form class='foot' method='post' action='rekl.php?sel=$sel&amp;add'>\n";
    echo "Название:<br />\n<input type=\"text\" name=\"name\" value=\"\" /><br />\n";
    echo "Ссылка:<br />\n<input type=\"text\" name=\"link\" value=\"\" /><br />\n";
    echo "Картинка:<br />\n<input type=\"text\" name=\"img\" value=\"\" /><br />\n";
    echo "Срок действия:<br />\n";
    echo "<input type=\"text\" name=\"ch\" size='3' value=\"1\" />\n";
    echo "<select name=\"mn\">\n";
    echo "  <option value=\"1\">Дней</option>\n";
    echo "  <option value=\"7\" selected='selected'>Недель</option>\n";
    echo "  <option value=\"31\">Месяцев</option>\n";
    echo "</select><br />\n";
    echo "<label><input type=\"checkbox\" checked='checked' name=\"dop_str\" value=\"1\" /> Доп. страница</label><br />\n";
    echo "<input value=\"Добавить\" type=\"submit\" />\n";
    echo "</form>\n";

    echo "<div class='foot'>\n";
    echo "<a href='rekl.php'>Список рекламы</a><br />\n";
    if (user_access('adm_panel_show')) {
        echo "&laquo;<a href='/adm_panel/'>В админку</a><br />\n";
    }
    echo "</div>\n";
    include_once '../sys/inc/tfoot.php';
}


$set['title']='Реклама';
include_once '../sys/inc/thead.php';
title() . err() . aut();

if (isset($_POST['rekl_key_fiera_save'])) {
    $temp_set['rekl_key_fiera'] = esc(stripcslashes(htmlspecialchars($_POST['rekl_key_fiera'])), 1);
    $temp_set['rekl_key_fiera_on'] = intval($_POST['rekl_key_fiera_on']);

    if (save_settings($temp_set)) {
        admin_log('Настройки', 'Реклама dcms-fiera', 'Изменение настроек Рекламы');
        msg('Настройки успешно приняты,обновите страницу');
    } else {
        $err='Нет прав для изменения файла настроек';
    }
}
echo "<div class='p_m'>\n";
echo "<a href='rekl.php?sel=1'>Под заголовком</a><br />\n";
echo "<a href='rekl.php?sel=2'>Главное меню</a><br />\n";
echo "<a href='rekl.php?sel=3'>Низ сайта (главная)</a><br />\n";
echo "<a href='rekl.php?sel=4'>Низ сайта (остальные)</a><br />\n";
echo "<a href='rekl.php?rekl_fiera'> Динамическая реклама dcms-fiera.ru (бесплатная)</a><br />\n";

echo "</div>\n";

if (isset($_GET['rekl_fiera'])) {
    echo "<form method=\"post\" action=\"?rekl_key_fiera_save\">\n";
    echo "<div class='p_m'>
<input type='text' name='rekl_key_fiera' value='$temp_set[rekl_key_fiera]'/><br />
Ключ для динамической рекламы от http://dcms-fiera.ru/ads :<br />
Бесплатная реклама по ссылки (требует авторизации на сайте)</div>";
    echo "<div class='p_m'>Настройка рекламы  :<br />\n<select name=\"rekl_key_fiera_on\">\n";

    if ($temp_set['rekl_key_fiera_on']==1) {
        $sel=' selected="selected"';
    } else {
        $sel=null;
    }
    echo "<option value=\"1\"$sel>Включена </option>\n";
    if ($temp_set['rekl_key_fiera_on']==0) {
        $sel=' selected="selected"';
    } else {
        $sel=null;
    }
    echo "<option value=\"0\"$sel>Отключена</option>\n";
    echo "</select></div>";
    echo "<div class='p_m'><input value=\"Сохранить\" name='rekl_key_fiera_save' type=\"submit\" />";
    echo "</form></div>";
}

echo "<div class='foot'>\n";
echo "&laquo;<a href='/adm_panel/'>В админку</a><br />\n";
echo "</div>\n";

include_once '../sys/inc/tfoot.php';
