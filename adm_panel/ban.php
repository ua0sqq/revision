<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';

if (!user_access('user_ban_set') && !user_access('user_ban_set_h') && !user_access('user_ban_unset')) {
    header("Location: /index.php?".SID);
    exit;
}

adm_check();
if (isset($_GET['id'])) {
    $ank['id']=intval($_GET['id']);
} else {
    header("Location: /index.php?".SID);
    exit;
}
//exit('=====');
if (!$db->query('SELECT COUNT(*) FROM `user` WHERE `id` =?i', [$ank['id']])->el()) {
    header("Location: /index.php?".SID);
    exit;
}

$ank=get_user($ank['id']);
if ($user['level']<=$ank['level']) {
    header("Location: /index.php?".SID);
    exit;
}

$set['title']='Бан пользователя '.$ank['nick'];
include_once '../sys/inc/thead.php';
title();

if (isset($_GET['unset']) &&
    $db->query('SELECT COUNT(*) FROM `ban` WHERE `id_user`=?i AND `id`=?i', [$ank['id'], $_GET['unset']])->el() && user_access('user_ban_unset')) {
    $ban_info = $db->query('SELECT b.*, u.group_access FROM `ban` b
JOIN `user` u ON b.id_ban=u.id
WHERE b.`id_user`=?i AND b.`id`=?i', [$ank['id'], $_GET['unset']])->row();

    if (($user['group_access'] > $ban_info['group_access'] || $user['id'] == $ban_info['id_ban']) || $user['group_access'] == 15) {
        $db->query('UPDATE `ban` SET `time`=?i WHERE `id` =?i LIMIT ?i', [$time, $_GET['unset'], 1]);
        admin_log('Пользователи', 'Бан', "Снятие бана пользователя '[url=/amd_panel/ban.php?id=$ank[id]]$ank[nick][/url]'");
        msg('Время бана обнулено');
    } else {
        $err[]='Нет прав';
    }
}

if (isset($_POST['ban_pr']) && isset($_POST['time']) && isset($_POST['vremja']) && (user_access('user_ban_set') || user_access('user_ban_set_h'))) {
    $timeban=$time;
    if ($_POST['vremja'] == 'min') {
        $timeban += intval($_POST['time'])*60;
    }
    if ($_POST['vremja'] == 'chas') {
        $timeban += intval($_POST['time'])*60*60;
    }
    if ($_POST['vremja'] == 'sut') {
        $timeban += intval($_POST['time'])*60*60*24;
    }
    if ($_POST['vremja'] == 'mes') {
        $timeban += intval($_POST['time'])*60*60*24*30;
    }
    if ($timeban < $time) {
        $err[] = 'Ошибка времени бана';
    }

    if (!user_access('user_ban_set')) {
        $timeban = min($timeban, $time+3600*24);
    }
    $adm_pr = $_POST['adm_pr'];
    $prich    = $_POST['ban_pr'];
    $cat       = $_POST['cat'];

    if (mb_strlen($prich) > 1024) {
        $err[] = 'Сообщение слишком длинное';
    }
    if (mb_strlen($prich) < 10) {
        $err[] = 'Необходимо подробнее указать причину';
    }
    if (mb_strlen($adm_pr) > 999) {
        $err[] = 'Админ причина слишком большя';
    }
    if (mb_strlen($adm_pr) < 1) {
        $err[] = 'Нужно указать админ причину';
    }

    if (!isset($err)) {
        $db->query('INSERT INTO `ban` (`id_user`, `id_ban`, `prich`, `time`,`adm_pr`,`cat`) VALUES ( ?i, ?i, ?string, ?i, ?string, ?string)',
           [$ank['id'], $user['id'], $prich, $timeban, $adm_pr, $cat]);
        admin_log('Пользователи', 'Бан', "Бан пользователя '[url=/adm_panel/ban.php?id=$ank[id]]$ank[nick][/url]' до ".vremja($timeban)." по причине '$prich'");

//убавляем рейтинг за бан
rating::ban($ank['id'], 'ban');
        msg('Пользователь успешно забанен');
    }
}

err();
aut();

$k_post  = $db->query('SELECT COUNT(*) FROM `ban` WHERE `id_user`=?i', [$ank['id']])->el();
$k_page = k_page($k_post, $set['p_str']);
$page    = page($k_page);
$start    = $set['p_str']*$page-$set['p_str'];
echo "<table class='post'>\n";

if ($k_post==0) {
    echo "   <tr>\n";
    echo "  <td class='p_t'>\n";
    echo "Нет нарушений\n";
    echo "  </td>\n";
    echo "   </tr>\n";
}

$q = $db->query('SELECT b.*, u.nick FROM `ban` b
JOIN `user` u ON b.id_user=u.id
WHERE `id_user`=?i ORDER BY b.`time` DESC LIMIT ?i, ?i',
              [$ank['id'], $start, $set['p_str']]);
while ($post = $q->row()) {
    $ank2 = ['id' => $post['id_user'], 'nick' => $post['nick']];
    echo "   <tr>\n";
    echo "  <td class='icon48' rowspan='2'>\n";
    avatar($ank2['id']);
    echo "  </td>\n";
    echo "  <td class='p_t'>\n";
    echo "<a href='/info.php?id=$ank2[id]'>$ank2[nick]</a>: Наказание: закончится в: ".vremja($post['time'])."\n";
    echo "  </td>\n";
    echo "   </tr>\n";
    echo "   <tr>\n";
    echo "  <td class='p_m' colspan='2'>\n";
    echo 'Причина :' . $post['prich'] . "<br />
Админ причина :" . $post['adm_pr'] . "<br />
Категория :".$post['cat']."<br />";

    if ($post['time']>$time && user_access('user_ban_unset')) {
        echo "<a href='?id=$ank[id]&amp;unset=$post[id]'>Снять бан</a><br />\n";
    }
    echo "  </td>\n";
    echo "   </tr>\n";
}

echo "</table>\n";
if ($k_page>1) {
    str('?id='.$ank['id'].'&amp;', $k_page, $page); // Вывод страниц
}

if (user_access('user_ban_set') || user_access('user_ban_set_h')) {
    echo "
<form action=\"ban.php?id=$ank[id]&amp;$passgen\" method=\"post\">
<div class='p_m'>
Причина:<br /><textarea name=\"ban_pr\"></textarea></div>
<div class='p_m'>
Категория причины:<br /><select name='cat'>
<option value='Такие ники запрещены' selected='selected'>Такие ники запрещены</option>
<option value='Вечный блок за спам' selected='selected'>Спам</option>
<option value='Повторная регистрация' selected='selected'>Повторная регистрация</option>
<option value='Неоднократные нарушения' selected='selected'>Неоднократные нарушения</option>
<option value='Временная блокировка' selected='selected'>Временная блокировка</option>
<option value='Флуд' selected='selected'>Флуд</option>
<option value='Офтоп' selected='selected'>Офтоп</option>
<option value='Тролинг' selected='selected'>Тролинг</option>
<option value='Мошенничество' selected='selected'>Мошенничество </option>
</select></div>
<div class='p_m'>
Админ причина:<br /><textarea name=\"adm_pr\"></textarea>
<br/>* видит только администрация
</div>";

    echo "<div class='p_m'>Время бана ".(user_access('user_ban_set')?null:'(max 1 сутки)').":<br />\n";
    echo "<input type='text' name='time' title='Время бана' value='60' maxlength='11' size='3' />\n";
    echo "<select class='form' name=\"vremja\">\n";
    echo "<option value='min'>Минуты</option>\n";
    echo "<option ".(($k_post>1)?'selected="selected" ':null)."value='chas'>Часы</option>\n";
    echo "<option value='sut'>Сутки</option>\n";
    echo "<option value='mes'".(user_access('user_ban_set')?null:' disabled="disabled"').">Месяцы</option>\n";
    echo "</select><br />\n";
    echo "<input type='submit' value='Забанить' />\n";
    echo "</div></form>";
} else {
    echo "<div class='err'>Нет прав для того, чтобы забанить пользователя</div>\n";
}

echo "<div class='foot'>\n";
echo "&raquo;<a href=\"/mail.php?id=$ank[id]\">Написать сообщение</a><br />\n";
echo "&laquo;<a href=\"/info.php?id=$ank[id]\">В анкету</a><br />\n";
if (user_access('adm_panel_show')) {
    echo "&laquo;<a href='/adm_panel/'>В админку</a><br />\n";
}
echo "</div>\n";

include_once '../sys/inc/tfoot.php';
