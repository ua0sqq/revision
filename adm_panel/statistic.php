<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';

user_access('adm_statistic', null, 'index.php?'.SID);
adm_check();

$set['title']='Статистика сайта';
include_once H . 'sys/inc/thead.php';
title() .  err() . aut();

echo "<div class='p_m'>";

for ($i = 0; $i < 24; $i++) {
    $mktime = [mktime($i, 0, 0), mktime($i+1, 0, 0)];
    $hit = $db->query('SELECT COUNT(*) FROM `visit_today` WHERE `time` >= ?i AND `time` < ?i', $mktime)->el();
    $host = $db->query('SELECT COUNT(DISTINCT `ip`) FROM `visit_today` WHERE `time` >= ?i AND `time` < ?i', $mktime)->el();
    $user_reg = $db->query('SELECT COUNT(*) FROM `user` WHERE `date_reg` >= ?i AND `date_reg` < ?i', $mktime)->el();
    $forum_them = $db->query('SELECT COUNT(*) FROM `forum_themes` WHERE `time` >= ?i AND `time` < ?i', $mktime)->el();
    $forum_post = $db->query('SELECT COUNT(*) FROM `forum_posts` WHERE `time` >= ?i AND `time` < ?i', $mktime)->el();
    $stat[] = array('hit' => $hit, 'host' => $host, 'time' => mktime($i, 0, 0), 'for_th' => $forum_them, 'for_p' => $forum_post, 'user' => $user_reg);
}
echo "Текущие сутки:<br />\n";

echo "<table border='1'>";
echo "<tr>\n";
echo "<td><b>Час</b></td>\n";
echo "<td><b>Хиты</b></td>\n";
echo "<td><b>Хосты</b></td>\n";
echo "<td><b>Рег.</b></td>\n";
echo "<td><b>Форум-темы</b></td>\n";
echo "<td><b>Форум-посты</b></td>\n";
echo "</tr>\n";
for ($i = 0; $i < sizeof($stat); $i++) {
    if ($time < $stat[$i]['time']) {
        continue;
    }
    echo "<tr>\n";
    echo "<td>".date('H', $stat[$i]['time'] + $user['set_timesdvig']*60*60)."</td>\n";
    echo "<td>" . $stat[$i]['hit'] . "</td>\n";
    echo "<td>" . $stat[$i]['host'] . "</td>\n";
    echo "<td>" . $stat[$i]['user'] . "</td>\n";
    echo "<td>" . $stat[$i]['for_th'] . "</td>\n";
    echo "<td>" . $stat[$i]['for_p'] . "</td>\n";
    echo "</tr>\n";
}
echo "</table><br />\n";
unset($stat);




$k_day = $db->query('SELECT COUNT(*) FROM `visit_everyday`')->el();

if ($q = $db->query('SELECT * FROM `visit_everyday` ORDER BY `time` ASC LIMIT ?i, ?i', [max($k_day - 30, 0), 30])) {
    echo "Последний месяц:<br />\n";

    while ($result = $q->row()) {
        $day_st = mktime(0, 0, 0, date('n', $result['time']), date('j', $result['time']));
        $day_fn = mktime(0, 0, 0, date('n', $result['time']), date('j', $result['time'])+1);
        $user_reg = $db->query('SELECT COUNT(*) FROM `user` WHERE `date_reg` >= ?i AND `date_reg` < ?i', [$day_st, $day_fn])->el();
        $forum_them = $db->query('SELECT COUNT(*) FROM `forum_themes` WHERE `time` >= ?i AND `time` < ?i', [$day_st, $day_fn])->el();
        $forum_post = $db->query('SELECT COUNT(*) FROM `forum_posts` WHERE `time` >= ?i AND `time` < ?i', [$day_st, $day_fn])->el();

        $stat[] = array('host' => ($result['host_ip_ua'] < $result['host']*2 ? $result['host_ip_ua'] : $result['host']), 'hit' => $result['hit'], 'time' => $result['time'], 'for_th' => $forum_them, 'for_p' => $forum_post, 'user' => $user_reg);
    }
    echo "<table  border='1'>";
    echo "<tr>\n";
    echo "<td><b>Дата</b></td>\n";
    echo "<td><b>Хиты</b></td>\n";
    echo "<td><b>Хосты</b></td>\n";
    echo "<td><b>Рег.</b></td>\n";
    echo "<td><b>Форум-темы</b></td>\n";
    echo "<td><b>Форум-посты</b></td>\n";
    echo "</tr>\n";
    for ($i = 0; $i < sizeof(@$stat); $i++) {
        echo "<tr>\n";
        echo "<td>" . date('d.m.Y', $stat[$i]['time']) . "</td>\n";
        echo "<td>" . $stat[$i]['hit'] . "</td>\n";
        echo "<td>" . $stat[$i]['host'] . "</td>\n";
        echo "<td>" . $stat[$i]['user'] . "</td>\n";
        echo "<td>" . $stat[$i]['for_th'] . "</td>\n";
        echo "<td>" . $stat[$i]['for_p'] . "</td>\n";
        echo "</tr>\n";
    }
    echo "</table><br />\n";
}

echo "</div>";

echo "<div class='foot'>\n";
echo "<a href='/adm_panel/'>Админка</a><br />\n";
echo "</div>\n";

include_once H . 'sys/inc/tfoot.php';
