<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';

user_access('modules_edit', null, 'index.php?'.SID);
adm_check();

$set['title'] = lang('Управление админ разделами');
include_once H.'sys/inc/thead.php';
title();

lang::start('default');

if (isset($_POST['add'])) {
    $name= trim($_POST['name']);
    $file= trim($_POST['file']);

    $pos = $db->query('SELECT MAX(`pos`) FROM `info_menu`')->el()+1;

    $db->query('INSERT INTO `info_menu` (`name`, `file`,`pos`) VALUES ( ?, ?, ?i)', [$name, $file,  $pos]);

    $_SESSION['message'] = lang('Ссылка успешно добавлена');
    exit(header('Location: ?'));
}

if (isset($_POST['change']) && isset($_GET['id']) && isset($_POST['name']) && $_POST['name']!=null) {
    $id=intval($_GET['id']);
    $name = trim($_POST['name']);
    $file = trim($_POST['file']);
    $type = trim($_POST['type']);

    $db->query('UPDATE `info_menu` SET `name`=?, `file`=?, `type`=? WHERE `id`=?i LIMIT ?i',
               [$name, $file, $type, $id, 1]);
    $_SESSION['message'] = lang('Пункт меню успешно изменен');
    exit(header('Location: ?'));
}

if (isset($_GET['id']) && isset($_GET['act']) && $db->query('SELECT COUNT(*) FROM `info_menu` WHERE `id` =?i', [$_GET['id']])->el()) {
    $menu= $db->query('SELECT * FROM `info_menu` WHERE `id`=?i LIMIT ?i', [$_GET['id'], 1])->row();
    if ($_GET['act']=='up') {
        $db->query("UPDATE `info_menu` SET `pos` = '".($menu['pos'])."' WHERE `pos` = '".($menu['pos']-1)."' LIMIT 1");
        $db->query('UPDATE `info_menu` SET `pos` = "' . ($menu['pos']-1) . '" WHERE `id` =?i LIMIT ?i', [$_GET['id'], 1]);

        $_SESSION['message'] = lang('Пункт меню сдвинут на позицию вверх');
        exit(header('Location: ?'));
    }
    if ($_GET['act']=='down') {
        $db->query("UPDATE `info_menu` SET `pos` = '".($menu['pos'])."' WHERE `pos` = '".($menu['pos']+1)."' LIMIT 1");
        $db->query('UPDATE `info_menu` SET `pos` = "' . ($menu['pos']+1) . '" WHERE `id` =?i LIMIT ?i', [$_GET['id'], 1]);

        $_SESSION['message'] = lang('Пункт меню сдвинут на позицию вниз');
        exit(header('Location: ?'));
    }
    if ($_GET['act']=='del') {
        $db->query('DELETE FROM `info_menu` WHERE `id`=?i LIMIT ?i', [$_GET['id'], 1]);

        $_SESSION['message'] = lang('Пункт меню удален');
        exit(header('Location: ?'));
    }
}

err();
aut();

echo "<table class='post'>";

$q= $db->query('SELECT * FROM `info_menu` ORDER BY `pos` ASC');
while ($post = $q->row()) {
    echo "   <tr>\n";
    echo "  <td class='adm_panel'>";
    //Выводим иконку
    if ($post['type'] == 'razd') {
        echo "<img src='/style/icons_admpanel/razd.png'> ";
    }

    echo  lang($post['name']) ." <span style='float:right' class='adm_panel_span'>  <a href='?id=$post[id]&amp;act=edit'>".lang('Редактировать')." </a></span>";
    echo "  </td>\n";
    echo "   </tr>\n";
    echo "   <tr>\n";
    echo "  <td class='p_m'>\n";

    echo($post['type'] == 'razd' ?  lang('Разделитель') : lang('Виджет')).'<br/><br/>';

    if (isset($_GET['id']) && $_GET['id']==$post['id'] && isset($_GET['act']) && $_GET['act']=='edit') {
        echo "<form action=\"?id=$post[id]\" method=\"post\">";
        echo lang('Название')." :<br />";
        echo "<input type='text' name='name' value='".lang($post['name'])."' /><br />";
        echo lang('Ссылка на виджет')." :<br />";
        echo "<input type='text' name='file' value='".lang($post['file'])."' /><br />";
        echo "<input class=\"submit\" name=\"change\" type=\"submit\" value='".lang('Изменить')."' /><br />\n";
        echo "</form>";
        echo "<a href='?'>".lang('Отмена')."</a><br />";
    } else {
        echo "<span class='adm_panel_span'><a href='?id=$post[id]&amp;act=up&amp;$passgen'>".lang('Выше')."</a> | ";
        echo "<a href='?id=$post[id]&amp;act=down&amp;$passgen'>".lang('Ниже')."</a> | ";
        echo "<a href='?id=$post[id]&amp;act=del&amp;$passgen'> ".lang('Удалить')."</a></span><br /><br />";
    }

    echo "  </td>";
    echo "   </tr>";
}
echo "</table>";


if (isset($_GET['add'])) {
    echo "  <div class='p_m'>";
    echo "<form action='?add=$passgen' method=\"post\">";
    echo "Тип:<br />";
    echo "<select name='type'>";
    echo "<option value='inc'> ".lang('Виджет')."</option>";
    echo "<option value='razd'> ".lang('Раздел')."</option>";
    echo "</select><br />";

    echo lang('Название').":<br />";
    echo "<input type=\"text\" name=\"name\" value=\"\"/><br />";

    echo lang('Ссылка на виджет').":<br />";
    echo "<input type=\"text\" name=\"file\" value=\"\"/><br />";


    echo "<input class='submit' name='add' type='submit' value='".lang('Добавить')."' /><br />";
    echo "<a href='?$passgen'>".lang('Отмена')."</a><br />\n";
    echo "</form></div>";
} else {
    echo "<div class='foot'><a href='?add=$passgen'>".lang('Добавить пункт')."</a></div>";
}

echo "<div class='foot'>";
echo "&laquo;<a href='".APANEL."/'>".lang('В админку')."</a><br />";
echo "</div>";

include_once '../sys/inc/tfoot.php';
