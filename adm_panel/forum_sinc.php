<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';

user_access('adm_forum_sinc', null, 'index.php?' . SID);
adm_check();

$set['title'] = 'Синхронизация таблиц форума';
include_once H . 'sys/inc/thead.php';
title();
err();
aut();
if (isset($_GET['ok']) && isset($_POST['accept'])) {
    $d_r = 0;
    $d_t = 0;
    $d_p = 0;
    $d_j = 0;
    // удаление разделов
        $d_r = $db->query('DELETE FROM `forum_razdels` WHERE `id_forum` NOT IN(SELECT `id` FROM `forum`)')->ar();
    // удаление тем
        $d_t = $db->query("DELETE FROM `forum_themes` WHERE `id_razdel` NOT IN(SELECT `id` FROM `forum_razdels`)")->ar();
    // удаление постов
        $d_p = $db->query("DELETE FROM `forum_posts` WHERE `id_theme` NOT IN(SELECT `id` FROM `forum_themes`)")->ar();
    // удаление несуществующих тем из журнала
        $d_j = $db->query("
    DELETE FROM `forum_journal` WHERE `id_theme` NOT IN(
        SELECT `id` FROM `forum_themes`)
    OR `id_user` NOT IN(
        SELECT `id` FROM `user`)
        ")->ar();
    // Замена постов несуществующих юзеров, на Систему,  хрен знает надо ли?
        $notUsers = $db->query('UPDATE `forum_posts` SET `id_user`=0 WHERE `id_user`  NOT IN(SELECT `id` FROM `user`)')->ar();
    $db->query('OPTIMIZE TABLE `forum_journal` , `forum_posts` , `forum_razdels` , `forum_themes` ');
    msg('Удалено разделов: ' . $d_r . ', тем: ' . $d_t . ', постов: ' . $d_p . ' ??? ' . $notUsers);
}
?>

<div class="p_m">
<form method="post" action="?ok">
    <fieldset>
        <legend>Очистка таблиц форума</legend>
        <input value="Начать" name="accept" type="submit" />
    </fieldset>
</form>
    <strong>* В зависимости от количества сообщений и тем, данное действие может занять длительное время.</strong><br />
    <strong>** Рекомендуется использовать только в случах расхождений счетчиков форума с реальными данными</strong><br />
</div>

<?php

if (user_access('adm_panel_show')) {
    ?>

<div class="foot">
    <p>&laquo;<a href="/adm_panel/">В админку</a></p>
</div>

<?php

}

include_once H . 'sys/inc/tfoot.php';
