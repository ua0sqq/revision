<?php

define('H', $_SERVER['DOCUMENT_ROOT'].'/');

foreach (array('start', 'compress', 'sess', 'settings', 'db_connect', 'ipua', 'fnc', 'user','adm_check') as $inc) {
    include_once H."sys/inc/{$inc}.php";
}

$temp_set = $set;

user_access('adm_set_forum', null, 'index.php');
adm_check();

$set['title'] = 'Настройки форума';
include_once '../sys/inc/thead.php';
title();

if (isset($_POST['save'])) {
    $temp_set['forum_new_them_min'] = intval($_POST['forum_new_them_min']);
    $temp_set['forum_new_them_max'] = intval($_POST['forum_new_them_max']);
    $temp_set['forum_new_them_name_min_pod'] = intval($_POST['forum_new_them_name_min_pod']);
    $temp_set['forum_new_them_name_max_pod'] = intval($_POST['forum_new_them_name_max_pod']);
    $temp_set['forum_new_them_name_max_pod_opis'] = intval($_POST['forum_new_them_name_max_pod_opis']);
    $temp_set['forum_edit_post_time'] = intval($_POST['forum_edit_post_time']);
    $temp_set['forum_balls_add_post'] = intval($_POST['forum_balls_add_post']);
    $temp_set['new_t_time'] = intval($_POST['new_t_time']);
    $temp_set['forum_new_them_max_i'] = intval($_POST['forum_new_them_max_i']);
    $temp_set['forum_new_them_min_i'] = intval($_POST['forum_new_them_min_i']);
    $temp_set['new_them_Location'] = intval($_POST['new_them_Location']);
    
    $temp_set['forum_r_post'] = intval($_POST['forum_r_post']);
    $temp_set['forum_r_post_j'] = intval($_POST['forum_r_post_j']);
    $temp_set['forum_color_best'] = output_text($_POST['forum_color_best']);

    
    
    
    if (save_settings($temp_set)) {
        admin_log('Настройки', 'Форум', 'Изменение настроек форума');
        msg('Настройки успешно приняты.');
    } else {
        $err = 'Нет прав для изменения файла настроек.';
    }
}

err().aut();

?>

<form method='post' action='?'>

    <div class='p_m'>
        Цвет надписи "Лучший ответ". По умолчанию: жёлтый:<br />
        <input type='text' name='forum_color_best' value='<?= $temp_set['forum_color_best'] ?>'/>
    </div>
	
    <div class='p_m'> Рейтинг поста в теме, при котором появится над постом "Лучший ответ":<br />
        <select name='forum_r_post'>
            <option value='5'<?= ($temp_set['forum_r_post'] == 5 ? " selected='selected'" : null) ?>> 5</option>
            <option value='7'<?= ($temp_set['forum_r_post'] == 7 ? " selected='selected'" : null) ?>> 7</option>
            <option value='10'<?= ($temp_set['forum_r_post'] == 10 ? " selected='selected'" : null) ?>> 10</option>
            <option value='15'<?= ($temp_set['forum_r_post'] == 15 ? " selected='selected'" : null) ?>> 15</option>
            <option value='20'<?= ($temp_set['forum_r_post'] == 20 ? " selected='selected'" : null) ?>> 20</option>
            <option value='25'<?= ($temp_set['forum_r_post'] == 25 ? " selected='selected'" : null) ?>> 25</option>
        </select>
    </div>
	
	<div class='p_m'> Отрицательный рейтинг поста в теме, при котором будет отправляться жалоба админам.:<br />
        <select name='forum_r_post_j'>
            <option value='5'<?= ($temp_set['forum_r_post_j'] == 5 ? " selected='selected'" : null) ?>> -5</option>
            <option value='7'<?= ($temp_set['forum_r_post_j'] == 7 ? " selected='selected'" : null) ?>> -7</option>
            <option value='10'<?= ($temp_set['forum_r_post_j'] == 10 ? " selected='selected'" : null) ?>> -10</option>
            <option value='15'<?= ($temp_set['forum_r_post_j'] == 15 ? " selected='selected'" : null) ?>> -15</option>
            <option value='20'<?= ($temp_set['forum_r_post_j'] == 20 ? " selected='selected'" : null) ?>> -20</option>
            <option value='25'<?= ($temp_set['forum_r_post_j'] == 25 ? " selected='selected'" : null) ?>> -25</option>
        </select>
    </div>
	
	
	
	
    <div class='p_m'>Баллов за 1 пост на форуме:<br />
        <select name='forum_balls_add_post'>
            <option value='0'<?= ($temp_set['forum_balls_add_post'] == 0 ? " selected='selected'" : null) ?>> не давать баллы</option>
            <option value='1'<?= ($temp_set['forum_balls_add_post'] == 1 ? " selected='selected'" : null) ?>> 1 балл</option>
            <option value='2'<?= ($temp_set['forum_balls_add_post'] == 2 ? " selected='selected'" : null) ?>> 2 балла</option>
            <option value='3'<?= ($temp_set['forum_balls_add_post'] == 3 ? " selected='selected'" : null) ?>> 3 балла</option>
            <option value='5'<?= ($temp_set['forum_balls_add_post'] == 5 ? " selected='selected'" : null) ?>> 5 баллов</option>
            <option value='10'<?= ($temp_set['forum_balls_add_post'] == 10 ? " selected='selected'" : null) ?>> 10 баллов</option>
            <option value='15'<?= ($temp_set['forum_balls_add_post'] == 15 ? " selected='selected'" : null) ?>> 15 баллов</option>
        </select>
    </div>
    
	
	
	
    <div class='p_m'>После создания новой темы:<br />
        <select name='new_them_Location'>
            <option value='0'<?= ($temp_set['new_them_Location'] == 0 ? " selected='selected'" : null) ?>> Не перенаправлять</option>
            <option value='1'<?= ($temp_set['new_them_Location'] == 1 ? " selected='selected'" : null) ?>> Перенаправлять в созданную тему</option>
        </select>
    </div>
    
    <div class='p_m'>Темы создавать можно не реже чем:<br />
        <select name='new_t_time'>
            <option value='0'<?= ($temp_set['new_t_time'] == 0 ? " selected='selected'" : null) ?>> Ограничение отсутствует</option>
            <option value='60'<?= ($temp_set['new_t_time'] == 120 ? " selected='selected'" : null) ?>> 1 раз в минуту</option>
            <option value='120'<?= ($temp_set['new_t_time'] == 120 ? " selected='selected'" : null) ?>> 2 раза в минуту</option>
            <option value='180'<?= ($temp_set['new_t_time'] == 180 ? " selected='selected'" : null) ?>> 3 раза в минуту</option>
            <option value='300'<?= ($temp_set['new_t_time'] == 300 ? " selected='selected'" : null) ?>> 5 раз в минуту</option>
            <option value='600'<?= ($temp_set['new_t_time'] == 600 ? " selected='selected'" : null) ?>> 10 раз в минуту</option>
            <option value='1200'<?= ($temp_set['new_t_time'] == 1200 ? " selected='selected'" : null) ?>> 20 раз в минуту</option>
        </select>
    </div>
    
    <div class='p_m'>
        Максимальное колл.символов в названии:<br />
        <input type='text' name='forum_new_them_max_i' value='<?= $temp_set['forum_new_them_max_i'] ?>'/>
    </div>
    
    <div class='p_m'>
        Минимальное колл.символов в названии темы:<br />
        <input type='text' name='forum_new_them_min_i' value='<?= $temp_set['forum_new_them_min_i'] ?>'/>
    </div>
    
    <div class='p_m'>
        Время на редактирование своего поста в форуме (если он поседний в теме):<br />
        <input type='text' name='forum_edit_post_time' value='<?= $temp_set['forum_edit_post_time'] ?>'/>
    </div>
    
    <div class='p_m'>
        Максимальное число символов сообщения в форуме:<br />
        <input type='text' name='forum_new_them_max' value='<?= $temp_set['forum_new_them_max'] ?>'/>
    </div>
    
    <div class='p_m'>
        Минимальное число символов сообщения в форуме:<br />
        <input type='text' name='forum_new_them_min' value='<?= $temp_set['forum_new_them_min'] ?>'/>
    </div>
    
    <div class='p_m'>
        Максимальное число символов в названии раздела:<br />
        <input type='text' name='forum_new_them_name_max_pod' value='<?= $temp_set['forum_new_them_name_max_pod'] ?>'/>
    </div>
    
    <div class='p_m'>
        Минимальное число символов в названии раздела:<br />
        <input type='text' name='forum_new_them_name_min_pod' value='<?= $temp_set['forum_new_them_name_min_pod'] ?>'/>
    </div>
    
    <div class='p_m'>
        Максимальное число символов в описании подфорумов/разделов:<br />
        <input type='text' name='forum_new_them_name_max_pod_opis' value='<?= $temp_set['forum_new_them_name_max_pod_opis'] ?>'/>
    </div>
    
    <div class='p_m'><input value='Cохранить изменения' name='save' type='submit' /></div>
    
</form>

<div class='foot'>
    <?php
    if (user_access('adm_forum_sinc')) {
        ?>
        &raquo; <a href='/adm_panel/forum_sinc.php'>Синхронизация таблиц форума</a><br />
        <?php

    }
    ?>
    &laquo; <a href='/adm_panel/'>В админку</a><br />
</div>
<?php

include_once '../sys/inc/tfoot.php';

?>