<?php
if (!defined('H')) {
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
}

include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';
user_access('adm_news', null, 'index.php?'.SID);
adm_check();

//подключаем языковой пакет
lang::start('news');


$set['title']= lang('Новости');
include_once H . 'sys/inc/thead.php';
title();

if (isset($_GET['del']) && is_numeric($_GET['del']) &&
    $db->query('select count(*) from `news` where `id`=?i', [$_GET['del']])->el() == 1) {
    $db->query('DELETE FROM `news` WHERE `id`=?i LIMIT ?i', [$_GET['del'], 1]);
    if (is_file(H . 'sys/cache/other/news_index.html')) {
        unlink(H . 'sys/cache/other/news_index.html');
    }
    msg(lang('Новости'));
}

if (isset($_POST['title']) && isset($_POST['msg'])) {
    $title=esc(stripcslashes(htmlspecialchars($_POST['title'], 1)));
    $link='';
    $msg = trim($_POST['msg']);

    if (mb_strlen($title)>32) {
        $err = lang('Слишком большой заголовок новости');
    }
    if (mb_strlen($title)<3) {
        $err = lang('Короткий заголовок');
    }
    if (mb_strlen($msg)>9999) {
        $err= lang('Содержиние новости слишком большое');
    }
    if (mb_strlen($msg)<2) {
        $err = lang('Новость слишком короткая');
    }

    if (!isset($err)) {
        $db->query('INSERT INTO `news` (`time`, `msg`, `title`, `link` ,`avtor`) VALUES( ?i, ?, ?,  ?, ?i)',
                   [$time, $msg, $title,  $link, $user['id']]);

        if (isset($_POST['mail'])) { // Расслылка новостей на майл
            $q = $db->query('SELECT `ank_mail` FROM `user` WHERE `id`<>?i AND `set_news_to_mail` =?string AND `ank_mail`<>?string', [$user['id'], 1, '']);
            while ($ank = $q->row()) {
                $db->query('INSERT INTO `mail_to_send` (`mail`, `them`, `msg`) VALUES( ?, ?, ?)',
                        [$ank['ank_mail'], lang('Новости'), output_text($msg)]);
            }
        }


        #	удаляем кэш
        if (is_file(H . 'sys/cache/other/news_index.html')) {
            unlink(H . 'sys/cache/other/news_index.html');
        }

        #	обновляем таблицу юзер
        $db->query('update `user` set `news_count` =?i', [1]);

        #	выкидываем на сообщение
        exit(header('Location: ?msg'));
    }
}

if (isset($_GET['msg'])) {
    msg(lang('Новость успешно добавлена'));
}

err() . aut();
echo "<a href='?cache_update'><div class='foot'>" . lang('Обновить кэш новостей') . "</div></a>";

if (isset($_GET['cache_update'])) {
    if (is_file(H . 'sys/cache/other/news_index.html')) {
        unlink(H . 'sys/cache/other/news_index.html');
    }
    $_SESSION['message'] = lang('Успешно');
    exit(header('Location: ?'));
}

echo "<div class='p_m'>";
echo "<form method=\"post\" action=\"news.php\">";
echo lang('Заголовок новости').":<br />\n<input name=\"title\" size=\"16\" maxlength=\"32\" value=\"\" type=\"text\" /><br />\n";
echo lang('Текст новости').":<br />\n<textarea name=\"msg\" ></textarea><br />\n";

if ($us_count = $db->query('select count(*) from `user` where `id`<>?i AND `set_news_to_mail`=?string AND `ank_mail`<>?string', [$user['id'], 1, ''])->el()) {
    echo "<label><input name=\"mail\" value=\"1\" type=\"checkbox\" checked=\"checked\" /> ".lang('Рассылка')." (". $us_count .")</label><br />\n";
}

echo "<input value=\"".lang('Добавить')."\" type=\"submit\" />\n";
echo "</form>

* ".lang('после создания новости ,она появится не сразу на главной ,а только после обновления кэша -это примерно около 5 минут')."

</div>";

$k_post  = $db->query('select count(*) from `news`')->el();
$k_page = k_page($k_post, $set['p_str']);
$page    = page($k_page);
$start    = $set['p_str']*$page-$set['p_str'];
$q = $db->query('SELECT * FROM `news` ORDER BY `id` DESC LIMIT ?i, ?i', [$start, $set['p_str']]);
echo "<table class='post'>\n";
if ($k_post==0) {
    echo "   <tr>\n";
    echo "  <td class='p_t'>\n";
    echo lang('Нет новостей');
    echo "  </td>\n";
    echo "   </tr>\n";
} else {
    while ($post = $q->row()) {
        echo "   <tr>\n";
        echo "  <td class='p_t'>\n";
        echo "$post[title]\n";
        echo "(".vremja($post['time']).")\n";
        echo "  </td>\n";
        echo "   </tr>\n";
        echo "   <tr>\n";
        echo "  <td class='p_m'>\n";
        echo output_text($post['msg'])."<br />\n";
        echo "<a href='?page=$page&amp;del=$post[id]' class='trash' ></a>";
        echo "  </td>\n";
        echo "   </tr>\n";
    }
}
echo "</table>\n";

if ($k_page>1) {
    str('news.php?', $k_page, $page); // Вывод страниц
}

echo "<div class='foot'>\n";
echo "&laquo; <a href='".APANEL."'>".lang('В админку')."</a><br />\n";
echo "</div>\n";

include_once H . 'sys/inc/tfoot.php';
